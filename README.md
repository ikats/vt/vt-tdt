# IKATS Viztool vt-tdt

## Definition

This viztool aimed to analyse and explore temporal decision tree.
TDecisionTree is created by [Laboratoire informatique de Grenoble](http://www.liglab.fr/) and made to be wrapped by Ikats framework.
It accepts only 'tdt' input format.

## How to use

This viztool is done to be used with [Temporal Decision Tree](https://gricad-gitlab.univ-grenoble-alpes.fr/ikats/op-tdt) operator output (tdt format).

![Image of First step](./assets/images/first_step.jpg)

## Layout

The viztool is composed of three parts :
- The **menu** on the left side lets you customize the view of a tree with the parameters of width, height, depth and scale. It gives you also informations that help to analyse data.
- The component on the right side shows **tree** where the user can interact with it.
- The **help** part on the top side that explains main features.

![Image of Layout](./assets/images/layout.jpg)

## Features

### SideMenu

#### Top part

- autofit : able user to automatically fit tree layout with the space availabe in the container.
- fit: fit tree layout with the space availabe in the container, hidden if autofit is activated.
- uncollapse all: uncollapse all nodes.

![Image of Permanent settings](./assets/images/top_part.jpg)

#### Settings

- depth : change depth to make show only node with the same or less depth as wanted
- scale: change tree size (but keeping same ratio) in using this scale value, able to zoom in or zoom out in changing the scale value. This field is hidden if autofit is activated.
- link mode: choose between 3 values : normal (same size everywhere), parent (link size is related to its observations in looking on the parent observations), root (link size is related to its observations in looking on the root observations)

![Image of Settings](./assets/images/settings.jpg)

#### General

It lists general information as :

- number of nodes
- number of leaves
- the version of the format
- the date of creation

![Image of General informations](./assets/images/general.jpg)

#### Node

If a node is selected it lists its properties, otherwise it will be empty.

![Image of Node description](./assets/images/node_description.jpg)

#### Validation

It lists informations related to the validation :

- method
- accuracy
- confusion matrix: each cell of the confusion matrix is selectable, it will affect traces list.
- arguments

#### Traces

It lists traces that can be individually selected.
If a trace is selected it modifies the viztool in order to highlight path representing the trace.

### VizTool

##### Node

- maj + click: collapse, uncollapse [video](./assets/videos/collapse_uncollapse.mp4)
- click: select node [video](./assets/videos/click.mp4)
- mod + click: zoom on the select node

##### Curve

- zoom in : using dble click, mouse wheel, modify minimap
- zoom out : using mouse wheel, modify minimap
- pan : using click + move mouse

[video](./assets/videos/zoom_and_pan.mp4)

## External depencies
- jquery: 3.2.1
- d3: 4.4.0
- lodash: 4.17.4
- bootstrap: 3.3.7