(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
!function(e,n){"function"==typeof define&&define.amd?define(["exports"],n):n("undefined"!=typeof exports?exports:e.dragscroll={})}(this,function(e){var n,t,o=window,l=document,c="mousemove",r="mouseup",i="mousedown",m="EventListener",d="add"+m,s="remove"+m,f=[],u=function(e,m){for(e=0;e<f.length;)m=f[e++],m=m.container||m,m[s](i,m.md,0),o[s](r,m.mu,0),o[s](c,m.mm,0);for(f=[].slice.call(l.getElementsByClassName("dragscroll")),e=0;e<f.length;)!function(e,m,s,f,u,a){(a=e.container||e)[d](i,a.md=function(n){e.hasAttribute("nochilddrag")&&l.elementFromPoint(n.pageX,n.pageY)!=a||(f=1,m=n.clientX,s=n.clientY,n.preventDefault())},0),o[d](r,a.mu=function(){f=0},0),o[d](c,a.mm=function(o){f&&((u=e.scroller||e).scrollLeft-=n=-m+(m=o.clientX),u.scrollTop-=t=-s+(s=o.clientY),e==l.body&&((u=l.documentElement).scrollLeft-=n,u.scrollTop-=t))},0)}(f[e++])};"complete"==l.readyState?u():o[d]("load",u,0),e.reset=u});
},{}],2:[function(require,module,exports){
//     keymaster.js
//     (c) 2011-2013 Thomas Fuchs
//     keymaster.js may be freely distributed under the MIT license.

;(function(global){
  var k,
    _handlers = {},
    _mods = { 16: false, 18: false, 17: false, 91: false },
    _scope = 'all',
    // modifier keys
    _MODIFIERS = {
      '⇧': 16, shift: 16,
      '⌥': 18, alt: 18, option: 18,
      '⌃': 17, ctrl: 17, control: 17,
      '⌘': 91, command: 91
    },
    // special keys
    _MAP = {
      backspace: 8, tab: 9, clear: 12,
      enter: 13, 'return': 13,
      esc: 27, escape: 27, space: 32,
      left: 37, up: 38,
      right: 39, down: 40,
      del: 46, 'delete': 46,
      home: 36, end: 35,
      pageup: 33, pagedown: 34,
      ',': 188, '.': 190, '/': 191,
      '`': 192, '-': 189, '=': 187,
      ';': 186, '\'': 222,
      '[': 219, ']': 221, '\\': 220
    },
    code = function(x){
      return _MAP[x] || x.toUpperCase().charCodeAt(0);
    },
    _downKeys = [];

  for(k=1;k<20;k++) _MAP['f'+k] = 111+k;

  // IE doesn't support Array#indexOf, so have a simple replacement
  function index(array, item){
    var i = array.length;
    while(i--) if(array[i]===item) return i;
    return -1;
  }

  // for comparing mods before unassignment
  function compareArray(a1, a2) {
    if (a1.length != a2.length) return false;
    for (var i = 0; i < a1.length; i++) {
        if (a1[i] !== a2[i]) return false;
    }
    return true;
  }

  var modifierMap = {
      16:'shiftKey',
      18:'altKey',
      17:'ctrlKey',
      91:'metaKey'
  };
  function updateModifierKey(event) {
      for(k in _mods) _mods[k] = event[modifierMap[k]];
  };

  // handle keydown event
  function dispatch(event) {
    var key, handler, k, i, modifiersMatch, scope;
    key = event.keyCode;

    if (index(_downKeys, key) == -1) {
        _downKeys.push(key);
    }

    // if a modifier key, set the key.<modifierkeyname> property to true and return
    if(key == 93 || key == 224) key = 91; // right command on webkit, command on Gecko
    if(key in _mods) {
      _mods[key] = true;
      // 'assignKey' from inside this closure is exported to window.key
      for(k in _MODIFIERS) if(_MODIFIERS[k] == key) assignKey[k] = true;
      return;
    }
    updateModifierKey(event);

    // see if we need to ignore the keypress (filter() can can be overridden)
    // by default ignore key presses if a select, textarea, or input is focused
    if(!assignKey.filter.call(this, event)) return;

    // abort if no potentially matching shortcuts found
    if (!(key in _handlers)) return;

    scope = getScope();

    // for each potential shortcut
    for (i = 0; i < _handlers[key].length; i++) {
      handler = _handlers[key][i];

      // see if it's in the current scope
      if(handler.scope == scope || handler.scope == 'all'){
        // check if modifiers match if any
        modifiersMatch = handler.mods.length > 0;
        for(k in _mods)
          if((!_mods[k] && index(handler.mods, +k) > -1) ||
            (_mods[k] && index(handler.mods, +k) == -1)) modifiersMatch = false;
        // call the handler and stop the event if neccessary
        if((handler.mods.length == 0 && !_mods[16] && !_mods[18] && !_mods[17] && !_mods[91]) || modifiersMatch){
          if(handler.method(event, handler)===false){
            if(event.preventDefault) event.preventDefault();
              else event.returnValue = false;
            if(event.stopPropagation) event.stopPropagation();
            if(event.cancelBubble) event.cancelBubble = true;
          }
        }
      }
    }
  };

  // unset modifier keys on keyup
  function clearModifier(event){
    var key = event.keyCode, k,
        i = index(_downKeys, key);

    // remove key from _downKeys
    if (i >= 0) {
        _downKeys.splice(i, 1);
    }

    if(key == 93 || key == 224) key = 91;
    if(key in _mods) {
      _mods[key] = false;
      for(k in _MODIFIERS) if(_MODIFIERS[k] == key) assignKey[k] = false;
    }
  };

  function resetModifiers() {
    for(k in _mods) _mods[k] = false;
    for(k in _MODIFIERS) assignKey[k] = false;
  };

  // parse and assign shortcut
  function assignKey(key, scope, method){
    var keys, mods;
    keys = getKeys(key);
    if (method === undefined) {
      method = scope;
      scope = 'all';
    }

    // for each shortcut
    for (var i = 0; i < keys.length; i++) {
      // set modifier keys if any
      mods = [];
      key = keys[i].split('+');
      if (key.length > 1){
        mods = getMods(key);
        key = [key[key.length-1]];
      }
      // convert to keycode and...
      key = key[0]
      key = code(key);
      // ...store handler
      if (!(key in _handlers)) _handlers[key] = [];
      _handlers[key].push({ shortcut: keys[i], scope: scope, method: method, key: keys[i], mods: mods });
    }
  };

  // unbind all handlers for given key in current scope
  function unbindKey(key, scope) {
    var multipleKeys, keys,
      mods = [],
      i, j, obj;

    multipleKeys = getKeys(key);

    for (j = 0; j < multipleKeys.length; j++) {
      keys = multipleKeys[j].split('+');

      if (keys.length > 1) {
        mods = getMods(keys);
        key = keys[keys.length - 1];
      }

      key = code(key);

      if (scope === undefined) {
        scope = getScope();
      }
      if (!_handlers[key]) {
        return;
      }
      for (i = 0; i < _handlers[key].length; i++) {
        obj = _handlers[key][i];
        // only clear handlers if correct scope and mods match
        if (obj.scope === scope && compareArray(obj.mods, mods)) {
          _handlers[key][i] = {};
        }
      }
    }
  };

  // Returns true if the key with code 'keyCode' is currently down
  // Converts strings into key codes.
  function isPressed(keyCode) {
      if (typeof(keyCode)=='string') {
        keyCode = code(keyCode);
      }
      return index(_downKeys, keyCode) != -1;
  }

  function getPressedKeyCodes() {
      return _downKeys.slice(0);
  }

  function filter(event){
    var tagName = (event.target || event.srcElement).tagName;
    // ignore keypressed in any elements that support keyboard data input
    return !(tagName == 'INPUT' || tagName == 'SELECT' || tagName == 'TEXTAREA');
  }

  // initialize key.<modifier> to false
  for(k in _MODIFIERS) assignKey[k] = false;

  // set current scope (default 'all')
  function setScope(scope){ _scope = scope || 'all' };
  function getScope(){ return _scope || 'all' };

  // delete all handlers for a given scope
  function deleteScope(scope){
    var key, handlers, i;

    for (key in _handlers) {
      handlers = _handlers[key];
      for (i = 0; i < handlers.length; ) {
        if (handlers[i].scope === scope) handlers.splice(i, 1);
        else i++;
      }
    }
  };

  // abstract key logic for assign and unassign
  function getKeys(key) {
    var keys;
    key = key.replace(/\s/g, '');
    keys = key.split(',');
    if ((keys[keys.length - 1]) == '') {
      keys[keys.length - 2] += ',';
    }
    return keys;
  }

  // abstract mods logic for assign and unassign
  function getMods(key) {
    var mods = key.slice(0, key.length - 1);
    for (var mi = 0; mi < mods.length; mi++)
    mods[mi] = _MODIFIERS[mods[mi]];
    return mods;
  }

  // cross-browser events
  function addEvent(object, event, method) {
    if (object.addEventListener)
      object.addEventListener(event, method, false);
    else if(object.attachEvent)
      object.attachEvent('on'+event, function(){ method(window.event) });
  };

  // set the handlers globally on document
  addEvent(document, 'keydown', function(event) { dispatch(event) }); // Passing _scope to a callback to ensure it remains the same by execution. Fixes #48
  addEvent(document, 'keyup', clearModifier);

  // reset modifiers to false whenever the window is (re)focused.
  addEvent(window, 'focus', resetModifiers);

  // store previously defined key
  var previousKey = global.key;

  // restore previously defined key and return reference to our key object
  function noConflict() {
    var k = global.key;
    global.key = previousKey;
    return k;
  }

  // set window.key and window.key.set/get/deleteScope, and the default filter
  global.key = assignKey;
  global.key.setScope = setScope;
  global.key.getScope = getScope;
  global.key.deleteScope = deleteScope;
  global.key.filter = filter;
  global.key.isPressed = isPressed;
  global.key.getPressedKeyCodes = getPressedKeyCodes;
  global.key.noConflict = noConflict;
  global.key.unbind = unbindKey;

  if(typeof module !== 'undefined') module.exports = assignKey;

})(this);

},{}],3:[function(require,module,exports){
'use strict';

module.exports = require(8);

},{"8":8}],4:[function(require,module,exports){
'use strict';

var DOM = {};

DOM.create = function (tagName, className) {
  var element = document.createElement(tagName);
  element.className = className;
  return element;
};

DOM.appendTo = function (child, parent) {
  parent.appendChild(child);
  return child;
};

function cssGet(element, styleName) {
  return window.getComputedStyle(element)[styleName];
}

function cssSet(element, styleName, styleValue) {
  if (typeof styleValue === 'number') {
    styleValue = styleValue.toString() + 'px';
  }
  element.style[styleName] = styleValue;
  return element;
}

function cssMultiSet(element, obj) {
  for (var key in obj) {
    var val = obj[key];
    if (typeof val === 'number') {
      val = val.toString() + 'px';
    }
    element.style[key] = val;
  }
  return element;
}

DOM.css = function (element, styleNameOrObject, styleValue) {
  if (typeof styleNameOrObject === 'object') {
    // multiple set with object
    return cssMultiSet(element, styleNameOrObject);
  } else {
    if (typeof styleValue === 'undefined') {
      return cssGet(element, styleNameOrObject);
    } else {
      return cssSet(element, styleNameOrObject, styleValue);
    }
  }
};

DOM.matches = function (element, query) {
  if (typeof element.matches !== 'undefined') {
    return element.matches(query);
  } else {
    // must be IE11 and Edge
    return element.msMatchesSelector(query);
  }
};

DOM.remove = function (element) {
  if (typeof element.remove !== 'undefined') {
    element.remove();
  } else {
    if (element.parentNode) {
      element.parentNode.removeChild(element);
    }
  }
};

DOM.queryChildren = function (element, selector) {
  return Array.prototype.filter.call(element.childNodes, function (child) {
    return DOM.matches(child, selector);
  });
};

module.exports = DOM;

},{}],5:[function(require,module,exports){
'use strict';

var EventElement = function (element) {
  this.element = element;
  this.events = {};
};

EventElement.prototype.bind = function (eventName, handler) {
  if (typeof this.events[eventName] === 'undefined') {
    this.events[eventName] = [];
  }
  this.events[eventName].push(handler);
  this.element.addEventListener(eventName, handler, false);
};

EventElement.prototype.unbind = function (eventName, handler) {
  var isHandlerProvided = (typeof handler !== 'undefined');
  this.events[eventName] = this.events[eventName].filter(function (hdlr) {
    if (isHandlerProvided && hdlr !== handler) {
      return true;
    }
    this.element.removeEventListener(eventName, hdlr, false);
    return false;
  }, this);
};

EventElement.prototype.unbindAll = function () {
  for (var name in this.events) {
    this.unbind(name);
  }
};

var EventManager = function () {
  this.eventElements = [];
};

EventManager.prototype.eventElement = function (element) {
  var ee = this.eventElements.filter(function (eventElement) {
    return eventElement.element === element;
  })[0];
  if (typeof ee === 'undefined') {
    ee = new EventElement(element);
    this.eventElements.push(ee);
  }
  return ee;
};

EventManager.prototype.bind = function (element, eventName, handler) {
  this.eventElement(element).bind(eventName, handler);
};

EventManager.prototype.unbind = function (element, eventName, handler) {
  this.eventElement(element).unbind(eventName, handler);
};

EventManager.prototype.unbindAll = function () {
  for (var i = 0; i < this.eventElements.length; i++) {
    this.eventElements[i].unbindAll();
  }
};

EventManager.prototype.once = function (element, eventName, handler) {
  var ee = this.eventElement(element);
  var onceHandler = function (e) {
    ee.unbind(eventName, onceHandler);
    handler(e);
  };
  ee.bind(eventName, onceHandler);
};

module.exports = EventManager;

},{}],6:[function(require,module,exports){
'use strict';

module.exports = (function () {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
               .toString(16)
               .substring(1);
  }
  return function () {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
           s4() + '-' + s4() + s4() + s4();
  };
})();

},{}],7:[function(require,module,exports){
'use strict';

var dom = require(4);

var toInt = exports.toInt = function (x) {
  return parseInt(x, 10) || 0;
};

exports.isEditable = function (el) {
  return dom.matches(el, "input,[contenteditable]") ||
         dom.matches(el, "select,[contenteditable]") ||
         dom.matches(el, "textarea,[contenteditable]") ||
         dom.matches(el, "button,[contenteditable]");
};

exports.removePsClasses = function (element) {
  for (var i = 0; i < element.classList.length; i++) {
    var className = element.classList[i];
    if (className.indexOf('ps-') === 0) {
      element.classList.remove(className);
    }
  }
};

exports.outerWidth = function (element) {
  return toInt(dom.css(element, 'width')) +
         toInt(dom.css(element, 'paddingLeft')) +
         toInt(dom.css(element, 'paddingRight')) +
         toInt(dom.css(element, 'borderLeftWidth')) +
         toInt(dom.css(element, 'borderRightWidth'));
};

function psClasses(axis) {
  var classes = ['ps--in-scrolling'];
  var axisClasses;
  if (typeof axis === 'undefined') {
    axisClasses = ['ps--x', 'ps--y'];
  } else {
    axisClasses = ['ps--' + axis];
  }
  return classes.concat(axisClasses);
}

exports.startScrolling = function (element, axis) {
  var classes = psClasses(axis);
  for (var i = 0; i < classes.length; i++) {
    element.classList.add(classes[i]);
  }
};

exports.stopScrolling = function (element, axis) {
  var classes = psClasses(axis);
  for (var i = 0; i < classes.length; i++) {
    element.classList.remove(classes[i]);
  }
};

exports.env = {
  isWebKit: typeof document !== 'undefined' && 'WebkitAppearance' in document.documentElement.style,
  supportsTouch: typeof window !== 'undefined' && (('ontouchstart' in window) || window.DocumentTouch && document instanceof window.DocumentTouch),
  supportsIePointer: typeof window !== 'undefined' && window.navigator.msMaxTouchPoints !== null
};

},{"4":4}],8:[function(require,module,exports){
'use strict';

var destroy = require(10);
var initialize = require(18);
var update = require(22);

module.exports = {
  initialize: initialize,
  update: update,
  destroy: destroy
};

},{"10":10,"18":18,"22":22}],9:[function(require,module,exports){
'use strict';

module.exports = function () {
  return {
    handlers: ['click-rail', 'drag-scrollbar', 'keyboard', 'wheel', 'touch'],
    maxScrollbarLength: null,
    minScrollbarLength: null,
    scrollXMarginOffset: 0,
    scrollYMarginOffset: 0,
    suppressScrollX: false,
    suppressScrollY: false,
    swipePropagation: true,
    swipeEasing: true,
    useBothWheelAxes: false,
    wheelPropagation: false,
    wheelSpeed: 1,
    theme: 'default'
  };
};

},{}],10:[function(require,module,exports){
'use strict';

var _ = require(7);
var dom = require(4);
var instances = require(19);

module.exports = function (element) {
  var i = instances.get(element);

  if (!i) {
    return;
  }

  i.event.unbindAll();
  dom.remove(i.scrollbarX);
  dom.remove(i.scrollbarY);
  dom.remove(i.scrollbarXRail);
  dom.remove(i.scrollbarYRail);
  _.removePsClasses(element);

  instances.remove(element);
};

},{"19":19,"4":4,"7":7}],11:[function(require,module,exports){
'use strict';

var instances = require(19);
var updateGeometry = require(20);
var updateScroll = require(21);

function bindClickRailHandler(element, i) {
  function pageOffset(el) {
    return el.getBoundingClientRect();
  }
  var stopPropagation = function (e) { e.stopPropagation(); };

  i.event.bind(i.scrollbarY, 'click', stopPropagation);
  i.event.bind(i.scrollbarYRail, 'click', function (e) {
    var positionTop = e.pageY - window.pageYOffset - pageOffset(i.scrollbarYRail).top;
    var direction = positionTop > i.scrollbarYTop ? 1 : -1;

    updateScroll(element, 'top', element.scrollTop + direction * i.containerHeight);
    updateGeometry(element);

    e.stopPropagation();
  });

  i.event.bind(i.scrollbarX, 'click', stopPropagation);
  i.event.bind(i.scrollbarXRail, 'click', function (e) {
    var positionLeft = e.pageX - window.pageXOffset - pageOffset(i.scrollbarXRail).left;
    var direction = positionLeft > i.scrollbarXLeft ? 1 : -1;

    updateScroll(element, 'left', element.scrollLeft + direction * i.containerWidth);
    updateGeometry(element);

    e.stopPropagation();
  });
}

module.exports = function (element) {
  var i = instances.get(element);
  bindClickRailHandler(element, i);
};

},{"19":19,"20":20,"21":21}],12:[function(require,module,exports){
'use strict';

var _ = require(7);
var dom = require(4);
var instances = require(19);
var updateGeometry = require(20);
var updateScroll = require(21);

function bindMouseScrollXHandler(element, i) {
  var currentLeft = null;
  var currentPageX = null;

  function updateScrollLeft(deltaX) {
    var newLeft = currentLeft + (deltaX * i.railXRatio);
    var maxLeft = Math.max(0, i.scrollbarXRail.getBoundingClientRect().left) + (i.railXRatio * (i.railXWidth - i.scrollbarXWidth));

    if (newLeft < 0) {
      i.scrollbarXLeft = 0;
    } else if (newLeft > maxLeft) {
      i.scrollbarXLeft = maxLeft;
    } else {
      i.scrollbarXLeft = newLeft;
    }

    var scrollLeft = _.toInt(i.scrollbarXLeft * (i.contentWidth - i.containerWidth) / (i.containerWidth - (i.railXRatio * i.scrollbarXWidth))) - i.negativeScrollAdjustment;
    updateScroll(element, 'left', scrollLeft);
  }

  var mouseMoveHandler = function (e) {
    updateScrollLeft(e.pageX - currentPageX);
    updateGeometry(element);
    e.stopPropagation();
    e.preventDefault();
  };

  var mouseUpHandler = function () {
    _.stopScrolling(element, 'x');
    i.event.unbind(i.ownerDocument, 'mousemove', mouseMoveHandler);
  };

  i.event.bind(i.scrollbarX, 'mousedown', function (e) {
    currentPageX = e.pageX;
    currentLeft = _.toInt(dom.css(i.scrollbarX, 'left')) * i.railXRatio;
    _.startScrolling(element, 'x');

    i.event.bind(i.ownerDocument, 'mousemove', mouseMoveHandler);
    i.event.once(i.ownerDocument, 'mouseup', mouseUpHandler);

    e.stopPropagation();
    e.preventDefault();
  });
}

function bindMouseScrollYHandler(element, i) {
  var currentTop = null;
  var currentPageY = null;

  function updateScrollTop(deltaY) {
    var newTop = currentTop + (deltaY * i.railYRatio);
    var maxTop = Math.max(0, i.scrollbarYRail.getBoundingClientRect().top) + (i.railYRatio * (i.railYHeight - i.scrollbarYHeight));

    if (newTop < 0) {
      i.scrollbarYTop = 0;
    } else if (newTop > maxTop) {
      i.scrollbarYTop = maxTop;
    } else {
      i.scrollbarYTop = newTop;
    }

    var scrollTop = _.toInt(i.scrollbarYTop * (i.contentHeight - i.containerHeight) / (i.containerHeight - (i.railYRatio * i.scrollbarYHeight)));
    updateScroll(element, 'top', scrollTop);
  }

  var mouseMoveHandler = function (e) {
    updateScrollTop(e.pageY - currentPageY);
    updateGeometry(element);
    e.stopPropagation();
    e.preventDefault();
  };

  var mouseUpHandler = function () {
    _.stopScrolling(element, 'y');
    i.event.unbind(i.ownerDocument, 'mousemove', mouseMoveHandler);
  };

  i.event.bind(i.scrollbarY, 'mousedown', function (e) {
    currentPageY = e.pageY;
    currentTop = _.toInt(dom.css(i.scrollbarY, 'top')) * i.railYRatio;
    _.startScrolling(element, 'y');

    i.event.bind(i.ownerDocument, 'mousemove', mouseMoveHandler);
    i.event.once(i.ownerDocument, 'mouseup', mouseUpHandler);

    e.stopPropagation();
    e.preventDefault();
  });
}

module.exports = function (element) {
  var i = instances.get(element);
  bindMouseScrollXHandler(element, i);
  bindMouseScrollYHandler(element, i);
};

},{"19":19,"20":20,"21":21,"4":4,"7":7}],13:[function(require,module,exports){
'use strict';

var _ = require(7);
var dom = require(4);
var instances = require(19);
var updateGeometry = require(20);
var updateScroll = require(21);

function bindKeyboardHandler(element, i) {
  var hovered = false;
  i.event.bind(element, 'mouseenter', function () {
    hovered = true;
  });
  i.event.bind(element, 'mouseleave', function () {
    hovered = false;
  });

  var shouldPrevent = false;
  function shouldPreventDefault(deltaX, deltaY) {
    var scrollTop = element.scrollTop;
    if (deltaX === 0) {
      if (!i.scrollbarYActive) {
        return false;
      }
      if ((scrollTop === 0 && deltaY > 0) || (scrollTop >= i.contentHeight - i.containerHeight && deltaY < 0)) {
        return !i.settings.wheelPropagation;
      }
    }

    var scrollLeft = element.scrollLeft;
    if (deltaY === 0) {
      if (!i.scrollbarXActive) {
        return false;
      }
      if ((scrollLeft === 0 && deltaX < 0) || (scrollLeft >= i.contentWidth - i.containerWidth && deltaX > 0)) {
        return !i.settings.wheelPropagation;
      }
    }
    return true;
  }

  i.event.bind(i.ownerDocument, 'keydown', function (e) {
    if ((e.isDefaultPrevented && e.isDefaultPrevented()) || e.defaultPrevented) {
      return;
    }

    var focused = dom.matches(i.scrollbarX, ':focus') ||
                  dom.matches(i.scrollbarY, ':focus');

    if (!hovered && !focused) {
      return;
    }

    var activeElement = document.activeElement ? document.activeElement : i.ownerDocument.activeElement;
    if (activeElement) {
      if (activeElement.tagName === 'IFRAME') {
        activeElement = activeElement.contentDocument.activeElement;
      } else {
        // go deeper if element is a webcomponent
        while (activeElement.shadowRoot) {
          activeElement = activeElement.shadowRoot.activeElement;
        }
      }
      if (_.isEditable(activeElement)) {
        return;
      }
    }

    var deltaX = 0;
    var deltaY = 0;

    switch (e.which) {
    case 37: // left
      if (e.metaKey) {
        deltaX = -i.contentWidth;
      } else if (e.altKey) {
        deltaX = -i.containerWidth;
      } else {
        deltaX = -30;
      }
      break;
    case 38: // up
      if (e.metaKey) {
        deltaY = i.contentHeight;
      } else if (e.altKey) {
        deltaY = i.containerHeight;
      } else {
        deltaY = 30;
      }
      break;
    case 39: // right
      if (e.metaKey) {
        deltaX = i.contentWidth;
      } else if (e.altKey) {
        deltaX = i.containerWidth;
      } else {
        deltaX = 30;
      }
      break;
    case 40: // down
      if (e.metaKey) {
        deltaY = -i.contentHeight;
      } else if (e.altKey) {
        deltaY = -i.containerHeight;
      } else {
        deltaY = -30;
      }
      break;
    case 33: // page up
      deltaY = 90;
      break;
    case 32: // space bar
      if (e.shiftKey) {
        deltaY = 90;
      } else {
        deltaY = -90;
      }
      break;
    case 34: // page down
      deltaY = -90;
      break;
    case 35: // end
      if (e.ctrlKey) {
        deltaY = -i.contentHeight;
      } else {
        deltaY = -i.containerHeight;
      }
      break;
    case 36: // home
      if (e.ctrlKey) {
        deltaY = element.scrollTop;
      } else {
        deltaY = i.containerHeight;
      }
      break;
    default:
      return;
    }

    updateScroll(element, 'top', element.scrollTop - deltaY);
    updateScroll(element, 'left', element.scrollLeft + deltaX);
    updateGeometry(element);

    shouldPrevent = shouldPreventDefault(deltaX, deltaY);
    if (shouldPrevent) {
      e.preventDefault();
    }
  });
}

module.exports = function (element) {
  var i = instances.get(element);
  bindKeyboardHandler(element, i);
};

},{"19":19,"20":20,"21":21,"4":4,"7":7}],14:[function(require,module,exports){
'use strict';

var instances = require(19);
var updateGeometry = require(20);
var updateScroll = require(21);

function bindMouseWheelHandler(element, i) {
  var shouldPrevent = false;

  function shouldPreventDefault(deltaX, deltaY) {
    var scrollTop = element.scrollTop;
    if (deltaX === 0) {
      if (!i.scrollbarYActive) {
        return false;
      }
      if ((scrollTop === 0 && deltaY > 0) || (scrollTop >= i.contentHeight - i.containerHeight && deltaY < 0)) {
        return !i.settings.wheelPropagation;
      }
    }

    var scrollLeft = element.scrollLeft;
    if (deltaY === 0) {
      if (!i.scrollbarXActive) {
        return false;
      }
      if ((scrollLeft === 0 && deltaX < 0) || (scrollLeft >= i.contentWidth - i.containerWidth && deltaX > 0)) {
        return !i.settings.wheelPropagation;
      }
    }
    return true;
  }

  function getDeltaFromEvent(e) {
    var deltaX = e.deltaX;
    var deltaY = -1 * e.deltaY;

    if (typeof deltaX === "undefined" || typeof deltaY === "undefined") {
      // OS X Safari
      deltaX = -1 * e.wheelDeltaX / 6;
      deltaY = e.wheelDeltaY / 6;
    }

    if (e.deltaMode && e.deltaMode === 1) {
      // Firefox in deltaMode 1: Line scrolling
      deltaX *= 10;
      deltaY *= 10;
    }

    if (deltaX !== deltaX && deltaY !== deltaY/* NaN checks */) {
      // IE in some mouse drivers
      deltaX = 0;
      deltaY = e.wheelDelta;
    }

    if (e.shiftKey) {
      // reverse axis with shift key
      return [-deltaY, -deltaX];
    }
    return [deltaX, deltaY];
  }

  function shouldBeConsumedByChild(deltaX, deltaY) {
    var child = element.querySelector('textarea:hover, select[multiple]:hover, .ps-child:hover');
    if (child) {
      var style = window.getComputedStyle(child);
      var overflow = [
        style.overflow,
        style.overflowX,
        style.overflowY
      ].join('');

      if (!overflow.match(/(scroll|auto)/)) {
        // if not scrollable
        return false;
      }

      var maxScrollTop = child.scrollHeight - child.clientHeight;
      if (maxScrollTop > 0) {
        if (!(child.scrollTop === 0 && deltaY > 0) && !(child.scrollTop === maxScrollTop && deltaY < 0)) {
          return true;
        }
      }
      var maxScrollLeft = child.scrollLeft - child.clientWidth;
      if (maxScrollLeft > 0) {
        if (!(child.scrollLeft === 0 && deltaX < 0) && !(child.scrollLeft === maxScrollLeft && deltaX > 0)) {
          return true;
        }
      }
    }
    return false;
  }

  function mousewheelHandler(e) {
    var delta = getDeltaFromEvent(e);

    var deltaX = delta[0];
    var deltaY = delta[1];

    if (shouldBeConsumedByChild(deltaX, deltaY)) {
      return;
    }

    shouldPrevent = false;
    if (!i.settings.useBothWheelAxes) {
      // deltaX will only be used for horizontal scrolling and deltaY will
      // only be used for vertical scrolling - this is the default
      updateScroll(element, 'top', element.scrollTop - (deltaY * i.settings.wheelSpeed));
      updateScroll(element, 'left', element.scrollLeft + (deltaX * i.settings.wheelSpeed));
    } else if (i.scrollbarYActive && !i.scrollbarXActive) {
      // only vertical scrollbar is active and useBothWheelAxes option is
      // active, so let's scroll vertical bar using both mouse wheel axes
      if (deltaY) {
        updateScroll(element, 'top', element.scrollTop - (deltaY * i.settings.wheelSpeed));
      } else {
        updateScroll(element, 'top', element.scrollTop + (deltaX * i.settings.wheelSpeed));
      }
      shouldPrevent = true;
    } else if (i.scrollbarXActive && !i.scrollbarYActive) {
      // useBothWheelAxes and only horizontal bar is active, so use both
      // wheel axes for horizontal bar
      if (deltaX) {
        updateScroll(element, 'left', element.scrollLeft + (deltaX * i.settings.wheelSpeed));
      } else {
        updateScroll(element, 'left', element.scrollLeft - (deltaY * i.settings.wheelSpeed));
      }
      shouldPrevent = true;
    }

    updateGeometry(element);

    shouldPrevent = (shouldPrevent || shouldPreventDefault(deltaX, deltaY));
    if (shouldPrevent) {
      e.stopPropagation();
      e.preventDefault();
    }
  }

  if (typeof window.onwheel !== "undefined") {
    i.event.bind(element, 'wheel', mousewheelHandler);
  } else if (typeof window.onmousewheel !== "undefined") {
    i.event.bind(element, 'mousewheel', mousewheelHandler);
  }
}

module.exports = function (element) {
  var i = instances.get(element);
  bindMouseWheelHandler(element, i);
};

},{"19":19,"20":20,"21":21}],15:[function(require,module,exports){
'use strict';

var instances = require(19);
var updateGeometry = require(20);

function bindNativeScrollHandler(element, i) {
  i.event.bind(element, 'scroll', function () {
    updateGeometry(element);
  });
}

module.exports = function (element) {
  var i = instances.get(element);
  bindNativeScrollHandler(element, i);
};

},{"19":19,"20":20}],16:[function(require,module,exports){
'use strict';

var _ = require(7);
var instances = require(19);
var updateGeometry = require(20);
var updateScroll = require(21);

function bindSelectionHandler(element, i) {
  function getRangeNode() {
    var selection = window.getSelection ? window.getSelection() :
                    document.getSelection ? document.getSelection() : '';
    if (selection.toString().length === 0) {
      return null;
    } else {
      return selection.getRangeAt(0).commonAncestorContainer;
    }
  }

  var scrollingLoop = null;
  var scrollDiff = {top: 0, left: 0};
  function startScrolling() {
    if (!scrollingLoop) {
      scrollingLoop = setInterval(function () {
        if (!instances.get(element)) {
          clearInterval(scrollingLoop);
          return;
        }

        updateScroll(element, 'top', element.scrollTop + scrollDiff.top);
        updateScroll(element, 'left', element.scrollLeft + scrollDiff.left);
        updateGeometry(element);
      }, 50); // every .1 sec
    }
  }
  function stopScrolling() {
    if (scrollingLoop) {
      clearInterval(scrollingLoop);
      scrollingLoop = null;
    }
    _.stopScrolling(element);
  }

  var isSelected = false;
  i.event.bind(i.ownerDocument, 'selectionchange', function () {
    if (element.contains(getRangeNode())) {
      isSelected = true;
    } else {
      isSelected = false;
      stopScrolling();
    }
  });
  i.event.bind(window, 'mouseup', function () {
    if (isSelected) {
      isSelected = false;
      stopScrolling();
    }
  });
  i.event.bind(window, 'keyup', function () {
    if (isSelected) {
      isSelected = false;
      stopScrolling();
    }
  });

  i.event.bind(window, 'mousemove', function (e) {
    if (isSelected) {
      var mousePosition = {x: e.pageX, y: e.pageY};
      var containerGeometry = {
        left: element.offsetLeft,
        right: element.offsetLeft + element.offsetWidth,
        top: element.offsetTop,
        bottom: element.offsetTop + element.offsetHeight
      };

      if (mousePosition.x < containerGeometry.left + 3) {
        scrollDiff.left = -5;
        _.startScrolling(element, 'x');
      } else if (mousePosition.x > containerGeometry.right - 3) {
        scrollDiff.left = 5;
        _.startScrolling(element, 'x');
      } else {
        scrollDiff.left = 0;
      }

      if (mousePosition.y < containerGeometry.top + 3) {
        if (containerGeometry.top + 3 - mousePosition.y < 5) {
          scrollDiff.top = -5;
        } else {
          scrollDiff.top = -20;
        }
        _.startScrolling(element, 'y');
      } else if (mousePosition.y > containerGeometry.bottom - 3) {
        if (mousePosition.y - containerGeometry.bottom + 3 < 5) {
          scrollDiff.top = 5;
        } else {
          scrollDiff.top = 20;
        }
        _.startScrolling(element, 'y');
      } else {
        scrollDiff.top = 0;
      }

      if (scrollDiff.top === 0 && scrollDiff.left === 0) {
        stopScrolling();
      } else {
        startScrolling();
      }
    }
  });
}

module.exports = function (element) {
  var i = instances.get(element);
  bindSelectionHandler(element, i);
};

},{"19":19,"20":20,"21":21,"7":7}],17:[function(require,module,exports){
'use strict';

var _ = require(7);
var instances = require(19);
var updateGeometry = require(20);
var updateScroll = require(21);

function bindTouchHandler(element, i, supportsTouch, supportsIePointer) {
  function shouldPreventDefault(deltaX, deltaY) {
    var scrollTop = element.scrollTop;
    var scrollLeft = element.scrollLeft;
    var magnitudeX = Math.abs(deltaX);
    var magnitudeY = Math.abs(deltaY);

    if (magnitudeY > magnitudeX) {
      // user is perhaps trying to swipe up/down the page

      if (((deltaY < 0) && (scrollTop === i.contentHeight - i.containerHeight)) ||
          ((deltaY > 0) && (scrollTop === 0))) {
        return !i.settings.swipePropagation;
      }
    } else if (magnitudeX > magnitudeY) {
      // user is perhaps trying to swipe left/right across the page

      if (((deltaX < 0) && (scrollLeft === i.contentWidth - i.containerWidth)) ||
          ((deltaX > 0) && (scrollLeft === 0))) {
        return !i.settings.swipePropagation;
      }
    }

    return true;
  }

  function applyTouchMove(differenceX, differenceY) {
    updateScroll(element, 'top', element.scrollTop - differenceY);
    updateScroll(element, 'left', element.scrollLeft - differenceX);

    updateGeometry(element);
  }

  var startOffset = {};
  var startTime = 0;
  var speed = {};
  var easingLoop = null;
  var inGlobalTouch = false;
  var inLocalTouch = false;

  function globalTouchStart() {
    inGlobalTouch = true;
  }
  function globalTouchEnd() {
    inGlobalTouch = false;
  }

  function getTouch(e) {
    if (e.targetTouches) {
      return e.targetTouches[0];
    } else {
      // Maybe IE pointer
      return e;
    }
  }
  function shouldHandle(e) {
    if (e.pointerType && e.pointerType === 'pen' && e.buttons === 0) {
      return false;
    }
    if (e.targetTouches && e.targetTouches.length === 1) {
      return true;
    }
    if (e.pointerType && e.pointerType !== 'mouse' && e.pointerType !== e.MSPOINTER_TYPE_MOUSE) {
      return true;
    }
    return false;
  }
  function touchStart(e) {
    if (shouldHandle(e)) {
      inLocalTouch = true;

      var touch = getTouch(e);

      startOffset.pageX = touch.pageX;
      startOffset.pageY = touch.pageY;

      startTime = (new Date()).getTime();

      if (easingLoop !== null) {
        clearInterval(easingLoop);
      }

      e.stopPropagation();
    }
  }
  function touchMove(e) {
    if (!inLocalTouch && i.settings.swipePropagation) {
      touchStart(e);
    }
    if (!inGlobalTouch && inLocalTouch && shouldHandle(e)) {
      var touch = getTouch(e);

      var currentOffset = {pageX: touch.pageX, pageY: touch.pageY};

      var differenceX = currentOffset.pageX - startOffset.pageX;
      var differenceY = currentOffset.pageY - startOffset.pageY;

      applyTouchMove(differenceX, differenceY);
      startOffset = currentOffset;

      var currentTime = (new Date()).getTime();

      var timeGap = currentTime - startTime;
      if (timeGap > 0) {
        speed.x = differenceX / timeGap;
        speed.y = differenceY / timeGap;
        startTime = currentTime;
      }

      if (shouldPreventDefault(differenceX, differenceY)) {
        e.stopPropagation();
        e.preventDefault();
      }
    }
  }
  function touchEnd() {
    if (!inGlobalTouch && inLocalTouch) {
      inLocalTouch = false;

      if (i.settings.swipeEasing) {
        clearInterval(easingLoop);
        easingLoop = setInterval(function () {
          if (!instances.get(element)) {
            clearInterval(easingLoop);
            return;
          }

          if (!speed.x && !speed.y) {
            clearInterval(easingLoop);
            return;
          }

          if (Math.abs(speed.x) < 0.01 && Math.abs(speed.y) < 0.01) {
            clearInterval(easingLoop);
            return;
          }

          applyTouchMove(speed.x * 30, speed.y * 30);

          speed.x *= 0.8;
          speed.y *= 0.8;
        }, 10);
      }
    }
  }

  if (supportsTouch) {
    i.event.bind(window, 'touchstart', globalTouchStart);
    i.event.bind(window, 'touchend', globalTouchEnd);
    i.event.bind(element, 'touchstart', touchStart);
    i.event.bind(element, 'touchmove', touchMove);
    i.event.bind(element, 'touchend', touchEnd);
  } else if (supportsIePointer) {
    if (window.PointerEvent) {
      i.event.bind(window, 'pointerdown', globalTouchStart);
      i.event.bind(window, 'pointerup', globalTouchEnd);
      i.event.bind(element, 'pointerdown', touchStart);
      i.event.bind(element, 'pointermove', touchMove);
      i.event.bind(element, 'pointerup', touchEnd);
    } else if (window.MSPointerEvent) {
      i.event.bind(window, 'MSPointerDown', globalTouchStart);
      i.event.bind(window, 'MSPointerUp', globalTouchEnd);
      i.event.bind(element, 'MSPointerDown', touchStart);
      i.event.bind(element, 'MSPointerMove', touchMove);
      i.event.bind(element, 'MSPointerUp', touchEnd);
    }
  }
}

module.exports = function (element) {
  if (!_.env.supportsTouch && !_.env.supportsIePointer) {
    return;
  }

  var i = instances.get(element);
  bindTouchHandler(element, i, _.env.supportsTouch, _.env.supportsIePointer);
};

},{"19":19,"20":20,"21":21,"7":7}],18:[function(require,module,exports){
'use strict';

var instances = require(19);
var updateGeometry = require(20);

// Handlers
var handlers = {
  'click-rail': require(11),
  'drag-scrollbar': require(12),
  'keyboard': require(13),
  'wheel': require(14),
  'touch': require(17),
  'selection': require(16)
};
var nativeScrollHandler = require(15);

module.exports = function (element, userSettings) {
  element.classList.add('ps');

  // Create a plugin instance.
  var i = instances.add(
    element,
    typeof userSettings === 'object' ? userSettings : {}
  );

  element.classList.add('ps--theme_' + i.settings.theme);

  i.settings.handlers.forEach(function (handlerName) {
    handlers[handlerName](element);
  });

  nativeScrollHandler(element);

  updateGeometry(element);
};

},{"11":11,"12":12,"13":13,"14":14,"15":15,"16":16,"17":17,"19":19,"20":20}],19:[function(require,module,exports){
'use strict';

var _ = require(7);
var defaultSettings = require(9);
var dom = require(4);
var EventManager = require(5);
var guid = require(6);

var instances = {};

function Instance(element, userSettings) {
  var i = this;

  i.settings = defaultSettings();
  for (var key in userSettings) {
    i.settings[key] = userSettings[key];
  }

  i.containerWidth = null;
  i.containerHeight = null;
  i.contentWidth = null;
  i.contentHeight = null;

  i.isRtl = dom.css(element, 'direction') === "rtl";
  i.isNegativeScroll = (function () {
    var originalScrollLeft = element.scrollLeft;
    var result = null;
    element.scrollLeft = -1;
    result = element.scrollLeft < 0;
    element.scrollLeft = originalScrollLeft;
    return result;
  })();
  i.negativeScrollAdjustment = i.isNegativeScroll ? element.scrollWidth - element.clientWidth : 0;
  i.event = new EventManager();
  i.ownerDocument = element.ownerDocument || document;

  function focus() {
    element.classList.add('ps--focus');
  }

  function blur() {
    element.classList.remove('ps--focus');
  }

  i.scrollbarXRail = dom.appendTo(dom.create('div', 'ps__scrollbar-x-rail'), element);
  i.scrollbarX = dom.appendTo(dom.create('div', 'ps__scrollbar-x'), i.scrollbarXRail);
  i.scrollbarX.setAttribute('tabindex', 0);
  i.event.bind(i.scrollbarX, 'focus', focus);
  i.event.bind(i.scrollbarX, 'blur', blur);
  i.scrollbarXActive = null;
  i.scrollbarXWidth = null;
  i.scrollbarXLeft = null;
  i.scrollbarXBottom = _.toInt(dom.css(i.scrollbarXRail, 'bottom'));
  i.isScrollbarXUsingBottom = i.scrollbarXBottom === i.scrollbarXBottom; // !isNaN
  i.scrollbarXTop = i.isScrollbarXUsingBottom ? null : _.toInt(dom.css(i.scrollbarXRail, 'top'));
  i.railBorderXWidth = _.toInt(dom.css(i.scrollbarXRail, 'borderLeftWidth')) + _.toInt(dom.css(i.scrollbarXRail, 'borderRightWidth'));
  // Set rail to display:block to calculate margins
  dom.css(i.scrollbarXRail, 'display', 'block');
  i.railXMarginWidth = _.toInt(dom.css(i.scrollbarXRail, 'marginLeft')) + _.toInt(dom.css(i.scrollbarXRail, 'marginRight'));
  dom.css(i.scrollbarXRail, 'display', '');
  i.railXWidth = null;
  i.railXRatio = null;

  i.scrollbarYRail = dom.appendTo(dom.create('div', 'ps__scrollbar-y-rail'), element);
  i.scrollbarY = dom.appendTo(dom.create('div', 'ps__scrollbar-y'), i.scrollbarYRail);
  i.scrollbarY.setAttribute('tabindex', 0);
  i.event.bind(i.scrollbarY, 'focus', focus);
  i.event.bind(i.scrollbarY, 'blur', blur);
  i.scrollbarYActive = null;
  i.scrollbarYHeight = null;
  i.scrollbarYTop = null;
  i.scrollbarYRight = _.toInt(dom.css(i.scrollbarYRail, 'right'));
  i.isScrollbarYUsingRight = i.scrollbarYRight === i.scrollbarYRight; // !isNaN
  i.scrollbarYLeft = i.isScrollbarYUsingRight ? null : _.toInt(dom.css(i.scrollbarYRail, 'left'));
  i.scrollbarYOuterWidth = i.isRtl ? _.outerWidth(i.scrollbarY) : null;
  i.railBorderYWidth = _.toInt(dom.css(i.scrollbarYRail, 'borderTopWidth')) + _.toInt(dom.css(i.scrollbarYRail, 'borderBottomWidth'));
  dom.css(i.scrollbarYRail, 'display', 'block');
  i.railYMarginHeight = _.toInt(dom.css(i.scrollbarYRail, 'marginTop')) + _.toInt(dom.css(i.scrollbarYRail, 'marginBottom'));
  dom.css(i.scrollbarYRail, 'display', '');
  i.railYHeight = null;
  i.railYRatio = null;
}

function getId(element) {
  return element.getAttribute('data-ps-id');
}

function setId(element, id) {
  element.setAttribute('data-ps-id', id);
}

function removeId(element) {
  element.removeAttribute('data-ps-id');
}

exports.add = function (element, userSettings) {
  var newId = guid();
  setId(element, newId);
  instances[newId] = new Instance(element, userSettings);
  return instances[newId];
};

exports.remove = function (element) {
  delete instances[getId(element)];
  removeId(element);
};

exports.get = function (element) {
  return instances[getId(element)];
};

},{"4":4,"5":5,"6":6,"7":7,"9":9}],20:[function(require,module,exports){
'use strict';

var _ = require(7);
var dom = require(4);
var instances = require(19);
var updateScroll = require(21);

function getThumbSize(i, thumbSize) {
  if (i.settings.minScrollbarLength) {
    thumbSize = Math.max(thumbSize, i.settings.minScrollbarLength);
  }
  if (i.settings.maxScrollbarLength) {
    thumbSize = Math.min(thumbSize, i.settings.maxScrollbarLength);
  }
  return thumbSize;
}

function updateCss(element, i) {
  var xRailOffset = {width: i.railXWidth};
  if (i.isRtl) {
    xRailOffset.left = i.negativeScrollAdjustment + element.scrollLeft + i.containerWidth - i.contentWidth;
  } else {
    xRailOffset.left = element.scrollLeft;
  }
  if (i.isScrollbarXUsingBottom) {
    xRailOffset.bottom = i.scrollbarXBottom - element.scrollTop;
  } else {
    xRailOffset.top = i.scrollbarXTop + element.scrollTop;
  }
  dom.css(i.scrollbarXRail, xRailOffset);

  var yRailOffset = {top: element.scrollTop, height: i.railYHeight};
  if (i.isScrollbarYUsingRight) {
    if (i.isRtl) {
      yRailOffset.right = i.contentWidth - (i.negativeScrollAdjustment + element.scrollLeft) - i.scrollbarYRight - i.scrollbarYOuterWidth;
    } else {
      yRailOffset.right = i.scrollbarYRight - element.scrollLeft;
    }
  } else {
    if (i.isRtl) {
      yRailOffset.left = i.negativeScrollAdjustment + element.scrollLeft + i.containerWidth * 2 - i.contentWidth - i.scrollbarYLeft - i.scrollbarYOuterWidth;
    } else {
      yRailOffset.left = i.scrollbarYLeft + element.scrollLeft;
    }
  }
  dom.css(i.scrollbarYRail, yRailOffset);

  dom.css(i.scrollbarX, {left: i.scrollbarXLeft, width: i.scrollbarXWidth - i.railBorderXWidth});
  dom.css(i.scrollbarY, {top: i.scrollbarYTop, height: i.scrollbarYHeight - i.railBorderYWidth});
}

module.exports = function (element) {
  var i = instances.get(element);

  i.containerWidth = element.clientWidth;
  i.containerHeight = element.clientHeight;
  i.contentWidth = element.scrollWidth;
  i.contentHeight = element.scrollHeight;

  var existingRails;
  if (!element.contains(i.scrollbarXRail)) {
    existingRails = dom.queryChildren(element, '.ps__scrollbar-x-rail');
    if (existingRails.length > 0) {
      existingRails.forEach(function (rail) {
        dom.remove(rail);
      });
    }
    dom.appendTo(i.scrollbarXRail, element);
  }
  if (!element.contains(i.scrollbarYRail)) {
    existingRails = dom.queryChildren(element, '.ps__scrollbar-y-rail');
    if (existingRails.length > 0) {
      existingRails.forEach(function (rail) {
        dom.remove(rail);
      });
    }
    dom.appendTo(i.scrollbarYRail, element);
  }

  if (!i.settings.suppressScrollX && i.containerWidth + i.settings.scrollXMarginOffset < i.contentWidth) {
    i.scrollbarXActive = true;
    i.railXWidth = i.containerWidth - i.railXMarginWidth;
    i.railXRatio = i.containerWidth / i.railXWidth;
    i.scrollbarXWidth = getThumbSize(i, _.toInt(i.railXWidth * i.containerWidth / i.contentWidth));
    i.scrollbarXLeft = _.toInt((i.negativeScrollAdjustment + element.scrollLeft) * (i.railXWidth - i.scrollbarXWidth) / (i.contentWidth - i.containerWidth));
  } else {
    i.scrollbarXActive = false;
  }

  if (!i.settings.suppressScrollY && i.containerHeight + i.settings.scrollYMarginOffset < i.contentHeight) {
    i.scrollbarYActive = true;
    i.railYHeight = i.containerHeight - i.railYMarginHeight;
    i.railYRatio = i.containerHeight / i.railYHeight;
    i.scrollbarYHeight = getThumbSize(i, _.toInt(i.railYHeight * i.containerHeight / i.contentHeight));
    i.scrollbarYTop = _.toInt(element.scrollTop * (i.railYHeight - i.scrollbarYHeight) / (i.contentHeight - i.containerHeight));
  } else {
    i.scrollbarYActive = false;
  }

  if (i.scrollbarXLeft >= i.railXWidth - i.scrollbarXWidth) {
    i.scrollbarXLeft = i.railXWidth - i.scrollbarXWidth;
  }
  if (i.scrollbarYTop >= i.railYHeight - i.scrollbarYHeight) {
    i.scrollbarYTop = i.railYHeight - i.scrollbarYHeight;
  }

  updateCss(element, i);

  if (i.scrollbarXActive) {
    element.classList.add('ps--active-x');
  } else {
    element.classList.remove('ps--active-x');
    i.scrollbarXWidth = 0;
    i.scrollbarXLeft = 0;
    updateScroll(element, 'left', 0);
  }
  if (i.scrollbarYActive) {
    element.classList.add('ps--active-y');
  } else {
    element.classList.remove('ps--active-y');
    i.scrollbarYHeight = 0;
    i.scrollbarYTop = 0;
    updateScroll(element, 'top', 0);
  }
};

},{"19":19,"21":21,"4":4,"7":7}],21:[function(require,module,exports){
'use strict';

var instances = require(19);

var createDOMEvent = function (name) {
  var event = document.createEvent("Event");
  event.initEvent(name, true, true);
  return event;
};

module.exports = function (element, axis, value) {
  if (typeof element === 'undefined') {
    throw 'You must provide an element to the update-scroll function';
  }

  if (typeof axis === 'undefined') {
    throw 'You must provide an axis to the update-scroll function';
  }

  if (typeof value === 'undefined') {
    throw 'You must provide a value to the update-scroll function';
  }

  if (axis === 'top' && value <= 0) {
    element.scrollTop = value = 0; // don't allow negative scroll
    element.dispatchEvent(createDOMEvent('ps-y-reach-start'));
  }

  if (axis === 'left' && value <= 0) {
    element.scrollLeft = value = 0; // don't allow negative scroll
    element.dispatchEvent(createDOMEvent('ps-x-reach-start'));
  }

  var i = instances.get(element);

  if (axis === 'top' && value >= i.contentHeight - i.containerHeight) {
    // don't allow scroll past container
    value = i.contentHeight - i.containerHeight;
    if (value - element.scrollTop <= 2) {
      // mitigates rounding errors on non-subpixel scroll values
      value = element.scrollTop;
    } else {
      element.scrollTop = value;
    }
    element.dispatchEvent(createDOMEvent('ps-y-reach-end'));
  }

  if (axis === 'left' && value >= i.contentWidth - i.containerWidth) {
    // don't allow scroll past container
    value = i.contentWidth - i.containerWidth;
    if (value - element.scrollLeft <= 2) {
      // mitigates rounding errors on non-subpixel scroll values
      value = element.scrollLeft;
    } else {
      element.scrollLeft = value;
    }
    element.dispatchEvent(createDOMEvent('ps-x-reach-end'));
  }

  if (i.lastTop === undefined) {
    i.lastTop = element.scrollTop;
  }

  if (i.lastLeft === undefined) {
    i.lastLeft = element.scrollLeft;
  }

  if (axis === 'top' && value < i.lastTop) {
    element.dispatchEvent(createDOMEvent('ps-scroll-up'));
  }

  if (axis === 'top' && value > i.lastTop) {
    element.dispatchEvent(createDOMEvent('ps-scroll-down'));
  }

  if (axis === 'left' && value < i.lastLeft) {
    element.dispatchEvent(createDOMEvent('ps-scroll-left'));
  }

  if (axis === 'left' && value > i.lastLeft) {
    element.dispatchEvent(createDOMEvent('ps-scroll-right'));
  }

  if (axis === 'top' && value !== i.lastTop) {
    element.scrollTop = i.lastTop = value;
    element.dispatchEvent(createDOMEvent('ps-scroll-y'));
  }

  if (axis === 'left' && value !== i.lastLeft) {
    element.scrollLeft = i.lastLeft = value;
    element.dispatchEvent(createDOMEvent('ps-scroll-x'));
  }

};

},{"19":19}],22:[function(require,module,exports){
'use strict';

var _ = require(7);
var dom = require(4);
var instances = require(19);
var updateGeometry = require(20);
var updateScroll = require(21);

module.exports = function (element) {
  var i = instances.get(element);

  if (!i) {
    return;
  }

  // Recalcuate negative scrollLeft adjustment
  i.negativeScrollAdjustment = i.isNegativeScroll ? element.scrollWidth - element.clientWidth : 0;

  // Recalculate rail margins
  dom.css(i.scrollbarXRail, 'display', 'block');
  dom.css(i.scrollbarYRail, 'display', 'block');
  i.railXMarginWidth = _.toInt(dom.css(i.scrollbarXRail, 'marginLeft')) + _.toInt(dom.css(i.scrollbarXRail, 'marginRight'));
  i.railYMarginHeight = _.toInt(dom.css(i.scrollbarYRail, 'marginTop')) + _.toInt(dom.css(i.scrollbarYRail, 'marginBottom'));

  // Hide scrollbars not to affect scrollWidth and scrollHeight
  dom.css(i.scrollbarXRail, 'display', 'none');
  dom.css(i.scrollbarYRail, 'display', 'none');

  updateGeometry(element);

  // Update top/left scroll to trigger events
  updateScroll(element, 'top', element.scrollTop);
  updateScroll(element, 'left', element.scrollLeft);

  dom.css(i.scrollbarXRail, 'display', '');
  dom.css(i.scrollbarYRail, 'display', '');
};

},{"19":19,"20":20,"21":21,"4":4,"7":7}],23:[function(require,module,exports){
(function (global){
/*!
 * Select2 4.0.6-rc.1
 * https://select2.github.io
 *
 * Released under the MIT license
 * https://github.com/select2/select2/blob/master/LICENSE.md
 */
;(function (factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['jquery'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // Node/CommonJS
    module.exports = function (root, jQuery) {
      if (jQuery === undefined) {
        // require('jQuery') returns a factory that requires window to
        // build a jQuery instance, we normalize how we use modules
        // that require this pattern but the window provided is a noop
        // if it's defined (how jquery works)
        if (typeof window !== 'undefined') {
          jQuery = (typeof window !== "undefined" ? window['$'] : typeof global !== "undefined" ? global['$'] : null);
        }
        else {
          jQuery = (typeof window !== "undefined" ? window['$'] : typeof global !== "undefined" ? global['$'] : null)(root);
        }
      }
      factory(jQuery);
      return jQuery;
    };
  } else {
    // Browser globals
    factory(jQuery);
  }
} (function (jQuery) {
  // This is needed so we can catch the AMD loader configuration and use it
  // The inner file should be wrapped (by `banner.start.js`) in a function that
  // returns the AMD loader references.
  var S2 =(function () {
  // Restore the Select2 AMD loader so it can be used
  // Needed mostly in the language files, where the loader is not inserted
  if (jQuery && jQuery.fn && jQuery.fn.select2 && jQuery.fn.select2.amd) {
    var S2 = jQuery.fn.select2.amd;
  }
var S2;(function () { if (!S2 || !S2.requirejs) {
if (!S2) { S2 = {}; } else { require = S2; }
/**
 * @license almond 0.3.3 Copyright jQuery Foundation and other contributors.
 * Released under MIT license, http://github.com/requirejs/almond/LICENSE
 */
//Going sloppy to avoid 'use strict' string cost, but strict practices should
//be followed.
/*global setTimeout: false */

var requirejs, require, define;
(function (undef) {
    var main, req, makeMap, handlers,
        defined = {},
        waiting = {},
        config = {},
        defining = {},
        hasOwn = Object.prototype.hasOwnProperty,
        aps = [].slice,
        jsSuffixRegExp = /\.js$/;

    function hasProp(obj, prop) {
        return hasOwn.call(obj, prop);
    }

    /**
     * Given a relative module name, like ./something, normalize it to
     * a real name that can be mapped to a path.
     * @param {String} name the relative name
     * @param {String} baseName a real name that the name arg is relative
     * to.
     * @returns {String} normalized name
     */
    function normalize(name, baseName) {
        var nameParts, nameSegment, mapValue, foundMap, lastIndex,
            foundI, foundStarMap, starI, i, j, part, normalizedBaseParts,
            baseParts = baseName && baseName.split("/"),
            map = config.map,
            starMap = (map && map['*']) || {};

        //Adjust any relative paths.
        if (name) {
            name = name.split('/');
            lastIndex = name.length - 1;

            // If wanting node ID compatibility, strip .js from end
            // of IDs. Have to do this here, and not in nameToUrl
            // because node allows either .js or non .js to map
            // to same file.
            if (config.nodeIdCompat && jsSuffixRegExp.test(name[lastIndex])) {
                name[lastIndex] = name[lastIndex].replace(jsSuffixRegExp, '');
            }

            // Starts with a '.' so need the baseName
            if (name[0].charAt(0) === '.' && baseParts) {
                //Convert baseName to array, and lop off the last part,
                //so that . matches that 'directory' and not name of the baseName's
                //module. For instance, baseName of 'one/two/three', maps to
                //'one/two/three.js', but we want the directory, 'one/two' for
                //this normalization.
                normalizedBaseParts = baseParts.slice(0, baseParts.length - 1);
                name = normalizedBaseParts.concat(name);
            }

            //start trimDots
            for (i = 0; i < name.length; i++) {
                part = name[i];
                if (part === '.') {
                    name.splice(i, 1);
                    i -= 1;
                } else if (part === '..') {
                    // If at the start, or previous value is still ..,
                    // keep them so that when converted to a path it may
                    // still work when converted to a path, even though
                    // as an ID it is less than ideal. In larger point
                    // releases, may be better to just kick out an error.
                    if (i === 0 || (i === 1 && name[2] === '..') || name[i - 1] === '..') {
                        continue;
                    } else if (i > 0) {
                        name.splice(i - 1, 2);
                        i -= 2;
                    }
                }
            }
            //end trimDots

            name = name.join('/');
        }

        //Apply map config if available.
        if ((baseParts || starMap) && map) {
            nameParts = name.split('/');

            for (i = nameParts.length; i > 0; i -= 1) {
                nameSegment = nameParts.slice(0, i).join("/");

                if (baseParts) {
                    //Find the longest baseName segment match in the config.
                    //So, do joins on the biggest to smallest lengths of baseParts.
                    for (j = baseParts.length; j > 0; j -= 1) {
                        mapValue = map[baseParts.slice(0, j).join('/')];

                        //baseName segment has  config, find if it has one for
                        //this name.
                        if (mapValue) {
                            mapValue = mapValue[nameSegment];
                            if (mapValue) {
                                //Match, update name to the new value.
                                foundMap = mapValue;
                                foundI = i;
                                break;
                            }
                        }
                    }
                }

                if (foundMap) {
                    break;
                }

                //Check for a star map match, but just hold on to it,
                //if there is a shorter segment match later in a matching
                //config, then favor over this star map.
                if (!foundStarMap && starMap && starMap[nameSegment]) {
                    foundStarMap = starMap[nameSegment];
                    starI = i;
                }
            }

            if (!foundMap && foundStarMap) {
                foundMap = foundStarMap;
                foundI = starI;
            }

            if (foundMap) {
                nameParts.splice(0, foundI, foundMap);
                name = nameParts.join('/');
            }
        }

        return name;
    }

    function makeRequire(relName, forceSync) {
        return function () {
            //A version of a require function that passes a moduleName
            //value for items that may need to
            //look up paths relative to the moduleName
            var args = aps.call(arguments, 0);

            //If first arg is not require('string'), and there is only
            //one arg, it is the array form without a callback. Insert
            //a null so that the following concat is correct.
            if (typeof args[0] !== 'string' && args.length === 1) {
                args.push(null);
            }
            return req.apply(undef, args.concat([relName, forceSync]));
        };
    }

    function makeNormalize(relName) {
        return function (name) {
            return normalize(name, relName);
        };
    }

    function makeLoad(depName) {
        return function (value) {
            defined[depName] = value;
        };
    }

    function callDep(name) {
        if (hasProp(waiting, name)) {
            var args = waiting[name];
            delete waiting[name];
            defining[name] = true;
            main.apply(undef, args);
        }

        if (!hasProp(defined, name) && !hasProp(defining, name)) {
            throw new Error('No ' + name);
        }
        return defined[name];
    }

    //Turns a plugin!resource to [plugin, resource]
    //with the plugin being undefined if the name
    //did not have a plugin prefix.
    function splitPrefix(name) {
        var prefix,
            index = name ? name.indexOf('!') : -1;
        if (index > -1) {
            prefix = name.substring(0, index);
            name = name.substring(index + 1, name.length);
        }
        return [prefix, name];
    }

    //Creates a parts array for a relName where first part is plugin ID,
    //second part is resource ID. Assumes relName has already been normalized.
    function makeRelParts(relName) {
        return relName ? splitPrefix(relName) : [];
    }

    /**
     * Makes a name map, normalizing the name, and using a plugin
     * for normalization if necessary. Grabs a ref to plugin
     * too, as an optimization.
     */
    makeMap = function (name, relParts) {
        var plugin,
            parts = splitPrefix(name),
            prefix = parts[0],
            relResourceName = relParts[1];

        name = parts[1];

        if (prefix) {
            prefix = normalize(prefix, relResourceName);
            plugin = callDep(prefix);
        }

        //Normalize according
        if (prefix) {
            if (plugin && plugin.normalize) {
                name = plugin.normalize(name, makeNormalize(relResourceName));
            } else {
                name = normalize(name, relResourceName);
            }
        } else {
            name = normalize(name, relResourceName);
            parts = splitPrefix(name);
            prefix = parts[0];
            name = parts[1];
            if (prefix) {
                plugin = callDep(prefix);
            }
        }

        //Using ridiculous property names for space reasons
        return {
            f: prefix ? prefix + '!' + name : name, //fullName
            n: name,
            pr: prefix,
            p: plugin
        };
    };

    function makeConfig(name) {
        return function () {
            return (config && config.config && config.config[name]) || {};
        };
    }

    handlers = {
        require: function (name) {
            return makeRequire(name);
        },
        exports: function (name) {
            var e = defined[name];
            if (typeof e !== 'undefined') {
                return e;
            } else {
                return (defined[name] = {});
            }
        },
        module: function (name) {
            return {
                id: name,
                uri: '',
                exports: defined[name],
                config: makeConfig(name)
            };
        }
    };

    main = function (name, deps, callback, relName) {
        var cjsModule, depName, ret, map, i, relParts,
            args = [],
            callbackType = typeof callback,
            usingExports;

        //Use name if no relName
        relName = relName || name;
        relParts = makeRelParts(relName);

        //Call the callback to define the module, if necessary.
        if (callbackType === 'undefined' || callbackType === 'function') {
            //Pull out the defined dependencies and pass the ordered
            //values to the callback.
            //Default to [require, exports, module] if no deps
            deps = !deps.length && callback.length ? ['require', 'exports', 'module'] : deps;
            for (i = 0; i < deps.length; i += 1) {
                map = makeMap(deps[i], relParts);
                depName = map.f;

                //Fast path CommonJS standard dependencies.
                if (depName === "require") {
                    args[i] = handlers.require(name);
                } else if (depName === "exports") {
                    //CommonJS module spec 1.1
                    args[i] = handlers.exports(name);
                    usingExports = true;
                } else if (depName === "module") {
                    //CommonJS module spec 1.1
                    cjsModule = args[i] = handlers.module(name);
                } else if (hasProp(defined, depName) ||
                           hasProp(waiting, depName) ||
                           hasProp(defining, depName)) {
                    args[i] = callDep(depName);
                } else if (map.p) {
                    map.p.load(map.n, makeRequire(relName, true), makeLoad(depName), {});
                    args[i] = defined[depName];
                } else {
                    throw new Error(name + ' missing ' + depName);
                }
            }

            ret = callback ? callback.apply(defined[name], args) : undefined;

            if (name) {
                //If setting exports via "module" is in play,
                //favor that over return value and exports. After that,
                //favor a non-undefined return value over exports use.
                if (cjsModule && cjsModule.exports !== undef &&
                        cjsModule.exports !== defined[name]) {
                    defined[name] = cjsModule.exports;
                } else if (ret !== undef || !usingExports) {
                    //Use the return value from the function.
                    defined[name] = ret;
                }
            }
        } else if (name) {
            //May just be an object definition for the module. Only
            //worry about defining if have a module name.
            defined[name] = callback;
        }
    };

    requirejs = require = req = function (deps, callback, relName, forceSync, alt) {
        if (typeof deps === "string") {
            if (handlers[deps]) {
                //callback in this case is really relName
                return handlers[deps](callback);
            }
            //Just return the module wanted. In this scenario, the
            //deps arg is the module name, and second arg (if passed)
            //is just the relName.
            //Normalize module name, if it contains . or ..
            return callDep(makeMap(deps, makeRelParts(callback)).f);
        } else if (!deps.splice) {
            //deps is a config object, not an array.
            config = deps;
            if (config.deps) {
                req(config.deps, config.callback);
            }
            if (!callback) {
                return;
            }

            if (callback.splice) {
                //callback is an array, which means it is a dependency list.
                //Adjust args if there are dependencies
                deps = callback;
                callback = relName;
                relName = null;
            } else {
                deps = undef;
            }
        }

        //Support require(['a'])
        callback = callback || function () {};

        //If relName is a function, it is an errback handler,
        //so remove it.
        if (typeof relName === 'function') {
            relName = forceSync;
            forceSync = alt;
        }

        //Simulate async callback;
        if (forceSync) {
            main(undef, deps, callback, relName);
        } else {
            //Using a non-zero value because of concern for what old browsers
            //do, and latest browsers "upgrade" to 4 if lower value is used:
            //http://www.whatwg.org/specs/web-apps/current-work/multipage/timers.html#dom-windowtimers-settimeout:
            //If want a value immediately, use require('id') instead -- something
            //that works in almond on the global level, but not guaranteed and
            //unlikely to work in other AMD implementations.
            setTimeout(function () {
                main(undef, deps, callback, relName);
            }, 4);
        }

        return req;
    };

    /**
     * Just drops the config on the floor, but returns req in case
     * the config return value is used.
     */
    req.config = function (cfg) {
        return req(cfg);
    };

    /**
     * Expose module registry for debugging and tooling
     */
    requirejs._defined = defined;

    define = function (name, deps, callback) {
        if (typeof name !== 'string') {
            throw new Error('See almond README: incorrect module build, no module name');
        }

        //This module may not have dependencies
        if (!deps.splice) {
            //deps is not an array, so probably means
            //an object literal or factory function for
            //the value. Adjust args.
            callback = deps;
            deps = [];
        }

        if (!hasProp(defined, name) && !hasProp(waiting, name)) {
            waiting[name] = [name, deps, callback];
        }
    };

    define.amd = {
        jQuery: true
    };
}());

S2.requirejs = requirejs;S2.require = require;S2.define = define;
}
}());
S2.define("almond", function(){});

/* global jQuery:false, $:false */
S2.define('jquery',[],function () {
  var _$ = jQuery || $;

  if (_$ == null && console && console.error) {
    console.error(
      'Select2: An instance of jQuery or a jQuery-compatible library was not ' +
      'found. Make sure that you are including jQuery before Select2 on your ' +
      'web page.'
    );
  }

  return _$;
});

S2.define('select2/utils',[
  'jquery'
], function ($) {
  var Utils = {};

  Utils.Extend = function (ChildClass, SuperClass) {
    var __hasProp = {}.hasOwnProperty;

    function BaseConstructor () {
      this.constructor = ChildClass;
    }

    for (var key in SuperClass) {
      if (__hasProp.call(SuperClass, key)) {
        ChildClass[key] = SuperClass[key];
      }
    }

    BaseConstructor.prototype = SuperClass.prototype;
    ChildClass.prototype = new BaseConstructor();
    ChildClass.__super__ = SuperClass.prototype;

    return ChildClass;
  };

  function getMethods (theClass) {
    var proto = theClass.prototype;

    var methods = [];

    for (var methodName in proto) {
      var m = proto[methodName];

      if (typeof m !== 'function') {
        continue;
      }

      if (methodName === 'constructor') {
        continue;
      }

      methods.push(methodName);
    }

    return methods;
  }

  Utils.Decorate = function (SuperClass, DecoratorClass) {
    var decoratedMethods = getMethods(DecoratorClass);
    var superMethods = getMethods(SuperClass);

    function DecoratedClass () {
      var unshift = Array.prototype.unshift;

      var argCount = DecoratorClass.prototype.constructor.length;

      var calledConstructor = SuperClass.prototype.constructor;

      if (argCount > 0) {
        unshift.call(arguments, SuperClass.prototype.constructor);

        calledConstructor = DecoratorClass.prototype.constructor;
      }

      calledConstructor.apply(this, arguments);
    }

    DecoratorClass.displayName = SuperClass.displayName;

    function ctr () {
      this.constructor = DecoratedClass;
    }

    DecoratedClass.prototype = new ctr();

    for (var m = 0; m < superMethods.length; m++) {
      var superMethod = superMethods[m];

      DecoratedClass.prototype[superMethod] =
        SuperClass.prototype[superMethod];
    }

    var calledMethod = function (methodName) {
      // Stub out the original method if it's not decorating an actual method
      var originalMethod = function () {};

      if (methodName in DecoratedClass.prototype) {
        originalMethod = DecoratedClass.prototype[methodName];
      }

      var decoratedMethod = DecoratorClass.prototype[methodName];

      return function () {
        var unshift = Array.prototype.unshift;

        unshift.call(arguments, originalMethod);

        return decoratedMethod.apply(this, arguments);
      };
    };

    for (var d = 0; d < decoratedMethods.length; d++) {
      var decoratedMethod = decoratedMethods[d];

      DecoratedClass.prototype[decoratedMethod] = calledMethod(decoratedMethod);
    }

    return DecoratedClass;
  };

  var Observable = function () {
    this.listeners = {};
  };

  Observable.prototype.on = function (event, callback) {
    this.listeners = this.listeners || {};

    if (event in this.listeners) {
      this.listeners[event].push(callback);
    } else {
      this.listeners[event] = [callback];
    }
  };

  Observable.prototype.trigger = function (event) {
    var slice = Array.prototype.slice;
    var params = slice.call(arguments, 1);

    this.listeners = this.listeners || {};

    // Params should always come in as an array
    if (params == null) {
      params = [];
    }

    // If there are no arguments to the event, use a temporary object
    if (params.length === 0) {
      params.push({});
    }

    // Set the `_type` of the first object to the event
    params[0]._type = event;

    if (event in this.listeners) {
      this.invoke(this.listeners[event], slice.call(arguments, 1));
    }

    if ('*' in this.listeners) {
      this.invoke(this.listeners['*'], arguments);
    }
  };

  Observable.prototype.invoke = function (listeners, params) {
    for (var i = 0, len = listeners.length; i < len; i++) {
      listeners[i].apply(this, params);
    }
  };

  Utils.Observable = Observable;

  Utils.generateChars = function (length) {
    var chars = '';

    for (var i = 0; i < length; i++) {
      var randomChar = Math.floor(Math.random() * 36);
      chars += randomChar.toString(36);
    }

    return chars;
  };

  Utils.bind = function (func, context) {
    return function () {
      func.apply(context, arguments);
    };
  };

  Utils._convertData = function (data) {
    for (var originalKey in data) {
      var keys = originalKey.split('-');

      var dataLevel = data;

      if (keys.length === 1) {
        continue;
      }

      for (var k = 0; k < keys.length; k++) {
        var key = keys[k];

        // Lowercase the first letter
        // By default, dash-separated becomes camelCase
        key = key.substring(0, 1).toLowerCase() + key.substring(1);

        if (!(key in dataLevel)) {
          dataLevel[key] = {};
        }

        if (k == keys.length - 1) {
          dataLevel[key] = data[originalKey];
        }

        dataLevel = dataLevel[key];
      }

      delete data[originalKey];
    }

    return data;
  };

  Utils.hasScroll = function (index, el) {
    // Adapted from the function created by @ShadowScripter
    // and adapted by @BillBarry on the Stack Exchange Code Review website.
    // The original code can be found at
    // http://codereview.stackexchange.com/q/13338
    // and was designed to be used with the Sizzle selector engine.

    var $el = $(el);
    var overflowX = el.style.overflowX;
    var overflowY = el.style.overflowY;

    //Check both x and y declarations
    if (overflowX === overflowY &&
        (overflowY === 'hidden' || overflowY === 'visible')) {
      return false;
    }

    if (overflowX === 'scroll' || overflowY === 'scroll') {
      return true;
    }

    return ($el.innerHeight() < el.scrollHeight ||
      $el.innerWidth() < el.scrollWidth);
  };

  Utils.escapeMarkup = function (markup) {
    var replaceMap = {
      '\\': '&#92;',
      '&': '&amp;',
      '<': '&lt;',
      '>': '&gt;',
      '"': '&quot;',
      '\'': '&#39;',
      '/': '&#47;'
    };

    // Do not try to escape the markup if it's not a string
    if (typeof markup !== 'string') {
      return markup;
    }

    return String(markup).replace(/[&<>"'\/\\]/g, function (match) {
      return replaceMap[match];
    });
  };

  // Append an array of jQuery nodes to a given element.
  Utils.appendMany = function ($element, $nodes) {
    // jQuery 1.7.x does not support $.fn.append() with an array
    // Fall back to a jQuery object collection using $.fn.add()
    if ($.fn.jquery.substr(0, 3) === '1.7') {
      var $jqNodes = $();

      $.map($nodes, function (node) {
        $jqNodes = $jqNodes.add(node);
      });

      $nodes = $jqNodes;
    }

    $element.append($nodes);
  };

  // Cache objects in Utils.__cache instead of $.data (see #4346)
  Utils.__cache = {};

  var id = 0;
  Utils.GetUniqueElementId = function (element) {
    // Get a unique element Id. If element has no id, 
    // creates a new unique number, stores it in the id 
    // attribute and returns the new id. 
    // If an id already exists, it simply returns it.

    var select2Id = element.getAttribute('data-select2-id');
    if (select2Id == null) {
      // If element has id, use it.
      if (element.id) {
        select2Id = element.id;
        element.setAttribute('data-select2-id', select2Id);
      } else {
        element.setAttribute('data-select2-id', ++id);
        select2Id = id.toString();
      }
    }
    return select2Id;
  };

  Utils.StoreData = function (element, name, value) {
    // Stores an item in the cache for a specified element.
    // name is the cache key.    
    var id = Utils.GetUniqueElementId(element);
    if (!Utils.__cache[id]) {
      Utils.__cache[id] = {};
    }

    Utils.__cache[id][name] = value;
  };

  Utils.GetData = function (element, name) {
    // Retrieves a value from the cache by its key (name)
    // name is optional. If no name specified, return 
    // all cache items for the specified element.
    // and for a specified element.
    var id = Utils.GetUniqueElementId(element);
    if (name) {
      if (Utils.__cache[id]) {
        return Utils.__cache[id][name] != null ? 
	      Utils.__cache[id][name]:
	      $(element).data(name); // Fallback to HTML5 data attribs.
      }
      return $(element).data(name); // Fallback to HTML5 data attribs.
    } else {
      return Utils.__cache[id];			   
    }
  };

  Utils.RemoveData = function (element) {
    // Removes all cached items for a specified element.
    var id = Utils.GetUniqueElementId(element);
    if (Utils.__cache[id] != null) {
      delete Utils.__cache[id];
    }
  };

  return Utils;
});

S2.define('select2/results',[
  'jquery',
  './utils'
], function ($, Utils) {
  function Results ($element, options, dataAdapter) {
    this.$element = $element;
    this.data = dataAdapter;
    this.options = options;

    Results.__super__.constructor.call(this);
  }

  Utils.Extend(Results, Utils.Observable);

  Results.prototype.render = function () {
    var $results = $(
      '<ul class="select2-results__options" role="tree"></ul>'
    );

    if (this.options.get('multiple')) {
      $results.attr('aria-multiselectable', 'true');
    }

    this.$results = $results;

    return $results;
  };

  Results.prototype.clear = function () {
    this.$results.empty();
  };

  Results.prototype.displayMessage = function (params) {
    var escapeMarkup = this.options.get('escapeMarkup');

    this.clear();
    this.hideLoading();

    var $message = $(
      '<li role="treeitem" aria-live="assertive"' +
      ' class="select2-results__option"></li>'
    );

    var message = this.options.get('translations').get(params.message);

    $message.append(
      escapeMarkup(
        message(params.args)
      )
    );

    $message[0].className += ' select2-results__message';

    this.$results.append($message);
  };

  Results.prototype.hideMessages = function () {
    this.$results.find('.select2-results__message').remove();
  };

  Results.prototype.append = function (data) {
    this.hideLoading();

    var $options = [];

    if (data.results == null || data.results.length === 0) {
      if (this.$results.children().length === 0) {
        this.trigger('results:message', {
          message: 'noResults'
        });
      }

      return;
    }

    data.results = this.sort(data.results);

    for (var d = 0; d < data.results.length; d++) {
      var item = data.results[d];

      var $option = this.option(item);

      $options.push($option);
    }

    this.$results.append($options);
  };

  Results.prototype.position = function ($results, $dropdown) {
    var $resultsContainer = $dropdown.find('.select2-results');
    $resultsContainer.append($results);
  };

  Results.prototype.sort = function (data) {
    var sorter = this.options.get('sorter');

    return sorter(data);
  };

  Results.prototype.highlightFirstItem = function () {
    var $options = this.$results
      .find('.select2-results__option[aria-selected]');

    var $selected = $options.filter('[aria-selected=true]');

    // Check if there are any selected options
    if ($selected.length > 0) {
      // If there are selected options, highlight the first
      $selected.first().trigger('mouseenter');
    } else {
      // If there are no selected options, highlight the first option
      // in the dropdown
      $options.first().trigger('mouseenter');
    }

    this.ensureHighlightVisible();
  };

  Results.prototype.setClasses = function () {
    var self = this;

    this.data.current(function (selected) {
      var selectedIds = $.map(selected, function (s) {
        return s.id.toString();
      });

      var $options = self.$results
        .find('.select2-results__option[aria-selected]');

      $options.each(function () {
        var $option = $(this);

        var item = Utils.GetData(this, 'data');

        // id needs to be converted to a string when comparing
        var id = '' + item.id;

        if ((item.element != null && item.element.selected) ||
            (item.element == null && $.inArray(id, selectedIds) > -1)) {
          $option.attr('aria-selected', 'true');
        } else {
          $option.attr('aria-selected', 'false');
        }
      });

    });
  };

  Results.prototype.showLoading = function (params) {
    this.hideLoading();

    var loadingMore = this.options.get('translations').get('searching');

    var loading = {
      disabled: true,
      loading: true,
      text: loadingMore(params)
    };
    var $loading = this.option(loading);
    $loading.className += ' loading-results';

    this.$results.prepend($loading);
  };

  Results.prototype.hideLoading = function () {
    this.$results.find('.loading-results').remove();
  };

  Results.prototype.option = function (data) {
    var option = document.createElement('li');
    option.className = 'select2-results__option';

    var attrs = {
      'role': 'treeitem',
      'aria-selected': 'false'
    };

    if (data.disabled) {
      delete attrs['aria-selected'];
      attrs['aria-disabled'] = 'true';
    }

    if (data.id == null) {
      delete attrs['aria-selected'];
    }

    if (data._resultId != null) {
      option.id = data._resultId;
    }

    if (data.title) {
      option.title = data.title;
    }

    if (data.children) {
      attrs.role = 'group';
      attrs['aria-label'] = data.text;
      delete attrs['aria-selected'];
    }

    for (var attr in attrs) {
      var val = attrs[attr];

      option.setAttribute(attr, val);
    }

    if (data.children) {
      var $option = $(option);

      var label = document.createElement('strong');
      label.className = 'select2-results__group';

      var $label = $(label);
      this.template(data, label);

      var $children = [];

      for (var c = 0; c < data.children.length; c++) {
        var child = data.children[c];

        var $child = this.option(child);

        $children.push($child);
      }

      var $childrenContainer = $('<ul></ul>', {
        'class': 'select2-results__options select2-results__options--nested'
      });

      $childrenContainer.append($children);

      $option.append(label);
      $option.append($childrenContainer);
    } else {
      this.template(data, option);
    }

    Utils.StoreData(option, 'data', data);

    return option;
  };

  Results.prototype.bind = function (container, $container) {
    var self = this;

    var id = container.id + '-results';

    this.$results.attr('id', id);

    container.on('results:all', function (params) {
      self.clear();
      self.append(params.data);

      if (container.isOpen()) {
        self.setClasses();
        self.highlightFirstItem();
      }
    });

    container.on('results:append', function (params) {
      self.append(params.data);

      if (container.isOpen()) {
        self.setClasses();
      }
    });

    container.on('query', function (params) {
      self.hideMessages();
      self.showLoading(params);
    });

    container.on('select', function () {
      if (!container.isOpen()) {
        return;
      }

      self.setClasses();
      self.highlightFirstItem();
    });

    container.on('unselect', function () {
      if (!container.isOpen()) {
        return;
      }

      self.setClasses();
      self.highlightFirstItem();
    });

    container.on('open', function () {
      // When the dropdown is open, aria-expended="true"
      self.$results.attr('aria-expanded', 'true');
      self.$results.attr('aria-hidden', 'false');

      self.setClasses();
      self.ensureHighlightVisible();
    });

    container.on('close', function () {
      // When the dropdown is closed, aria-expended="false"
      self.$results.attr('aria-expanded', 'false');
      self.$results.attr('aria-hidden', 'true');
      self.$results.removeAttr('aria-activedescendant');
    });

    container.on('results:toggle', function () {
      var $highlighted = self.getHighlightedResults();

      if ($highlighted.length === 0) {
        return;
      }

      $highlighted.trigger('mouseup');
    });

    container.on('results:select', function () {
      var $highlighted = self.getHighlightedResults();

      if ($highlighted.length === 0) {
        return;
      }

      var data = Utils.GetData($highlighted[0], 'data');

      if ($highlighted.attr('aria-selected') == 'true') {
        self.trigger('close', {});
      } else {
        self.trigger('select', {
          data: data
        });
      }
    });

    container.on('results:previous', function () {
      var $highlighted = self.getHighlightedResults();

      var $options = self.$results.find('[aria-selected]');

      var currentIndex = $options.index($highlighted);

      // If we are already at te top, don't move further
      // If no options, currentIndex will be -1
      if (currentIndex <= 0) {
        return;
      }

      var nextIndex = currentIndex - 1;

      // If none are highlighted, highlight the first
      if ($highlighted.length === 0) {
        nextIndex = 0;
      }

      var $next = $options.eq(nextIndex);

      $next.trigger('mouseenter');

      var currentOffset = self.$results.offset().top;
      var nextTop = $next.offset().top;
      var nextOffset = self.$results.scrollTop() + (nextTop - currentOffset);

      if (nextIndex === 0) {
        self.$results.scrollTop(0);
      } else if (nextTop - currentOffset < 0) {
        self.$results.scrollTop(nextOffset);
      }
    });

    container.on('results:next', function () {
      var $highlighted = self.getHighlightedResults();

      var $options = self.$results.find('[aria-selected]');

      var currentIndex = $options.index($highlighted);

      var nextIndex = currentIndex + 1;

      // If we are at the last option, stay there
      if (nextIndex >= $options.length) {
        return;
      }

      var $next = $options.eq(nextIndex);

      $next.trigger('mouseenter');

      var currentOffset = self.$results.offset().top +
        self.$results.outerHeight(false);
      var nextBottom = $next.offset().top + $next.outerHeight(false);
      var nextOffset = self.$results.scrollTop() + nextBottom - currentOffset;

      if (nextIndex === 0) {
        self.$results.scrollTop(0);
      } else if (nextBottom > currentOffset) {
        self.$results.scrollTop(nextOffset);
      }
    });

    container.on('results:focus', function (params) {
      params.element.addClass('select2-results__option--highlighted');
    });

    container.on('results:message', function (params) {
      self.displayMessage(params);
    });

    if ($.fn.mousewheel) {
      this.$results.on('mousewheel', function (e) {
        var top = self.$results.scrollTop();

        var bottom = self.$results.get(0).scrollHeight - top + e.deltaY;

        var isAtTop = e.deltaY > 0 && top - e.deltaY <= 0;
        var isAtBottom = e.deltaY < 0 && bottom <= self.$results.height();

        if (isAtTop) {
          self.$results.scrollTop(0);

          e.preventDefault();
          e.stopPropagation();
        } else if (isAtBottom) {
          self.$results.scrollTop(
            self.$results.get(0).scrollHeight - self.$results.height()
          );

          e.preventDefault();
          e.stopPropagation();
        }
      });
    }

    this.$results.on('mouseup', '.select2-results__option[aria-selected]',
      function (evt) {
      var $this = $(this);

      var data = Utils.GetData(this, 'data');

      if ($this.attr('aria-selected') === 'true') {
        if (self.options.get('multiple')) {
          self.trigger('unselect', {
            originalEvent: evt,
            data: data
          });
        } else {
          self.trigger('close', {});
        }

        return;
      }

      self.trigger('select', {
        originalEvent: evt,
        data: data
      });
    });

    this.$results.on('mouseenter', '.select2-results__option[aria-selected]',
      function (evt) {
      var data = Utils.GetData(this, 'data');

      self.getHighlightedResults()
          .removeClass('select2-results__option--highlighted');

      self.trigger('results:focus', {
        data: data,
        element: $(this)
      });
    });
  };

  Results.prototype.getHighlightedResults = function () {
    var $highlighted = this.$results
    .find('.select2-results__option--highlighted');

    return $highlighted;
  };

  Results.prototype.destroy = function () {
    this.$results.remove();
  };

  Results.prototype.ensureHighlightVisible = function () {
    var $highlighted = this.getHighlightedResults();

    if ($highlighted.length === 0) {
      return;
    }

    var $options = this.$results.find('[aria-selected]');

    var currentIndex = $options.index($highlighted);

    var currentOffset = this.$results.offset().top;
    var nextTop = $highlighted.offset().top;
    var nextOffset = this.$results.scrollTop() + (nextTop - currentOffset);

    var offsetDelta = nextTop - currentOffset;
    nextOffset -= $highlighted.outerHeight(false) * 2;

    if (currentIndex <= 2) {
      this.$results.scrollTop(0);
    } else if (offsetDelta > this.$results.outerHeight() || offsetDelta < 0) {
      this.$results.scrollTop(nextOffset);
    }
  };

  Results.prototype.template = function (result, container) {
    var template = this.options.get('templateResult');
    var escapeMarkup = this.options.get('escapeMarkup');

    var content = template(result, container);

    if (content == null) {
      container.style.display = 'none';
    } else if (typeof content === 'string') {
      container.innerHTML = escapeMarkup(content);
    } else {
      $(container).append(content);
    }
  };

  return Results;
});

S2.define('select2/keys',[

], function () {
  var KEYS = {
    BACKSPACE: 8,
    TAB: 9,
    ENTER: 13,
    SHIFT: 16,
    CTRL: 17,
    ALT: 18,
    ESC: 27,
    SPACE: 32,
    PAGE_UP: 33,
    PAGE_DOWN: 34,
    END: 35,
    HOME: 36,
    LEFT: 37,
    UP: 38,
    RIGHT: 39,
    DOWN: 40,
    DELETE: 46
  };

  return KEYS;
});

S2.define('select2/selection/base',[
  'jquery',
  '../utils',
  '../keys'
], function ($, Utils, KEYS) {
  function BaseSelection ($element, options) {
    this.$element = $element;
    this.options = options;

    BaseSelection.__super__.constructor.call(this);
  }

  Utils.Extend(BaseSelection, Utils.Observable);

  BaseSelection.prototype.render = function () {
    var $selection = $(
      '<span class="select2-selection" role="combobox" ' +
      ' aria-haspopup="true" aria-expanded="false">' +
      '</span>'
    );

    this._tabindex = 0;

    if (Utils.GetData(this.$element[0], 'old-tabindex') != null) {
      this._tabindex = Utils.GetData(this.$element[0], 'old-tabindex');
    } else if (this.$element.attr('tabindex') != null) {
      this._tabindex = this.$element.attr('tabindex');
    }

    $selection.attr('title', this.$element.attr('title'));
    $selection.attr('tabindex', this._tabindex);

    this.$selection = $selection;

    return $selection;
  };

  BaseSelection.prototype.bind = function (container, $container) {
    var self = this;

    var id = container.id + '-container';
    var resultsId = container.id + '-results';

    this.container = container;

    this.$selection.on('focus', function (evt) {
      self.trigger('focus', evt);
    });

    this.$selection.on('blur', function (evt) {
      self._handleBlur(evt);
    });

    this.$selection.on('keydown', function (evt) {
      self.trigger('keypress', evt);

      if (evt.which === KEYS.SPACE) {
        evt.preventDefault();
      }
    });

    container.on('results:focus', function (params) {
      self.$selection.attr('aria-activedescendant', params.data._resultId);
    });

    container.on('selection:update', function (params) {
      self.update(params.data);
    });

    container.on('open', function () {
      // When the dropdown is open, aria-expanded="true"
      self.$selection.attr('aria-expanded', 'true');
      self.$selection.attr('aria-owns', resultsId);

      self._attachCloseHandler(container);
    });

    container.on('close', function () {
      // When the dropdown is closed, aria-expanded="false"
      self.$selection.attr('aria-expanded', 'false');
      self.$selection.removeAttr('aria-activedescendant');
      self.$selection.removeAttr('aria-owns');

      self.$selection.focus();
      window.setTimeout(function () {
        self.$selection.focus();
      }, 0);

      self._detachCloseHandler(container);
    });

    container.on('enable', function () {
      self.$selection.attr('tabindex', self._tabindex);
    });

    container.on('disable', function () {
      self.$selection.attr('tabindex', '-1');
    });
  };

  BaseSelection.prototype._handleBlur = function (evt) {
    var self = this;

    // This needs to be delayed as the active element is the body when the tab
    // key is pressed, possibly along with others.
    window.setTimeout(function () {
      // Don't trigger `blur` if the focus is still in the selection
      if (
        (document.activeElement == self.$selection[0]) ||
        ($.contains(self.$selection[0], document.activeElement))
      ) {
        return;
      }

      self.trigger('blur', evt);
    }, 1);
  };

  BaseSelection.prototype._attachCloseHandler = function (container) {
    var self = this;

    $(document.body).on('mousedown.select2.' + container.id, function (e) {
      var $target = $(e.target);

      var $select = $target.closest('.select2');

      var $all = $('.select2.select2-container--open');

      $all.each(function () {
        var $this = $(this);

        if (this == $select[0]) {
          return;
        }

        var $element = Utils.GetData(this, 'element');

        $element.select2('close');
      });
    });
  };

  BaseSelection.prototype._detachCloseHandler = function (container) {
    $(document.body).off('mousedown.select2.' + container.id);
  };

  BaseSelection.prototype.position = function ($selection, $container) {
    var $selectionContainer = $container.find('.selection');
    $selectionContainer.append($selection);
  };

  BaseSelection.prototype.destroy = function () {
    this._detachCloseHandler(this.container);
  };

  BaseSelection.prototype.update = function (data) {
    throw new Error('The `update` method must be defined in child classes.');
  };

  return BaseSelection;
});

S2.define('select2/selection/single',[
  'jquery',
  './base',
  '../utils',
  '../keys'
], function ($, BaseSelection, Utils, KEYS) {
  function SingleSelection () {
    SingleSelection.__super__.constructor.apply(this, arguments);
  }

  Utils.Extend(SingleSelection, BaseSelection);

  SingleSelection.prototype.render = function () {
    var $selection = SingleSelection.__super__.render.call(this);

    $selection.addClass('select2-selection--single');

    $selection.html(
      '<span class="select2-selection__rendered"></span>' +
      '<span class="select2-selection__arrow" role="presentation">' +
        '<b role="presentation"></b>' +
      '</span>'
    );

    return $selection;
  };

  SingleSelection.prototype.bind = function (container, $container) {
    var self = this;

    SingleSelection.__super__.bind.apply(this, arguments);

    var id = container.id + '-container';

    this.$selection.find('.select2-selection__rendered')
      .attr('id', id)
      .attr('role', 'textbox')
      .attr('aria-readonly', 'true');
    this.$selection.attr('aria-labelledby', id);

    this.$selection.on('mousedown', function (evt) {
      // Only respond to left clicks
      if (evt.which !== 1) {
        return;
      }

      self.trigger('toggle', {
        originalEvent: evt
      });
    });

    this.$selection.on('focus', function (evt) {
      // User focuses on the container
    });

    this.$selection.on('blur', function (evt) {
      // User exits the container
    });

    container.on('focus', function (evt) {
      if (!container.isOpen()) {
        self.$selection.focus();
      }
    });
  };

  SingleSelection.prototype.clear = function () {
    var $rendered = this.$selection.find('.select2-selection__rendered');
    $rendered.empty();
    $rendered.removeAttr('title'); // clear tooltip on empty
  };

  SingleSelection.prototype.display = function (data, container) {
    var template = this.options.get('templateSelection');
    var escapeMarkup = this.options.get('escapeMarkup');

    return escapeMarkup(template(data, container));
  };

  SingleSelection.prototype.selectionContainer = function () {
    return $('<span></span>');
  };

  SingleSelection.prototype.update = function (data) {
    if (data.length === 0) {
      this.clear();
      return;
    }

    var selection = data[0];

    var $rendered = this.$selection.find('.select2-selection__rendered');
    var formatted = this.display(selection, $rendered);

    $rendered.empty().append(formatted);
    $rendered.attr('title', selection.title || selection.text);
  };

  return SingleSelection;
});

S2.define('select2/selection/multiple',[
  'jquery',
  './base',
  '../utils'
], function ($, BaseSelection, Utils) {
  function MultipleSelection ($element, options) {
    MultipleSelection.__super__.constructor.apply(this, arguments);
  }

  Utils.Extend(MultipleSelection, BaseSelection);

  MultipleSelection.prototype.render = function () {
    var $selection = MultipleSelection.__super__.render.call(this);

    $selection.addClass('select2-selection--multiple');

    $selection.html(
      '<ul class="select2-selection__rendered"></ul>'
    );

    return $selection;
  };

  MultipleSelection.prototype.bind = function (container, $container) {
    var self = this;

    MultipleSelection.__super__.bind.apply(this, arguments);

    this.$selection.on('click', function (evt) {
      self.trigger('toggle', {
        originalEvent: evt
      });
    });

    this.$selection.on(
      'click',
      '.select2-selection__choice__remove',
      function (evt) {
        // Ignore the event if it is disabled
        if (self.options.get('disabled')) {
          return;
        }

        var $remove = $(this);
        var $selection = $remove.parent();

        var data = Utils.GetData($selection[0], 'data');

        self.trigger('unselect', {
          originalEvent: evt,
          data: data
        });
      }
    );
  };

  MultipleSelection.prototype.clear = function () {
    var $rendered = this.$selection.find('.select2-selection__rendered');
    $rendered.empty();
    $rendered.removeAttr('title');
  };

  MultipleSelection.prototype.display = function (data, container) {
    var template = this.options.get('templateSelection');
    var escapeMarkup = this.options.get('escapeMarkup');

    return escapeMarkup(template(data, container));
  };

  MultipleSelection.prototype.selectionContainer = function () {
    var $container = $(
      '<li class="select2-selection__choice">' +
        '<span class="select2-selection__choice__remove" role="presentation">' +
          '&times;' +
        '</span>' +
      '</li>'
    );

    return $container;
  };

  MultipleSelection.prototype.update = function (data) {
    this.clear();

    if (data.length === 0) {
      return;
    }

    var $selections = [];

    for (var d = 0; d < data.length; d++) {
      var selection = data[d];

      var $selection = this.selectionContainer();
      var formatted = this.display(selection, $selection);

      $selection.append(formatted);
      $selection.attr('title', selection.title || selection.text);

      Utils.StoreData($selection[0], 'data', selection);

      $selections.push($selection);
    }

    var $rendered = this.$selection.find('.select2-selection__rendered');

    Utils.appendMany($rendered, $selections);
  };

  return MultipleSelection;
});

S2.define('select2/selection/placeholder',[
  '../utils'
], function (Utils) {
  function Placeholder (decorated, $element, options) {
    this.placeholder = this.normalizePlaceholder(options.get('placeholder'));

    decorated.call(this, $element, options);
  }

  Placeholder.prototype.normalizePlaceholder = function (_, placeholder) {
    if (typeof placeholder === 'string') {
      placeholder = {
        id: '',
        text: placeholder
      };
    }

    return placeholder;
  };

  Placeholder.prototype.createPlaceholder = function (decorated, placeholder) {
    var $placeholder = this.selectionContainer();

    $placeholder.html(this.display(placeholder));
    $placeholder.addClass('select2-selection__placeholder')
                .removeClass('select2-selection__choice');

    return $placeholder;
  };

  Placeholder.prototype.update = function (decorated, data) {
    var singlePlaceholder = (
      data.length == 1 && data[0].id != this.placeholder.id
    );
    var multipleSelections = data.length > 1;

    if (multipleSelections || singlePlaceholder) {
      return decorated.call(this, data);
    }

    this.clear();

    var $placeholder = this.createPlaceholder(this.placeholder);

    this.$selection.find('.select2-selection__rendered').append($placeholder);
  };

  return Placeholder;
});

S2.define('select2/selection/allowClear',[
  'jquery',
  '../keys',
  '../utils'
], function ($, KEYS, Utils) {
  function AllowClear () { }

  AllowClear.prototype.bind = function (decorated, container, $container) {
    var self = this;

    decorated.call(this, container, $container);

    if (this.placeholder == null) {
      if (this.options.get('debug') && window.console && console.error) {
        console.error(
          'Select2: The `allowClear` option should be used in combination ' +
          'with the `placeholder` option.'
        );
      }
    }

    this.$selection.on('mousedown', '.select2-selection__clear',
      function (evt) {
        self._handleClear(evt);
    });

    container.on('keypress', function (evt) {
      self._handleKeyboardClear(evt, container);
    });
  };

  AllowClear.prototype._handleClear = function (_, evt) {
    // Ignore the event if it is disabled
    if (this.options.get('disabled')) {
      return;
    }

    var $clear = this.$selection.find('.select2-selection__clear');

    // Ignore the event if nothing has been selected
    if ($clear.length === 0) {
      return;
    }

    evt.stopPropagation();

    var data = Utils.GetData($clear[0], 'data');

    var previousVal = this.$element.val();
    this.$element.val(this.placeholder.id);

    var unselectData = {
      data: data
    };
    this.trigger('clear', unselectData);
    if (unselectData.prevented) {
      this.$element.val(previousVal);
      return;
    }

    for (var d = 0; d < data.length; d++) {
      unselectData = {
        data: data[d]
      };

      // Trigger the `unselect` event, so people can prevent it from being
      // cleared.
      this.trigger('unselect', unselectData);

      // If the event was prevented, don't clear it out.
      if (unselectData.prevented) {
        this.$element.val(previousVal);
        return;
      }
    }

    this.$element.trigger('change');

    this.trigger('toggle', {});
  };

  AllowClear.prototype._handleKeyboardClear = function (_, evt, container) {
    if (container.isOpen()) {
      return;
    }

    if (evt.which == KEYS.DELETE || evt.which == KEYS.BACKSPACE) {
      this._handleClear(evt);
    }
  };

  AllowClear.prototype.update = function (decorated, data) {
    decorated.call(this, data);

    if (this.$selection.find('.select2-selection__placeholder').length > 0 ||
        data.length === 0) {
      return;
    }

    var $remove = $(
      '<span class="select2-selection__clear">' +
        '&times;' +
      '</span>'
    );
    Utils.StoreData($remove[0], 'data', data);

    this.$selection.find('.select2-selection__rendered').prepend($remove);
  };

  return AllowClear;
});

S2.define('select2/selection/search',[
  'jquery',
  '../utils',
  '../keys'
], function ($, Utils, KEYS) {
  function Search (decorated, $element, options) {
    decorated.call(this, $element, options);
  }

  Search.prototype.render = function (decorated) {
    var $search = $(
      '<li class="select2-search select2-search--inline">' +
        '<input class="select2-search__field" type="search" tabindex="-1"' +
        ' autocomplete="off" autocorrect="off" autocapitalize="none"' +
        ' spellcheck="false" role="textbox" aria-autocomplete="list" />' +
      '</li>'
    );

    this.$searchContainer = $search;
    this.$search = $search.find('input');

    var $rendered = decorated.call(this);

    this._transferTabIndex();

    return $rendered;
  };

  Search.prototype.bind = function (decorated, container, $container) {
    var self = this;

    decorated.call(this, container, $container);

    container.on('open', function () {
      self.$search.trigger('focus');
    });

    container.on('close', function () {
      self.$search.val('');
      self.$search.removeAttr('aria-activedescendant');
      self.$search.trigger('focus');
    });

    container.on('enable', function () {
      self.$search.prop('disabled', false);

      self._transferTabIndex();
    });

    container.on('disable', function () {
      self.$search.prop('disabled', true);
    });

    container.on('focus', function (evt) {
      self.$search.trigger('focus');
    });

    container.on('results:focus', function (params) {
      self.$search.attr('aria-activedescendant', params.id);
    });

    this.$selection.on('focusin', '.select2-search--inline', function (evt) {
      self.trigger('focus', evt);
    });

    this.$selection.on('focusout', '.select2-search--inline', function (evt) {
      self._handleBlur(evt);
    });

    this.$selection.on('keydown', '.select2-search--inline', function (evt) {
      evt.stopPropagation();

      self.trigger('keypress', evt);

      self._keyUpPrevented = evt.isDefaultPrevented();

      var key = evt.which;

      if (key === KEYS.BACKSPACE && self.$search.val() === '') {
        var $previousChoice = self.$searchContainer
          .prev('.select2-selection__choice');

        if ($previousChoice.length > 0) {
          var item = Utils.GetData($previousChoice[0], 'data');

          self.searchRemoveChoice(item);

          evt.preventDefault();
        }
      }
    });

    // Try to detect the IE version should the `documentMode` property that
    // is stored on the document. This is only implemented in IE and is
    // slightly cleaner than doing a user agent check.
    // This property is not available in Edge, but Edge also doesn't have
    // this bug.
    var msie = document.documentMode;
    var disableInputEvents = msie && msie <= 11;

    // Workaround for browsers which do not support the `input` event
    // This will prevent double-triggering of events for browsers which support
    // both the `keyup` and `input` events.
    this.$selection.on(
      'input.searchcheck',
      '.select2-search--inline',
      function (evt) {
        // IE will trigger the `input` event when a placeholder is used on a
        // search box. To get around this issue, we are forced to ignore all
        // `input` events in IE and keep using `keyup`.
        if (disableInputEvents) {
          self.$selection.off('input.search input.searchcheck');
          return;
        }

        // Unbind the duplicated `keyup` event
        self.$selection.off('keyup.search');
      }
    );

    this.$selection.on(
      'keyup.search input.search',
      '.select2-search--inline',
      function (evt) {
        // IE will trigger the `input` event when a placeholder is used on a
        // search box. To get around this issue, we are forced to ignore all
        // `input` events in IE and keep using `keyup`.
        if (disableInputEvents && evt.type === 'input') {
          self.$selection.off('input.search input.searchcheck');
          return;
        }

        var key = evt.which;

        // We can freely ignore events from modifier keys
        if (key == KEYS.SHIFT || key == KEYS.CTRL || key == KEYS.ALT) {
          return;
        }

        // Tabbing will be handled during the `keydown` phase
        if (key == KEYS.TAB) {
          return;
        }

        self.handleSearch(evt);
      }
    );
  };

  /**
   * This method will transfer the tabindex attribute from the rendered
   * selection to the search box. This allows for the search box to be used as
   * the primary focus instead of the selection container.
   *
   * @private
   */
  Search.prototype._transferTabIndex = function (decorated) {
    this.$search.attr('tabindex', this.$selection.attr('tabindex'));
    this.$selection.attr('tabindex', '-1');
  };

  Search.prototype.createPlaceholder = function (decorated, placeholder) {
    this.$search.attr('placeholder', placeholder.text);
  };

  Search.prototype.update = function (decorated, data) {
    var searchHadFocus = this.$search[0] == document.activeElement;

    this.$search.attr('placeholder', '');

    decorated.call(this, data);

    this.$selection.find('.select2-selection__rendered')
                   .append(this.$searchContainer);

    this.resizeSearch();
    if (searchHadFocus) {
      var isTagInput = this.$element.find('[data-select2-tag]').length;
      if (isTagInput) {
        // fix IE11 bug where tag input lost focus
        this.$element.focus();
      } else {
        this.$search.focus();
      }
    }
  };

  Search.prototype.handleSearch = function () {
    this.resizeSearch();

    if (!this._keyUpPrevented) {
      var input = this.$search.val();

      this.trigger('query', {
        term: input
      });
    }

    this._keyUpPrevented = false;
  };

  Search.prototype.searchRemoveChoice = function (decorated, item) {
    this.trigger('unselect', {
      data: item
    });

    this.$search.val(item.text);
    this.handleSearch();
  };

  Search.prototype.resizeSearch = function () {
    this.$search.css('width', '25px');

    var width = '';

    if (this.$search.attr('placeholder') !== '') {
      width = this.$selection.find('.select2-selection__rendered').innerWidth();
    } else {
      var minimumWidth = this.$search.val().length + 1;

      width = (minimumWidth * 0.75) + 'em';
    }

    this.$search.css('width', width);
  };

  return Search;
});

S2.define('select2/selection/eventRelay',[
  'jquery'
], function ($) {
  function EventRelay () { }

  EventRelay.prototype.bind = function (decorated, container, $container) {
    var self = this;
    var relayEvents = [
      'open', 'opening',
      'close', 'closing',
      'select', 'selecting',
      'unselect', 'unselecting',
      'clear', 'clearing'
    ];

    var preventableEvents = [
      'opening', 'closing', 'selecting', 'unselecting', 'clearing'
    ];

    decorated.call(this, container, $container);

    container.on('*', function (name, params) {
      // Ignore events that should not be relayed
      if ($.inArray(name, relayEvents) === -1) {
        return;
      }

      // The parameters should always be an object
      params = params || {};

      // Generate the jQuery event for the Select2 event
      var evt = $.Event('select2:' + name, {
        params: params
      });

      self.$element.trigger(evt);

      // Only handle preventable events if it was one
      if ($.inArray(name, preventableEvents) === -1) {
        return;
      }

      params.prevented = evt.isDefaultPrevented();
    });
  };

  return EventRelay;
});

S2.define('select2/translation',[
  'jquery',
  'require'
], function ($, require) {
  function Translation (dict) {
    this.dict = dict || {};
  }

  Translation.prototype.all = function () {
    return this.dict;
  };

  Translation.prototype.get = function (key) {
    return this.dict[key];
  };

  Translation.prototype.extend = function (translation) {
    this.dict = $.extend({}, translation.all(), this.dict);
  };

  // Static functions

  Translation._cache = {};

  Translation.loadPath = function (path) {
    if (!(path in Translation._cache)) {
      var translations = require(path);

      Translation._cache[path] = translations;
    }

    return new Translation(Translation._cache[path]);
  };

  return Translation;
});

S2.define('select2/diacritics',[

], function () {
  var diacritics = {
    '\u24B6': 'A',
    '\uFF21': 'A',
    '\u00C0': 'A',
    '\u00C1': 'A',
    '\u00C2': 'A',
    '\u1EA6': 'A',
    '\u1EA4': 'A',
    '\u1EAA': 'A',
    '\u1EA8': 'A',
    '\u00C3': 'A',
    '\u0100': 'A',
    '\u0102': 'A',
    '\u1EB0': 'A',
    '\u1EAE': 'A',
    '\u1EB4': 'A',
    '\u1EB2': 'A',
    '\u0226': 'A',
    '\u01E0': 'A',
    '\u00C4': 'A',
    '\u01DE': 'A',
    '\u1EA2': 'A',
    '\u00C5': 'A',
    '\u01FA': 'A',
    '\u01CD': 'A',
    '\u0200': 'A',
    '\u0202': 'A',
    '\u1EA0': 'A',
    '\u1EAC': 'A',
    '\u1EB6': 'A',
    '\u1E00': 'A',
    '\u0104': 'A',
    '\u023A': 'A',
    '\u2C6F': 'A',
    '\uA732': 'AA',
    '\u00C6': 'AE',
    '\u01FC': 'AE',
    '\u01E2': 'AE',
    '\uA734': 'AO',
    '\uA736': 'AU',
    '\uA738': 'AV',
    '\uA73A': 'AV',
    '\uA73C': 'AY',
    '\u24B7': 'B',
    '\uFF22': 'B',
    '\u1E02': 'B',
    '\u1E04': 'B',
    '\u1E06': 'B',
    '\u0243': 'B',
    '\u0182': 'B',
    '\u0181': 'B',
    '\u24B8': 'C',
    '\uFF23': 'C',
    '\u0106': 'C',
    '\u0108': 'C',
    '\u010A': 'C',
    '\u010C': 'C',
    '\u00C7': 'C',
    '\u1E08': 'C',
    '\u0187': 'C',
    '\u023B': 'C',
    '\uA73E': 'C',
    '\u24B9': 'D',
    '\uFF24': 'D',
    '\u1E0A': 'D',
    '\u010E': 'D',
    '\u1E0C': 'D',
    '\u1E10': 'D',
    '\u1E12': 'D',
    '\u1E0E': 'D',
    '\u0110': 'D',
    '\u018B': 'D',
    '\u018A': 'D',
    '\u0189': 'D',
    '\uA779': 'D',
    '\u01F1': 'DZ',
    '\u01C4': 'DZ',
    '\u01F2': 'Dz',
    '\u01C5': 'Dz',
    '\u24BA': 'E',
    '\uFF25': 'E',
    '\u00C8': 'E',
    '\u00C9': 'E',
    '\u00CA': 'E',
    '\u1EC0': 'E',
    '\u1EBE': 'E',
    '\u1EC4': 'E',
    '\u1EC2': 'E',
    '\u1EBC': 'E',
    '\u0112': 'E',
    '\u1E14': 'E',
    '\u1E16': 'E',
    '\u0114': 'E',
    '\u0116': 'E',
    '\u00CB': 'E',
    '\u1EBA': 'E',
    '\u011A': 'E',
    '\u0204': 'E',
    '\u0206': 'E',
    '\u1EB8': 'E',
    '\u1EC6': 'E',
    '\u0228': 'E',
    '\u1E1C': 'E',
    '\u0118': 'E',
    '\u1E18': 'E',
    '\u1E1A': 'E',
    '\u0190': 'E',
    '\u018E': 'E',
    '\u24BB': 'F',
    '\uFF26': 'F',
    '\u1E1E': 'F',
    '\u0191': 'F',
    '\uA77B': 'F',
    '\u24BC': 'G',
    '\uFF27': 'G',
    '\u01F4': 'G',
    '\u011C': 'G',
    '\u1E20': 'G',
    '\u011E': 'G',
    '\u0120': 'G',
    '\u01E6': 'G',
    '\u0122': 'G',
    '\u01E4': 'G',
    '\u0193': 'G',
    '\uA7A0': 'G',
    '\uA77D': 'G',
    '\uA77E': 'G',
    '\u24BD': 'H',
    '\uFF28': 'H',
    '\u0124': 'H',
    '\u1E22': 'H',
    '\u1E26': 'H',
    '\u021E': 'H',
    '\u1E24': 'H',
    '\u1E28': 'H',
    '\u1E2A': 'H',
    '\u0126': 'H',
    '\u2C67': 'H',
    '\u2C75': 'H',
    '\uA78D': 'H',
    '\u24BE': 'I',
    '\uFF29': 'I',
    '\u00CC': 'I',
    '\u00CD': 'I',
    '\u00CE': 'I',
    '\u0128': 'I',
    '\u012A': 'I',
    '\u012C': 'I',
    '\u0130': 'I',
    '\u00CF': 'I',
    '\u1E2E': 'I',
    '\u1EC8': 'I',
    '\u01CF': 'I',
    '\u0208': 'I',
    '\u020A': 'I',
    '\u1ECA': 'I',
    '\u012E': 'I',
    '\u1E2C': 'I',
    '\u0197': 'I',
    '\u24BF': 'J',
    '\uFF2A': 'J',
    '\u0134': 'J',
    '\u0248': 'J',
    '\u24C0': 'K',
    '\uFF2B': 'K',
    '\u1E30': 'K',
    '\u01E8': 'K',
    '\u1E32': 'K',
    '\u0136': 'K',
    '\u1E34': 'K',
    '\u0198': 'K',
    '\u2C69': 'K',
    '\uA740': 'K',
    '\uA742': 'K',
    '\uA744': 'K',
    '\uA7A2': 'K',
    '\u24C1': 'L',
    '\uFF2C': 'L',
    '\u013F': 'L',
    '\u0139': 'L',
    '\u013D': 'L',
    '\u1E36': 'L',
    '\u1E38': 'L',
    '\u013B': 'L',
    '\u1E3C': 'L',
    '\u1E3A': 'L',
    '\u0141': 'L',
    '\u023D': 'L',
    '\u2C62': 'L',
    '\u2C60': 'L',
    '\uA748': 'L',
    '\uA746': 'L',
    '\uA780': 'L',
    '\u01C7': 'LJ',
    '\u01C8': 'Lj',
    '\u24C2': 'M',
    '\uFF2D': 'M',
    '\u1E3E': 'M',
    '\u1E40': 'M',
    '\u1E42': 'M',
    '\u2C6E': 'M',
    '\u019C': 'M',
    '\u24C3': 'N',
    '\uFF2E': 'N',
    '\u01F8': 'N',
    '\u0143': 'N',
    '\u00D1': 'N',
    '\u1E44': 'N',
    '\u0147': 'N',
    '\u1E46': 'N',
    '\u0145': 'N',
    '\u1E4A': 'N',
    '\u1E48': 'N',
    '\u0220': 'N',
    '\u019D': 'N',
    '\uA790': 'N',
    '\uA7A4': 'N',
    '\u01CA': 'NJ',
    '\u01CB': 'Nj',
    '\u24C4': 'O',
    '\uFF2F': 'O',
    '\u00D2': 'O',
    '\u00D3': 'O',
    '\u00D4': 'O',
    '\u1ED2': 'O',
    '\u1ED0': 'O',
    '\u1ED6': 'O',
    '\u1ED4': 'O',
    '\u00D5': 'O',
    '\u1E4C': 'O',
    '\u022C': 'O',
    '\u1E4E': 'O',
    '\u014C': 'O',
    '\u1E50': 'O',
    '\u1E52': 'O',
    '\u014E': 'O',
    '\u022E': 'O',
    '\u0230': 'O',
    '\u00D6': 'O',
    '\u022A': 'O',
    '\u1ECE': 'O',
    '\u0150': 'O',
    '\u01D1': 'O',
    '\u020C': 'O',
    '\u020E': 'O',
    '\u01A0': 'O',
    '\u1EDC': 'O',
    '\u1EDA': 'O',
    '\u1EE0': 'O',
    '\u1EDE': 'O',
    '\u1EE2': 'O',
    '\u1ECC': 'O',
    '\u1ED8': 'O',
    '\u01EA': 'O',
    '\u01EC': 'O',
    '\u00D8': 'O',
    '\u01FE': 'O',
    '\u0186': 'O',
    '\u019F': 'O',
    '\uA74A': 'O',
    '\uA74C': 'O',
    '\u01A2': 'OI',
    '\uA74E': 'OO',
    '\u0222': 'OU',
    '\u24C5': 'P',
    '\uFF30': 'P',
    '\u1E54': 'P',
    '\u1E56': 'P',
    '\u01A4': 'P',
    '\u2C63': 'P',
    '\uA750': 'P',
    '\uA752': 'P',
    '\uA754': 'P',
    '\u24C6': 'Q',
    '\uFF31': 'Q',
    '\uA756': 'Q',
    '\uA758': 'Q',
    '\u024A': 'Q',
    '\u24C7': 'R',
    '\uFF32': 'R',
    '\u0154': 'R',
    '\u1E58': 'R',
    '\u0158': 'R',
    '\u0210': 'R',
    '\u0212': 'R',
    '\u1E5A': 'R',
    '\u1E5C': 'R',
    '\u0156': 'R',
    '\u1E5E': 'R',
    '\u024C': 'R',
    '\u2C64': 'R',
    '\uA75A': 'R',
    '\uA7A6': 'R',
    '\uA782': 'R',
    '\u24C8': 'S',
    '\uFF33': 'S',
    '\u1E9E': 'S',
    '\u015A': 'S',
    '\u1E64': 'S',
    '\u015C': 'S',
    '\u1E60': 'S',
    '\u0160': 'S',
    '\u1E66': 'S',
    '\u1E62': 'S',
    '\u1E68': 'S',
    '\u0218': 'S',
    '\u015E': 'S',
    '\u2C7E': 'S',
    '\uA7A8': 'S',
    '\uA784': 'S',
    '\u24C9': 'T',
    '\uFF34': 'T',
    '\u1E6A': 'T',
    '\u0164': 'T',
    '\u1E6C': 'T',
    '\u021A': 'T',
    '\u0162': 'T',
    '\u1E70': 'T',
    '\u1E6E': 'T',
    '\u0166': 'T',
    '\u01AC': 'T',
    '\u01AE': 'T',
    '\u023E': 'T',
    '\uA786': 'T',
    '\uA728': 'TZ',
    '\u24CA': 'U',
    '\uFF35': 'U',
    '\u00D9': 'U',
    '\u00DA': 'U',
    '\u00DB': 'U',
    '\u0168': 'U',
    '\u1E78': 'U',
    '\u016A': 'U',
    '\u1E7A': 'U',
    '\u016C': 'U',
    '\u00DC': 'U',
    '\u01DB': 'U',
    '\u01D7': 'U',
    '\u01D5': 'U',
    '\u01D9': 'U',
    '\u1EE6': 'U',
    '\u016E': 'U',
    '\u0170': 'U',
    '\u01D3': 'U',
    '\u0214': 'U',
    '\u0216': 'U',
    '\u01AF': 'U',
    '\u1EEA': 'U',
    '\u1EE8': 'U',
    '\u1EEE': 'U',
    '\u1EEC': 'U',
    '\u1EF0': 'U',
    '\u1EE4': 'U',
    '\u1E72': 'U',
    '\u0172': 'U',
    '\u1E76': 'U',
    '\u1E74': 'U',
    '\u0244': 'U',
    '\u24CB': 'V',
    '\uFF36': 'V',
    '\u1E7C': 'V',
    '\u1E7E': 'V',
    '\u01B2': 'V',
    '\uA75E': 'V',
    '\u0245': 'V',
    '\uA760': 'VY',
    '\u24CC': 'W',
    '\uFF37': 'W',
    '\u1E80': 'W',
    '\u1E82': 'W',
    '\u0174': 'W',
    '\u1E86': 'W',
    '\u1E84': 'W',
    '\u1E88': 'W',
    '\u2C72': 'W',
    '\u24CD': 'X',
    '\uFF38': 'X',
    '\u1E8A': 'X',
    '\u1E8C': 'X',
    '\u24CE': 'Y',
    '\uFF39': 'Y',
    '\u1EF2': 'Y',
    '\u00DD': 'Y',
    '\u0176': 'Y',
    '\u1EF8': 'Y',
    '\u0232': 'Y',
    '\u1E8E': 'Y',
    '\u0178': 'Y',
    '\u1EF6': 'Y',
    '\u1EF4': 'Y',
    '\u01B3': 'Y',
    '\u024E': 'Y',
    '\u1EFE': 'Y',
    '\u24CF': 'Z',
    '\uFF3A': 'Z',
    '\u0179': 'Z',
    '\u1E90': 'Z',
    '\u017B': 'Z',
    '\u017D': 'Z',
    '\u1E92': 'Z',
    '\u1E94': 'Z',
    '\u01B5': 'Z',
    '\u0224': 'Z',
    '\u2C7F': 'Z',
    '\u2C6B': 'Z',
    '\uA762': 'Z',
    '\u24D0': 'a',
    '\uFF41': 'a',
    '\u1E9A': 'a',
    '\u00E0': 'a',
    '\u00E1': 'a',
    '\u00E2': 'a',
    '\u1EA7': 'a',
    '\u1EA5': 'a',
    '\u1EAB': 'a',
    '\u1EA9': 'a',
    '\u00E3': 'a',
    '\u0101': 'a',
    '\u0103': 'a',
    '\u1EB1': 'a',
    '\u1EAF': 'a',
    '\u1EB5': 'a',
    '\u1EB3': 'a',
    '\u0227': 'a',
    '\u01E1': 'a',
    '\u00E4': 'a',
    '\u01DF': 'a',
    '\u1EA3': 'a',
    '\u00E5': 'a',
    '\u01FB': 'a',
    '\u01CE': 'a',
    '\u0201': 'a',
    '\u0203': 'a',
    '\u1EA1': 'a',
    '\u1EAD': 'a',
    '\u1EB7': 'a',
    '\u1E01': 'a',
    '\u0105': 'a',
    '\u2C65': 'a',
    '\u0250': 'a',
    '\uA733': 'aa',
    '\u00E6': 'ae',
    '\u01FD': 'ae',
    '\u01E3': 'ae',
    '\uA735': 'ao',
    '\uA737': 'au',
    '\uA739': 'av',
    '\uA73B': 'av',
    '\uA73D': 'ay',
    '\u24D1': 'b',
    '\uFF42': 'b',
    '\u1E03': 'b',
    '\u1E05': 'b',
    '\u1E07': 'b',
    '\u0180': 'b',
    '\u0183': 'b',
    '\u0253': 'b',
    '\u24D2': 'c',
    '\uFF43': 'c',
    '\u0107': 'c',
    '\u0109': 'c',
    '\u010B': 'c',
    '\u010D': 'c',
    '\u00E7': 'c',
    '\u1E09': 'c',
    '\u0188': 'c',
    '\u023C': 'c',
    '\uA73F': 'c',
    '\u2184': 'c',
    '\u24D3': 'd',
    '\uFF44': 'd',
    '\u1E0B': 'd',
    '\u010F': 'd',
    '\u1E0D': 'd',
    '\u1E11': 'd',
    '\u1E13': 'd',
    '\u1E0F': 'd',
    '\u0111': 'd',
    '\u018C': 'd',
    '\u0256': 'd',
    '\u0257': 'd',
    '\uA77A': 'd',
    '\u01F3': 'dz',
    '\u01C6': 'dz',
    '\u24D4': 'e',
    '\uFF45': 'e',
    '\u00E8': 'e',
    '\u00E9': 'e',
    '\u00EA': 'e',
    '\u1EC1': 'e',
    '\u1EBF': 'e',
    '\u1EC5': 'e',
    '\u1EC3': 'e',
    '\u1EBD': 'e',
    '\u0113': 'e',
    '\u1E15': 'e',
    '\u1E17': 'e',
    '\u0115': 'e',
    '\u0117': 'e',
    '\u00EB': 'e',
    '\u1EBB': 'e',
    '\u011B': 'e',
    '\u0205': 'e',
    '\u0207': 'e',
    '\u1EB9': 'e',
    '\u1EC7': 'e',
    '\u0229': 'e',
    '\u1E1D': 'e',
    '\u0119': 'e',
    '\u1E19': 'e',
    '\u1E1B': 'e',
    '\u0247': 'e',
    '\u025B': 'e',
    '\u01DD': 'e',
    '\u24D5': 'f',
    '\uFF46': 'f',
    '\u1E1F': 'f',
    '\u0192': 'f',
    '\uA77C': 'f',
    '\u24D6': 'g',
    '\uFF47': 'g',
    '\u01F5': 'g',
    '\u011D': 'g',
    '\u1E21': 'g',
    '\u011F': 'g',
    '\u0121': 'g',
    '\u01E7': 'g',
    '\u0123': 'g',
    '\u01E5': 'g',
    '\u0260': 'g',
    '\uA7A1': 'g',
    '\u1D79': 'g',
    '\uA77F': 'g',
    '\u24D7': 'h',
    '\uFF48': 'h',
    '\u0125': 'h',
    '\u1E23': 'h',
    '\u1E27': 'h',
    '\u021F': 'h',
    '\u1E25': 'h',
    '\u1E29': 'h',
    '\u1E2B': 'h',
    '\u1E96': 'h',
    '\u0127': 'h',
    '\u2C68': 'h',
    '\u2C76': 'h',
    '\u0265': 'h',
    '\u0195': 'hv',
    '\u24D8': 'i',
    '\uFF49': 'i',
    '\u00EC': 'i',
    '\u00ED': 'i',
    '\u00EE': 'i',
    '\u0129': 'i',
    '\u012B': 'i',
    '\u012D': 'i',
    '\u00EF': 'i',
    '\u1E2F': 'i',
    '\u1EC9': 'i',
    '\u01D0': 'i',
    '\u0209': 'i',
    '\u020B': 'i',
    '\u1ECB': 'i',
    '\u012F': 'i',
    '\u1E2D': 'i',
    '\u0268': 'i',
    '\u0131': 'i',
    '\u24D9': 'j',
    '\uFF4A': 'j',
    '\u0135': 'j',
    '\u01F0': 'j',
    '\u0249': 'j',
    '\u24DA': 'k',
    '\uFF4B': 'k',
    '\u1E31': 'k',
    '\u01E9': 'k',
    '\u1E33': 'k',
    '\u0137': 'k',
    '\u1E35': 'k',
    '\u0199': 'k',
    '\u2C6A': 'k',
    '\uA741': 'k',
    '\uA743': 'k',
    '\uA745': 'k',
    '\uA7A3': 'k',
    '\u24DB': 'l',
    '\uFF4C': 'l',
    '\u0140': 'l',
    '\u013A': 'l',
    '\u013E': 'l',
    '\u1E37': 'l',
    '\u1E39': 'l',
    '\u013C': 'l',
    '\u1E3D': 'l',
    '\u1E3B': 'l',
    '\u017F': 'l',
    '\u0142': 'l',
    '\u019A': 'l',
    '\u026B': 'l',
    '\u2C61': 'l',
    '\uA749': 'l',
    '\uA781': 'l',
    '\uA747': 'l',
    '\u01C9': 'lj',
    '\u24DC': 'm',
    '\uFF4D': 'm',
    '\u1E3F': 'm',
    '\u1E41': 'm',
    '\u1E43': 'm',
    '\u0271': 'm',
    '\u026F': 'm',
    '\u24DD': 'n',
    '\uFF4E': 'n',
    '\u01F9': 'n',
    '\u0144': 'n',
    '\u00F1': 'n',
    '\u1E45': 'n',
    '\u0148': 'n',
    '\u1E47': 'n',
    '\u0146': 'n',
    '\u1E4B': 'n',
    '\u1E49': 'n',
    '\u019E': 'n',
    '\u0272': 'n',
    '\u0149': 'n',
    '\uA791': 'n',
    '\uA7A5': 'n',
    '\u01CC': 'nj',
    '\u24DE': 'o',
    '\uFF4F': 'o',
    '\u00F2': 'o',
    '\u00F3': 'o',
    '\u00F4': 'o',
    '\u1ED3': 'o',
    '\u1ED1': 'o',
    '\u1ED7': 'o',
    '\u1ED5': 'o',
    '\u00F5': 'o',
    '\u1E4D': 'o',
    '\u022D': 'o',
    '\u1E4F': 'o',
    '\u014D': 'o',
    '\u1E51': 'o',
    '\u1E53': 'o',
    '\u014F': 'o',
    '\u022F': 'o',
    '\u0231': 'o',
    '\u00F6': 'o',
    '\u022B': 'o',
    '\u1ECF': 'o',
    '\u0151': 'o',
    '\u01D2': 'o',
    '\u020D': 'o',
    '\u020F': 'o',
    '\u01A1': 'o',
    '\u1EDD': 'o',
    '\u1EDB': 'o',
    '\u1EE1': 'o',
    '\u1EDF': 'o',
    '\u1EE3': 'o',
    '\u1ECD': 'o',
    '\u1ED9': 'o',
    '\u01EB': 'o',
    '\u01ED': 'o',
    '\u00F8': 'o',
    '\u01FF': 'o',
    '\u0254': 'o',
    '\uA74B': 'o',
    '\uA74D': 'o',
    '\u0275': 'o',
    '\u01A3': 'oi',
    '\u0223': 'ou',
    '\uA74F': 'oo',
    '\u24DF': 'p',
    '\uFF50': 'p',
    '\u1E55': 'p',
    '\u1E57': 'p',
    '\u01A5': 'p',
    '\u1D7D': 'p',
    '\uA751': 'p',
    '\uA753': 'p',
    '\uA755': 'p',
    '\u24E0': 'q',
    '\uFF51': 'q',
    '\u024B': 'q',
    '\uA757': 'q',
    '\uA759': 'q',
    '\u24E1': 'r',
    '\uFF52': 'r',
    '\u0155': 'r',
    '\u1E59': 'r',
    '\u0159': 'r',
    '\u0211': 'r',
    '\u0213': 'r',
    '\u1E5B': 'r',
    '\u1E5D': 'r',
    '\u0157': 'r',
    '\u1E5F': 'r',
    '\u024D': 'r',
    '\u027D': 'r',
    '\uA75B': 'r',
    '\uA7A7': 'r',
    '\uA783': 'r',
    '\u24E2': 's',
    '\uFF53': 's',
    '\u00DF': 's',
    '\u015B': 's',
    '\u1E65': 's',
    '\u015D': 's',
    '\u1E61': 's',
    '\u0161': 's',
    '\u1E67': 's',
    '\u1E63': 's',
    '\u1E69': 's',
    '\u0219': 's',
    '\u015F': 's',
    '\u023F': 's',
    '\uA7A9': 's',
    '\uA785': 's',
    '\u1E9B': 's',
    '\u24E3': 't',
    '\uFF54': 't',
    '\u1E6B': 't',
    '\u1E97': 't',
    '\u0165': 't',
    '\u1E6D': 't',
    '\u021B': 't',
    '\u0163': 't',
    '\u1E71': 't',
    '\u1E6F': 't',
    '\u0167': 't',
    '\u01AD': 't',
    '\u0288': 't',
    '\u2C66': 't',
    '\uA787': 't',
    '\uA729': 'tz',
    '\u24E4': 'u',
    '\uFF55': 'u',
    '\u00F9': 'u',
    '\u00FA': 'u',
    '\u00FB': 'u',
    '\u0169': 'u',
    '\u1E79': 'u',
    '\u016B': 'u',
    '\u1E7B': 'u',
    '\u016D': 'u',
    '\u00FC': 'u',
    '\u01DC': 'u',
    '\u01D8': 'u',
    '\u01D6': 'u',
    '\u01DA': 'u',
    '\u1EE7': 'u',
    '\u016F': 'u',
    '\u0171': 'u',
    '\u01D4': 'u',
    '\u0215': 'u',
    '\u0217': 'u',
    '\u01B0': 'u',
    '\u1EEB': 'u',
    '\u1EE9': 'u',
    '\u1EEF': 'u',
    '\u1EED': 'u',
    '\u1EF1': 'u',
    '\u1EE5': 'u',
    '\u1E73': 'u',
    '\u0173': 'u',
    '\u1E77': 'u',
    '\u1E75': 'u',
    '\u0289': 'u',
    '\u24E5': 'v',
    '\uFF56': 'v',
    '\u1E7D': 'v',
    '\u1E7F': 'v',
    '\u028B': 'v',
    '\uA75F': 'v',
    '\u028C': 'v',
    '\uA761': 'vy',
    '\u24E6': 'w',
    '\uFF57': 'w',
    '\u1E81': 'w',
    '\u1E83': 'w',
    '\u0175': 'w',
    '\u1E87': 'w',
    '\u1E85': 'w',
    '\u1E98': 'w',
    '\u1E89': 'w',
    '\u2C73': 'w',
    '\u24E7': 'x',
    '\uFF58': 'x',
    '\u1E8B': 'x',
    '\u1E8D': 'x',
    '\u24E8': 'y',
    '\uFF59': 'y',
    '\u1EF3': 'y',
    '\u00FD': 'y',
    '\u0177': 'y',
    '\u1EF9': 'y',
    '\u0233': 'y',
    '\u1E8F': 'y',
    '\u00FF': 'y',
    '\u1EF7': 'y',
    '\u1E99': 'y',
    '\u1EF5': 'y',
    '\u01B4': 'y',
    '\u024F': 'y',
    '\u1EFF': 'y',
    '\u24E9': 'z',
    '\uFF5A': 'z',
    '\u017A': 'z',
    '\u1E91': 'z',
    '\u017C': 'z',
    '\u017E': 'z',
    '\u1E93': 'z',
    '\u1E95': 'z',
    '\u01B6': 'z',
    '\u0225': 'z',
    '\u0240': 'z',
    '\u2C6C': 'z',
    '\uA763': 'z',
    '\u0386': '\u0391',
    '\u0388': '\u0395',
    '\u0389': '\u0397',
    '\u038A': '\u0399',
    '\u03AA': '\u0399',
    '\u038C': '\u039F',
    '\u038E': '\u03A5',
    '\u03AB': '\u03A5',
    '\u038F': '\u03A9',
    '\u03AC': '\u03B1',
    '\u03AD': '\u03B5',
    '\u03AE': '\u03B7',
    '\u03AF': '\u03B9',
    '\u03CA': '\u03B9',
    '\u0390': '\u03B9',
    '\u03CC': '\u03BF',
    '\u03CD': '\u03C5',
    '\u03CB': '\u03C5',
    '\u03B0': '\u03C5',
    '\u03C9': '\u03C9',
    '\u03C2': '\u03C3'
  };

  return diacritics;
});

S2.define('select2/data/base',[
  '../utils'
], function (Utils) {
  function BaseAdapter ($element, options) {
    BaseAdapter.__super__.constructor.call(this);
  }

  Utils.Extend(BaseAdapter, Utils.Observable);

  BaseAdapter.prototype.current = function (callback) {
    throw new Error('The `current` method must be defined in child classes.');
  };

  BaseAdapter.prototype.query = function (params, callback) {
    throw new Error('The `query` method must be defined in child classes.');
  };

  BaseAdapter.prototype.bind = function (container, $container) {
    // Can be implemented in subclasses
  };

  BaseAdapter.prototype.destroy = function () {
    // Can be implemented in subclasses
  };

  BaseAdapter.prototype.generateResultId = function (container, data) {
    var id = container.id + '-result-';

    id += Utils.generateChars(4);

    if (data.id != null) {
      id += '-' + data.id.toString();
    } else {
      id += '-' + Utils.generateChars(4);
    }
    return id;
  };

  return BaseAdapter;
});

S2.define('select2/data/select',[
  './base',
  '../utils',
  'jquery'
], function (BaseAdapter, Utils, $) {
  function SelectAdapter ($element, options) {
    this.$element = $element;
    this.options = options;

    SelectAdapter.__super__.constructor.call(this);
  }

  Utils.Extend(SelectAdapter, BaseAdapter);

  SelectAdapter.prototype.current = function (callback) {
    var data = [];
    var self = this;

    this.$element.find(':selected').each(function () {
      var $option = $(this);

      var option = self.item($option);

      data.push(option);
    });

    callback(data);
  };

  SelectAdapter.prototype.select = function (data) {
    var self = this;

    data.selected = true;

    // If data.element is a DOM node, use it instead
    if ($(data.element).is('option')) {
      data.element.selected = true;

      this.$element.trigger('change');

      return;
    }

    if (this.$element.prop('multiple')) {
      this.current(function (currentData) {
        var val = [];

        data = [data];
        data.push.apply(data, currentData);

        for (var d = 0; d < data.length; d++) {
          var id = data[d].id;

          if ($.inArray(id, val) === -1) {
            val.push(id);
          }
        }

        self.$element.val(val);
        self.$element.trigger('change');
      });
    } else {
      var val = data.id;

      this.$element.val(val);
      this.$element.trigger('change');
    }
  };

  SelectAdapter.prototype.unselect = function (data) {
    var self = this;

    if (!this.$element.prop('multiple')) {
      return;
    }

    data.selected = false;

    if ($(data.element).is('option')) {
      data.element.selected = false;

      this.$element.trigger('change');

      return;
    }

    this.current(function (currentData) {
      var val = [];

      for (var d = 0; d < currentData.length; d++) {
        var id = currentData[d].id;

        if (id !== data.id && $.inArray(id, val) === -1) {
          val.push(id);
        }
      }

      self.$element.val(val);

      self.$element.trigger('change');
    });
  };

  SelectAdapter.prototype.bind = function (container, $container) {
    var self = this;

    this.container = container;

    container.on('select', function (params) {
      self.select(params.data);
    });

    container.on('unselect', function (params) {
      self.unselect(params.data);
    });
  };

  SelectAdapter.prototype.destroy = function () {
    // Remove anything added to child elements
    this.$element.find('*').each(function () {
      // Remove any custom data set by Select2
      Utils.RemoveData(this);
    });
  };

  SelectAdapter.prototype.query = function (params, callback) {
    var data = [];
    var self = this;

    var $options = this.$element.children();

    $options.each(function () {
      var $option = $(this);

      if (!$option.is('option') && !$option.is('optgroup')) {
        return;
      }

      var option = self.item($option);

      var matches = self.matches(params, option);

      if (matches !== null) {
        data.push(matches);
      }
    });

    callback({
      results: data
    });
  };

  SelectAdapter.prototype.addOptions = function ($options) {
    Utils.appendMany(this.$element, $options);
  };

  SelectAdapter.prototype.option = function (data) {
    var option;

    if (data.children) {
      option = document.createElement('optgroup');
      option.label = data.text;
    } else {
      option = document.createElement('option');

      if (option.textContent !== undefined) {
        option.textContent = data.text;
      } else {
        option.innerText = data.text;
      }
    }

    if (data.id !== undefined) {
      option.value = data.id;
    }

    if (data.disabled) {
      option.disabled = true;
    }

    if (data.selected) {
      option.selected = true;
    }

    if (data.title) {
      option.title = data.title;
    }

    var $option = $(option);

    var normalizedData = this._normalizeItem(data);
    normalizedData.element = option;

    // Override the option's data with the combined data
    Utils.StoreData(option, 'data', normalizedData);

    return $option;
  };

  SelectAdapter.prototype.item = function ($option) {
    var data = {};

    data = Utils.GetData($option[0], 'data');

    if (data != null) {
      return data;
    }

    if ($option.is('option')) {
      data = {
        id: $option.val(),
        text: $option.text(),
        disabled: $option.prop('disabled'),
        selected: $option.prop('selected'),
        title: $option.prop('title')
      };
    } else if ($option.is('optgroup')) {
      data = {
        text: $option.prop('label'),
        children: [],
        title: $option.prop('title')
      };

      var $children = $option.children('option');
      var children = [];

      for (var c = 0; c < $children.length; c++) {
        var $child = $($children[c]);

        var child = this.item($child);

        children.push(child);
      }

      data.children = children;
    }

    data = this._normalizeItem(data);
    data.element = $option[0];

    Utils.StoreData($option[0], 'data', data);

    return data;
  };

  SelectAdapter.prototype._normalizeItem = function (item) {
    if (item !== Object(item)) {
      item = {
        id: item,
        text: item
      };
    }

    item = $.extend({}, {
      text: ''
    }, item);

    var defaults = {
      selected: false,
      disabled: false
    };

    if (item.id != null) {
      item.id = item.id.toString();
    }

    if (item.text != null) {
      item.text = item.text.toString();
    }

    if (item._resultId == null && item.id && this.container != null) {
      item._resultId = this.generateResultId(this.container, item);
    }

    return $.extend({}, defaults, item);
  };

  SelectAdapter.prototype.matches = function (params, data) {
    var matcher = this.options.get('matcher');

    return matcher(params, data);
  };

  return SelectAdapter;
});

S2.define('select2/data/array',[
  './select',
  '../utils',
  'jquery'
], function (SelectAdapter, Utils, $) {
  function ArrayAdapter ($element, options) {
    var data = options.get('data') || [];

    ArrayAdapter.__super__.constructor.call(this, $element, options);

    this.addOptions(this.convertToOptions(data));
  }

  Utils.Extend(ArrayAdapter, SelectAdapter);

  ArrayAdapter.prototype.select = function (data) {
    var $option = this.$element.find('option').filter(function (i, elm) {
      return elm.value == data.id.toString();
    });

    if ($option.length === 0) {
      $option = this.option(data);

      this.addOptions($option);
    }

    ArrayAdapter.__super__.select.call(this, data);
  };

  ArrayAdapter.prototype.convertToOptions = function (data) {
    var self = this;

    var $existing = this.$element.find('option');
    var existingIds = $existing.map(function () {
      return self.item($(this)).id;
    }).get();

    var $options = [];

    // Filter out all items except for the one passed in the argument
    function onlyItem (item) {
      return function () {
        return $(this).val() == item.id;
      };
    }

    for (var d = 0; d < data.length; d++) {
      var item = this._normalizeItem(data[d]);

      // Skip items which were pre-loaded, only merge the data
      if ($.inArray(item.id, existingIds) >= 0) {
        var $existingOption = $existing.filter(onlyItem(item));

        var existingData = this.item($existingOption);
        var newData = $.extend(true, {}, item, existingData);

        var $newOption = this.option(newData);

        $existingOption.replaceWith($newOption);

        continue;
      }

      var $option = this.option(item);

      if (item.children) {
        var $children = this.convertToOptions(item.children);

        Utils.appendMany($option, $children);
      }

      $options.push($option);
    }

    return $options;
  };

  return ArrayAdapter;
});

S2.define('select2/data/ajax',[
  './array',
  '../utils',
  'jquery'
], function (ArrayAdapter, Utils, $) {
  function AjaxAdapter ($element, options) {
    this.ajaxOptions = this._applyDefaults(options.get('ajax'));

    if (this.ajaxOptions.processResults != null) {
      this.processResults = this.ajaxOptions.processResults;
    }

    AjaxAdapter.__super__.constructor.call(this, $element, options);
  }

  Utils.Extend(AjaxAdapter, ArrayAdapter);

  AjaxAdapter.prototype._applyDefaults = function (options) {
    var defaults = {
      data: function (params) {
        return $.extend({}, params, {
          q: params.term
        });
      },
      transport: function (params, success, failure) {
        var $request = $.ajax(params);

        $request.then(success);
        $request.fail(failure);

        return $request;
      }
    };

    return $.extend({}, defaults, options, true);
  };

  AjaxAdapter.prototype.processResults = function (results) {
    return results;
  };

  AjaxAdapter.prototype.query = function (params, callback) {
    var matches = [];
    var self = this;

    if (this._request != null) {
      // JSONP requests cannot always be aborted
      if ($.isFunction(this._request.abort)) {
        this._request.abort();
      }

      this._request = null;
    }

    var options = $.extend({
      type: 'GET'
    }, this.ajaxOptions);

    if (typeof options.url === 'function') {
      options.url = options.url.call(this.$element, params);
    }

    if (typeof options.data === 'function') {
      options.data = options.data.call(this.$element, params);
    }

    function request () {
      var $request = options.transport(options, function (data) {
        var results = self.processResults(data, params);

        if (self.options.get('debug') && window.console && console.error) {
          // Check to make sure that the response included a `results` key.
          if (!results || !results.results || !$.isArray(results.results)) {
            console.error(
              'Select2: The AJAX results did not return an array in the ' +
              '`results` key of the response.'
            );
          }
        }

        callback(results);
      }, function () {
        // Attempt to detect if a request was aborted
        // Only works if the transport exposes a status property
        if ('status' in $request &&
            ($request.status === 0 || $request.status === '0')) {
          return;
        }

        self.trigger('results:message', {
          message: 'errorLoading'
        });
      });

      self._request = $request;
    }

    if (this.ajaxOptions.delay && params.term != null) {
      if (this._queryTimeout) {
        window.clearTimeout(this._queryTimeout);
      }

      this._queryTimeout = window.setTimeout(request, this.ajaxOptions.delay);
    } else {
      request();
    }
  };

  return AjaxAdapter;
});

S2.define('select2/data/tags',[
  'jquery'
], function ($) {
  function Tags (decorated, $element, options) {
    var tags = options.get('tags');

    var createTag = options.get('createTag');

    if (createTag !== undefined) {
      this.createTag = createTag;
    }

    var insertTag = options.get('insertTag');

    if (insertTag !== undefined) {
        this.insertTag = insertTag;
    }

    decorated.call(this, $element, options);

    if ($.isArray(tags)) {
      for (var t = 0; t < tags.length; t++) {
        var tag = tags[t];
        var item = this._normalizeItem(tag);

        var $option = this.option(item);

        this.$element.append($option);
      }
    }
  }

  Tags.prototype.query = function (decorated, params, callback) {
    var self = this;

    this._removeOldTags();

    if (params.term == null || params.page != null) {
      decorated.call(this, params, callback);
      return;
    }

    function wrapper (obj, child) {
      var data = obj.results;

      for (var i = 0; i < data.length; i++) {
        var option = data[i];

        var checkChildren = (
          option.children != null &&
          !wrapper({
            results: option.children
          }, true)
        );

        var optionText = (option.text || '').toUpperCase();
        var paramsTerm = (params.term || '').toUpperCase();

        var checkText = optionText === paramsTerm;

        if (checkText || checkChildren) {
          if (child) {
            return false;
          }

          obj.data = data;
          callback(obj);

          return;
        }
      }

      if (child) {
        return true;
      }

      var tag = self.createTag(params);

      if (tag != null) {
        var $option = self.option(tag);
        $option.attr('data-select2-tag', true);

        self.addOptions([$option]);

        self.insertTag(data, tag);
      }

      obj.results = data;

      callback(obj);
    }

    decorated.call(this, params, wrapper);
  };

  Tags.prototype.createTag = function (decorated, params) {
    var term = $.trim(params.term);

    if (term === '') {
      return null;
    }

    return {
      id: term,
      text: term
    };
  };

  Tags.prototype.insertTag = function (_, data, tag) {
    data.unshift(tag);
  };

  Tags.prototype._removeOldTags = function (_) {
    var tag = this._lastTag;

    var $options = this.$element.find('option[data-select2-tag]');

    $options.each(function () {
      if (this.selected) {
        return;
      }

      $(this).remove();
    });
  };

  return Tags;
});

S2.define('select2/data/tokenizer',[
  'jquery'
], function ($) {
  function Tokenizer (decorated, $element, options) {
    var tokenizer = options.get('tokenizer');

    if (tokenizer !== undefined) {
      this.tokenizer = tokenizer;
    }

    decorated.call(this, $element, options);
  }

  Tokenizer.prototype.bind = function (decorated, container, $container) {
    decorated.call(this, container, $container);

    this.$search =  container.dropdown.$search || container.selection.$search ||
      $container.find('.select2-search__field');
  };

  Tokenizer.prototype.query = function (decorated, params, callback) {
    var self = this;

    function createAndSelect (data) {
      // Normalize the data object so we can use it for checks
      var item = self._normalizeItem(data);

      // Check if the data object already exists as a tag
      // Select it if it doesn't
      var $existingOptions = self.$element.find('option').filter(function () {
        return $(this).val() === item.id;
      });

      // If an existing option wasn't found for it, create the option
      if (!$existingOptions.length) {
        var $option = self.option(item);
        $option.attr('data-select2-tag', true);

        self._removeOldTags();
        self.addOptions([$option]);
      }

      // Select the item, now that we know there is an option for it
      select(item);
    }

    function select (data) {
      self.trigger('select', {
        data: data
      });
    }

    params.term = params.term || '';

    var tokenData = this.tokenizer(params, this.options, createAndSelect);

    if (tokenData.term !== params.term) {
      // Replace the search term if we have the search box
      if (this.$search.length) {
        this.$search.val(tokenData.term);
        this.$search.focus();
      }

      params.term = tokenData.term;
    }

    decorated.call(this, params, callback);
  };

  Tokenizer.prototype.tokenizer = function (_, params, options, callback) {
    var separators = options.get('tokenSeparators') || [];
    var term = params.term;
    var i = 0;

    var createTag = this.createTag || function (params) {
      return {
        id: params.term,
        text: params.term
      };
    };

    while (i < term.length) {
      var termChar = term[i];

      if ($.inArray(termChar, separators) === -1) {
        i++;

        continue;
      }

      var part = term.substr(0, i);
      var partParams = $.extend({}, params, {
        term: part
      });

      var data = createTag(partParams);

      if (data == null) {
        i++;
        continue;
      }

      callback(data);

      // Reset the term to not include the tokenized portion
      term = term.substr(i + 1) || '';
      i = 0;
    }

    return {
      term: term
    };
  };

  return Tokenizer;
});

S2.define('select2/data/minimumInputLength',[

], function () {
  function MinimumInputLength (decorated, $e, options) {
    this.minimumInputLength = options.get('minimumInputLength');

    decorated.call(this, $e, options);
  }

  MinimumInputLength.prototype.query = function (decorated, params, callback) {
    params.term = params.term || '';

    if (params.term.length < this.minimumInputLength) {
      this.trigger('results:message', {
        message: 'inputTooShort',
        args: {
          minimum: this.minimumInputLength,
          input: params.term,
          params: params
        }
      });

      return;
    }

    decorated.call(this, params, callback);
  };

  return MinimumInputLength;
});

S2.define('select2/data/maximumInputLength',[

], function () {
  function MaximumInputLength (decorated, $e, options) {
    this.maximumInputLength = options.get('maximumInputLength');

    decorated.call(this, $e, options);
  }

  MaximumInputLength.prototype.query = function (decorated, params, callback) {
    params.term = params.term || '';

    if (this.maximumInputLength > 0 &&
        params.term.length > this.maximumInputLength) {
      this.trigger('results:message', {
        message: 'inputTooLong',
        args: {
          maximum: this.maximumInputLength,
          input: params.term,
          params: params
        }
      });

      return;
    }

    decorated.call(this, params, callback);
  };

  return MaximumInputLength;
});

S2.define('select2/data/maximumSelectionLength',[

], function (){
  function MaximumSelectionLength (decorated, $e, options) {
    this.maximumSelectionLength = options.get('maximumSelectionLength');

    decorated.call(this, $e, options);
  }

  MaximumSelectionLength.prototype.query =
    function (decorated, params, callback) {
      var self = this;

      this.current(function (currentData) {
        var count = currentData != null ? currentData.length : 0;
        if (self.maximumSelectionLength > 0 &&
          count >= self.maximumSelectionLength) {
          self.trigger('results:message', {
            message: 'maximumSelected',
            args: {
              maximum: self.maximumSelectionLength
            }
          });
          return;
        }
        decorated.call(self, params, callback);
      });
  };

  return MaximumSelectionLength;
});

S2.define('select2/dropdown',[
  'jquery',
  './utils'
], function ($, Utils) {
  function Dropdown ($element, options) {
    this.$element = $element;
    this.options = options;

    Dropdown.__super__.constructor.call(this);
  }

  Utils.Extend(Dropdown, Utils.Observable);

  Dropdown.prototype.render = function () {
    var $dropdown = $(
      '<span class="select2-dropdown">' +
        '<span class="select2-results"></span>' +
      '</span>'
    );

    $dropdown.attr('dir', this.options.get('dir'));

    this.$dropdown = $dropdown;

    return $dropdown;
  };

  Dropdown.prototype.bind = function () {
    // Should be implemented in subclasses
  };

  Dropdown.prototype.position = function ($dropdown, $container) {
    // Should be implmented in subclasses
  };

  Dropdown.prototype.destroy = function () {
    // Remove the dropdown from the DOM
    this.$dropdown.remove();
  };

  return Dropdown;
});

S2.define('select2/dropdown/search',[
  'jquery',
  '../utils'
], function ($, Utils) {
  function Search () { }

  Search.prototype.render = function (decorated) {
    var $rendered = decorated.call(this);

    var $search = $(
      '<span class="select2-search select2-search--dropdown">' +
        '<input class="select2-search__field" type="search" tabindex="-1"' +
        ' autocomplete="off" autocorrect="off" autocapitalize="none"' +
        ' spellcheck="false" role="textbox" />' +
      '</span>'
    );

    this.$searchContainer = $search;
    this.$search = $search.find('input');

    $rendered.prepend($search);

    return $rendered;
  };

  Search.prototype.bind = function (decorated, container, $container) {
    var self = this;

    decorated.call(this, container, $container);

    this.$search.on('keydown', function (evt) {
      self.trigger('keypress', evt);

      self._keyUpPrevented = evt.isDefaultPrevented();
    });

    // Workaround for browsers which do not support the `input` event
    // This will prevent double-triggering of events for browsers which support
    // both the `keyup` and `input` events.
    this.$search.on('input', function (evt) {
      // Unbind the duplicated `keyup` event
      $(this).off('keyup');
    });

    this.$search.on('keyup input', function (evt) {
      self.handleSearch(evt);
    });

    container.on('open', function () {
      self.$search.attr('tabindex', 0);

      self.$search.focus();

      window.setTimeout(function () {
        self.$search.focus();
      }, 0);
    });

    container.on('close', function () {
      self.$search.attr('tabindex', -1);

      self.$search.val('');
      self.$search.blur();
    });

    container.on('focus', function () {
      if (!container.isOpen()) {
        self.$search.focus();
      }
    });

    container.on('results:all', function (params) {
      if (params.query.term == null || params.query.term === '') {
        var showSearch = self.showSearch(params);

        if (showSearch) {
          self.$searchContainer.removeClass('select2-search--hide');
        } else {
          self.$searchContainer.addClass('select2-search--hide');
        }
      }
    });
  };

  Search.prototype.handleSearch = function (evt) {
    if (!this._keyUpPrevented) {
      var input = this.$search.val();

      this.trigger('query', {
        term: input
      });
    }

    this._keyUpPrevented = false;
  };

  Search.prototype.showSearch = function (_, params) {
    return true;
  };

  return Search;
});

S2.define('select2/dropdown/hidePlaceholder',[

], function () {
  function HidePlaceholder (decorated, $element, options, dataAdapter) {
    this.placeholder = this.normalizePlaceholder(options.get('placeholder'));

    decorated.call(this, $element, options, dataAdapter);
  }

  HidePlaceholder.prototype.append = function (decorated, data) {
    data.results = this.removePlaceholder(data.results);

    decorated.call(this, data);
  };

  HidePlaceholder.prototype.normalizePlaceholder = function (_, placeholder) {
    if (typeof placeholder === 'string') {
      placeholder = {
        id: '',
        text: placeholder
      };
    }

    return placeholder;
  };

  HidePlaceholder.prototype.removePlaceholder = function (_, data) {
    var modifiedData = data.slice(0);

    for (var d = data.length - 1; d >= 0; d--) {
      var item = data[d];

      if (this.placeholder.id === item.id) {
        modifiedData.splice(d, 1);
      }
    }

    return modifiedData;
  };

  return HidePlaceholder;
});

S2.define('select2/dropdown/infiniteScroll',[
  'jquery'
], function ($) {
  function InfiniteScroll (decorated, $element, options, dataAdapter) {
    this.lastParams = {};

    decorated.call(this, $element, options, dataAdapter);

    this.$loadingMore = this.createLoadingMore();
    this.loading = false;
  }

  InfiniteScroll.prototype.append = function (decorated, data) {
    this.$loadingMore.remove();
    this.loading = false;

    decorated.call(this, data);

    if (this.showLoadingMore(data)) {
      this.$results.append(this.$loadingMore);
    }
  };

  InfiniteScroll.prototype.bind = function (decorated, container, $container) {
    var self = this;

    decorated.call(this, container, $container);

    container.on('query', function (params) {
      self.lastParams = params;
      self.loading = true;
    });

    container.on('query:append', function (params) {
      self.lastParams = params;
      self.loading = true;
    });

    this.$results.on('scroll', function () {
      var isLoadMoreVisible = $.contains(
        document.documentElement,
        self.$loadingMore[0]
      );

      if (self.loading || !isLoadMoreVisible) {
        return;
      }

      var currentOffset = self.$results.offset().top +
        self.$results.outerHeight(false);
      var loadingMoreOffset = self.$loadingMore.offset().top +
        self.$loadingMore.outerHeight(false);

      if (currentOffset + 50 >= loadingMoreOffset) {
        self.loadMore();
      }
    });
  };

  InfiniteScroll.prototype.loadMore = function () {
    this.loading = true;

    var params = $.extend({}, {page: 1}, this.lastParams);

    params.page++;

    this.trigger('query:append', params);
  };

  InfiniteScroll.prototype.showLoadingMore = function (_, data) {
    return data.pagination && data.pagination.more;
  };

  InfiniteScroll.prototype.createLoadingMore = function () {
    var $option = $(
      '<li ' +
      'class="select2-results__option select2-results__option--load-more"' +
      'role="treeitem" aria-disabled="true"></li>'
    );

    var message = this.options.get('translations').get('loadingMore');

    $option.html(message(this.lastParams));

    return $option;
  };

  return InfiniteScroll;
});

S2.define('select2/dropdown/attachBody',[
  'jquery',
  '../utils'
], function ($, Utils) {
  function AttachBody (decorated, $element, options) {
    this.$dropdownParent = options.get('dropdownParent') || $(document.body);

    decorated.call(this, $element, options);
  }

  AttachBody.prototype.bind = function (decorated, container, $container) {
    var self = this;

    var setupResultsEvents = false;

    decorated.call(this, container, $container);

    container.on('open', function () {
      self._showDropdown();
      self._attachPositioningHandler(container);

      if (!setupResultsEvents) {
        setupResultsEvents = true;

        container.on('results:all', function () {
          self._positionDropdown();
          self._resizeDropdown();
        });

        container.on('results:append', function () {
          self._positionDropdown();
          self._resizeDropdown();
        });
      }
    });

    container.on('close', function () {
      self._hideDropdown();
      self._detachPositioningHandler(container);
    });

    this.$dropdownContainer.on('mousedown', function (evt) {
      evt.stopPropagation();
    });
  };

  AttachBody.prototype.destroy = function (decorated) {
    decorated.call(this);

    this.$dropdownContainer.remove();
  };

  AttachBody.prototype.position = function (decorated, $dropdown, $container) {
    // Clone all of the container classes
    $dropdown.attr('class', $container.attr('class'));

    $dropdown.removeClass('select2');
    $dropdown.addClass('select2-container--open');

    $dropdown.css({
      position: 'absolute',
      top: -999999
    });

    this.$container = $container;
  };

  AttachBody.prototype.render = function (decorated) {
    var $container = $('<span></span>');

    var $dropdown = decorated.call(this);
    $container.append($dropdown);

    this.$dropdownContainer = $container;

    return $container;
  };

  AttachBody.prototype._hideDropdown = function (decorated) {
    this.$dropdownContainer.detach();
  };

  AttachBody.prototype._attachPositioningHandler =
      function (decorated, container) {
    var self = this;

    var scrollEvent = 'scroll.select2.' + container.id;
    var resizeEvent = 'resize.select2.' + container.id;
    var orientationEvent = 'orientationchange.select2.' + container.id;

    var $watchers = this.$container.parents().filter(Utils.hasScroll);
    $watchers.each(function () {
      Utils.StoreData(this, 'select2-scroll-position', {
        x: $(this).scrollLeft(),
        y: $(this).scrollTop()
      });
    });

    $watchers.on(scrollEvent, function (ev) {
      var position = Utils.GetData(this, 'select2-scroll-position');
      $(this).scrollTop(position.y);
    });

    $(window).on(scrollEvent + ' ' + resizeEvent + ' ' + orientationEvent,
      function (e) {
      self._positionDropdown();
      self._resizeDropdown();
    });
  };

  AttachBody.prototype._detachPositioningHandler =
      function (decorated, container) {
    var scrollEvent = 'scroll.select2.' + container.id;
    var resizeEvent = 'resize.select2.' + container.id;
    var orientationEvent = 'orientationchange.select2.' + container.id;

    var $watchers = this.$container.parents().filter(Utils.hasScroll);
    $watchers.off(scrollEvent);

    $(window).off(scrollEvent + ' ' + resizeEvent + ' ' + orientationEvent);
  };

  AttachBody.prototype._positionDropdown = function () {
    var $window = $(window);

    var isCurrentlyAbove = this.$dropdown.hasClass('select2-dropdown--above');
    var isCurrentlyBelow = this.$dropdown.hasClass('select2-dropdown--below');

    var newDirection = null;

    var offset = this.$container.offset();

    offset.bottom = offset.top + this.$container.outerHeight(false);

    var container = {
      height: this.$container.outerHeight(false)
    };

    container.top = offset.top;
    container.bottom = offset.top + container.height;

    var dropdown = {
      height: this.$dropdown.outerHeight(false)
    };

    var viewport = {
      top: $window.scrollTop(),
      bottom: $window.scrollTop() + $window.height()
    };

    var enoughRoomAbove = viewport.top < (offset.top - dropdown.height);
    var enoughRoomBelow = viewport.bottom > (offset.bottom + dropdown.height);

    var css = {
      left: offset.left,
      top: container.bottom
    };

    // Determine what the parent element is to use for calciulating the offset
    var $offsetParent = this.$dropdownParent;

    // For statically positoned elements, we need to get the element
    // that is determining the offset
    if ($offsetParent.css('position') === 'static') {
      $offsetParent = $offsetParent.offsetParent();
    }

    var parentOffset = $offsetParent.offset();

    css.top -= parentOffset.top;
    css.left -= parentOffset.left;

    if (!isCurrentlyAbove && !isCurrentlyBelow) {
      newDirection = 'below';
    }

    if (!enoughRoomBelow && enoughRoomAbove && !isCurrentlyAbove) {
      newDirection = 'above';
    } else if (!enoughRoomAbove && enoughRoomBelow && isCurrentlyAbove) {
      newDirection = 'below';
    }

    if (newDirection == 'above' ||
      (isCurrentlyAbove && newDirection !== 'below')) {
      css.top = container.top - parentOffset.top - dropdown.height;
    }

    if (newDirection != null) {
      this.$dropdown
        .removeClass('select2-dropdown--below select2-dropdown--above')
        .addClass('select2-dropdown--' + newDirection);
      this.$container
        .removeClass('select2-container--below select2-container--above')
        .addClass('select2-container--' + newDirection);
    }

    this.$dropdownContainer.css(css);
  };

  AttachBody.prototype._resizeDropdown = function () {
    var css = {
      width: this.$container.outerWidth(false) + 'px'
    };

    if (this.options.get('dropdownAutoWidth')) {
      css.minWidth = css.width;
      css.position = 'relative';
      css.width = 'auto';
    }

    this.$dropdown.css(css);
  };

  AttachBody.prototype._showDropdown = function (decorated) {
    this.$dropdownContainer.appendTo(this.$dropdownParent);

    this._positionDropdown();
    this._resizeDropdown();
  };

  return AttachBody;
});

S2.define('select2/dropdown/minimumResultsForSearch',[

], function () {
  function countResults (data) {
    var count = 0;

    for (var d = 0; d < data.length; d++) {
      var item = data[d];

      if (item.children) {
        count += countResults(item.children);
      } else {
        count++;
      }
    }

    return count;
  }

  function MinimumResultsForSearch (decorated, $element, options, dataAdapter) {
    this.minimumResultsForSearch = options.get('minimumResultsForSearch');

    if (this.minimumResultsForSearch < 0) {
      this.minimumResultsForSearch = Infinity;
    }

    decorated.call(this, $element, options, dataAdapter);
  }

  MinimumResultsForSearch.prototype.showSearch = function (decorated, params) {
    if (countResults(params.data.results) < this.minimumResultsForSearch) {
      return false;
    }

    return decorated.call(this, params);
  };

  return MinimumResultsForSearch;
});

S2.define('select2/dropdown/selectOnClose',[
  '../utils'
], function (Utils) {
  function SelectOnClose () { }

  SelectOnClose.prototype.bind = function (decorated, container, $container) {
    var self = this;

    decorated.call(this, container, $container);

    container.on('close', function (params) {
      self._handleSelectOnClose(params);
    });
  };

  SelectOnClose.prototype._handleSelectOnClose = function (_, params) {
    if (params && params.originalSelect2Event != null) {
      var event = params.originalSelect2Event;

      // Don't select an item if the close event was triggered from a select or
      // unselect event
      if (event._type === 'select' || event._type === 'unselect') {
        return;
      }
    }

    var $highlightedResults = this.getHighlightedResults();

    // Only select highlighted results
    if ($highlightedResults.length < 1) {
      return;
    }

    var data = Utils.GetData($highlightedResults[0], 'data');

    // Don't re-select already selected resulte
    if (
      (data.element != null && data.element.selected) ||
      (data.element == null && data.selected)
    ) {
      return;
    }

    this.trigger('select', {
        data: data
    });
  };

  return SelectOnClose;
});

S2.define('select2/dropdown/closeOnSelect',[

], function () {
  function CloseOnSelect () { }

  CloseOnSelect.prototype.bind = function (decorated, container, $container) {
    var self = this;

    decorated.call(this, container, $container);

    container.on('select', function (evt) {
      self._selectTriggered(evt);
    });

    container.on('unselect', function (evt) {
      self._selectTriggered(evt);
    });
  };

  CloseOnSelect.prototype._selectTriggered = function (_, evt) {
    var originalEvent = evt.originalEvent;

    // Don't close if the control key is being held
    if (originalEvent && originalEvent.ctrlKey) {
      return;
    }

    this.trigger('close', {
      originalEvent: originalEvent,
      originalSelect2Event: evt
    });
  };

  return CloseOnSelect;
});

S2.define('select2/i18n/en',[],function () {
  // English
  return {
    errorLoading: function () {
      return 'The results could not be loaded.';
    },
    inputTooLong: function (args) {
      var overChars = args.input.length - args.maximum;

      var message = 'Please delete ' + overChars + ' character';

      if (overChars != 1) {
        message += 's';
      }

      return message;
    },
    inputTooShort: function (args) {
      var remainingChars = args.minimum - args.input.length;

      var message = 'Please enter ' + remainingChars + ' or more characters';

      return message;
    },
    loadingMore: function () {
      return 'Loading more results…';
    },
    maximumSelected: function (args) {
      var message = 'You can only select ' + args.maximum + ' item';

      if (args.maximum != 1) {
        message += 's';
      }

      return message;
    },
    noResults: function () {
      return 'No results found';
    },
    searching: function () {
      return 'Searching…';
    }
  };
});

S2.define('select2/defaults',[
  'jquery',
  'require',

  './results',

  './selection/single',
  './selection/multiple',
  './selection/placeholder',
  './selection/allowClear',
  './selection/search',
  './selection/eventRelay',

  './utils',
  './translation',
  './diacritics',

  './data/select',
  './data/array',
  './data/ajax',
  './data/tags',
  './data/tokenizer',
  './data/minimumInputLength',
  './data/maximumInputLength',
  './data/maximumSelectionLength',

  './dropdown',
  './dropdown/search',
  './dropdown/hidePlaceholder',
  './dropdown/infiniteScroll',
  './dropdown/attachBody',
  './dropdown/minimumResultsForSearch',
  './dropdown/selectOnClose',
  './dropdown/closeOnSelect',

  './i18n/en'
], function ($, require,

             ResultsList,

             SingleSelection, MultipleSelection, Placeholder, AllowClear,
             SelectionSearch, EventRelay,

             Utils, Translation, DIACRITICS,

             SelectData, ArrayData, AjaxData, Tags, Tokenizer,
             MinimumInputLength, MaximumInputLength, MaximumSelectionLength,

             Dropdown, DropdownSearch, HidePlaceholder, InfiniteScroll,
             AttachBody, MinimumResultsForSearch, SelectOnClose, CloseOnSelect,

             EnglishTranslation) {
  function Defaults () {
    this.reset();
  }

  Defaults.prototype.apply = function (options) {
    options = $.extend(true, {}, this.defaults, options);

    if (options.dataAdapter == null) {
      if (options.ajax != null) {
        options.dataAdapter = AjaxData;
      } else if (options.data != null) {
        options.dataAdapter = ArrayData;
      } else {
        options.dataAdapter = SelectData;
      }

      if (options.minimumInputLength > 0) {
        options.dataAdapter = Utils.Decorate(
          options.dataAdapter,
          MinimumInputLength
        );
      }

      if (options.maximumInputLength > 0) {
        options.dataAdapter = Utils.Decorate(
          options.dataAdapter,
          MaximumInputLength
        );
      }

      if (options.maximumSelectionLength > 0) {
        options.dataAdapter = Utils.Decorate(
          options.dataAdapter,
          MaximumSelectionLength
        );
      }

      if (options.tags) {
        options.dataAdapter = Utils.Decorate(options.dataAdapter, Tags);
      }

      if (options.tokenSeparators != null || options.tokenizer != null) {
        options.dataAdapter = Utils.Decorate(
          options.dataAdapter,
          Tokenizer
        );
      }

      if (options.query != null) {
        var Query = require(options.amdBase + 'compat/query');

        options.dataAdapter = Utils.Decorate(
          options.dataAdapter,
          Query
        );
      }

      if (options.initSelection != null) {
        var InitSelection = require(options.amdBase + 'compat/initSelection');

        options.dataAdapter = Utils.Decorate(
          options.dataAdapter,
          InitSelection
        );
      }
    }

    if (options.resultsAdapter == null) {
      options.resultsAdapter = ResultsList;

      if (options.ajax != null) {
        options.resultsAdapter = Utils.Decorate(
          options.resultsAdapter,
          InfiniteScroll
        );
      }

      if (options.placeholder != null) {
        options.resultsAdapter = Utils.Decorate(
          options.resultsAdapter,
          HidePlaceholder
        );
      }

      if (options.selectOnClose) {
        options.resultsAdapter = Utils.Decorate(
          options.resultsAdapter,
          SelectOnClose
        );
      }
    }

    if (options.dropdownAdapter == null) {
      if (options.multiple) {
        options.dropdownAdapter = Dropdown;
      } else {
        var SearchableDropdown = Utils.Decorate(Dropdown, DropdownSearch);

        options.dropdownAdapter = SearchableDropdown;
      }

      if (options.minimumResultsForSearch !== 0) {
        options.dropdownAdapter = Utils.Decorate(
          options.dropdownAdapter,
          MinimumResultsForSearch
        );
      }

      if (options.closeOnSelect) {
        options.dropdownAdapter = Utils.Decorate(
          options.dropdownAdapter,
          CloseOnSelect
        );
      }

      if (
        options.dropdownCssClass != null ||
        options.dropdownCss != null ||
        options.adaptDropdownCssClass != null
      ) {
        var DropdownCSS = require(options.amdBase + 'compat/dropdownCss');

        options.dropdownAdapter = Utils.Decorate(
          options.dropdownAdapter,
          DropdownCSS
        );
      }

      options.dropdownAdapter = Utils.Decorate(
        options.dropdownAdapter,
        AttachBody
      );
    }

    if (options.selectionAdapter == null) {
      if (options.multiple) {
        options.selectionAdapter = MultipleSelection;
      } else {
        options.selectionAdapter = SingleSelection;
      }

      // Add the placeholder mixin if a placeholder was specified
      if (options.placeholder != null) {
        options.selectionAdapter = Utils.Decorate(
          options.selectionAdapter,
          Placeholder
        );
      }

      if (options.allowClear) {
        options.selectionAdapter = Utils.Decorate(
          options.selectionAdapter,
          AllowClear
        );
      }

      if (options.multiple) {
        options.selectionAdapter = Utils.Decorate(
          options.selectionAdapter,
          SelectionSearch
        );
      }

      if (
        options.containerCssClass != null ||
        options.containerCss != null ||
        options.adaptContainerCssClass != null
      ) {
        var ContainerCSS = require(options.amdBase + 'compat/containerCss');

        options.selectionAdapter = Utils.Decorate(
          options.selectionAdapter,
          ContainerCSS
        );
      }

      options.selectionAdapter = Utils.Decorate(
        options.selectionAdapter,
        EventRelay
      );
    }

    if (typeof options.language === 'string') {
      // Check if the language is specified with a region
      if (options.language.indexOf('-') > 0) {
        // Extract the region information if it is included
        var languageParts = options.language.split('-');
        var baseLanguage = languageParts[0];

        options.language = [options.language, baseLanguage];
      } else {
        options.language = [options.language];
      }
    }

    if ($.isArray(options.language)) {
      var languages = new Translation();
      options.language.push('en');

      var languageNames = options.language;

      for (var l = 0; l < languageNames.length; l++) {
        var name = languageNames[l];
        var language = {};

        try {
          // Try to load it with the original name
          language = Translation.loadPath(name);
        } catch (e) {
          try {
            // If we couldn't load it, check if it wasn't the full path
            name = this.defaults.amdLanguageBase + name;
            language = Translation.loadPath(name);
          } catch (ex) {
            // The translation could not be loaded at all. Sometimes this is
            // because of a configuration problem, other times this can be
            // because of how Select2 helps load all possible translation files.
            if (options.debug && window.console && console.warn) {
              console.warn(
                'Select2: The language file for "' + name + '" could not be ' +
                'automatically loaded. A fallback will be used instead.'
              );
            }

            continue;
          }
        }

        languages.extend(language);
      }

      options.translations = languages;
    } else {
      var baseTranslation = Translation.loadPath(
        this.defaults.amdLanguageBase + 'en'
      );
      var customTranslation = new Translation(options.language);

      customTranslation.extend(baseTranslation);

      options.translations = customTranslation;
    }

    return options;
  };

  Defaults.prototype.reset = function () {
    function stripDiacritics (text) {
      // Used 'uni range + named function' from http://jsperf.com/diacritics/18
      function match(a) {
        return DIACRITICS[a] || a;
      }

      return text.replace(/[^\u0000-\u007E]/g, match);
    }

    function matcher (params, data) {
      // Always return the object if there is nothing to compare
      if ($.trim(params.term) === '') {
        return data;
      }

      // Do a recursive check for options with children
      if (data.children && data.children.length > 0) {
        // Clone the data object if there are children
        // This is required as we modify the object to remove any non-matches
        var match = $.extend(true, {}, data);

        // Check each child of the option
        for (var c = data.children.length - 1; c >= 0; c--) {
          var child = data.children[c];

          var matches = matcher(params, child);

          // If there wasn't a match, remove the object in the array
          if (matches == null) {
            match.children.splice(c, 1);
          }
        }

        // If any children matched, return the new object
        if (match.children.length > 0) {
          return match;
        }

        // If there were no matching children, check just the plain object
        return matcher(params, match);
      }

      var original = stripDiacritics(data.text).toUpperCase();
      var term = stripDiacritics(params.term).toUpperCase();

      // Check if the text contains the term
      if (original.indexOf(term) > -1) {
        return data;
      }

      // If it doesn't contain the term, don't return anything
      return null;
    }

    this.defaults = {
      amdBase: './',
      amdLanguageBase: './i18n/',
      closeOnSelect: true,
      debug: false,
      dropdownAutoWidth: false,
      escapeMarkup: Utils.escapeMarkup,
      language: EnglishTranslation,
      matcher: matcher,
      minimumInputLength: 0,
      maximumInputLength: 0,
      maximumSelectionLength: 0,
      minimumResultsForSearch: 0,
      selectOnClose: false,
      sorter: function (data) {
        return data;
      },
      templateResult: function (result) {
        return result.text;
      },
      templateSelection: function (selection) {
        return selection.text;
      },
      theme: 'default',
      width: 'resolve'
    };
  };

  Defaults.prototype.set = function (key, value) {
    var camelKey = $.camelCase(key);

    var data = {};
    data[camelKey] = value;

    var convertedData = Utils._convertData(data);

    $.extend(true, this.defaults, convertedData);
  };

  var defaults = new Defaults();

  return defaults;
});

S2.define('select2/options',[
  'require',
  'jquery',
  './defaults',
  './utils'
], function (require, $, Defaults, Utils) {
  function Options (options, $element) {
    this.options = options;

    if ($element != null) {
      this.fromElement($element);
    }

    this.options = Defaults.apply(this.options);

    if ($element && $element.is('input')) {
      var InputCompat = require(this.get('amdBase') + 'compat/inputData');

      this.options.dataAdapter = Utils.Decorate(
        this.options.dataAdapter,
        InputCompat
      );
    }
  }

  Options.prototype.fromElement = function ($e) {
    var excludedData = ['select2'];

    if (this.options.multiple == null) {
      this.options.multiple = $e.prop('multiple');
    }

    if (this.options.disabled == null) {
      this.options.disabled = $e.prop('disabled');
    }

    if (this.options.language == null) {
      if ($e.prop('lang')) {
        this.options.language = $e.prop('lang').toLowerCase();
      } else if ($e.closest('[lang]').prop('lang')) {
        this.options.language = $e.closest('[lang]').prop('lang');
      }
    }

    if (this.options.dir == null) {
      if ($e.prop('dir')) {
        this.options.dir = $e.prop('dir');
      } else if ($e.closest('[dir]').prop('dir')) {
        this.options.dir = $e.closest('[dir]').prop('dir');
      } else {
        this.options.dir = 'ltr';
      }
    }

    $e.prop('disabled', this.options.disabled);
    $e.prop('multiple', this.options.multiple);

    if (Utils.GetData($e[0], 'select2Tags')) {
      if (this.options.debug && window.console && console.warn) {
        console.warn(
          'Select2: The `data-select2-tags` attribute has been changed to ' +
          'use the `data-data` and `data-tags="true"` attributes and will be ' +
          'removed in future versions of Select2.'
        );
      }

      Utils.StoreData($e[0], 'data', Utils.GetData($e[0], 'select2Tags'));
      Utils.StoreData($e[0], 'tags', true);
    }

    if (Utils.GetData($e[0], 'ajaxUrl')) {
      if (this.options.debug && window.console && console.warn) {
        console.warn(
          'Select2: The `data-ajax-url` attribute has been changed to ' +
          '`data-ajax--url` and support for the old attribute will be removed' +
          ' in future versions of Select2.'
        );
      }

      $e.attr('ajax--url', Utils.GetData($e[0], 'ajaxUrl'));
      Utils.StoreData($e[0], 'ajax-Url', Utils.GetData($e[0], 'ajaxUrl'));
	  
    }

    var dataset = {};

    // Prefer the element's `dataset` attribute if it exists
    // jQuery 1.x does not correctly handle data attributes with multiple dashes
    if ($.fn.jquery && $.fn.jquery.substr(0, 2) == '1.' && $e[0].dataset) {
      dataset = $.extend(true, {}, $e[0].dataset, Utils.GetData($e[0]));
    } else {
      dataset = Utils.GetData($e[0]);
    }

    var data = $.extend(true, {}, dataset);

    data = Utils._convertData(data);

    for (var key in data) {
      if ($.inArray(key, excludedData) > -1) {
        continue;
      }

      if ($.isPlainObject(this.options[key])) {
        $.extend(this.options[key], data[key]);
      } else {
        this.options[key] = data[key];
      }
    }

    return this;
  };

  Options.prototype.get = function (key) {
    return this.options[key];
  };

  Options.prototype.set = function (key, val) {
    this.options[key] = val;
  };

  return Options;
});

S2.define('select2/core',[
  'jquery',
  './options',
  './utils',
  './keys'
], function ($, Options, Utils, KEYS) {
  var Select2 = function ($element, options) {
    if (Utils.GetData($element[0], 'select2') != null) {
      Utils.GetData($element[0], 'select2').destroy();
    }

    this.$element = $element;

    this.id = this._generateId($element);

    options = options || {};

    this.options = new Options(options, $element);

    Select2.__super__.constructor.call(this);

    // Set up the tabindex

    var tabindex = $element.attr('tabindex') || 0;
    Utils.StoreData($element[0], 'old-tabindex', tabindex);
    $element.attr('tabindex', '-1');

    // Set up containers and adapters

    var DataAdapter = this.options.get('dataAdapter');
    this.dataAdapter = new DataAdapter($element, this.options);

    var $container = this.render();

    this._placeContainer($container);

    var SelectionAdapter = this.options.get('selectionAdapter');
    this.selection = new SelectionAdapter($element, this.options);
    this.$selection = this.selection.render();

    this.selection.position(this.$selection, $container);

    var DropdownAdapter = this.options.get('dropdownAdapter');
    this.dropdown = new DropdownAdapter($element, this.options);
    this.$dropdown = this.dropdown.render();

    this.dropdown.position(this.$dropdown, $container);

    var ResultsAdapter = this.options.get('resultsAdapter');
    this.results = new ResultsAdapter($element, this.options, this.dataAdapter);
    this.$results = this.results.render();

    this.results.position(this.$results, this.$dropdown);

    // Bind events

    var self = this;

    // Bind the container to all of the adapters
    this._bindAdapters();

    // Register any DOM event handlers
    this._registerDomEvents();

    // Register any internal event handlers
    this._registerDataEvents();
    this._registerSelectionEvents();
    this._registerDropdownEvents();
    this._registerResultsEvents();
    this._registerEvents();

    // Set the initial state
    this.dataAdapter.current(function (initialData) {
      self.trigger('selection:update', {
        data: initialData
      });
    });

    // Hide the original select
    $element.addClass('select2-hidden-accessible');
    $element.attr('aria-hidden', 'true');

    // Synchronize any monitored attributes
    this._syncAttributes();

    Utils.StoreData($element[0], 'select2', this);

    // Ensure backwards compatibility with $element.data('select2').
    $element.data('select2', this);
  };

  Utils.Extend(Select2, Utils.Observable);

  Select2.prototype._generateId = function ($element) {
    var id = '';

    if ($element.attr('id') != null) {
      id = $element.attr('id');
    } else if ($element.attr('name') != null) {
      id = $element.attr('name') + '-' + Utils.generateChars(2);
    } else {
      id = Utils.generateChars(4);
    }

    id = id.replace(/(:|\.|\[|\]|,)/g, '');
    id = 'select2-' + id;

    return id;
  };

  Select2.prototype._placeContainer = function ($container) {
    $container.insertAfter(this.$element);

    var width = this._resolveWidth(this.$element, this.options.get('width'));

    if (width != null) {
      $container.css('width', width);
    }
  };

  Select2.prototype._resolveWidth = function ($element, method) {
    var WIDTH = /^width:(([-+]?([0-9]*\.)?[0-9]+)(px|em|ex|%|in|cm|mm|pt|pc))/i;

    if (method == 'resolve') {
      var styleWidth = this._resolveWidth($element, 'style');

      if (styleWidth != null) {
        return styleWidth;
      }

      return this._resolveWidth($element, 'element');
    }

    if (method == 'element') {
      var elementWidth = $element.outerWidth(false);

      if (elementWidth <= 0) {
        return 'auto';
      }

      return elementWidth + 'px';
    }

    if (method == 'style') {
      var style = $element.attr('style');

      if (typeof(style) !== 'string') {
        return null;
      }

      var attrs = style.split(';');

      for (var i = 0, l = attrs.length; i < l; i = i + 1) {
        var attr = attrs[i].replace(/\s/g, '');
        var matches = attr.match(WIDTH);

        if (matches !== null && matches.length >= 1) {
          return matches[1];
        }
      }

      return null;
    }

    return method;
  };

  Select2.prototype._bindAdapters = function () {
    this.dataAdapter.bind(this, this.$container);
    this.selection.bind(this, this.$container);

    this.dropdown.bind(this, this.$container);
    this.results.bind(this, this.$container);
  };

  Select2.prototype._registerDomEvents = function () {
    var self = this;

    this.$element.on('change.select2', function () {
      self.dataAdapter.current(function (data) {
        self.trigger('selection:update', {
          data: data
        });
      });
    });

    this.$element.on('focus.select2', function (evt) {
      self.trigger('focus', evt);
    });

    this._syncA = Utils.bind(this._syncAttributes, this);
    this._syncS = Utils.bind(this._syncSubtree, this);

    if (this.$element[0].attachEvent) {
      this.$element[0].attachEvent('onpropertychange', this._syncA);
    }

    var observer = window.MutationObserver ||
      window.WebKitMutationObserver ||
      window.MozMutationObserver
    ;

    if (observer != null) {
      this._observer = new observer(function (mutations) {
        $.each(mutations, self._syncA);
        $.each(mutations, self._syncS);
      });
      this._observer.observe(this.$element[0], {
        attributes: true,
        childList: true,
        subtree: false
      });
    } else if (this.$element[0].addEventListener) {
      this.$element[0].addEventListener(
        'DOMAttrModified',
        self._syncA,
        false
      );
      this.$element[0].addEventListener(
        'DOMNodeInserted',
        self._syncS,
        false
      );
      this.$element[0].addEventListener(
        'DOMNodeRemoved',
        self._syncS,
        false
      );
    }
  };

  Select2.prototype._registerDataEvents = function () {
    var self = this;

    this.dataAdapter.on('*', function (name, params) {
      self.trigger(name, params);
    });
  };

  Select2.prototype._registerSelectionEvents = function () {
    var self = this;
    var nonRelayEvents = ['toggle', 'focus'];

    this.selection.on('toggle', function () {
      self.toggleDropdown();
    });

    this.selection.on('focus', function (params) {
      self.focus(params);
    });

    this.selection.on('*', function (name, params) {
      if ($.inArray(name, nonRelayEvents) !== -1) {
        return;
      }

      self.trigger(name, params);
    });
  };

  Select2.prototype._registerDropdownEvents = function () {
    var self = this;

    this.dropdown.on('*', function (name, params) {
      self.trigger(name, params);
    });
  };

  Select2.prototype._registerResultsEvents = function () {
    var self = this;

    this.results.on('*', function (name, params) {
      self.trigger(name, params);
    });
  };

  Select2.prototype._registerEvents = function () {
    var self = this;

    this.on('open', function () {
      self.$container.addClass('select2-container--open');
    });

    this.on('close', function () {
      self.$container.removeClass('select2-container--open');
    });

    this.on('enable', function () {
      self.$container.removeClass('select2-container--disabled');
    });

    this.on('disable', function () {
      self.$container.addClass('select2-container--disabled');
    });

    this.on('blur', function () {
      self.$container.removeClass('select2-container--focus');
    });

    this.on('query', function (params) {
      if (!self.isOpen()) {
        self.trigger('open', {});
      }

      this.dataAdapter.query(params, function (data) {
        self.trigger('results:all', {
          data: data,
          query: params
        });
      });
    });

    this.on('query:append', function (params) {
      this.dataAdapter.query(params, function (data) {
        self.trigger('results:append', {
          data: data,
          query: params
        });
      });
    });

    this.on('keypress', function (evt) {
      var key = evt.which;

      if (self.isOpen()) {
        if (key === KEYS.ESC || key === KEYS.TAB ||
            (key === KEYS.UP && evt.altKey)) {
          self.close();

          evt.preventDefault();
        } else if (key === KEYS.ENTER) {
          self.trigger('results:select', {});

          evt.preventDefault();
        } else if ((key === KEYS.SPACE && evt.ctrlKey)) {
          self.trigger('results:toggle', {});

          evt.preventDefault();
        } else if (key === KEYS.UP) {
          self.trigger('results:previous', {});

          evt.preventDefault();
        } else if (key === KEYS.DOWN) {
          self.trigger('results:next', {});

          evt.preventDefault();
        }
      } else {
        if (key === KEYS.ENTER || key === KEYS.SPACE ||
            (key === KEYS.DOWN && evt.altKey)) {
          self.open();

          evt.preventDefault();
        }
      }
    });
  };

  Select2.prototype._syncAttributes = function () {
    this.options.set('disabled', this.$element.prop('disabled'));

    if (this.options.get('disabled')) {
      if (this.isOpen()) {
        this.close();
      }

      this.trigger('disable', {});
    } else {
      this.trigger('enable', {});
    }
  };

  Select2.prototype._syncSubtree = function (evt, mutations) {
    var changed = false;
    var self = this;

    // Ignore any mutation events raised for elements that aren't options or
    // optgroups. This handles the case when the select element is destroyed
    if (
      evt && evt.target && (
        evt.target.nodeName !== 'OPTION' && evt.target.nodeName !== 'OPTGROUP'
      )
    ) {
      return;
    }

    if (!mutations) {
      // If mutation events aren't supported, then we can only assume that the
      // change affected the selections
      changed = true;
    } else if (mutations.addedNodes && mutations.addedNodes.length > 0) {
      for (var n = 0; n < mutations.addedNodes.length; n++) {
        var node = mutations.addedNodes[n];

        if (node.selected) {
          changed = true;
        }
      }
    } else if (mutations.removedNodes && mutations.removedNodes.length > 0) {
      changed = true;
    }

    // Only re-pull the data if we think there is a change
    if (changed) {
      this.dataAdapter.current(function (currentData) {
        self.trigger('selection:update', {
          data: currentData
        });
      });
    }
  };

  /**
   * Override the trigger method to automatically trigger pre-events when
   * there are events that can be prevented.
   */
  Select2.prototype.trigger = function (name, args) {
    var actualTrigger = Select2.__super__.trigger;
    var preTriggerMap = {
      'open': 'opening',
      'close': 'closing',
      'select': 'selecting',
      'unselect': 'unselecting',
      'clear': 'clearing'
    };

    if (args === undefined) {
      args = {};
    }

    if (name in preTriggerMap) {
      var preTriggerName = preTriggerMap[name];
      var preTriggerArgs = {
        prevented: false,
        name: name,
        args: args
      };

      actualTrigger.call(this, preTriggerName, preTriggerArgs);

      if (preTriggerArgs.prevented) {
        args.prevented = true;

        return;
      }
    }

    actualTrigger.call(this, name, args);
  };

  Select2.prototype.toggleDropdown = function () {
    if (this.options.get('disabled')) {
      return;
    }

    if (this.isOpen()) {
      this.close();
    } else {
      this.open();
    }
  };

  Select2.prototype.open = function () {
    if (this.isOpen()) {
      return;
    }

    this.trigger('query', {});
  };

  Select2.prototype.close = function () {
    if (!this.isOpen()) {
      return;
    }

    this.trigger('close', {});
  };

  Select2.prototype.isOpen = function () {
    return this.$container.hasClass('select2-container--open');
  };

  Select2.prototype.hasFocus = function () {
    return this.$container.hasClass('select2-container--focus');
  };

  Select2.prototype.focus = function (data) {
    // No need to re-trigger focus events if we are already focused
    if (this.hasFocus()) {
      return;
    }

    this.$container.addClass('select2-container--focus');
    this.trigger('focus', {});
  };

  Select2.prototype.enable = function (args) {
    if (this.options.get('debug') && window.console && console.warn) {
      console.warn(
        'Select2: The `select2("enable")` method has been deprecated and will' +
        ' be removed in later Select2 versions. Use $element.prop("disabled")' +
        ' instead.'
      );
    }

    if (args == null || args.length === 0) {
      args = [true];
    }

    var disabled = !args[0];

    this.$element.prop('disabled', disabled);
  };

  Select2.prototype.data = function () {
    if (this.options.get('debug') &&
        arguments.length > 0 && window.console && console.warn) {
      console.warn(
        'Select2: Data can no longer be set using `select2("data")`. You ' +
        'should consider setting the value instead using `$element.val()`.'
      );
    }

    var data = [];

    this.dataAdapter.current(function (currentData) {
      data = currentData;
    });

    return data;
  };

  Select2.prototype.val = function (args) {
    if (this.options.get('debug') && window.console && console.warn) {
      console.warn(
        'Select2: The `select2("val")` method has been deprecated and will be' +
        ' removed in later Select2 versions. Use $element.val() instead.'
      );
    }

    if (args == null || args.length === 0) {
      return this.$element.val();
    }

    var newVal = args[0];

    if ($.isArray(newVal)) {
      newVal = $.map(newVal, function (obj) {
        return obj.toString();
      });
    }

    this.$element.val(newVal).trigger('change');
  };

  Select2.prototype.destroy = function () {
    this.$container.remove();

    if (this.$element[0].detachEvent) {
      this.$element[0].detachEvent('onpropertychange', this._syncA);
    }

    if (this._observer != null) {
      this._observer.disconnect();
      this._observer = null;
    } else if (this.$element[0].removeEventListener) {
      this.$element[0]
        .removeEventListener('DOMAttrModified', this._syncA, false);
      this.$element[0]
        .removeEventListener('DOMNodeInserted', this._syncS, false);
      this.$element[0]
        .removeEventListener('DOMNodeRemoved', this._syncS, false);
    }

    this._syncA = null;
    this._syncS = null;

    this.$element.off('.select2');
    this.$element.attr('tabindex',
    Utils.GetData(this.$element[0], 'old-tabindex'));

    this.$element.removeClass('select2-hidden-accessible');
    this.$element.attr('aria-hidden', 'false');
    Utils.RemoveData(this.$element[0]);
    this.$element.removeData('select2');

    this.dataAdapter.destroy();
    this.selection.destroy();
    this.dropdown.destroy();
    this.results.destroy();

    this.dataAdapter = null;
    this.selection = null;
    this.dropdown = null;
    this.results = null;
  };

  Select2.prototype.render = function () {
    var $container = $(
      '<span class="select2 select2-container">' +
        '<span class="selection"></span>' +
        '<span class="dropdown-wrapper" aria-hidden="true"></span>' +
      '</span>'
    );

    $container.attr('dir', this.options.get('dir'));

    this.$container = $container;

    this.$container.addClass('select2-container--' + this.options.get('theme'));

    Utils.StoreData($container[0], 'element', this.$element);

    return $container;
  };

  return Select2;
});

S2.define('jquery-mousewheel',[
  'jquery'
], function ($) {
  // Used to shim jQuery.mousewheel for non-full builds.
  return $;
});

S2.define('jquery.select2',[
  'jquery',
  'jquery-mousewheel',

  './select2/core',
  './select2/defaults',
  './select2/utils'
], function ($, _, Select2, Defaults, Utils) {
  if ($.fn.select2 == null) {
    // All methods that should return the element
    var thisMethods = ['open', 'close', 'destroy'];

    $.fn.select2 = function (options) {
      options = options || {};

      if (typeof options === 'object') {
        this.each(function () {
          var instanceOptions = $.extend(true, {}, options);

          var instance = new Select2($(this), instanceOptions);
        });

        return this;
      } else if (typeof options === 'string') {
        var ret;
        var args = Array.prototype.slice.call(arguments, 1);

        this.each(function () {
          var instance = Utils.GetData(this, 'select2');

          if (instance == null && window.console && console.error) {
            console.error(
              'The select2(\'' + options + '\') method was called on an ' +
              'element that is not using Select2.'
            );
          }

          ret = instance[options].apply(instance, args);
        });

        // Check if we should be returning `this`
        if ($.inArray(options, thisMethods) > -1) {
          return this;
        }

        return ret;
      } else {
        throw new Error('Invalid arguments for Select2: ' + options);
      }
    };
  }

  if ($.fn.select2.defaults == null) {
    $.fn.select2.defaults = Defaults;
  }

  return Select2;
});

  // Return the AMD loader configuration so it can be used outside of this file
  return {
    define: S2.define,
    require: S2.require
  };
}());

  // Autoload the jQuery bindings
  // We know that all of the modules exist above this, so we're safe
  var select2 = S2.require('jquery.select2');

  // Hold the AMD module references on the jQuery function that was just loaded
  // This allows Select2 to use the internal loader outside of this file, such
  // as in the language files.
  jQuery.fn.select2.amd = S2;

  // Return the Select2 instance for anyone who is importing it.
  return select2;
}));

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],24:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * A component displayed by a VizEngine.
 *
 * @constructor
 * @param {string} container - the ID of the injected div
 * @param {Object} data - the data used in the visualization
 * @param {Object} callbacks - Dictionary of the callbacks available
 *                  Contains : engine - the parent engine instance
 */

// Abstract class
var VizTool = function () {
    function VizTool(container, data, callbacks) {
        _classCallCheck(this, VizTool);

        // Avoid abstract instance
        if (new.target === VizTool) {
            throw new TypeError("Cannot construct Abstract instances directly");
        }
        this.name = "";
        this.data = data;
        this.container = container;
        this.callbacks = callbacks;
    }

    /**
     * Init a VizTool : collect and format the data (if necessary) then render the VizTool
     */


    _createClass(VizTool, [{
        key: "display",
        value: function display() {
            throw new Error("display method of VizTool " + this.name + " has to be overridden");
        }

        /**
         * Wake up (Restore) the VizTool with minimal recalculation.
         */

    }, {
        key: "wakeUp",
        value: function wakeUp() {
            throw new Error("wakeUp method of VizTool " + this.name + " has to be overridden");
        }

        /**
         * Persist the VizTool for a quick restoration.
         */

    }, {
        key: "sleep",
        value: function sleep() {}
        // If actions must be performed before hiding a viztool, there shall be done here
        // Example: remove pending requests, kill timers, ...


        /**
         * Common callback to add a Viz to parent VizEngine
         * @param {string} name - the name of the VizTool to add
         * @param {*} data - the data used by the added VizTool
         */

    }, {
        key: "addViz",
        value: function addViz(name, data) {
            this.callbacks.engine.addViz(name, data, false);
        }
    }]);

    return VizTool;
}();

module.exports = VizTool;

},{}],25:[function(require,module,exports){
(function (global){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/*globals require, module*/
/*exported D3CanvasCurve*/

//less/style integration

require(40);
require(42);
require(47);

//dependencies
var d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null),
    LIGViz = require(27),
    DataProvider = require(30),
    scaleHelper = require(34),
    CanvasCurveDrawer = require(26),
    $ = (typeof window !== "undefined" ? window['$'] : typeof global !== "undefined" ? global['$'] : null),
    _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null),
    LigLogger = require(33);

//const TsPointsCache = require('../Tools/TsPointsCache');

var _formatTime = function _formatTime(d) {
    'use strict';

    return new Date(d).toISOString().substr(0, 23);
};

var computeHover = function computeHover(point, ts) {
    'use strict';

    var computed = Object.assign({}, point),
        patterns = [];
    Object.assign(computed, {
        tsuid: ts.tsuid,
        key: ts.key,
        color: ts.color,
        variable: ts.md.variable,
        idx: point.idx
    });
    var locations = void 0,
        pattern = void 0;
    for (var k = 0; k < (ts.patterns || []).length; k++) {
        /*jshint loopfunc: true */
        pattern = ts.patterns[k];
        locations = pattern.locations.filter(function (l) {
            return point.idx >= l && point.idx <= l + pattern.length;
        });
        if (locations.length > 0 && pattern.activated) {
            patterns.push(Object.assign({}, pattern, { locations: locations }));
        }
    }
    computed.patterns = patterns;
    return computed;
};

var getText = function getText(newhover) {
    'use strict';

    var txt = '<div><b>TS: ' + newhover.tsuid + '</b></div><div>idx: ' + newhover.idx + '</div><div>value: ' + newhover.value + '</div><div>time: ' + _formatTime(newhover.timestamp) + '</div>';

    if (newhover.size > 1) {
        txt += '<div>size : ' + newhover.size + ' </div><div>minmax : [' + [newhover.min, newhover.max] + ']</div>';
    }
    txt += '<div>variable : ' + newhover.variable + ' </div>';

    var ptxt = '<div>patterns : ' + newhover.patterns.map(function (p) {
        return p.regex + ' <br> ' + p.locations;
    }).join(', ') + '</div>';

    if (newhover.patterns.length > 0) {
        txt += ptxt;
    }
    return txt;
};

/**
 * Visualization showing timeseries using canvas
 *
 * @constructor
 * @param {string} containerId - the ID of the injected div
 * @param {Object} data - the data used in the visualization
 */

var CanvasCurve = function (_LIGViz) {
    _inherits(CanvasCurve, _LIGViz);

    function CanvasCurve(containerId, data) {
        _classCallCheck(this, CanvasCurve);

        var _this = _possibleConstructorReturn(this, (CanvasCurve.__proto__ || Object.getPrototypeOf(CanvasCurve)).call(this, containerId, data));
        // Call super-class constructor


        _this.containerId = containerId;
        _this.md = {};
        _this.name = 'CanvasCurve';
        _this.container = d3.select('#' + containerId).html('');
        _this.graph = null;
        _this.logger = new LigLogger('lig:curve');
        _this.dispatch = d3.dispatch('zoomed', 'reset', 'syncChange', 'removed', 'tsRemoved', 'change', 'pointmouseenter', 'pointmouseleave');
        _this.context = {
            start: null,
            stop: null,
            duration: 700,
            hover: null,
            x: null,
            scale: 1,
            indrawing: false,
            xInterval: [0, 0],
            xMinMax: [0, 0],
            timestampMinMax: [0, 0],
            indexMinMax: [0, 0],
            variables: [],
            yMinMax1: [undefined, undefined],
            yMinMax2: null
        };
        _this.scales = {};
        _this._options = {
            mode: 'timestamp', // {index|timestamp}
            margin: {
                left: 40,
                right: 40,
                top: 20,
                bottom: 0
            },
            minimap: {
                height: 15,
                show: true,
                paddingBottom: 5
            },
            weights: {
                height: 15,
                show: false,
                minColor: 'rgb(231, 228, 228)',
                maxColor: 'darkgrey'
            },
            dots: {
                radius: 1,
                hoverRadius: 5,
                highlightRadius: 2,
                mousePrecision: 2
            },
            lines: {
                width: 1,
                hoverWidth: 3
            },
            tooltip: {
                show: true, // per default
                position: 'top' // {auto|top|bottom}
            },
            minHTickDist: 10,
            minTickDist: 20, // use to know if we show tick as following event or not
            once: false, // used to know if we use cache or not
            autosize: true, // used to know if we recompute height and width dynamically
            height: 500,
            width: 1200,
            xMinMax: null,
            yMinMax: null,
            showAxis: true, // option to know if we show/hide axis
            showVariable: true, // option to know if we show/hide legends related to variables
            transform: null, // zoom object
            maxCurves: 100, // number max of curves
            dataProvider: new DataProvider(),
            debugMode: true
        };
        _this._inputs = {};
        _this.dom = {
            canvas: null,
            canvasMouse: null,
            canvasHidden: null,
            container: null,
            focus: null,
            minimap: null,
            zoom: null,
            brush: null
        };
        _this.obj = {
            dispatchZoom: true, // flag to know if we dispath the zoom Event
            canvasContext: null,
            canvasMouseContext: null,
            zoom: null,
            canvasDrawer: new CanvasCurveDrawer(),
            colors: d3.scaleOrdinal(d3.schemeCategory10),
            prevDomain: null,
            currentPointsRequest: null, // current promise that is the points requesting
            cacheMetadata: null, // property used to store async data (metadata) and use as cache
            cachePoints: null, // property used to store async data (points) and use as cache
            cache: {
                metadata: {},
                start: null,
                end: null,
                points: {}
            }
        };
        return _this;
    }

    _createClass(CanvasCurve, [{
        key: '_computeContext',
        value: function _computeContext() {
            var self = this;
            self.context = Object.assign(self.context || {}, {
                xMinMax: [0, d3.max(self.data.map(function (d) {
                    return +d.md.size - 1;
                }))],
                timestampMinMax: [d3.min(self.data.map(function (d) {
                    return +d.md.start_date;
                })), d3.max(self.data.map(function (d) {
                    return +d.md.end_date;
                }))],
                indexMinMax: [0, d3.max(self.data.map(function (d) {
                    return +d.md.end;
                }))],
                variables: Array.from(new Set(self.data.map(function (d) {
                    return d.md.variable;
                }))),
                yMinMax1: [undefined, undefined],
                yMinMax2: null,
                yTicks1: [],
                yTicks2: []
            });

            self.context.variables.forEach(function (variable, i) {
                // filter ts with same variable
                var ts_list = self.data.filter(function (d) {
                    return d.md.variable === variable;
                });
                // compute min/max
                var min = void 0,
                    max = void 0;
                if (self._options.yMinMax && self._options.yMinMax[variable]) {
                    min = self._options.yMinMax[variable][0];
                    max = self._options.yMinMax[variable][1];
                }
                if (_.isUndefined(min) || _.isUndefined(max)) {
                    min = d3.min(ts_list.map(function (d) {
                        return +d.md.min;
                    }));
                    max = d3.max(ts_list.map(function (d) {
                        return +d.md.max;
                    }));
                }
                if (_.isUndefined(min) || _.isUndefined(max)) {
                    self.logger.error('min (' + min + ') and (' + max + ') must be defined');
                    return;
                }
                // If there is bps, use it to adjust min / max
                if (self._inputs.breakpoints && self._inputs.breakpoints.findIndex(function (t) {
                    return t.key === variable;
                }) > -1) {
                    self.logger.log('has bps, use it to define minmax', self._inputs.breakpoints);
                    var bps = self._inputs.breakpoints[self._inputs.breakpoints.findIndex(function (t) {
                        return t.key === variable;
                    })].bps;
                    if (bps) {
                        var minBps = bps[0],
                            maxBps = bps[bps.length - 1],
                            tickValues = [];
                        bps.forEach(function (m) {
                            tickValues.push(m.min + (m.max - m.min) / 2);
                            if (maxBps.key !== m.key) {
                                tickValues.push(m.max);
                            }
                        });

                        self.context['yMinMax' + (i + 1)] = [minBps.min, maxBps.max];
                        self.context['yTicks' + (i + 1)] = tickValues;
                    }
                }
                // else apply diff of 10 pourcents to make the graph breath
                else {
                        self.logger.log('no bps, use values to define minmax', self._inputs.breakpoints, self._inputs);
                        // add 10% (y)
                        var diff = (max - min) * (10 / 100);
                        // apply it
                        self.context['yMinMax' + (i + 1)] = [min - diff, max + diff];
                    }
            });
            self.logger.log('context', self.context);
        }

        /**
         * Display widtget
         */

    }, {
        key: 'display',
        value: function display() {
            var self = this;
            self.logger.log('display \'' + self.containerId + '\'');
            // assign d3 container element
            this.container = d3.select('#' + this.containerId);

            // rseet content
            this.container.html('').classed('in-loading', true);

            this.init();
            this.update();

            return Promise.resolve(self.obj.cache.metadata).then(function (cachedMetadata) {
                var isInCache = self.data.findIndex(function (d) {
                    return !cachedMetadata[d.tsuid];
                }) === -1;
                if (isInCache) {
                    return cachedMetadata;
                }
                return self._options.dataProvider.getMetaData(self.data, self._inputs).then(function (metadata) {
                    return (self.obj.cache.metadata = metadata) && metadata;
                });
            }).then(function (metadata) {
                self.md = metadata;
                self.data.forEach(function (d) {
                    d.filtered = [];
                    d.scope = {};
                    d.concerned = null;
                    d.md = self.md[d.tsuid] || self.md[d.tsuid.split('#')[0]];
                });
                self.container.classed('in-loading', false);
                return self.render();
            });
        }
    }, {
        key: 'wakeUp',
        value: function wakeUp() {
            this.display();
        }

        /**
         * getComputedPoints with current context (scale, range)
         * @return {Promise} Promise with points as result
         */

    }, {
        key: 'getComputedPoints',
        value: function getComputedPoints() {
            var self = this;
            // beginning
            var start = self.scales.x.domain()[0],

            // ending
            end = self.scales.x.domain()[1];
            // get the diff in time between the beginning and the end
            var diff = Math.abs(end - start),

            // get the diff in px between the beginning and the end
            width = self.scales.x.range()[1] - self.scales.x.range()[0],
                height = self.scales.y.range()[1] - self.scales.y.range()[0],
                concerned = self.data.filter(function (d) {
                return d.filtered && (!d.scope || d.scope.start !== start || d.scope.width !== width || d.scope.height !== height);
            });

            // if there is no concerned ts, return an empty array
            if (concerned.length === 0) {
                return Promise.resolve([]);
            }

            var fn = function fn() {
                //console.log('fetching points', self.containerId, concerned.map(d =>d.tsuid), start, end, width, diff, self._options.mode);
                // loader on
                self.dom.subContainer.classed('in-loading', true);

                // get async data
                promise = Promise.resolve(self.obj.cache.points).then(function (data) {
                    var isInCache = self.data.findIndex(function (d) {
                        return !data[d.tsuid];
                    }) === -1;
                    // if cache is enough
                    if (isInCache && (self.obj.cache.start === start && self.obj.cache.end === end || self._options.once)) {
                        //console.log('cache is enough', self.data.map(d=>d.tsuid), self.obj.cache.points);
                        return self.data.map(function (d) {
                            return data[d.tsuid];
                        });
                    }
                    //else {
                    //console.log('cache is not enough', self.data.map(d=>d.tsuid), self.obj.cache.points);
                    //}
                    // reset old promise if it exists and cencellable
                    if (self.obj.promise && self.obj.promise.cancel) {
                        self.obj.promise.cancel();
                    }
                    // initialize new async promise
                    self.obj.promise = self._options.dataProvider.getPoints(concerned, start, end, width, diff, self._options.mode, self._inputs);
                    // keep result in cache
                    return self.obj.promise.then(function (results) {
                        self.obj.cache.end = end;
                        self.obj.cache.start = start;
                        self.obj.cache.points = {};
                        concerned.forEach(function (c, i) {
                            self.obj.cache.points[c.tsuid] = results[i];
                        });
                        return results;
                    });
                });
                // compute data
                return promise.then(function (results) {
                    results.forEach(function (result_tmp, i) {
                        // assign result
                        concerned[i].filtered = result_tmp.map(function (result) {
                            return Object.assign({}, result);
                        });
                        // keep trace of scope in
                        concerned[i].scope = {
                            start: start,
                            end: end,
                            width: width,
                            height: height
                        };
                        // reset cached points
                        concerned[i].concerned = null;
                    });
                    // store in context the min / max gathered
                    self.context.xInterval = [start - diff, end + diff];
                    // loader off
                    self.dom.subContainer.classed('in-loading', false);

                    return results;
                }).catch(function () {
                    // loader off
                    self.dom.subContainer.classed('in-loading', false);
                });
            };

            // create a new promise for optimization
            var promise = new Promise(function (resolve) {
                // abort previous request, if its not aleady processed
                clearTimeout(self.obj.currentPointsRequest);
                // ask points with a delay ( in order to be able to abort it)
                self.obj.currentPointsRequest = setTimeout(function () {
                    // propagate answer
                    resolve(fn());
                }, 50);
            });

            return promise;
        }
    }, {
        key: '_renderToolTip',
        value: function _renderToolTip(newhover) {
            var self = this;
            if (newhover && self._options.tooltip.show) {
                self.dom.tooltip.classed('hidden', false).style('opacity', 1).html(getText(newhover));
                var coords = scaleHelper.getUnscaledMousePosition(document.body);
                //var containerSize = self.dom.canvas.node().getBoundingClientRect();
                //var tooltipSize = self.dom.tooltip.node().getBoundingClientRect();
                //var rightWidth = Math.abs(containerSize.width - coords[0]);
                //var leftPosition = (rightWidth < tooltipSize.width + 50) ? coords[0] - tooltipSize.width : coords[0] + 50;
                //var topPosition = (coords[1] > containerSize.height / 2) ? coords[1] - tooltipSize.height + 40 : coords[1] - 5;
                self.dom.tooltip.style('position', 'fixed').style('left', coords[0] + 'px').style('top', coords[1] - 20 + 'px');
            } else if (newhover !== self.context.hover) {
                self.dom.tooltip.transition().duration(100).on('end', function () {
                    d3.select(this).classed('hidden', true);
                });
            }
        }

        /**
         * Highlight specific curve
         * @param  {String} tsuid * ts which will be highlighted
         * @return {ProtoCanvasCurve} current object
         */

    }, {
        key: 'highlightCurve',
        value: function highlightCurve(tsuid) {
            var self = this;
            // if tsuid equals to null, dont highlight a curve (reset)
            var sequence = self.data[self.data.findIndex(function (d) {
                return d.tsuid === tsuid;
            })];
            if (sequence) {
                self.context.hover = {
                    tsuid: sequence.tsuid,
                    variable: sequence.variable
                };
            } else {
                self.context.hover = null;
            }

            self.redrawCanvas();
            return self;
        }

        /**
         * Main function to initialize and create all needed elements
         */

    }, {
        key: 'init',
        value: function init() {
            var _this2 = this;

            var self = this;

            if (self._options.autosize) {
                self._options.width = $(self.container.node()).width();
                var h = $(self.container.node()).height();
                if (h !== 0) {
                    self._options.height = h;
                }
            }
            //self.container.style('position', 'relative');
            var innerChartWidth = self._options.width - self._options.margin.left - self._options.margin.right,
                minimapHeight = self._options.minimap.show ? self._options.minimap.height : 0,
                weightsHeight = self._options.weights.show ? self._options.weights.height : 0,
                innerChartHeight = self._options.height - self._options.margin.top - self._options.margin.bottom - minimapHeight - weightsHeight,
                mouseDiff = self._options.dots.mousePrecision;

            var getCurrentHover = function getCurrentHover() {
                var coords = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
                var tsArray = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : _this2.data;
                var hover = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : _this2.context.hover;

                var minY = coords[1] + mouseDiff,
                    maxY = coords[1] - mouseDiff;
                // sort data to have the ts in hover in last
                var data = tsArray.slice(0);
                if (hover) {
                    data.sort(function (d) {
                        return hover && hover.tsuid === d.tsuid;
                    });
                }
                for (var i = data.length - 1; i >= 0; i--) {
                    var ts = data[i];
                    if (ts.closest && ts.closest.y >= maxY && ts.closest.y <= minY) {
                        return computeHover(ts.closest, ts);
                    }
                }
            };

            var getClosest = function getClosest(x, points) {
                // TODO -> clean this block (too dirty)
                if (x && points && points.length > 0) {
                    var afterIdx = points.findIndex(function (p) {
                        return x < p.x;
                    });
                    if (afterIdx < 0) {
                        afterIdx = 0;
                    }
                    var beforeIdx = afterIdx > 0 ? afterIdx - 1 : 0;

                    if (Math.abs(points[beforeIdx].x - x) < Math.abs(points[afterIdx].x - x)) {
                        return points[beforeIdx];
                    } else {
                        return points[afterIdx];
                    }
                }
            };

            self.container = d3.select('#' + self.containerId);

            self.dom.subContainer = self.container.append('div').attr('class', 'curve-subcontainer').style("width", self._options.width + 'px').style("height", self._options.height + 'px').style('position', 'relative');

            self.dom.loader = self.dom.subContainer.append('div').attr('class', 'loader small');

            self.dom.canvas = self.dom.subContainer.append('canvas').attr('class', 'curve-canvas').style('top', weightsHeight + 'px').style('left', self._options.margin.left + 'px');

            self.dom.canvasMouse = self.dom.subContainer.append('canvas').attr('class', 'curve-canvas-mouse').attr("width", innerChartWidth).style('top', weightsHeight + 'px').attr("height", innerChartHeight).style('left', self._options.margin.left + 'px');

            Object.assign(self.obj, {
                canvasContext: self.dom.canvas.node().getContext('2d'),
                canvasMouseContext: self.dom.canvasMouse.node().getContext('2d')
            });

            self.dom.canvasHidden = document.createElement('canvas');
            Object.assign(self.dom.canvasHidden, { width: innerChartWidth, height: innerChartHeight });

            self.graph = self.dom.subContainer.append('svg').attr("class", "curve").attr("width", self._options.width).attr("height", self._options.height).on('mouseleave', function () {
                self.obj.mouseCoords = null;
                if (self.context.hover) {
                    self.context.hover = null;
                    self.dom.tooltip.transition().duration(100).style("opacity", 0);
                }
                self.data.forEach(function (d) {
                    return d.closest = null;
                });
                self.container.selectAll('.yaxis, .yaxis2, .xaxis').classed('invisible', !self._options.showAxis);
                self.dom.canvasMouse.classed('hidden', true);
                self.redrawCanvas();
                self.renderWeightsBand();
            }).on('mousemove mouseenter', function () {
                self.dom.canvasMouse.classed('hidden', false);
                self.obj.mouseCoords = self.obj.canvasDrawer.drawMouseCross(self.dom.canvasMouse.node(), self.scales.x, self.scales.y, self.scales.y2);

                // hide zoom area if ctrl key is pressed (in order to handle brush)
                self.dom.zoom.style('display', d3.event.ctrlKey || d3.event.shiftKey ? 'none' : 'initial');

                var coords = scaleHelper.getUnscaledMousePosition(self.dom.canvas.node()),
                    newhover = null,
                    insideChart = innerChartWidth >= coords[0] && coords[0] >= 0 && coords[1] <= innerChartHeight;

                if (insideChart) {
                    self.data.forEach(function (d) {
                        d.closest = getClosest(coords[0], d.concerned);
                    });
                    newhover = getCurrentHover(coords);
                } else {
                    self.data.forEach(function (d) {
                        return d.closest = null;
                    });
                }

                self._renderToolTip(newhover);

                if (!newhover && self.context.hover) {
                    self.dispatch.call('pointmouseleave', self, Object.assign({}, self.context.hover));
                } else if (newhover && newhover !== self.context.hover) {
                    self.dispatch.call('pointmouseenter', self, Object.assign({}, newhover));
                }

                if ((newhover || self.context.hover) && newhover !== self.context.hover) {
                    self.context.hover = newhover;
                }

                if (newhover) {
                    if (self.context.variables.indexOf(newhover.variable) === 1) {
                        self.container.selectAll('.yaxis').classed('invisible', true);
                        self.container.selectAll('.yaxis2').classed('invisible', false);
                    } else {
                        self.container.selectAll('.yaxis').classed('invisible', false);
                        self.container.selectAll('.yaxis2').classed('invisible', true);
                    }
                } else {
                    self.container.selectAll('.yaxis, .yaxis2').classed('invisible', !self._options.showAxis);
                }
                self.redrawCanvas();
                self.renderWeightsBand();

                self.graph.classed("pointing", newhover !== null);
            });
            d3.select('body').selectAll('.lig-tooltip.main-tooltip').data([0]).enter().append('div').attr("class", "lig-tooltip main-tooltip").style("opacity", 0);

            self.dom.tooltip = d3.select('body>.lig-tooltip');

            //self.graph.attr("viewBox", `0 0 ${self.options.width} ${self.options.height}`);
            self.dom.focus = self.graph.append("g").attr("class", "focus").attr('transform', 'translate(' + self._options.margin.left + ')');

            self.dom.xaxis = self.dom.focus.append("g").attr("class", "xaxis").attr("transform", 'translate(0,' + (self._options.height - self._options.margin.top - minimapHeight - weightsHeight) + ')');

            self.dom.yaxis = self.dom.focus.append("g").attr("class", "yaxis");

            self.dom.yaxis2 = self.dom.focus.append("g").attr("class", "yaxis2");

            self.container.selectAll('.yaxis, .yaxis2, .xaxis').classed('invisible', !self._options.showAxis);
            if (self._options.weights.show) {
                self.dom.weights = self.graph.append('g').attr('class', 'weights').attr('transform', 'translate(' + self._options.margin.left + ', 0)');
                // redraw minimap
            }
            if (self._options.minimap.show) {
                self.dom.minimap = self.graph.append('g').attr('class', 'minimap').attr('transform', 'translate(' + self._options.margin.left + ', ' + (self._options.height - minimapHeight) + ')');
                // redraw minimap
            }

            self.dom.brush = self.graph.append('g').attr('class', 'brush').attr('transform', 'translate(' + self._options.margin.left + ', ' + weightsHeight + ')');

            self.dom.zoom = self.graph.append("rect").attr('class', 'zoom').attr('width', innerChartWidth).attr('height', innerChartHeight).attr('transform', 'translate(' + self._options.margin.left + ', ' + weightsHeight + ')').style('fill', 'none');

            //var prevScale;
            self.zoomed = function () {
                var transformObject = d3.event.transform;
                if (!self.context.scale || !self.context.x || self.context.scale !== transformObject.k || Math.abs((self.context.x || 0) - transformObject.x) > 1) {
                    self.logger.log('zoom event', self.containerId, d3.event.transform);
                    self.context.scale = d3.event.transform.k;
                    self.context.x = d3.event.transform.x;
                    // apply transformation to scaleX
                    self.scales.x.domain(d3.event.transform.rescaleX(self.scales.x2).domain());
                    self.data.forEach(function (d) {
                        d.concerned = null;
                        d.closest = null;
                        d.scope = null;
                    });
                    // redraw xaxis
                    self.renderHorizontalAxis();

                    window.requestAnimationFrame(function () {
                        self.redrawCanvas(null, true);
                        self.renderWeightsBand();
                    });

                    if (self._options.minimap.show && self.scales.x) {
                        // redraw minimap
                        self.dom.minimap.call(self.brushMinimap.move, self.scales.x.range().map(transformObject.invertX, transformObject));
                    }
                }
            };
        }

        /**
         * Init shared d3 scales
         * @param {*} width
         * @param {*} height
         */

    }, {
        key: 'initScales',
        value: function initScales(width, height) {
            var self = this;

            // Sets up a D3 scales
            self.scales.x = d3.scaleLinear().range([0, width]).domain(self._options.mode === 'timestamp' ? self.context.timestampMinMax : self.context.indexMinMax);
            self.scales.x2 = d3.scaleLinear().range([0, width]).domain(self.scales.x.domain());
            self.scales.y = d3.scaleLinear().range([height, 0]).domain(self.context.yMinMax1);

            self.scales.y2 = null;

            if (self.context.yMinMax2) {
                self.scales.y2 = d3.scaleLinear().range([height, 0]).domain(self.context.yMinMax2);
            }
        }

        /**
         * Alias to register handler for specific event
         * @param  {String} eventName [description]
         * @param  {Function} handler   [Tdescription]
         * @return {ProtoWithLegend}           [description]
         */

    }, {
        key: 'on',
        value: function on(eventName, handler) {
            this.dispatch.on(eventName, handler);
            return this;
        }

        /**
         * Get or set current options
         * @param  {Object} _ * new options if it's given
         * @return {ProtoCanvasCurve} current object
         */

    }, {
        key: 'options',
        value: function options(_) {
            if (!arguments.length) {
                return this._options;
            }
            Object.assign(this._options, _);
            return this;
        }

        /**
         * Get the current node
         * @return Object
         */

    }, {
        key: 'node',
        value: function node() {
            return this.container.node();
        }

        /**
         * Render horizontal axis + its vertical ticks
         */

    }, {
        key: 'renderHorizontalAxis',
        value: function renderHorizontalAxis() {
            var self = this;
            // redresh horizontal axis
            var width = self.scales.x.range()[1] - self.scales.x.range()[0];
            var ticks = [],
                r_ticks = [];

            var nbticks = parseInt(width / 200);
            // choose what vertical ticks will be shown
            var example = self.data[0];
            if (example && example.filtered && width / example.filtered.length >= self._options.minTickDist) {
                self.logger.log('use examples to render ticks');
                // if dist between events is enough use events as ticks
                ticks = example.filtered.map(function (d) {
                    return self._options.mode === 'timestamp' ? d.timestamp : d.idx;
                }).filter(function (d) {
                    return width >= self.scales.x(d);
                });
                var current = null;
                ticks.forEach(function (t) {
                    var v = self.scales.x(t);
                    if (!current || v - self.scales.x(current) >= 200) {
                        r_ticks.push(t);
                        current = t;
                    }
                });
                self.xAxis.tickValues(r_ticks);
                self.graph.select(".xaxis").call(self.xAxis);
            } else {
                // use normal ticks
                self.xAxis.tickValues(null).ticks(nbticks);
                ticks = self.xAxis.scale().ticks();
                self.graph.select(".xaxis").call(self.xAxis);
                self.logger.log('bticks =>', ticks);
            }
            self.graph.select(".xaxis").call(self.xAxis);
            // render vertical ticks
            var tickData = self.graph.select('.xaxis').selectAll('line.tick-v').data(ticks);

            tickData.exit().remove();

            var tickEnter = tickData.enter().append('line').attr('class', 'tick-v');

            tickEnter.merge(tickData).attr('transform', function (d) {
                return 'translate(' + self.scales.x(d) + ')';
            }).attr('y2', -self._options.height);
        }

        /**
         * Render band that will represent weights (interesting points have high weight)
         */

    }, {
        key: 'renderWeightsBand',
        value: function renderWeightsBand() {
            var self = this;
            if (self._options.weights.show) {
                // indicators in minimap
                var weightBand = self.dom.weights.selectAll('g.sequence-weights').data(self.data || [], function (d) {
                    return d.tsuid;
                });

                var weightBandEnter = weightBand.enter().append('g').attr('class', 'sequence-weights');

                weightBand.exit().remove();

                var weightBandUpdate = weightBandEnter.merge(weightBand);
                weightBandUpdate.each(function (sequence) {
                    var greyScale = d3.scaleLinear().domain(d3.extent((sequence.concerned || []).map(function (d) {
                        return d.weight;
                    }))).range([d3.color(self._options.weights.minColor), d3.color(self._options.weights.maxColor)]);
                    var weightOccurence = weightBandUpdate.selectAll('rect.sequence-weight').data(sequence.concerned || []);
                    var weightOccurenceEnter = weightOccurence.enter().append('rect').attr('y', 0).attr('fill', 'grey').attr('height', self._options.weights.height).attr('class', 'sequence-weight');
                    weightOccurenceEnter.merge(weightOccurence).attr('fill', function (d) {
                        return greyScale(d.weight);
                    }).each(function (point, i) {
                        var elem = d3.select(this);
                        var isFirst = i === 0,
                            isLast = i === sequence.concerned.length - 1,
                            widthBeforePoint = !isFirst ? (sequence.concerned[i].fullX - sequence.concerned[i - 1].fullX) / 2 : 0,
                            widthAfterPoint = !isLast ? (sequence.concerned[i + 1].fullX - sequence.concerned[i].fullX) / 2 : 0;
                        elem.attr('width', widthBeforePoint + widthAfterPoint + 1).attr('x', Math.floor(point.x - (isLast ? widthBeforePoint : isFirst ? 0 : (widthBeforePoint + widthAfterPoint) / 2)));
                    }).on('mousemove mouseenter', function () {
                        self.dom.canvasMouse.classed('hidden', false);
                        self.obj.mouseCoords = self.obj.canvasDrawer.drawMouseCross(self.dom.canvasMouse.node(), self.scales.x, self.scales.y, self.scales.y2);
                    });
                });
                // Manage the extension of the mouse cross to be displayed on the weights band
                var indicator = self.dom.weights.selectAll('rect.indicator').data(self.obj.mouseCoords ? [self.obj.mouseCoords] : []);
                var indicatorEnter = indicator.enter().append('rect').attr('class', 'indicator').attr('width', 1).attr('y', 0);
                indicator.exit().remove();
                indicatorEnter.merge(indicator).attr('x', function (d) {
                    return d[0];
                }).attr('height', self._options.weights.height);
            }
        }

        /**
         * Main function to render viz
         */

    }, {
        key: 'render',
        value: function render() {
            var self = this;

            self.logger.log('render \'' + self.containerId + '\'');

            var margin = self._options.margin;
            var width = +self._options.width - margin.left - margin.right,
                weightsHeight = self._options.weights.show ? self._options.weights.height : 0,
                minimapHeight = self._options.minimap.show ? self._options.minimap.height : 0,
                height = +self._options.height - margin.top - margin.bottom - minimapHeight - weightsHeight;

            self._computeContext();

            self.initScales(width, height);

            self.xAxis = d3.axisBottom(self.scales.x).tickFormat(self._options.mode === 'timestamp' ? _formatTime : function (d) {
                return d;
            });

            var yAxis = d3.axisLeft(self.scales.y).tickValues(d3.min([height / 20, 10])).tickFormat(d3.format(".2"));

            self.obj.zoom = d3.zoom().duration(self.context.duration).scaleExtent([1, 50000]).translateExtent([[0, 0], [width, height]]).extent([[0, 0], [width, height]]).on("end", function () {
                var newDomain = self.scales.x.domain();
                // stop process if previous domain is same as new domain
                if (self.obj.prevDomain && self.obj.prevDomain[0] === newDomain[0] && self.obj.prevDomain[1] === newDomain[1]) {
                    //} || sourceEvent === 2 || sourceEvent.detail < 2 || sourceEvent.type === 'brush') {
                    return;
                }
                // store transform object
                self._options.transform = d3.event.transform;

                // dispatch event it its property dispatchZoom is equal to true
                if (self.obj.dispatchZoom) {
                    if (self._options.debugMode) {
                        self.logger.log('dispatch zoom event', self._options.transform);
                    }
                    self.dispatch.call('zoomed', d3.event.transform, self.containerId, self._options.transform);
                } else if (self._options.debugMode) {
                    self.logger.log('dispatch zoom disable', self._options.transform);
                }

                // store current domain as previous domain
                self.obj.prevDomain = self.scales.x.domain();

                // TODO : work on getting data (async)
                if (self.data && self.data.length > 0) {
                    self.getComputedPoints().then(function () {
                        // reset the flag to true
                        self.obj.dispatchZoom = true;
                        // redraw
                        window.requestAnimationFrame(function () {
                            self.redrawCanvas();
                            self.renderWeightsBand();
                            self.renderHorizontalAxis();
                        });
                    });
                }
            }).on("zoom", self.zoomed).filter(function () {
                return !d3.event.ctrlKey && !d3.event.shiftKey;
            });

            /**
             * Apply ticks on an d3 axis and check min distance
             * @param {Object} axis - d3 axis
             * @param {Object} scale - d3 scale
             * @param {Array} ticks - array of tick values
             * @param {Integer} min_distance - min distance
             */
            var _setTicks = function _setTicks(axis, scale, ticks, min_distance) {
                var values = null,
                    current = void 0;
                if (ticks && ticks.length > 0) {
                    values = [];
                    // raw ticks
                    if (scale(ticks[0]) - scale(ticks[1]) > min_distance) {
                        values = ticks;
                    }
                    // filter ticks in looking min distance
                    else {
                            ticks.forEach(function (d, i) {
                                if (i !== 0 && (!current || current - scale(d) > min_distance)) {
                                    current = scale(d);
                                    values.push(d);
                                }
                            });
                        }
                }
                axis.tickValues(values);
            };

            var brushedFromGraph = function brushedFromGraph() {
                if (d3.event.sourceEvent && (d3.event.sourceEvent.type === "zoom" || d3.event.sourceEvent.type === "end")) {
                    return;
                } // ignore brush-by-zoom
                var s = d3.event.selection || self.scales.x.range();
                self.scales.x.domain(s.map(self.scales.x.invert, self.scales.x));
                var currentDomain = self.scales.x.domain();
                var scaleValue = void 0;
                if (self._options.mode === 'timestamp') {
                    // timestamp mode
                    scaleValue = (self.context.timestampMinMax[1] - self.context.timestampMinMax[0]) / (currentDomain[1] - currentDomain[0]);
                } else {
                    // index mode
                    scaleValue = (self.context.indexMinMax[1] - self.context.indexMinMax[0]) / (currentDomain[1] - currentDomain[0]);
                }
                // call zoom
                self.graph.select('.zoom').transition().duration(700).call(self.obj.zoom.transform, d3.zoomIdentity.scale(scaleValue).translate(-self.scales.x2(currentDomain[0]), 0));
                // hide tooltip
                self.dom.tooltip.style("opacity", 0);
                // remove brush
                self.graph.select('.brush').call(self.brushGraph.move, null);
            };

            self.brushMinimap = d3.brushX().extent([[0, 0], [width, minimapHeight - self._options.minimap.paddingBottom]]).on("brush", function () {
                if (d3.event.sourceEvent && d3.event.sourceEvent.type === "mousemove") {
                    var s = d3.event.selection;
                    if (!s || isNaN(s[0] || isNaN(s[1]))) {
                        s = self.scales.x.range();
                    }
                    // call zoom
                    self.dom.zoom.call(self.obj.zoom.transform, d3.zoomIdentity.scale((self.scales.x.range()[1] - self.scales.x.range()[0]) / (s[1] - s[0])).translate(-s[0], 0));
                }
            }).on("end", function () {
                if (d3.event.selection === null) {
                    // keep same scale but move the bar
                    var coords = d3.mouse(self.dom.minimap.node()),
                        position_x = d3.min([coords[0], width - width / self._options.transform.k]);
                    // call zoom
                    self.dom.zoom.call(self.obj.zoom.transform, d3.zoomIdentity.scale(self._options.transform.k).translate(-position_x, 0));
                }
            });

            self.brushGraph = d3.brushX().extent([[0, 0], [width, height]]).on("end", brushedFromGraph);

            /**new ZoomControls()
               .on('zoomIn', () => self.scale(self.context.scale * 2))
               .on('zoomOut', () => self.scale(self.context.scale / 2))
               .init(self.container, self.graph);
            **/

            _setTicks(yAxis, self.scales.y, self.context.yTicks1, self._options.minHTickDist);

            if (self._options.minimap.show) {
                self.dom.minimap.call(self.brushMinimap);
                // indicators in minimap
                var indicatorsData = self.dom.minimap.selectAll('rect.indicator').data(self.context.patternLocations || []);

                indicatorsData.enter().append('rect').attr('class', 'indicator').attr('y', 2).attr('height', 6);

                indicatorsData.exit().remove();

                self.dom.minimap.selectAll('rect.indicator').attr('fill', function (d) {
                    return d.color;
                }).attr('transform', function (d) {
                    return 'translate(' + self.scales.x(self._options.mode === 'timestamp' ? d.startDate : d.startIndex) + ', 0)';
                }).attr('width', function (d) {
                    var start, end;
                    if (self._options.mode === 'timestamp') {
                        start = d.startDate;
                        end = d.endDate;
                    } else {
                        start = d.startIndex;
                        end = d.endIndex;
                    }
                    return self.scales.x(end) - self.scales.x(start);
                });
            }

            var range = self.scales.x.range();
            if (self._options.transform) {
                // Does it need it here?
                self.scales.x.domain(self._options.transform.rescaleX(self.scales.x2).domain());
                range = self.scales.x.range().map(self._options.transform.invertX, self._options.transform);
            }

            // call minimap with range
            self.dom.minimap.call(self.brushMinimap.move, range);
            // assign zoom to dom
            self.dom.zoom.call(self.obj.zoom);
            // assign brush to dom
            self.dom.brush.call(self.brushGraph);

            self.getComputedPoints().then(function () {
                if (self._options.transform) {
                    // call zoom with transform option
                    self.dom.zoom.call(self.obj.zoom.transform, self._options.transform);
                }
                self.redrawCanvas();
                self.renderWeightsBand();
                self.renderHorizontalAxis();
            });

            self.graph.select(".yaxis").call(yAxis).selectAll('text.variable').data([0]).enter().append("text").attr('class', 'variable').attr("fill", "#000").attr("transform", "rotate(-90)").attr("y", 6).attr("dy", "0.71em").attr("text-anchor", "end").text(function () {
                return self._options.showVariable ? self.context.variables[0] : '';
            });

            if (self.scales.y2) {
                var yAxis2 = d3.axisRight(self.scales.y2).ticks(d3.min([height / 20, 10])).tickFormat(d3.format('.2'));

                _setTicks(yAxis2, self.scales.y2, self.context.yTicks2, self._options.minHTickDist);

                self.graph.select(".yaxis2").attr('transform', 'translate(' + width + ', 0)').call(yAxis2).selectAll('text.variable').data([[]]).enter().append("text").attr('class', 'variable').attr("fill", "#000").attr("transform", "rotate(-90)").attr("y", "-1em").attr("dy", "0.71em").attr("text-anchor", "end").text(function () {
                    return self._options.showVariable ? self.context.variables[1] : '';
                });
            }
        }

        /**
         * get/set inputs
         * @param  {Object} _ inputs
         *      ex : { ts_list: [], patterns: {}}
         * @return {Object}
         */

    }, {
        key: 'inputs',
        value: function inputs(_) {
            if (!arguments.length) {
                return this.inputs;
            }
            this._inputs = _;
            return this;
        }

        /**
         * Redraw main canvas
         * @param  {Array} ds - array of ts
         */
        /**
         * Redraw main canvas
         * @param  {Array} ds - array of ts
         */

    }, {
        key: 'redrawCanvas',
        value: function redrawCanvas(ds) {
            var quickdraw = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

            var self = this;

            /**
             * Helper to get d3 scale (in looking on variable)
             * @param  {Object} ts - timeseries
             * @return {Object}      color
             */
            var _getYScale = function _getYScale(variable) {
                return self.context.variables.indexOf(variable) === 1 ? self.scales.y2 : self.scales.y;
            };

            self.obj.mouseCoords = self.obj.canvasDrawer.drawMouseCross(self.dom.canvasMouse.node(), self.scales.x, self.scales.y, self.scales.y2);

            if (self.context.indrawing) {
                console.warn('Already in drawing');
            }

            var canvasNode = self.dom.canvas.node(),
                tmp = self.dom.canvasHidden;

            self.context.indrawing = true;
            var minimapHeight = self._options.minimap.show ? self._options.minimap.height : 0;
            var weightsHeight = self._options.weights.show ? self._options.weights.height : 0;
            canvasNode.width = tmp.width = self._options.width - self._options.margin.left - self._options.margin.right;
            canvasNode.height = tmp.height = +self._options.height - self._options.margin.top - self._options.margin.bottom - minimapHeight - weightsHeight;

            // sort data to have the ts in hover in last
            var data = (ds || self.data).slice(0);
            if (self.context.hover) {
                data.sort(function (d) {
                    return self.context.hover && self.context.hover.tsuid === d.tsuid;
                });
            }

            var preparedData = data.map(function (ts) {
                //Cut in all points only some displayed
                var yScale = _getYScale(ts.variable || ts.md.variable);
                if (!ts.concerned) {
                    var startCutIdx = -1; //ts.filtered.findIndex(d=>d.timestamp > start) - 1;
                    if (startCutIdx < 0) {
                        startCutIdx = 0;
                    }
                    var endCutIdx = -1; //ts.filtered.findIndex(d=>d.timestamp > end) + 1;
                    if (endCutIdx === 0) {
                        endCutIdx = ts.filtered.length;
                    }
                    ts.concerned = ts.filtered.map(function (v) {
                        return Object.assign(v, {
                            x: Math.round(self.scales.x(self._options.mode === 'timestamp' ? v.timestamp : v.idx)),
                            fullX: self.scales.x(self._options.mode === 'timestamp' ? v.timestamp : v.idx),
                            y: Math.round(yScale(v.value)),
                            y1: Math.round(yScale(v.min)),
                            y2: Math.round(yScale(v.max))
                        });
                    });
                }

                var result = {
                    color: ts.color,
                    coords: ts.concerned,
                    tsuid: ts.tsuid,
                    xScale: self.scales.x,
                    yScale: yScale,
                    style: ts.style,
                    bps: ts.bps,
                    closest: ts.closest,
                    patterns: ts.patterns,
                    radius: self.context.hover && self.context.hover.tsuid === ts.tsuid ? self._options.dots.highlightRadius : self._options.dots.radius,
                    underlined: self.context.hover && self.context.hover.tsuid === ts.tsuid,
                    visible: !self.context.hover || self.context.hover.tsuid === ts.tsuid,
                    variable: ts.variable || ts.md.variable
                };
                // TODO: complexity
                if (self._inputs) {
                    if (self._inputs.breakpoints) {
                        result.bps = self._inputs.breakpoints[self._inputs.breakpoints.findIndex(function (t) {
                            return t.key === (ts.variable || ts.md.variable);
                        })];
                    }
                    if (self._inputs.disc_break_points && self._inputs.variables) {
                        result.discretizedDomain = self._inputs.disc_break_points[self._inputs.variables.indexOf(ts.variable || ts.md.variable)];
                    }
                }

                return result;
            });

            var variables = Array.from(new Set(self.data.map(function (d) {
                return d.variable;
            })));
            var hover, bps, mainVariable;
            // if there is an highlighted point
            if (self.context.hover) {
                hover = Object.assign({}, self.context.hover);
                Object.assign(hover, {
                    x: self.scales.x(self._options.mode === 'timestamp' ? self.context.hover.timestamp : self.context.hover.idx),
                    y: _getYScale(self.context.hover.variable)(hover.value),
                    radius: self._options.dots.hoverRadius
                });
                mainVariable = self.context.hover.variable;
            } else if (variables.length === 1) {
                mainVariable = variables[0];
            }
            // compute displayed breakpoints
            // ikats case
            if (mainVariable && self._inputs && self._inputs.breakpoints) {
                bps = self._inputs.breakpoints[self._inputs.breakpoints.findIndex(function (t) {
                    return t.key === mainVariable;
                })];
            }
            // lig case
            else if (mainVariable && preparedData.findIndex(function (p) {
                    return p.variable === mainVariable;
                }) > -1) {
                    bps = preparedData[preparedData.findIndex(function (p) {
                        return p.variable === mainVariable;
                    })].bps;
                }
            if (bps) {
                bps.left = variables.indexOf(mainVariable) === 0;
                self.obj.canvasDrawer.drawBreakPoints(self.obj.canvasContext, bps, tmp.width, _getYScale(mainVariable));
            }
            self.obj.canvasDrawer.redraw(preparedData, self.obj.canvasContext, hover, [tmp.width, tmp.height], quickdraw, self._options.mode);

            self.context.indrawing = false;
        }

        /**
         * Transform graph
         * @param  {Object}  transform
         * @param  {Boolean} dispatchZoom
         */

    }, {
        key: 'transform',
        value: function transform(_transform) {
            var dispatchZoom = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

            if (_transform && _transform !== this._options.transform) {
                this.obj.dispatchZoom = dispatchZoom;
                this._options.transform = _transform;
                this.graph.select('.zoom').call(this.obj.zoom.transform, _transform);
            }
        }

        /**
         * Scale to a specific scale
         * @param  {Number}  scale
         * @param  {Boolean} dispatchZoom
         */

    }, {
        key: 'scale',
        value: function scale(_scale) {
            var dispatchZoom = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

            if ((_scale || _scale > -1) && _scale !== this.context.scale) {
                this.obj.dispatchZoom = dispatchZoom;
                var target = this.graph.select('.zoom').transition().duration(this.context.duration / 2);
                this.obj.zoom.scaleTo(target, _scale);
            }
        }
    }, {
        key: 'update',
        value: function update(ts_list, inputs) {
            var self = this;

            if (ts_list) {
                self.data = ts_list;
            }

            if (inputs) {
                self._inputs = inputs;
            }

            if (self.data.length > self._options.maxCurves) {
                console.warn('too many to ts to draw (' + self.data.length + '), take only ' + self._options.maxCurves + ' ts');
                self.data = self.data.slice(0, self._options.maxCurves);
            }

            if (self.data) {
                // FIXME: reduce complexity
                var locations = d3.merge(self.data.map(function (d) {
                    return d3.merge((d.patterns || []).map(function (p) {
                        return p.locations.map(function (l) {
                            var diff = l.duration / p.length / 2;
                            return { startDate: l.timestamp - diff, startIndex: l.pos, endIndex: l.pos + p.length, endDate: l.timestamp + l.duration + diff, color: p.color };
                        });
                    }));
                }));
                self.context.patternLocations = locations;
            }

            // DEFAULT properties
            self.data.forEach(function (d, i) {
                d.color = d.color || self.obj.colors(i);
                d.filtered = d.filtered || [];
            });

            return self;
        }
    }]);

    return CanvasCurve;
}(LIGViz);

module.exports = CanvasCurve;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"26":26,"27":27,"30":30,"33":33,"34":34,"40":40,"42":42,"47":47}],26:[function(require,module,exports){
(function (global){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null),
    _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null),
    scaleHelper = require(34);

/*exported CanvasCurveDrawer */

/*
 * Class CanvasCurveDrawer.
 * Class that manage as the service to draw elements from curve (in canvas mode)
 * @constructor
 */

var CanvasCurveDrawer = function () {
  function CanvasCurveDrawer() {
    _classCallCheck(this, CanvasCurveDrawer);

    this.tmpCanvas = document.createElement('canvas');
    this.debug = false;
  }

  /**
   * Draw the algo breakpoints (horizontal)
   * @param  {Object} ctx       - canvas context
   * @param  {Object} bpsData   - Breakpoints object
   * ex: {
   *       left: true/false,
   *       bps: [],
   *       map: {}
   *     }
   * @param  {Number} width     - in px
   * @param  {Function} yScale  - function to scale y value
   */


  _createClass(CanvasCurveDrawer, [{
    key: 'drawBreakPoints',
    value: function drawBreakPoints(ctx, bpsData, width, yScale) {
      var pixelDiff = Math.abs(yScale(bpsData.bps[0].max) - yScale(bpsData.bps[0].min));

      ctx.beginPath();
      ctx.strokeStyle = 'rgba(0, 0, 0, 0.2)';
      ctx.fillStyle = 'rgba(0, 0, 0, 0.4)';
      ctx.lineWidth = 1;
      ctx.font = d3.min([pixelDiff * 2 / 3, 20]) + "px Arial";

      var y = void 0;
      bpsData.bps.forEach(function (bp, i) {
        y = Math.round(yScale(bp.min));
        // draw each horizontal breakpoint
        ctx.moveTo(0, y);
        ctx.lineTo(width, y);
        // if line is not the last one, draw labels
        // if variable is the first one, draw in left else draw in right
        ctx.fillText(bp.key, bpsData.left ? 5 : width - 30, y - pixelDiff / 3);
        // draw last horizontal breakpoint
        if (bpsData.bps.length - 1 === i) {
          y = Math.round(yScale(bp.max));
          ctx.moveTo(0, y);
          ctx.lineTo(width, y);
        }
      });
      ctx.stroke();
    }

    /**
     * Draw single dot
     * @param  {Object} coords
     * @param  {Number} radius
     * @param  {String} color
     * @param  {Object} context
     */

  }, {
    key: 'drawDot',
    value: function drawDot(coords, radius) {
      var color = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'grey';
      var context = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

      context.beginPath();
      context.lineWidth = 1;
      context.strokeStyle = d3.color(color).darker(1.7);
      context.fillStyle = color;
      context.arc(coords.x, coords.y, radius, 0, 2 * Math.PI);
      context.stroke();
      context.fill();
    }

    /**
     * Draw several dots
     * @param  {Array} ds
     * @param  {Object} ctx
     */

  }, {
    key: 'drawDotsLine',
    value: function drawDotsLine() {
      var coords = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
      var radius = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var color = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
      var ctx = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

      var self = this;
      self.tmpCanvas.width = radius * 2;
      self.tmpCanvas.height = radius * 2;
      var ctx2 = self.tmpCanvas.getContext('2d');
      ctx2.fillStyle = color;
      ctx2.arc(radius, radius, radius, 0, 2 * Math.PI);
      ctx2.fill();
      var coordsLength = coords.length;
      for (var i = 0; i < coordsLength; i++) {
        ctx.drawImage(self.tmpCanvas, coords[i].x - radius, coords[i].y - radius);
      }
    }

    /**
     * Draw canvas line
     * @param  {Array}  coords    - coords
     * @param  {Object} ctx       - canvas context
     * @param  {Number} lineWidth - width of line
     * @param  {Object} color     - colorization
     */

  }, {
    key: 'drawLine',
    value: function drawLine(coords, ctx, lineWidth, color) {
      var style = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 'line';

      var valueLine = d3.line()
      //.curve(d3.curveCatmullRom)
      .x(function (d) {
        return d.x;
      }).y(function (d) {
        return d.y;
      }).context(ctx);
      if (style && _.isArray(style[1])) {
        ctx.setLineDash(style[1]);
      }
      ctx.lineWidth = lineWidth;
      ctx.strokeStyle = color;
      ctx.beginPath();
      valueLine(coords);
      ctx.stroke();
      ctx.setLineDash([]);
    }

    /**
     * Draw canvas area
     * @param  {Array}  coords    - coords
     * @param  {Object} ctx       - canvas context
     * @param  {Number} height - height of the canvas
     * @param  {Object} color     - colorization
     */

  }, {
    key: 'drawArea',
    value: function drawArea(coords, ctx, height, color) {
      var valueLine = d3.area().x(function (d) {
        return d.x;
      }).y1(function (d) {
        return d.y;
      }).y0(height).context(ctx);
      ctx.fillStyle = color;
      ctx.beginPath();
      valueLine(coords);
      ctx.fill();
    }

    /**
     * Draw variations for a specific point (min, max) (aggregation)
     * @param  {Array}  coords
     * @param  {String} color
     * @param  {Object} ctx
     */

  }, {
    key: 'drawMinMaxLines',
    value: function drawMinMaxLines() {
      var coords = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
      var color = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'rgba(0, 0, 0, 0.4)';
      var ctx = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

      ctx.beginPath();
      ctx.lineWidth = 1;
      ctx.strokeStyle = color;

      var node = void 0;
      for (var i = coords.length - 1; i >= 0; i--) {
        node = coords[i];
        if (node.y1 !== node.y2) {
          ctx.moveTo(node.x, node.y1);
          ctx.lineTo(node.x, node.y2);
        }
      }

      ctx.stroke();
    }

    /**
     * Draw mouse cross representing the cursor target
     */

  }, {
    key: 'drawMouseCross',
    value: function drawMouseCross(canvasNode, xScale, yScaleLeft, yScaleRight) {
      var ctx = canvasNode.getContext('2d');
      var width = canvasNode.width;
      var height = canvasNode.height;

      ctx.fillStyle = 'transparent';
      ctx.clearRect(0, 0, width, height);
      try {
        var coords = scaleHelper.getUnscaledMousePosition(canvasNode);

        if (height >= coords[1] && width >= coords[0] && coords[0] >= 0) {

          ctx.beginPath();
          ctx.lineWidth = 1;
          ctx.strokeStyle = 'grey';

          // horizontal
          ctx.moveTo(0, coords[1]);
          ctx.lineTo(width, coords[1]);
          // vertical
          ctx.moveTo(coords[0], 0);
          ctx.lineTo(coords[0], height);

          ctx.stroke();

          // text
          ctx.font = "10px Arial";
          ctx.fillStyle = 'rgba(0, 0, 0, 0.4)';

          // x scale
          var x_value = xScale.invert(coords[0]).toFixed();
          if (!isNaN(x_value)) {
            ctx.fillText(x_value, coords[0] + 2, 10);
            ctx.fillText(x_value, coords[0] + 2, height - 5);
          }

          // first y scale
          var y1_value = yScaleLeft.invert(coords[1]).toPrecision(2);
          if (!isNaN(y1_value)) {
            ctx.fillText(y1_value, 5, coords[1] - 2);
            ctx.fillText(y1_value, width - 3 - y1_value.length * 6, coords[1] - 2);
          }

          if (yScaleRight) {
            // second y scale
            var y2_value = yScaleRight.invert(coords[1]).toPrecision(2);
            if (!isNaN(y2_value)) {
              ctx.fillText(y2_value, width - 30, coords[1] - 2);
            }
          }
          return coords;
        }
      } catch (e) {
        //console.error(e);
        return;
      }
    }

    /**
     * Draw patterns
     * @param  {Array} ds      - timeseries
     * @param  {Object} ctx    - canvas context
     * @param  {Number} height - height
     */

  }, {
    key: 'drawPatterns',
    value: function drawPatterns(ds, ctx, height) {
      var diff = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;
      var mode = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 'timestamp';

      var pattern, patternsLength, locationsLength, lineWidth, hColor, bColor;
      var tsDone = {};
      ctx.strokeStyle = '#939393FF';

      ds.forEach(function (data) {
        // usecase for paired variable dont show patterns two times
        if (!data.visible || tsDone[data.tsuid.split('#')[0]] && !data.underlined) {
          return;
        } else {
          tsDone[data.tsuid.split('#')[0]] = true;
        }

        patternsLength = (data.patterns || []).length;
        lineWidth = data.underlined ? 2 : 1;

        ctx.lineWidth = lineWidth;
        var len = void 0;

        for (var i = 0; i < patternsLength; i++) {
          pattern = data.patterns[i];
          len = pattern.length;
          // if pattern is not activated, it doesnt have to be drawn
          if (!pattern.activated) {
            continue;
          }

          hColor = d3.color(pattern.color).brighter(1);
          hColor.opacity = 1;
          bColor = d3.color(hColor);
          if (bColor.opacity > 0.5) {
            bColor.opacity = 0.5;
          }

          locationsLength = pattern.locations.length;
          var xc1 = null,
              xc2 = null;
          for (var j = 0; j < locationsLength; j++) {
            var x1 = void 0,
                x2 = void 0;
            var location = pattern.locations[j];
            ctx.beginPath();
            if (mode === 'timestamp') {
              diff = location.duration / len;
              x1 = Math.round(data.xScale(pattern.locations[j].timestamp - diff / 2));
              x2 = Math.round(data.xScale(pattern.locations[j].timestamp + diff * (pattern.length - 1) + diff / 2));
            } else {
              x1 = Math.round(data.xScale(pattern.locations[j].pos) - diff / 2);
              x2 = Math.round(data.xScale(pattern.locations[j].pos) + diff * (pattern.length - 1) + diff / 2);
            }
            /** OBSOLETE
            ctx.fillStyle = (pattern.locations[j+1] && (pattern.locations[j+1].pos - pattern.locations[j].pos) <= pattern.length) ? bColor: hColor;
            ctx.strokeStyle = 'grey';
            //ctx.strokeStyle = 'red';
            ctx.fillRect(x1, 0, (x2 - x1), height);
            // TODO: find better way
            ctx.strokeRect(x1, 0, (x2 - x1), height);
            **/
            if (pattern.locations[j + 1] && pattern.locations[j + 1].pos - pattern.locations[j].pos <= pattern.length) {
              if (xc1 === null) {
                xc1 = +x1;
              }
              xc2 = d3.max([x2, xc2]);
            } else {
              ctx.fillStyle = hColor;
              ctx.fillRect(x1, 0, x2 - x1, height);
              if (xc1 !== null && xc1 !== null) {
                ctx.fillStyle = bColor;
                ctx.fillRect(xc1, 0, x1 - xc1, height);
              }
              xc1 = null;xc2 = null;
              ctx.strokeStyle = 'grey';
              ctx.strokeRect(x1, 0, x2 - x1, height);
            }
          }
          ctx.strokeStyle = '#939393FF';
          ctx.stroke();
        }
      });
    }

    /**
     * Draw the pattern areas representing discretized values.
     * @param  {Array} ds   - Array of timeseries
     * @param  {Object} ctx - Canvas context
     */

  }, {
    key: 'drawPatternAreas',
    value: function drawPatternAreas(ds, ctx) {
      var diff = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
      var mode = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 'timestamp';


      var self = this;
      var variableSeparator = '-';
      //diff = 0;
      //console.time('drawPatternAreas ')

      if (diff < 3 && mode !== 'timestamp') {
        if (self.debug) {
          console.warn('too small to draw pattern areas');
        }
        return;
      }
      // tmp variable
      var ts, pattern, location, variable, variables, square;
      var tsSize, patternSize, locationSize, variablesSize, regexSize;
      // indexs
      var patternIndex, locationIndex, variableIndex, regexIndex;
      //
      var x1, y1, y2;

      ctx.strokeStyle = 'black';
      ctx.lineWidth = 1;
      tsSize = ds.length;
      // loop on each sequence
      for (var i = 0; i < tsSize; i++) {
        // TODO
        /*jshint maxdepth: 6 */

        ctx.beginPath();
        ts = ds[i];
        var minDomain = ts.xScale.domain()[0];
        if (!ts.visible) {
          continue;
        }
        patternSize = (ts.patterns || []).length;

        // loop on each pattern
        for (patternIndex = 0; patternIndex < patternSize; patternIndex++) {
          pattern = ts.patterns[patternIndex];
          variables = pattern.variable.split(variableSeparator);
          variablesSize = variables.length;
          locationSize = pattern.locations.length;

          // if the pattern is not activated, exit
          if (!pattern.activated) {
            continue;
          }
          // loop on splitted variables (in case we have paired variables)
          for (variableIndex = variablesSize - 1; variableIndex >= 0; variableIndex--) {
            variable = variables[variableIndex];
            regexSize = pattern.regexPlain[variableIndex].length;
            // if variable is not same as the one from timeseries, exit
            if (ts.variable !== variable) {
              continue;
            }

            //ctx.fillStyle = (variables.length  0) ? p.color : d3.color(p.color).brighter(0.5);
            // loop on locations
            for (locationIndex = 0; locationIndex < locationSize; locationIndex++) {
              location = pattern.locations[locationIndex];
              // if pattern that we want to draw has another location overlappped, exit
              if (mode === 'timestamp') {
                diff = ts.xScale(minDomain + location.duration / pattern.length);
              }
              if (diff < 3 || pattern.locations[locationIndex + 1] && pattern.locations[locationIndex + 1].pos - location.pos <= pattern.length) {
                continue;
              }

              // if its paired variable and k is different to 0 (not first), show it with brighter color
              var color;
              if (variableIndex === 0) {
                color = d3.color(pattern.color);
              } else {
                color = d3.color(pattern.color).brighter(0.5);
                color.opacity = 0.8;
              }
              var minmaxRegex = ts.bps.bps[0].key + ts.bps.bps[ts.bps.bps.length - 1].key;
              //ctx.fillStyle = color;
              // All conditions are ok, draw each squares from pattern representation
              // TODO : look on regexPlain (a bit tricky)
              for (regexIndex = regexSize - 1; regexIndex >= 0; regexIndex--) {
                var c = d3.color(color);
                square = pattern.regexPlain[variableIndex][regexIndex];
                if (square === minmaxRegex) {
                  c.opacity = 0.4;
                }
                ctx.fillStyle = c;
                // TODO: find better way
                x1 = ts.xScale(mode === 'timestamp' ? location.timestamp : location.pos) + regexIndex * diff - diff / 2;
                y1 = ts.yScale(ts.bps.map[square[0]].min);
                y2 = ts.yScale(square.length > 1 ? ts.bps.map[square[1]].max : ts.bps.map[square[0]].max);
                // draw square
                ctx.fillRect(x1, y2, diff, Math.abs(y2 - y1));

                if (diff > 6 && ts.underlined) {
                  ctx.strokeStyle = 'black';
                  ctx.lineWidth = 1;
                  // draw borders
                  ctx.strokeRect(x1, y2, diff, Math.abs(y2 - y1));
                }
              }
            }
          }
        }
      }
    }

    /**
     * Redraw main function
     * @param  {Array}   ds          timeseries
     * @param  {Object}  ctx         canvas context
     * @param  {Object}  hover       object in hover
     * @param  {Object}  size        width and height
     * @param  {Boolean} quickRedraw option
     */

  }, {
    key: 'redraw',
    value: function redraw() {
      var ds = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
      var ctx = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var hover = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
      var size = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
      var quickRedraw = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
      var mode = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : 'timestamp';

      var self = this;
      // default value
      var _lineWidth = 1,
          _highlightLineWidth = 2;

      if (ds.length === 0) {
        return;
      }
      // debug usecase
      if (self.debug) {
        console.time('redrawCanvas');
      }
      // diff between each point (estimation)
      var diff = 0;
      // we look on data (first ts as sample)
      if (ds[0] && ds[0].coords && ds[0].coords.length > 1) {
        //diff = d3.max([Math.abs(ds[0].coords[0].x - ds[0].coords[1].x), diff]);
        var coord = ds[0].coords[0];
        var coord2 = ds[0].coords[1];
        // TODO: find better way
        var key = mode === 'timestamp' ? 'timestamp' : 'idx';
        diff = Math.round(Math.abs(ds[0].xScale(coord[key]) - ds[0].xScale(coord2[key]))) * 2 / ((coord.size || 1) + (coord2.size || 1));
      }

      var ts = void 0,
          color = void 0,
          lineWidth = void 0,
          nbPoints = 0;
      // loop on timeseries
      for (var i = 0; i < ds.length; i++) {
        /*jshint loopfunc: true */
        ts = ds[i];

        // draw patterns (background)
        // tsuid use to look like on ts01_var1-var2#var2 => it's the sequence of the second variable
        // this flag is used to know if we skip or not background pattern rendering for this ts
        // if it is the second part of a pair variable, we skip
        // TODO: try to simplify it
        var toBeSkipped = false;

        var tsuidSplitted = ts.tsuid.split('#');
        if (tsuidSplitted.length > 1 && ts.visible && !hover) {
          var _variable = tsuidSplitted[1];
          var _items = tsuidSplitted[0].split('-');
          if (_items.indexOf(_variable) === _items.length - 1) {
            toBeSkipped = true;
          }
        }

        if (!toBeSkipped) {
          self.drawPatterns([ts], ctx, size[1], diff, mode);
        }
        // draw patterns areas (squares)
        self.drawPatternAreas([ts], ctx, diff, mode);
      }

      // loop on timeseries
      for (var _i = 0; _i < ds.length; _i++) {
        /*jshint loopfunc: true */
        ts = ds[_i];

        // draw patterns (background)
        // tsuid use to look like on ts01_var1-var2#var2 => it's the sequence of the second variable
        // this flag is used to know if we skip or not background pattern rendering for this ts
        // if it is the second part of a pair variable, we skip
        // TODO: try to simplify it

        // count points (debug)
        nbPoints += ts.coords.length;
        // get color
        color = d3.color(ts.color);
        // set linewidth using default value
        lineWidth = _lineWidth;

        if (hover) {
          if (hover.tsuid === ts.tsuid) {
            lineWidth = _highlightLineWidth;
            var white = d3.color('white');
            white.opacity = 0.5;
            self.drawLine(ts.coords, ctx, lineWidth * 5, white, ts.style);
          }
          //else {
          //color.opacity = 0.4;
          //}
        }

        if (!quickRedraw) {
          // draw variations
          //self.drawMinMaxLines(coords, undefined, ctx);
          // draw dots
          if (hover && hover.tsuid === ts.tsuid) {
            self.drawDotsLine(ts.coords, ts.radius, color, ctx);
          }
        }

        // draw line
        self.drawLine(ts.coords, ctx, lineWidth, color, ts.style);
        //self.drawArea(coords, ctx, size[1], ts.color);
      }
      // draw highlighted dot
      if (!quickRedraw) {
        ds.forEach(function (d) {
          if (d.closest && (!hover || d.variable === hover.variable)) {
            self.drawDot(d.closest, 3, d.color, ctx);
          }
        });
        if (hover) {
          self.drawDot(hover, hover.radius, hover.color, ctx);
        }
      }

      // debug usecase
      if (self.debug) {
        console.log(nbPoints + ' points drawed in canvas');
        console.timeEnd('redrawCanvas');
      }
    }
  }]);

  return CanvasCurveDrawer;
}();

module.exports = CanvasCurveDrawer;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"34":34}],27:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * An interface to use to implement vizsss from LIG.
 *
 * @constructor
 * @param {string} container - the ID of the injected div
 * @param {Object} data - the data used in the visualization
 *                  Contains : engine - the parent engine instance
 */

// Abstract class
var LIGViz = function () {
    function LIGViz(container, data) {
        _classCallCheck(this, LIGViz);

        // Avoid abstract instance
        if (new.target === LIGViz) {
            throw new TypeError("Cannot construct Abstract instances directly");
        }
        this.name = "";
        this.data = data;
        this.container = container;
    }

    /**
     * Init a LIGViz : collect and format the data (if necessary) then render the VizTool
     */


    _createClass(LIGViz, [{
        key: "display",
        value: function display() {
            throw new Error("display method of VizTool " + this.name + " has to be overridden");
        }

        /**
         * Wake up (Restore) the LIGViz with minimal recalculation.
         */

    }, {
        key: "wakeUp",
        value: function wakeUp() {
            throw new Error("wakeUp method of VizTool " + this.name + " has to be overridden");
        }

        /**
         * Persist the LIGViz for a quick restoration.
         */

    }, {
        key: "sleep",
        value: function sleep() {
            // If actions must be performed before hiding a viztool, there shall be done here
            // Example: remove pending requests, kill timers, ...
        }
    }]);

    return LIGViz;
}();

module.exports = LIGViz;

},{}],28:[function(require,module,exports){
(function (global){
'use strict';

/*jslint node: true*/
/*globals require, module*/

// less integration

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

require(46);

// dependencies
var d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null),
    dragscroll = require(1),
    $ = (typeof window !== "undefined" ? window['$'] : typeof global !== "undefined" ? global['$'] : null),
    _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null),
    VizTool = require(24),
    CanvasCurve = require(25),
    DataProvider = require(30),
    scrollBarHelper = require(35),
    LigLogger = require(33),
    SideMenu = require(37),
    keyMaster = require(2),
    ps = require(38);

/**
 * Helper to know if the val is a float value
 * @param {*} val
 */
var isFloat = function isFloat(n) {
    return n.toString().match(/^[0-9]*[.][0-9]+$/);
};
var formatNumeric = function formatNumeric(val) {
    var numberPattern = /[+-]?\d+(\.\d+)?/g;
    var newVal = '' + val;
    var matches = newVal.match(numberPattern) || [];
    matches.forEach(function (m) {
        if (isFloat(m)) {
            newVal = newVal.replace(m, parseFloat(m).toFixed(3));
        }
    });
    return newVal;
};

/**
 * Decision tree managing variables as pattern / timeseries
 *
 * @constructor
 * @param {string} container - the ID of the injected div
 * @param {Object} data - the data used in the visualization
 * @param {Array} callbacks - the list of the callbacks used by Viz
 */

var TDecisionTree = function (_VizTool) {
    _inherits(TDecisionTree, _VizTool);

    function TDecisionTree(container, data, callbacks) {
        _classCallCheck(this, TDecisionTree);

        var _this = _possibleConstructorReturn(this, (TDecisionTree.__proto__ || Object.getPrototypeOf(TDecisionTree)).call(this, container, {}, callbacks));
        // Call super-class constructor


        _this._raw_data = data;
        _this.name = "TDecisionTree";
        _this.logger = new LigLogger('lig-tdt');
        _this._options = {
            duration: 800, // use in animation transiion
            margin: {
                top: 10,
                right: 0,
                bottom: 0,
                left: 0
            },
            node: {
                known_keys: ['name', 'type', 'statistics', 'evaluation', 'distance'],
                blacklist_keys: ['node', 'observations', 'description', 'criteria', 'sequence', 'variable']
            },
            fontsize: {
                min: 12, // min fontsize depending of the available height
                max: 25, // max fonsize
                pourcent: 50, // pourcent of the available height
                isDynamic: true // use to know if the viz will adapt fontsize
            },
            pattern: {
                minFontSize: 10, // min fontsize depending of the available height
                maxFontSize: 15, // max fontsize
                legendWidthPourcent: 10, // pourcent of the legend in the pattern representation
                fontSizePourcent: 66, // pourcent of the pattern height from the spa
                minusFontSize: 2, // value that we remove from the pattern height if the font size is bigger than the space available
                containerPourcent: 70 // helper to know the height of the pattern representation
            },
            allowedDiff: 5, // diff allowed between previous and current state (INTERN)
            separationLabelNode: 5, // use to know the distance between the node and its label
            separationDoubleDash: 3, // use to know the distance outer and inner circles
            pourcentSeparationPattern: 3, // used to display pattern node
            minChartWidth: 80, // if chart width is less than it, collapseMode is turned on
            minChartHeight: 20, // if chart height is less than it, collapseMode is turned on
            endingCircleRadius: 50, // use to know the radius onf the ending leaf
            foldedCircleRadius: 50, // use to know the raidus of the folded node
            width: null, // width of the widget //TODO: move to obj
            height: null, // height of the widget //TODO: move to obj
            autosize: true, // if it's true set height and width in looking on the parent container
            autofit: true,
            nodeWidth: null, // width of the node //TODO: move to obj
            nodeHeight: null, // height of the node //TODO: move to obj
            waitBeforeDisplay: 0,
            maxNodeHeight: 2000, // max height of the node TODO: check
            coeffWidth: 1, // coefficient width proportionality // doesnt impact the tree size
            coeffHeight: 1, // coefficient height proportionality // doesnt impact the tree size
            curveMode: 'index', // use in node with charts (timestamp|index)
            scale: 1, // scale of the widget
            minNodeHeight: 10, // min height of the node
            minNodeWidth: 10, // min width of the node
            pourcentVerticalSeparation: 20, // use to now the vertical separation between each node,
            linkWidthRelativeTo: 'normal', // (normal|root|parent)
            linkMinWidth: 2, // min allowed width for a link
            linkMaxWidth: 30, // use in parent/root mode (linkWidthRelative)
            linkMaxWidthShowArrow: 5,
            menuType: 'slide', // {slide|fixed}
            menuPourcentWidth: 15,
            initialMaxNbNodes: 30,
            depth: null, // use this option to limit number of displayed in looking on its depth value
            _separation: function _separation(d) {
                return d._children ? 1 : 1;
            }, // separation fn used by d3 tree layout // dont change
            dataset: null // dataset
        };

        // objects used in the widget
        _this.obj = {
            tree: null, // d3 tree layout
            root: null, // d3 assigned data,
            arrows: [{ active: true, id: 0, order: 'asc', class: 0 }, { id: 1, order: 'asc', class: 1 }, { id: 2, order: 'asc', class: 2 }, { id: 3, order: 'asc', class: 3 }, { id: 4, order: 'asc', class: 4 }],
            colors: null, // list of colors (one color -> one class)
            menu: new SideMenu(), // menu to interact with the graph
            dataProvider: new DataProvider(), // dataprovider
            cacheMenu: {}, // use to save async results in a cache
            scrollBarWidth: scrollBarHelper.getGenericScrollbarWidth(),
            selectedNode: null, // use to save the selected node object,
            highlightPath: [],
            focusMode: false, // use to create a mode to zoom on the selected node,,
            selectedTrace: null,
            selectedCell: null
        };

        // raw dom or d3 node element
        _this.dom = {
            svg: null, // d3 svg element
            linksContainer: null, // d3 group element for links
            nodesContainer: null, // d3 group element for nodes
            htmlNodesContainer: null, // d3 div element to group html nodes
            subContainer: null, // d3 div element (first child of the container)
            wrapper: null // d3 div element (first child of the subcontainer)
        };

        // in case we have an already tree result, fetch dataset name
        if (_this._raw_data && _this._raw_data.tree && _this._raw_data.header) {
            _this._options.dataset = _this._raw_data.header.learningSet || _this._raw_data.header.LearningSet;
            // look if the provider is from IKATS
            // FIXME: hardcoding
            _this._options.ikats = _this._raw_data.header.builder === 'IKATS';
        }
        return _this;
    }
    /**
     * Function to fold in depth (each child will have its children folded)
     * @param  {Object} d , the current node
     */


    _createClass(TDecisionTree, [{
        key: '_collapseNode',
        value: function _collapseNode(d) {
            var uncollapse = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
            var indepth = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

            var self = this;
            // Collapse the node and all it's children
            if (d.children && !uncollapse) {
                d._children = d.children;
                if (indepth) {
                    d._children.forEach(function (v) {
                        return self._collapseNode(v, uncollapse, indepth);
                    });
                }
                d.children = null;
            } else if (uncollapse && (d.children || d._children)) {
                d.children = d._children || d.children;
                if (indepth) {
                    d.children.forEach(function (v) {
                        return self._collapseNode(v, uncollapse, indepth);
                    });
                }
                (d.children || []).forEach(function (v) {
                    return self._collapseNode(v, true);
                });
                d._children = null;
            }
        }
        /**
         * Render Menu that will be used to interact with tree layout
         *
         * @memberof TDecisionTree
         */

    }, {
        key: '_renderMenu',
        value: function _renderMenu() {
            var self = this;
            // TODO: check if there is another way (easier)
            // compute the maxdepth
            var maxDepth = d3.max(d3.hierarchy(self.data.tree).descendants().filter(function (d) {
                return d._children || d.children;
            }).map(function (d) {
                return d.depth;
            }));
            var first_bloc_items = [];
            if (!self._options.autofit) {
                first_bloc_items.push({
                    label: 'fit',
                    help: 'Change the tree display to fit all space available',
                    type: 'button',
                    onClick: function onClick() {
                        self._options.nodeWidth = null;
                        self._options.nodeHeight = null;
                        self._options.scale = 1;
                        self.obj.focusMode = false;
                        self.drawsTree();
                        self._renderMenu();
                    }
                });
            }
            first_bloc_items.push({
                label: 'autofit',
                value: self._options.autofit,
                help: 'Display the whole tree',
                type: 'boolean',
                onChange: function onChange(a, b) {
                    self._options.autofit = b.checked;
                    if (self._options.autofit) {
                        self.drawsTree();
                    }
                    self._renderMenu();
                }
            }, {
                label: 'uncollapse all',
                help: 'Display the whole tree',
                type: 'button',
                onClick: function onClick() {
                    self._collapseNode(self.obj.root, true);
                    self.drawsTree();
                }
            });

            if (!self._options.ikats) {
                first_bloc_items.push({
                    label: 'datasets',
                    help: 'Change the scale/zoom',
                    type: 'select',
                    inline: true,
                    value: self._options.dataset,
                    values: null,
                    provider: function provider() {
                        var labelId = this.label;
                        // get cache item
                        return Promise.resolve(self.obj.cacheMenu[labelId]).then(function (cachedValues) {
                            // return cache values or promise from dataprovider
                            if (self._raw_data && self._raw_data.tree && self._raw_data.header) {
                                return [self._raw_data.header.learningSet || self._raw_data.header.LearningSet];
                            }
                            return cachedValues ? cachedValues : self.obj.dataProvider.getDatasetIds(false);
                        }).then(function (values) {
                            // store values in cache
                            self.obj.cacheMenu[labelId] = values;
                            // in case we have no initial datasetid
                            // find it in the result from the provider
                            // TODO: find another way less hacky
                            if (!self._options.dataset) {
                                self._options.dataset = values[0];
                                self.display();
                            }
                            return values;
                        });
                    },
                    onChange: function onChange(data, elem) {
                        self._options.dataset = elem.value;
                        self._options.scale = 1;
                        self._options.nodeHeight = null;
                        self._options.nodeWidth = null;
                        self.obj.focusMode = false;
                        self._options.depth = null;
                        self.obj.selectedNode = null;
                        self.obj.root.charts = null;
                        self.obj.root.pictures = null;
                        self.obj.selectedCell = null;
                        self.obj.root.descendants().forEach(function (d) {
                            d.charts = null;
                            d.pictures = null;
                        });
                        // make empty the links/nodes/htmlnodes
                        self.dom.container.selectAll('.nodes,.html-nodes,.links').html('');
                        self._getTreeResult().then(function () {
                            // render the viz
                            self.render();
                            // render the menu
                            self._renderMenu();
                        });
                    }
                });
            }

            var additionals = [];
            // skipped if we are in autofit mode
            if (!self._options.autofit) {
                additionals = [{
                    label: 'width',
                    help: 'Change the displayed node width (impact the tree coords)',
                    type: 'range',
                    inline: true,
                    value: self._options.nodeWidth,
                    interval: [self._options.minNodeWidth, self._options.width * 0.7],
                    step: 10,
                    onChange: function onChange(data, elem) {
                        self._options.nodeWidth = elem.value;
                        self.drawsTree();
                    }
                }, {
                    label: 'height',
                    help: 'Change the displayed node height',
                    type: 'range',
                    inline: true,
                    value: self._options.nodeHeight,
                    interval: [self._options.minNodeHeight, self._options.height * self._options.coeffHeight],
                    step: 1,
                    onChange: function onChange(data, elem) {
                        self._options.nodeHeight = elem.value;
                        self.drawsTree();
                    }
                }];
            }
            additionals.push({
                label: 'depth',
                help: 'Change filter depth',
                type: 'number',
                inline: true,
                value: self._options.depth,
                interval: [0, maxDepth],
                step: 1,
                onChange: function onChange(data, elem) {
                    self._options.depth = elem.value;
                    //  assigns the data to a hierarchy using parent-child relationships
                    self.obj.root = d3.hierarchy(self.data.tree);
                    self.obj.root.descendants().forEach(function (d) {
                        return self._options.depth && d.depth > self._options.depth ? self._collapseNode(d) : null;
                    });
                    self.drawsTree();
                    self._renderMenu();
                }
            });
            // skipped if we are in autofit mode
            if (!self._options.autofit) {
                additionals.push({
                    label: 'scale',
                    help: 'Change the scale/zoom',
                    type: 'range',
                    inline: true,
                    value: self._options.scale,
                    interval: [0.1, 3],
                    step: 0.1,
                    onChange: function onChange(data, elem) {
                        self._options.scale = elem.value;
                        self.drawsTree();
                    }
                });
            }

            additionals.push({
                label: 'link mode',
                help: 'Change link width mode',
                type: 'select',
                inline: true,
                values: ['normal', 'parent', 'root'],
                value: self._options.linkWidthRelativeTo,
                onChange: function onChange(data, elem) {
                    self._options.linkWidthRelativeTo = elem.value;
                    self.drawsTree();
                }
            });

            // maps the node data to the tree layout
            var nodes = self.obj.tree(self.obj.root).descendants();
            var nodes2 = self.obj.tree(d3.hierarchy(self.data.tree)).descendants();
            var nb = {
                leaves: [nodes2.filter(function (d) {
                    return d.data.type === 'leaf';
                }).length, nodes.filter(function (d) {
                    return d.data.type === 'leaf';
                }).length]
            };
            var infosGeneral = [];
            var infosNode = [];
            var _formatValue = function _formatValue(property_name, node_data) {
                var value = node_data.data.raw[property_name] || (node_data.data.raw.description || {})[property_name];
                if (property_name === 'statistics') {
                    var stastisticsContent = node_data.data.repartition.map(function (a) {
                        return '<tr><td>' + a.class + '</td><td>' + a.pourcent.toFixed(2) + '%</td><td>' + a.nb + '</td></tr>';
                    }).join('');
                    value = '<table class="table">' + stastisticsContent + '</table>';
                } else if (property_name === 'evaluation' && value) {
                    value[1] = formatNumeric(value[1]);
                }
                return '' + (value || '');
            };
            var _pushValue = function _pushValue(property_name, node_data) {
                var value = _formatValue(property_name, node_data);
                if (value !== null) {
                    infosNode.push([property_name, value]);
                }
            };
            // prepare display of additional informations about the selected node
            if (self.obj.selectedNode) {
                var object_keys = d3.keys(self.obj.selectedNode.data.raw || {}),
                    description_keys = d3.keys((self.obj.selectedNode.data.raw || {}).description || {}),
                    keys = object_keys.concat(description_keys);
                self._options.node.known_keys.forEach(function (a) {
                    return _pushValue(a, self.obj.selectedNode);
                });
                keys.forEach(function (a) {
                    if (self._options.node.known_keys.indexOf(a) === -1 && self._options.node.blacklist_keys.indexOf(a) === -1) {
                        _pushValue(a, self.obj.selectedNode);
                    }
                });

                if (self.obj.selectedNode.data.nodeType === 'similarity') {
                    infosNode.push(['obs', self.obj.selectedNode.data.viz[0][0].key + ' (' + self.obj.selectedNode.data.viz[0][0].class + ')']);
                } else if (self.obj.selectedNode.data.nodeType === 'comparison') {
                    infosNode.push(['obs 1', self.obj.selectedNode.data.viz[0][0].key + ' (' + self.obj.selectedNode.data.viz[0][0].class + ')']);
                    infosNode.push(['obs 2', self.obj.selectedNode.data.viz[1][0].key + ' (' + self.obj.selectedNode.data.viz[1][0].class + ')']);
                }
            }

            infosGeneral.push(['nodes', '<i>' + nodes2.length + '</i>']);
            infosGeneral.push(['leaves', '<i>' + nb.leaves[0] + '</i>']);
            if (self.data.md) {
                infosGeneral.push(['version', '' + self.data.md.version]);
                infosGeneral.push(['date', '' + self.data.md.date]);
            }

            var traceKeyWord = 'trace';
            var traces = [];
            if (self.obj.validation) {
                traces = ((self.obj.validation[self.obj.validation.findIndex(function (d) {
                    return d.key === traceKeyWord;
                })] || []).value || []).map(function (t) {
                    var cls = "different";
                    var idx = t[3].indexOf(t[1]);
                    if (idx === 0) {
                        cls = t[3].length > 1 ? "majority" : "same";
                    } else if (idx > -1) {
                        cls = "minority";
                    }
                    return t.concat(['<span class="' + cls + '"></span>']);
                });
            }

            var validations = [];
            (self.obj.validation || []).forEach(function (v) {
                var text = void 0,
                    fullscreenable = void 0,
                    compute = void 0;

                if (v.key === traceKeyWord) {
                    return;
                }

                if (v.key === 'confusion-matrix') {
                    // function to compute html matrix table, will be called during menu rendering
                    compute = function compute(elem) {
                        var html = '';
                        v.value.forEach(function (l, i) {
                            fullscreenable = true;
                            var cells = [v.value[0][i - 1] || ''].concat(v.value[i]).map(function (a, j) {
                                var clss = '',
                                    color = '';
                                if (j === i) {
                                    clss = j > 0 ? 'diagonal' : 'separator';
                                } else if (j === 0 && i > 0) {
                                    color = self.obj.colors(a);
                                }
                                if (j > 0 && i > 0) {
                                    clss += ' cell';
                                }
                                return '<td style="background-color: ' + color + '" class="' + clss + '" predict="' + (v.value[0][i - 1] || '') + '" real="' + (v.value[0][j - 1] || '') + '">' + a + '</td>';
                            });
                            html += '<tr>' + cells.join('') + '</tr>';
                        });
                        var innerText = '<div class="confusion-matrix"><table class="table table-bordered table-responsive text-center">' + html + '</table></div>';
                        var d3Elem = d3.select(elem).html(innerText);
                        d3Elem.selectAll('td.cell').on('click', function () {
                            var d3Elem = d3.select(this);
                            var cell = {
                                predict: d3Elem.attr('predict'),
                                real: d3Elem.attr('real'),
                                value: d3Elem.text()
                            };
                            if (cell.predict && cell.real) {
                                if (self.obj.selectedCell && cell.predict === self.obj.selectedCell.predict && cell.real === self.obj.selectedCell.real) {
                                    self.obj.selectedCell = null;
                                } else {
                                    self.obj.selectedCell = cell;
                                }
                                self._renderMenu();
                            }
                        }).classed('selected', function () {
                            var d3Elem = d3.select(this);
                            return self.obj.selectedCell && self.obj.selectedCell.predict === d3Elem.attr('predict') && self.obj.selectedCell.real === d3Elem.attr('real');
                        });
                    };
                } else if (v.key === 'arguments') {
                    text = '<table class="table">' + v.value.map(function (a) {
                        return '<tr><td>' + a[0] + '</td><td>' + a[1] + '</td></tr>';
                    }).join('') + '</table>';
                } else if (v.key === 'method') {
                    text = v.value[0] + ' => ' + v.value.slice(1).map(JSON.stringify).join('');
                } else if (v.key === 'accuracy') {
                    text = v.value;
                } else {
                    text = JSON.stringify(v.value);
                }
                validations.push([v.key, text, {
                    fullscreenable: fullscreenable,
                    compute: compute
                }]);
            });
            var blocs = [{
                collapsable: false,
                showTitle: false,
                title: 'permanent',
                type: 'controls',
                data: first_bloc_items
            }, {
                collapsable: true,
                title: 'settings',
                type: 'controls',
                data: additionals
            }, {
                collapsable: true,
                title: 'general',
                type: 'informations',
                data: infosGeneral
            }, {
                collapsable: true,
                title: 'node',
                type: 'informations',
                data: infosNode
            }, {
                collapsable: true,
                title: 'validation',
                type: 'informations',
                data: validations
            }];
            if (traces) {
                blocs.push({
                    collapsable: true,
                    title: 'trace',
                    type: 'controls',
                    data: [{
                        help: 'Trace management',
                        type: 'search',
                        columns: [{
                            label: 'Obs'
                        }, {
                            label: 'R'
                        }, {
                            visible: false
                        }, {
                            label: 'P'
                        }, {
                            label: 'Diag',
                            onSort: function onSort(d) {
                                return ['good', 'majority', 'minority', 'different'].findIndex(function (v) {
                                    return d.indexOf(v) > -1;
                                });
                            }
                        }],
                        showCount: true,
                        arrows: self.obj.arrows,
                        selected: self.obj.selectedTrace,
                        data: traces.filter(function (trace) {
                            return self.obj.selectedCell ? // if there is a selected cell, use it as criteria
                            trace[3][0] === self.obj.selectedCell.predict && trace[1].indexOf(self.obj.selectedCell.real) > -1 : !self.obj.selectedNode || trace[2].indexOf(self.obj.selectedNode.id) > -1; // else, look on selectedNode
                        }),
                        onClick: function onClick(trace, elem, opts) {
                            self.obj.selectedTrace = opts.selected;
                            self.obj.highlightPath = opts.selected ? trace[2] : [];
                            self.renderHighlightedPath();
                        },
                        onSortChange: function onSortChange(arrows) {
                            return self.obj.arrows = arrows;
                        }
                    }]
                });
            }
            self.obj.menu.setContainer(d3.select('#' + self.container).select('.tree-controls'));
            self.obj.menu.blocs(blocs);
            self.obj.menu.refresh();
            self.obj.menu.on('change', function () {
                self._options.nodeHeight = null;
                self._options.nodeWidth = null;
                self.drawsTree();
                setTimeout(function () {
                    ps.update(d3.select('#' + self.container).select('.tree-controls').node());
                }, 50);
            });
            self.obj.menu.on('layoutchange', function () {
                setTimeout(function () {
                    ps.update(d3.select('#' + self.container).select('.tree-controls').node());
                }, 50);
            });
            // handle event when user resize the panel
            self.obj.menu.on('resize-x', function (newValue) {
                self._options.menuPourcentWidth = newValue * 100 / self._options.width;
                self.render();
            });
            ps.update(d3.select('#' + self.container).select('.tree-controls').node());
        }

        /**
         * Get async result data for the tree
         * @return {Promise} promise
         */

    }, {
        key: '_getTreeResult',
        value: function _getTreeResult() {
            var self = this;
            if (self._options.dataset === null) {
                // init empty tree
                self.data.tree = {};
                return Promise.resolve();
            }
            return self.obj.dataProvider.getDataset(self._options.dataset, self._raw_data).then(function (dataset) {
                self.obj.validation = d3.entries(dataset.validation);
                self.obj.hasWeights = dataset.md.hasWeights;
                self.data.md = dataset.md;
                return self.obj.dataProvider.getTreeResult(self._options.dataset, self._raw_data);
            }).then(function (treeResult) {
                self.data.tree = treeResult;
                var allClasses = [],
                    allColors;
                if (self.data.tree && self.data.tree.repartition) {
                    allClasses = self.data.tree.repartition.map(function (d) {
                        return d.class;
                    });
                    allColors = self.data.tree.repartition.map(function (d) {
                        return d.color;
                    });
                }
                self.obj.colors = d3.scaleOrdinal().domain(allClasses).range(allColors || d3.schemeCategory20c);
            });
        }

        /**
         * Init the VizTool : collect and format the data (if necessary) then render the VizTool
         */

    }, {
        key: 'display',
        value: function display() {
            var self = this;
            self.logger.log('display');
            self._getTreeResult().then(function () {
                setTimeout(function () {
                    self.render();
                    self._renderMenu();
                    // resize handler
                    d3.select(window).on('resize', function () {
                        // get the d3 element
                        var container = d3.select('#' + self.container);
                        var boundRect = container.node().getBoundingClientRect();
                        // if autosize and if the diff is bigger than 10 recompute width and height.
                        if (self._options.autosize && (Math.abs(self._options.width - boundRect.width) > 10 || Math.abs(self._options.height - boundRect.height) > 10)) {
                            self._options.nodeHeight = self._options.nodeWidth = null;
                            self._options.width = boundRect.width;
                            self._options.height = boundRect.height > 0 ? boundRect.height : self._options.height;
                            self.drawsTree();
                            self._renderMenu();
                        }
                    });
                }, self._options.waitBeforeDisplay);
            });
        }

        /**
         * Render the vizTool
         */

    }, {
        key: 'render',
        value: function render() {
            var self = this;
            self.logger.log('render');

            //  assigns the data to a hierarchy using parent-child relationships
            var root = d3.hierarchy(self.data.tree, function (d) {
                return d.children || d._children;
            });

            // get the d3 element
            var container = d3.select('#' + self.container);
            if (container.selectAll('.tree').size() === 0) {
                container.html('');
            }

            // remove all content
            //container.html('');

            // if autosize, get width and height in looking on the container
            if (self._options.autosize) {
                var boundRect = container.node().getBoundingClientRect();
                self._options.width = Math.floor(boundRect.width);
                self._options.height = boundRect.height > 0 ? Math.floor(boundRect.height) : self._options.height;
            }

            var width = self._options.width - (!self.obj.menu.isCollapsed() ? self._options.width * (self._options.menuPourcentWidth / 100) : 0) - self._options.margin.left - self._options.margin.right - 3;
            // collapse all descendants where depth > self._options.depth
            root.descendants().forEach(function (d) {
                return self._options.depth && d.depth > self._options.depth ? self._collapseNode(d) : null;
            });
            // assign first position
            root.x0 = width / 2;
            root.y0 = 0;

            self.obj.root = root;

            // appends a div as sub container to manage better layout (dragscroll feature)
            var treeContainer = container.selectAll('.tree').data([0]).enter().append('div').attr('class', 'tree');

            self.dom.container = container.select('.tree'); //treeContainer.append('div').attr('class', 'tree');
            // appends a 'div' element to the container that will be used to display controller
            var controlContainer = treeContainer.append('div').attr('class', 'tree-controls');
            self.dom.controlContainer = self.dom.container.select('.tree-controls');
            // init scrollbar for the control container

            // appends a 'div' element to the container that will be ussed tp
            var subContainer = treeContainer.append('div').attr('class', 'tree-graph dragscroll');
            self.dom.subContainer = self.dom.container.select('.tree-graph');
            // init scrollbar for the subcontainer
            if (controlContainer.size() > 0) {
                ps.initialize(self.dom.controlContainer.node());
                ps.initialize(self.dom.subContainer.node());
            }
            // appends the svg obgect to the body of the page
            var wrapper = subContainer.append('div').attr('class', 'tree-wrapper');
            self.dom.wrapper = self.dom.subContainer.select('.tree-wrapper');
            // appends a 'div' element to the wrapper that will be use to group html nodes
            wrapper.append('div').attr('class', 'html-nodes');
            self.dom.htmlNodesContainer = self.dom.wrapper.select('.html-nodes');
            // appends the svg obgect to the body of the page
            // appends a 'group' element to 'svg'
            // moves the 'group' element to the top left margin
            var svg = wrapper.append("svg").attr('class', 'svg').append("g").append("g").attr("transform", 'translate(' + self._options.margin.left + ',' + self._options.margin.top + ')');
            self.dom.svg = self.dom.wrapper.select('svg.svg > g > g');
            // appends a 'group' element to 'svg' that will be use to group links
            svg.append('g').attr('class', 'links');
            svg.append('g').attr('class', 'nodes');
            self.dom.linksContainer = self.dom.svg.select('.links');
            // appends a 'group' element to 'svg' that will be use to group nodes
            self.dom.nodesContainer = self.dom.svg.select('.nodes');
            self.drawsTree();

            // reset dragscroll behaviour
            dragscroll.reset();
        }

        /**
         * Render Highlighted path of nodes
         */

    }, {
        key: 'renderHighlightedPath',
        value: function renderHighlightedPath() {
            var self = this,
                container = d3.select('#' + self.container);

            container.selectAll('.link').classed('highlighted', function (link) {
                return self.obj.highlightPath.indexOf(link.parent.id) > -1 && self.obj.highlightPath.indexOf(link.id) > -1;
            });
            container.selectAll('g.node, .graph-node').classed('highlighted', function (node) {
                return self.obj.highlightPath.indexOf(node.id) > -1;
            });
        }

        /**
         * Tree display
         * @param  {Object} source - will be used in transitions
         */

    }, {
        key: 'drawsTree',
        value: function drawsTree() {
            var source = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.obj.root;


            var self = this;
            var menuWidth = self._options.menuType === 'slide' && !self.obj.menu.isCollapsed() ? self._options.width * (self._options.menuPourcentWidth / 100) : 0;
            var graphWidth = self._options.width - menuWidth;
            var _getBpsIndex = function _getBpsIndex(bps, e) {
                var idx = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : e.length - 1;
                return bps.findIndex(function (b) {
                    return b.key === e.charAt(idx);
                });
            };
            var _generatePattern = function _generatePattern(elem, pattern, variable) {
                var length = pattern.length,
                    legendWidth = Math.floor(nodeWidth * self._options.pattern.legendWidthPourcent / 100),
                    widthStep = Math.floor(nodeWidth / pattern.variables.length - legendWidth) / length;
                // index of the variable we will look
                var variableIdx = pattern.variables.indexOf(function (v) {
                    return v === variable;
                });
                if (variableIdx === -1) {
                    variableIdx = 0;
                }
                var heightStep = nodeHeight * (self._options.pattern.containerPourcent / 100) / pattern.bps[variableIdx].length;
                var patternFontSize = Math.floor(heightStep < self._options.pattern.minFontSize ? heightStep - self._options.pattern.minusFontSize : d3.min([self._options.pattern.maxFontSize, heightStep * (self._options.pattern.fontSizePourcent / 100)]));

                var svg = elem.append('svg').attr('class', 'pattern').attr('width', '100%').attr('height', '100%').attr('preserveAspectRatio', 'none').attr('viewBox', '0 0 ' + nodeWidth / pattern.variables.length + ' ' + pattern.bps[variableIdx].length * heightStep).append('g');

                var domain = pattern.bps[variableIdx].map(function (b) {
                    return b.key;
                });
                domain.push('');

                var yScale = d3.scalePoint().domain(domain.reverse()).range([0, pattern.bps[variableIdx].length * heightStep]);

                var yaxisGroup = svg.append("g").attr("class", "yaxis").attr("transform", 'translate(' + legendWidth + ',0)');
                yaxisGroup.call(d3.axisLeft(yScale).tickSize(legendWidth));
                yaxisGroup.selectAll('.tick').append('line').attr('class', 'big-line').attr('x1', -legendWidth).attr('x2', pattern.length * widthStep).attr('stroke-width', 0.5);
                //.attr('dy', 0.5);
                yaxisGroup.selectAll('.tick line').attr('y1', null).attr('y2', null).attr('dy', null);

                yaxisGroup.selectAll('text').style('font-size', patternFontSize + 'px').attr('transform', 'translate(' + legendWidth * 2 / 3 + ', -' + heightStep / 2 + ')');

                var xaxisGroup = svg.append("g").attr("class", "xaxis").attr('transform', 'translate(' + legendWidth + ',0)');

                var index = void 0;
                for (index = 0; index < pattern.length; index++) {
                    xaxisGroup.append('line').attr('class', 'big-line').attr('x1', index * widthStep).attr('x2', index * widthStep).attr('y1', '0').attr('y2', pattern.bps[variableIdx].length * heightStep);
                }
                pattern.variables.forEach(function (v, i) {
                    if (variable && variable !== v) {
                        return;
                    }
                    var rp = pattern.regexPlain[i];
                    for (index = 0; index < rp.length; index++) {
                        var e = rp[index],
                            h = e.length !== 1 ? Math.abs(_getBpsIndex(pattern.bps[i], e, 0) - _getBpsIndex(pattern.bps[i], e, 1) - 1) * heightStep : heightStep;
                        svg.append('rect').attr('x', index * widthStep + legendWidth).attr('y', pattern.bps[i].length * heightStep - (_getBpsIndex(pattern.bps[i], e) + 1) * heightStep).attr('width', widthStep).attr('height', h).attr('fill', pattern.color[i]).classed('dot', pattern.bps[i][0].key === e[0] && pattern.bps[i][pattern.bps[i].length - 1].key === e[1]);
                    }
                });

                svg.selectAll('line, path, text, rect').attr('vector-effect', 'non-scaling-stroke');
            };

            //const container = d3.select('#' + self.container);
            self.dom.subContainer.style('width', graphWidth + 'px').style('height', self._options.height + 'px');
            var treeData, links, nodes;

            var detectBestSize = function detectBestSize() {
                var detectSize = function detectSize() {
                    var radius = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;

                    // compute width and height
                    var width = graphWidth - self._options.margin.left - self._options.margin.right,
                        // - (radius * 2),
                    height = self._options.height - self._options.margin.top - self._options.margin.bottom - radius * 2 - 5;

                    // declares a tree layout and assigns the size
                    var tmpTree = d3.tree().size([width, height]).separation(self._options._separation);

                    // maps the node data to the tree layout
                    var tmpNodes = tmpTree(self.obj.root).descendants(),
                        maxDepth = d3.max(tmpNodes.map(function (d) {
                        return d.depth;
                    })),
                        deepeNodes = tmpNodes.filter(function (d) {
                        return d.depth === maxDepth;
                    });
                    var nodeHeight = void 0,
                        nodeWidth = void 0;
                    if (deepeNodes && deepeNodes.length > 1) {
                        nodeHeight = Math.floor(d3.min([self._options.maxNodeHeight, Math.abs(deepeNodes[0].parent.y - deepeNodes[0].y)]));
                        nodeWidth = Math.floor(d3.min(deepeNodes.map(function (nodeA) {
                            return d3.min(deepeNodes.filter(function (tmp) {
                                return tmp !== nodeA;
                            }).map(function (nodeB) {
                                return Math.abs(nodeA.x - nodeB.x);
                            }));
                        })));
                    } else if (tmpNodes.length === 1) {
                        nodeWidth = 0;
                        nodeHeight = 0;
                    } else {
                        self.logger.warn('not able to detect height and width node');
                    }
                    return {
                        nodeWidth: nodeWidth,
                        nodeHeight: nodeHeight
                    };
                };
                var size = detectSize();
                // declares a tree layout and assigns the size
                /**const tree = d3.tree()
                    .nodeSize([size.nodeWidth, size.nodeHeight])
                    .separation(self._options._separation);
                **/
                var radius2 = d3.min([self._options.endingCircleRadius, size.nodeWidth / 3, size.nodeHeight / 3]);
                return detectSize(radius2);
            };

            // if there is no nodewidth or nodeheight
            if (!(self._options.nodeWidth || self._options.nodeHeight) || self._options.autofit) {
                // compute width and height
                var sizeResult = detectBestSize();
                self._options.nodeWidth = sizeResult.nodeWidth;
                self._options.nodeHeight = sizeResult.nodeHeight;
            }
            var scale = self.obj.focusMode ? d3.min([graphWidth / (self._options.nodeWidth * self._options.coeffWidth * 1.2), self._options.height / self._options.nodeHeight]) : self._options.scale;
            var nodeWidth = self._options.nodeWidth * scale * self._options.coeffWidth,
                nodePartHeight = self._options.nodeHeight * scale,
                nodeHeight = Math.floor((nodePartHeight - self._options.pourcentVerticalSeparation * nodePartHeight / 100) * self._options.coeffHeight);

            // declares a tree layout and assigns the size
            self.obj.tree = d3.tree().nodeSize([nodeWidth / self._options.coeffWidth, nodePartHeight]).separation(self._options._separation);

            // maps the node data to the tree layout
            treeData = self.obj.tree(self.obj.root);
            nodes = treeData.descendants();

            if (!self._options.depth) {
                if (self._options.initialMaxNbNodes && self._options.initialMaxNbNodes < nodes.length) {
                    nodes.slice(self._options.initialMaxNbNodes).forEach(function (n) {
                        return self._collapseNode(n, false, true);
                    });
                    Object.assign(self._options, detectBestSize());
                    scale = self.obj.focusMode ? d3.min([graphWidth / (self._options.nodeWidth * self._options.coeffWidth * 1.2), self._options.height / self._options.nodeHeight]) : self._options.scale;
                    nodeWidth = self._options.nodeWidth * scale * self._options.coeffWidth;
                    nodePartHeight = self._options.nodeHeight * scale;
                    nodeHeight = Math.floor((nodePartHeight - self._options.pourcentVerticalSeparation * nodePartHeight / 100) * self._options.coeffHeight);

                    // declares a tree layout and assigns the size
                    self.obj.tree = d3.tree().nodeSize([nodeWidth / self._options.coeffWidth, nodePartHeight]).separation(self._options._separation);

                    treeData = self.obj.tree(self.obj.root);
                    nodes = treeData.descendants();
                }
                self._options.depth = d3.max(nodes.filter(function (d) {
                    return d.data.type !== 'leaf';
                }).map(function (d) {
                    return d.depth;
                }));
                self.logger.log('showing ' + nodes.length + ' nodes');
            }
            links = treeData.descendants().slice(1);
            var minMaxX = d3.extent(nodes, function (d) {
                return d.x;
            }),
                minMaxY = d3.extent(nodes, function (d) {
                return d.y;
            });
            var treeDiffX = Math.abs(minMaxX[1] - minMaxX[0]),
                treeDiffY = Math.abs(minMaxY[1] - minMaxY[0]),
                leftDiff = Math.abs(nodes[0].x - minMaxX[0]);
            var diff = (graphWidth - treeDiffX) / 2;

            nodes.forEach(function (n) {
                // if the width of the tree is bigger than the container width
                if (treeDiffX >= graphWidth) {
                    n.x += nodeWidth / 4;
                } else {
                    n.x += diff;
                }
                n.x = Math.floor(n.x + leftDiff);
                n.y = Math.floor(n.y);
                n.x0 = n.x0 || n.x;
                n.y0 = n.y0 || n.y;
                n.id = n.data.id;
                if (n.depth > 0 && self._options.linkWidthRelativeTo !== 'normal') {
                    var v1 = d3.sum(n.data.repartition.map(function (d) {
                        return +d.nb;
                    }));
                    var v2;
                    if (self._options.linkWidthRelativeTo === 'parent') {
                        v2 = d3.sum(n.parent.data.repartition.map(function (d) {
                            return +d.nb;
                        }));
                    } else {
                        v2 = d3.sum(self.obj.root.data.repartition.map(function (d) {
                            return +d.nb;
                        }));
                    }
                    n.linkPourcent = v1 / v2;
                } else {
                    n.linkPourcent = null;
                }
            });

            var node = self.dom.nodesContainer.selectAll("g.node").data(nodes, function (d) {
                return d.id;
            });

            var _getFirstParentVisible = function _getFirstParentVisible(d) {
                var test = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

                if (d.parent) {
                    var t = (!test ? node.data() : nodes).filter(function (h) {
                        return h.data.id === d.parent.data.id;
                    })[0];
                    if (t) {
                        return Object.assign({}, t);
                    } else {
                        return _getFirstParentVisible(d.parent, test) || d.parent;
                    }
                }
            };

            // adapt radius to fit current context
            var endingCircleRadius = Math.floor(d3.min([self._options.endingCircleRadius, nodeWidth / 3, nodeHeight / 3])),
                foldedCircleRadius = Math.floor(d3.min([self._options.foldedCircleRadius, nodeWidth / 3, nodeHeight / 3])); //,
            // detect best font size
            var subjectHeight = Math.floor(nodeHeight * (15 / 100)),
                // 15%
            ftSize = d3.max([d3.min([Math.floor(subjectHeight * (self._options.fontsize.pourcent / 100)), self._options.fontsize.max]), self._options.fontsize.min]);
            // if font size is bigger than the height of the available area, fit it
            if (subjectHeight < self._options.fontsize.min) {
                ftSize = subjectHeight - 1;
            }

            // Creates a diagonal path from parent to the child nodes
            function diagonal(s, d) {
                /**
                return `M ${d.x} ${d.y}
                        L ${d.x}, ${d.y + (nodeHeight / 2)}
                        C ${(d.x + s.x) / 2}, ${d.y + (nodeHeight / 2)}
                          ${(d.x + s.x) / 2}, ${d.y + (nodeHeight / 2)}
                          ${s.x}, ${s.y}`;
                **/
                // TODO: how mange case if foldedCircleRadius and endingCircleRadius are different
                return 'M ' + d.x + ' ' + (d.y + nodeHeight / 2) + '\n                    L ' + s.x + ' ' + s.y + '\n                    L ' + s.x + ' ' + (s.y + (s.children ? nodeHeight / 2 : foldedCircleRadius));
            }

            // Toggle children on click.
            function click(d) {
                if (d.children) {
                    // collapse in depth
                    self._collapseNode(d, false, false);
                } else {
                    // uncollapse
                    d.children = d._children;
                    d._children = null;
                }
                // refresh graph with new chnages
                self.drawsTree(d);
                self._renderMenu();
            }

            var arc = d3.arc().outerRadius(foldedCircleRadius).innerRadius(0);

            var labelArc = d3.arc().outerRadius(foldedCircleRadius).innerRadius(foldedCircleRadius / 4);

            var pie = d3.pie().sort(null).value(function (d) {
                return d.pourcent;
            });

            /***** LINKS *****/
            var link = self.dom.linksContainer.selectAll(".link").data(links, function (d) {
                return d.id;
            });

            // ENTER
            var linkEnter = link.enter().append("path").attr("class", "link").attr("d", function (d) {
                var ob = _getFirstParentVisible(d) || source;
                var o = {
                    x: ob.x0,
                    y: ob.y0
                };
                return diagonal(o, o);
            });
            // UPDATE
            var linkUpdate = linkEnter.merge(link);

            // Transition back to the parent element position
            linkUpdate.transition().duration(self._options.duration).attr('d', function (d) {
                return diagonal(d, d.parent);
            }).style('stroke-width', function (d) {
                return self._options.linkWidthRelativeTo !== 'normal' ? d3.max([d.linkPourcent * self._options.linkMaxWidth, self._options.linkMinWidth]) + 'px' : '';
            });

            // EXIT : Remove any exiting links
            link.exit().transition().duration(self._options.duration).attr('d', function (d) {
                var ob = _getFirstParentVisible(d, true) || source;
                var o = {
                    x: ob.x,
                    y: ob.y
                };
                return diagonal(o, o);
            }).remove();

            /***** NODES *****/

            var htmlNode = self.dom.htmlNodesContainer.selectAll("div.graph-node").data(nodes.filter(function (d) {
                return d.children;
            }), function (d) {
                return d.id;
            });

            // EXIT : Remove any exiting nodes
            htmlNode.exit().remove();

            /**
             * old solution avoiding removing, just hidding
            // EXIT : Remove any exiting nodes
            htmlNode.exit()
            .filter((d) => d.hidden === false)
            .style('width', `0px`)
            .style('height', `0px`)
            .each(d => d.hidden = true)
            .classed('hidden', true);
            **/

            var updateSelectionDisplay = function updateSelectionDisplay(d) {
                var toggleValue = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

                self.obj.selectedNode = toggleValue === false || !self.obj.selectedNode || d.id !== self.obj.selectedNode.id ? d : null;
                self.dom.wrapper.selectAll('.node,.graph-node').classed('selected', function (a) {
                    return self.obj.selectedNode && self.obj.selectedNode.id === a.id;
                });
                self._renderMenu();
            };

            // ENTER : any new modes at the parent's previous position.
            var htmlNodeEnter = htmlNode.enter().append('div').attr('class', 'graph-node hidden').classed('variable--node', function (d) {
                return d.data.nodeType === 'variable';
            }).style('width', 1e-6).style('height', 1e-6).style('transform', function () {
                return 'translate(' + (source.x0 + self._options.margin.left) + 'px,' + (source.y0 + self._options.margin.top) + 'px)';
            }).on('click', function (d) {
                // if not meta pressed, select/unselect behavior
                if (keyMaster.shift) {
                    // collapse/uncollapse behavior
                    click(d);
                    d3.event.stopPropagation();
                }
                // hacky
                else if (keyMaster.command || keyMaster.getPressedKeyCodes().indexOf(0) > -1) {
                        var prevSelectNode = self.obj.selectedNode;
                        updateSelectionDisplay(d, !prevSelectNode || prevSelectNode.id !== d.id);
                        self.obj.focusMode = self.obj.selectedNode !== prevSelectNode ? !!self.obj.selectedNode : !self.obj.focusMode;
                        self._renderMenu();
                        self.drawsTree();
                    } else {
                        updateSelectionDisplay(d);
                        d3.event.stopPropagation();
                    }
            });

            var htmlNodeUpdate = htmlNodeEnter.merge(htmlNode);

            // Transition to the proper position for the node
            htmlNodeUpdate.transition().duration(self._options.duration).style('width', nodeWidth + 'px').style('height', function (d) {
                return (d.data.nodeType === 'variable' ? nodeHeight * 3 / 5 : nodeHeight) + 'px';
            }).style('transform', function (d) {
                var translate = {
                    x: d.x + self._options.margin.left - (d.children ? nodeWidth / 2 : 0),
                    y: d.y + self._options.margin.top
                };
                return 'translate(' + translate.x + 'px,' + translate.y + 'px)';
            }).on('start', function (d) {
                d.hidden = false;
                var elem = d3.select(this);
                // class used to recognize selected node
                elem.classed('selected', self.obj.selectedNode && self.obj.selectedNode.id === d.id);
                if (elem.classed('hidden')) {
                    //elem.classed('hidden', false);
                    var translate = {
                        x: d.x0 + self._options.margin.left - (d.children ? nodeWidth / 2 : 0),
                        y: d.y0 + self._options.margin.top
                    };
                    elem.style('transform', 'translate(' + translate.x + 'px,' + translate.y + 'px)');
                }
                var isCollapsed = nodeWidth / d.data.viz.length > self._options.minChartWidth && nodeHeight / 2 > self._options.minChartHeight;

                d3.select(this).classed('light', !isCollapsed).classed('in-transition', true);

                if (this.children.length === 0) {

                    elem.append('div').attr('class', 'subject').style("font-size", self._options.fontsize.isDynamic ? ftSize + 'px' : undefined).append('p').html(function (v) {
                        return v.data.subject;
                    }).attr('title', function (v) {
                        return v.data.subject;
                    });

                    var repartitionContainer = elem.append('div').attr('class', 'class-repartition').style("font-size", self._options.fontsize.isDynamic ? ftSize + 'px' : undefined);

                    repartitionContainer.selectAll('div.class').data(d.data.repartition).enter().append('div').attr('class', 'class').style('background-color', function (s) {
                        return s.color;
                    }).style('width', function (s) {
                        return s.pourcent + '%';
                    }).attr('title', function (v) {
                        return v.class + ' (' + v.nb + ')';
                    }).append('p').html(function (v) {
                        return '<span class="class-name">' + v.class + '</span> <span>(' + v.nb + ')</span>';
                    });

                    var representationContainer = elem.append('div').attr('class', 'representation');

                    var chart = representationContainer.selectAll('div.chart').data(d.data.viz);

                    chart.enter().append('div').attr('class', 'chart').attr('id', function (c, i) {
                        return 'graph-node-' + d.id + '-' + i;
                    })
                    //8p.style('margin-left', '8px')
                    .style('width', 'calc(' + 100 / d.data.viz.length + '%'); // - 8px)`);

                    chart.exit().remove();

                    if (d.data.nodeType === 'pattern') {
                        // TODO: adjust the arrow font size in depending on the node width
                        // TODO: Improve letters rendering
                        var respresentations = [],
                            sepWidth = d.data.pattern.variables.length > 1 ? self._options.pourcentSeparationPattern / (d.data.pattern.variables.length - 1) : 0,
                            patWidth = d.data.pattern.variables.length > 1 ? (100 - self._options.pourcentSeparationPattern) / d.data.pattern.variables.length : 100 / d.data.pattern.variables.length;
                        // prepare data to display in representation
                        d.data.pattern.variables.forEach(function (v, i) {
                            if (i !== 0) {
                                // arrow
                                respresentations.push({
                                    type: 'separator',
                                    width: sepWidth
                                });
                            }
                            // pattern
                            respresentations.push({
                                type: 'pattern',
                                variable: v,
                                width: patWidth
                            });
                        });
                        // if pattern has a paired variables we will create 2 svg
                        representationContainer.selectAll('.pattern__container')
                        // variables
                        .data(respresentations).enter().append('div').attr('class', 'pattern__container').classed('pattern__arrow', function (v) {
                            return v.type !== 'pattern';
                        }).style('width', function (v) {
                            return v.width + '%';
                        }).each(function (v) {
                            if (v.type === 'pattern') {
                                // generate a single pattern
                                _generatePattern(d3.select(this), d.data.pattern, v.variable);
                            }
                        });
                    }
                }
                //elem.selectAll('.repartition-container').html(v => `<b>${v.class}</b><(${v.nb})`);
            }).on('end', function (nodeData) {
                nodeData.charts = nodeData.charts || [];
                var isCollapsed = nodeWidth / nodeData.data.viz.length > self._options.minChartWidth && nodeHeight / 2 > self._options.minChartHeight;

                d3.select(this).classed('hidden', false).classed('in-transition', false);

                if (nodeData.children && isCollapsed) {
                    var elem = this.querySelector('.representation');
                    // display
                    nodeData.data.viz.forEach(function (vizData, i) {
                        var forced = false;
                        var newGraphId = 'graph-node-' + nodeData.id + '-' + i;

                        if (!nodeData.charts[i]) {
                            // create once chart object if it doesnt exist
                            self.logger.log('create graph', newGraphId, vizData);
                            nodeData.charts.push(new CanvasCurve(newGraphId, vizData.map(function (d) {
                                return Object.assign({}, d);
                            })));
                        } else {
                            forced = document.getElementById(newGraphId).childElementCount === 0;
                            // occurs when curve container is empty (collapse/uncollapse),
                            // it needs to diplay and not just render it
                            if (forced) {
                                self.logger.warn('container \'#' + newGraphId + ' is empty, need to force display instead of render');
                            }
                        }
                        var width = Math.floor(elem.offsetWidth / nodeData.data.viz.length),
                            height = elem.offsetHeight,
                            chartOptions = nodeData.charts[i].options();
                        // if the current width is the same as the graph one, dont need to ask data
                        //TODO: play more with data directly
                        if (forced || chartOptions.mode !== self._options.curveMode || Math.abs(width - chartOptions.width) > self._options.allowedDiff || Math.abs(height - chartOptions.height) > self._options.allowedDiff) {
                            var graphOptions = {
                                // put all margin to 0 in order to fill in the container
                                margin: {
                                    left: 0,
                                    right: 0,
                                    top: 0,
                                    bottom: 0
                                },
                                minimap: {
                                    show: true,
                                    height: 7,
                                    paddingBottom: 0
                                },
                                weights: {
                                    show: self.obj.hasWeights,
                                    height: 9,
                                    minColor: 'rgb(231, 228, 228)',
                                    maxColor: 'rgb(115, 115, 115)'
                                },
                                mode: self._options.curveMode,
                                width: width,
                                height: height,
                                autosize: false,
                                showAxis: false,
                                once: true, // option to say that we ask only one time the async data
                                showVariable: false
                            };
                            // display and asking async data
                            nodeData.charts[i].options(graphOptions).inputs(Object.assign({}, self._raw_data, {
                                nodeId: nodeData.id
                            })).on('zoomed', function () {
                                // try to avoid sync behavior for unknow events
                                // TODO: understand how d3.event works with brush/zoomed/pan
                                if (!d3.event.sourceEvent || d3.event.sourceEvent.sourceEvent) {
                                    var transform = this;
                                    nodeData.charts.forEach(function (chart, j) {
                                        if (j !== i) {
                                            chart.transform(transform, false);
                                        }
                                    });
                                }
                            }).display();
                        } else {
                            //TODO : what to do in case of the graph has already the data
                            // render without asking data
                            nodeData.charts[i].render();
                        }
                    });
                }
            });

            // EXPERIMENTAL
            // Try to create img that represents the canvas and play with it
            htmlNodeUpdate.filter(function () {
                return !d3.select(this).classed('light');
            }).each(function (d) {
                d.pictures = [];
                // prepare picture data
                d3.select(this).selectAll('.representation .curve-canvas').each(function () {
                    var n = d3.select(this).node();
                    d.pictures.push({
                        width: n.width,
                        height: n.height,
                        url: n.toDataURL(),
                        nb: d.data.viz.length
                    });
                });
                // loop on pictures and create the corresponding img
                d3.select(this).select('.representation').selectAll('.image__preview').data(d.pictures).enter().append('img').attr('class', 'image__preview').attr('height', '100%').attr('width', function (d) {
                    return 100 / d.nb + '%';
                });

                d3.select(this).selectAll('.image__preview')
                // change directly the data
                .attr('src', function (d) {
                    return d.url;
                });
            });

            // ENTER : any new modes at the parent's previous position.
            var nodeEnter = node.enter().append('g').attr('class', 'node').attr("transform", function (n) {
                var p = _getFirstParentVisible(n) || source;
                return 'translate(' + p.x0 + ',' + p.y0 + ')';
            }).each(function (d) {
                var nodeElem = d3.select(this);

                // construct the pie (collapsed node)
                // pie element
                var pieElement = nodeElem.append('g').attr('class', 'pie').on('click', function (d) {
                    // if not meta/shift pressed, select/unselect behavior
                    if (!d3.event.metaKey && !d3.event.shiftKey) {
                        updateSelectionDisplay(d);
                    } else {
                        // collapse/uncollapse behavior
                        click(d);
                    }
                });
                // each pieces of the pie
                var arcGroup = pieElement.selectAll(".arc").data(pie(d.data.repartition || [])).enter().append('g').attr('class', 'arc');

                arcGroup.append('path').attr('d', arc).style('fill', function (e) {
                    return e.data.color;
                });

                arcGroup.append('title').text(function (d) {
                    return d.data.class + '(' + d.data.nb + ')';
                });

                if (d.data.type !== "leaf") {
                    pieElement.append('circle').attr('class', 'circle--outer').attr('r', endingCircleRadius + self._options.separationDoubleDash);
                }

                pieElement.append('circle').attr('class', 'circle--inner').classed('ending-node', d.data.type === 'leaf').attr('r', endingCircleRadius).on('click', updateSelectionDisplay);

                if ((d.data.repartition || []).length > 1) {
                    arcGroup.append("text").attr("transform", function (d) {
                        return "translate(" + labelArc.centroid(d) + ")";
                    }).attr("dy", ".35em").text(function (e) {
                        return e.data.class;
                    });
                }
            });

            var leafEnter = nodeEnter.filter(function (d) {
                return d.data.type === 'leaf';
            });
            leafEnter
            // label
            .append('text').attr('class', 'node-label').attr("dy", ".35em").attr("text-anchor", "middle").text(function (d) {
                return (d.data.repartition || []).length === 1 ? d.data.class : "";
            });

            leafEnter
            // title
            .append('title').text(function (d) {
                return d.data.class + ' (' + ((d.data.repartition || [])[0] || {}).nb + ')';
            });

            // Add labels for the link
            nodeEnter.filter(function (n) {
                return n.depth > 0;
            }).append('text').attr('class', 'link-label').text(function (d) {
                return formatNumeric(d.data.case);
            })
            // TODO: check performance
            .style("font-size", self._options.fontsize.isDynamic ? ftSize + 'px' : undefined);

            // UPDATE
            var nodeUpdate = nodeEnter.merge(node);
            nodeUpdate.classed('selected', function (d) {
                return self.obj.selectedNode && self.obj.selectedNode.id === d.id;
            });
            nodeUpdate.select('.node-label').transition().duration(self._options.duration).attr('transform', 'translate(0,' + endingCircleRadius + ')')
            // TODO: check performance
            .style("font-size", self._options.fontsize.isDynamic ? ftSize + 'px' : undefined);

            // Transition to the proper position for the node
            nodeUpdate.transition().duration(self._options.duration).attr("transform", function (d) {
                return 'translate(' + d.x + ',' + d.y + ')';
            });

            // Update pie
            nodeUpdate.select('.pie').transition().duration(self._options.duration).attr('transform', 'translate(0,' + foldedCircleRadius + ')').on('end', function () {
                d3.select(this).classed('hidden', function (d) {
                    return d.children;
                });
            });

            // Update pie
            nodeUpdate.select('.pie circle.circle--inner').transition().duration(self._options.duration).attr('r', foldedCircleRadius);

            // Update pie
            nodeUpdate.select('.pie circle.circle--outer').transition().duration(self._options.duration).attr('r', foldedCircleRadius + self._options.separationDoubleDash);

            nodeUpdate.select('.pie').selectAll('path').transition().duration(self._options.duration).attr('d', arc);

            nodeUpdate.select('.pie').selectAll('text').classed('hidden', foldedCircleRadius < 20).transition().duration(self._options.duration).attr("transform", function (d) {
                return "translate(" + labelArc.centroid(d) + ")";
            });

            nodeUpdate.select('text.link-label').transition().duration(self._options.duration).attr("dy", function () {
                return -(self._options.nodeHeight * scale * self._options.pourcentVerticalSeparation / 100 / 2);
            }).attr("dx", function (d) {
                return (d.parent.x - d.x) * self._options.pourcentVerticalSeparation / 100 / 2;
            })
            // TODO: check performance
            .style("font-size", self._options.fontsize.isDynamic ? ftSize + 'px' : undefined);

            // Update text
            htmlNodeUpdate.selectAll('.subject, .class-repartition').transition().duration(self._options.duration)
            // TODO: check performance
            .style("font-size", self._options.fontsize.isDynamic ? ftSize + 'px' : undefined);

            // EXIT : Remove any exiting nodes
            node.exit().transition().duration(self._options.duration).attr("transform", function (n) {
                var p = _getFirstParentVisible(n, true) || source;
                return 'translate(' + p.x + ',' + p.y + ')';
            }).remove();

            var potentialWidth = treeDiffX + nodeWidth / 2,
                wrapperWidth = (potentialWidth > graphWidth ? potentialWidth : graphWidth) - self.obj.scrollBarWidth,
                wrapperHeight = treeDiffY + self._options.margin.top + foldedCircleRadius * 2;

            var scaledWidth = Math.floor(d3.max([wrapperWidth, self._options.height - self._options.scrollBarWidth])),
                scaledHeight = Math.floor(d3.max([wrapperHeight, self._options.height - self._options.scrollBarWidth])) + 5;

            self.dom.subContainer.select('.tree-wrapper>svg').transition().duration(self._options.duration).attr('width', scaledWidth).attr('height', scaledHeight);

            var treeGraphNode = document.querySelector('.tree-graph'),
                initialScrollLeft = treeGraphNode.scrollLeft,
                initialScrollTop = treeGraphNode.scrollTop,
                initialWidth = $(self.dom.wrapper.node()).width(),
                initialHeight = $(self.dom.wrapper.node()).height();

            var newScrollLeft = void 0,
                newScrollTop = void 0;
            // zoom and center on selected node
            if (self.obj.selectedNode && !_.isUndefined(self.obj.selectedNode.x) && !_.isUndefined(self.obj.selectedNode.y)) {
                newScrollLeft = self.obj.selectedNode.x - graphWidth / 2;
                newScrollTop = self.obj.selectedNode.y - (self._options.height - nodeHeight) / 2;
            }
            // zoom using current scroll positions.
            else {
                    newScrollLeft = scaledWidth / initialWidth * (initialScrollLeft + graphWidth / 2) - graphWidth / 2;
                    newScrollTop = scaledHeight / initialHeight * (initialScrollTop + self._options.height / 2) - self._options.height / 2;
                }

            self.dom.wrapper.transition().duration(self._options.duration).style('margin-left', '-' + (scaledWidth > graphWidth ? newScrollLeft : 0) + 'px').style('margin-top', '-' + (scaledHeight > self._options.height ? newScrollTop : 0) + 'px').style('width', scaledWidth + 'px').style('height', scaledHeight + 'px').on('start', function () {
                var $elem = $(this),
                    tmpW = $elem.width(),
                    tmpH = $elem.height();
                // reset scroll values
                treeGraphNode.scrollLeft = 0;
                treeGraphNode.scrollTop = 0;
                // assign margin
                d3.select(this).style('margin-left', '-' + (tmpW > graphWidth ? initialScrollLeft : 0) + 'px').style('margin-top', '-' + (tmpH > self._options.height ? initialScrollTop : 0) + 'px');
            }).tween('customTween', function () {
                return function () {
                    ps.update(self.dom.subContainer.node());
                };
            }).on('end', function () {
                // reset margin
                d3.select(this).style('margin-left', 0).style('margin-top', 0);
                // update scroll widget
                ps.update(self.dom.subContainer.node());
                // assign scroll
                if (scaledWidth > graphWidth) {
                    // update left scroll position
                    treeGraphNode.scrollLeft = scaledWidth > graphWidth ? newScrollLeft : 0;
                }
                if (scaledHeight > self._options.height) {
                    // update top scroll position
                    treeGraphNode.scrollTop = scaledHeight > self._options.height ? newScrollTop : 0;
                }
            });

            self.dom.controlContainer.style('width', menuWidth + 'px').style('height', self._options.height - 3 + 'px').on('end', function () {
                // update scrollbar
                ps.update(this);
            });

            // STORE the old positions for transition.
            nodes.forEach(function (node) {
                node.x0 = node.x;
                node.y0 = node.y;
            });

            // update scroll widget finally // TODO: check why we need to do that
            setTimeout(function () {
                ps.update(self.dom.subContainer.node());
            }, self._options.duration * 2);
        }

        /**
         * Wake up (Restore) the VizTool.
         */

    }, {
        key: 'wakeUp',
        value: function wakeUp() {
            this._options.scale = 1;
            this._options.nodeHeight = null;
            this._options.nodeWidth = null;
            this.display();
        }

        /**
         * Persist the VizTool for a quick restoration.
         */

    }, {
        key: 'sleep',
        value: function sleep() {
            // get the d3 element
            var container = d3.select('#' + this.container);
            // remove all content
            container.html('');
        }
    }]);

    return TDecisionTree;
}(VizTool);

module.exports = TDecisionTree;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"1":1,"2":2,"24":24,"25":25,"30":30,"33":33,"35":35,"37":37,"38":38,"46":46}],29:[function(require,module,exports){
(function (global){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null);
/* jshint unused: false */

/*
 * Interface DataProvider.
 * Tool to manage data requests for testing
 * @constructor
 */

var DataProvider = function () {
    function DataProvider(rootPath) {
        _classCallCheck(this, DataProvider);

        this.rootPath = rootPath;
    }

    ///
    /// DATASETS
    ///

    _createClass(DataProvider, [{
        key: 'getScopes',
        value: function getScopes() {
            throw new Error("getScopes method of DataProvider has to be overridden");
        }

        /**
         * Get specific dataset scopes
         * @param  {String}  dataset_id           - dataset identifier
         * @param  {String}  scope_id             - scope identifier
         * @param  {Boolean} generate_if_no_exist - option
         * @return {Object}                      [description]
         */

    }, {
        key: 'getScope',
        value: function getScope(dataset_id, scope_id, generate_if_no_exist) {
            throw new Error("getScope method of DataProvider has to be overridden");
        }

        /**
         * Save specific scope
         * @param  {String} datasetid - dataset identifier
         * @param  {String} scopeid   - scope identifier
         * @param  {Object} data      - data we want to save
         * @return {Object}           - data
         */

    }, {
        key: 'saveScope',
        value: function saveScope(scope_id) {
            throw new Error("saveScope method of DataProvider has to be overridden");
        }

        ///
        /// SCOPES
        ///

        /**
         * Get all dataset ids from database
         * @return {Promise} - ds_names
         */

    }, {
        key: 'getDatasetIds',
        value: function getDatasetIds() {
            throw new Error("getDatasetIds method of DataProvider has to be overridden");
        }

        /**
         * get specific dataset representation
         * @param  {String} ds_name - dataset name
         * @return {Promise} - dataset representation
         */

    }, {
        key: 'getDataset',
        value: function getDataset(ds_name) {
            throw new Error("getDataset method of DataProvider has to be overridden");
        }

        /**
        * Get metadata for a list of ts
        * @param  {Array} ts_list - list of ts
        * @return {Promise} dictionnary - {ts1: md, ts2: md}
        */

    }, {
        key: 'getMetaData',
        value: function getMetaData(ts_list) {
            throw new Error("getMetaData method of DataProvider has to be overridden");
        }

        /**
         * Get points
         * @param  {Array} ts_list  - list of ts
         * @param  {Integer} start  - start (date / index)
         * @param  {Integer} end    - end (date / index)
         * @param  {Integer} nbmax  - number of pixels
         * @return {Promise}          - array of points
         */

    }, {
        key: 'getPoints',
        value: function getPoints(ts_list, start, end, nbmax) {
            throw new Error("getPoints method of DataProvider has to be overridden");
        }
    }, {
        key: 'parseRegex',
        value: function parseRegex(regex) {
            var t = [];
            for (var i = 0; i < regex.length; i++) {
                var letter = regex[i];
                if (letter === '[') {
                    var idx = regex.indexOf(']', i);
                    letter = regex.substring(i + 1, idx);
                    i = idx;
                }
                t.push(letter);
            }
            return t;
        }
    }, {
        key: '_parseTableToLS',
        value: function _parseTableToLS(table) {
            var ls = [];
            table.headers.row.data.forEach(function (obs_id, i) {
                if (obs_id !== null) {
                    var idx = i - 1;
                    var target = table.content.cells[idx][table.content.cells[idx].length - 1];
                    var obj = {
                        obs_id: obs_id,
                        key: obs_id,
                        ts_list: [],
                        class: target
                    };
                    table.headers.col.data.forEach(function (headerKey, j) {
                        if (j !== 0) {
                            var link = table.content.links[idx][j - 1];
                            if (!link) {
                                obj[headerKey] = table.content.cells[idx][j - 1];
                            } else {
                                var ts = {
                                    funcId: link.val[0].funcId,
                                    tsuid: link.val[0].tsuid,
                                    key: obs_id,
                                    obs_id: obs_id,
                                    variable: headerKey,
                                    class: target
                                };
                                obj.ts_list.push(ts);
                            }
                        }
                    });
                    ls.push(obj);
                }
            });
            return ls;
        }
    }]);

    return DataProvider;
}();

module.exports = DataProvider;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],30:[function(require,module,exports){
(function (global){
'use strict';

// libs

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DataProvider = require(29),
    d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null),
    _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null),
    ikats = (typeof window !== "undefined" ? window['ikats'] : typeof global !== "undefined" ? global['ikats'] : null),
    LigLogger = require(33),
    treeHelper = require(31);

// caches
var _cachedMetadatas = {},
    _cachedTables = {},
    _cachedPoints = {};

// constants
var VARIABLE_SEPARATOR = '-',
    REGEX_VARIABLE_SEPARATOR = '$',
    TSUID_VARIABLE_SEPARATOR = '#';

/*
 * Class DataProvider.
 * Tool to manage data requests in IKATS (CS) context
 * @constructor
 */

var IkatsDataProvider = function (_DataProvider) {
    _inherits(IkatsDataProvider, _DataProvider);

    function IkatsDataProvider(rootPath) {
        var _ret;

        _classCallCheck(this, IkatsDataProvider);

        var _this = _possibleConstructorReturn(this, (IkatsDataProvider.__proto__ || Object.getPrototypeOf(IkatsDataProvider)).call(this, rootPath));

        _this.colors = d3.scaleOrdinal(d3.schemeCategory10);
        _this.scopes = {
            getScopes: function getScopes() {
                return [];
            },
            getScope: function getScope() {
                return {};
            },
            saveScope: function saveScope() {},
            deleteScope: function deleteScope() {}
        };
        _this.metadatas = _cachedMetadatas;
        _this.tables = _cachedTables;
        _this.points = _cachedPoints;
        _this.logger = new LigLogger('ikats:data');
        return _ret = _this, _possibleConstructorReturn(_this, _ret);
    }

    /**
     * Helper to parse inputs from direct result / md / callbacks
     * @param {*} result - raw result as dict
     * @param {*} md - metadatas as dict
     * @param {*} callbacks - callbacks as dict
     */


    _createClass(IkatsDataProvider, [{
        key: '_parseInputs',
        value: function _parseInputs(result) {
            var md = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
            var callbacks = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

            var self = this;
            var inputs = {};
            if (result) {
                // ds_name
                if (Object.prototype.toString.call(result) === '[object String]') {
                    inputs = {
                        datasetId: result
                    };
                }
                // table
                else if (result.headers && result.content) {
                        var ls = self._parseTableToLS(result);
                        inputs = {
                            ts_list: d3.merge(ls.map(function (l) {
                                return l.ts_list;
                            })), // can be fit
                            patterns: [],
                            disc_break_points: [],
                            breakpoints: [],
                            variables: [], // can be fit
                            learning_set: ls
                        };
                    }
                    // rhm output
                    else if (callbacks && callbacks.inputs && callbacks.inputs.learningset) {
                            // parse table from inputs
                            var _ls = self._parseTableToLS(callbacks.inputs.learningset);
                            var breakpoints = result.variables.filter(function (d) {
                                return d.indexOf('-') === -1;
                            }).map(function (d, i) {
                                var t = result.break_points[i].map(function (r, j) {
                                    return {
                                        key: result.disc_break_points[i][j],
                                        min: r,
                                        max: result.break_points[i][j + 1] || r + Math.abs(r - result.break_points[i][j - 1])
                                    };
                                });
                                var map = {};
                                t.forEach(function (d) {
                                    return map[d.key] = d;
                                });
                                return {
                                    key: d,
                                    bps: t,
                                    map: map
                                };
                            });

                            var patterns = d3.values(result.patterns);
                            var minmax = result.disc_break_points.map(function (d) {
                                return '[' + d[0] + d[d.length - 1] + ']';
                            });
                            patterns.forEach(function (d) {
                                return d.regexPlain = d.regex.split(REGEX_VARIABLE_SEPARATOR).map(function (t, i) {
                                    return self.parseRegex(t.replace(/\./g, minmax[result.variables.indexOf(d.variable.split(VARIABLE_SEPARATOR)[i])]));
                                });
                            });

                            inputs = {
                                ts_list: d3.merge(_ls.map(function (l) {
                                    return l.ts_list;
                                })),
                                patterns: patterns,
                                disc_break_points: result.disc_break_points,
                                breakpoints: breakpoints,
                                variables: result.variables,
                                learning_set: _ls
                            };
                        }
            }
            return Object.assign({}, md, inputs);
        }
    }, {
        key: 'getScopes',
        value: function getScopes() /**datasetid**/{
            this.logger.warn('getScopes is not implmented for now in IkatsDataProvider');
            return [];
        }
    }, {
        key: 'getScope',
        value: function getScope() /**datasetid**/{
            this.logger.warn('getScopes is not implmented for now in IkatsDataProvider');
            return {};
        }
    }, {
        key: 'saveScope',
        value: function saveScope() {
            this.logger.warn('getScopes is not implmented for now in IkatsDataProvider');
        }

        /**
         * Get ts data
         * @param  {String} datasetid - dataset identifier
         * @param  {Object} params - scopes
         * @return {Object} dictionnary - {ts1: data, ts2: data}
         */

    }, {
        key: 'getDatasetIds',
        value: function getDatasetIds() {
            return new Promise(function (resolve, reject) {
                return ikats.api.ds.list({
                    async: true,
                    success: function success(result) {
                        resolve(result.data.map(function (d) {
                            return d.name;
                        }));
                    },
                    error: function error(e) {
                        reject("Could not get list of datasets.", e);
                    }
                });
            });
        }

        /**
         * Get dataset from a tree
         * @param  {String} ds_name
         * @param  {Object} _inputs
         * @return {Object} dataset
         */

    }, {
        key: 'getTreeDataset',
        value: function getTreeDataset(ds_name, _inputs) {
            return new Promise(function (resolve, reject) {
                if (_inputs && _inputs.header && _inputs.tree) {
                    resolve(treeHelper.parseTreeOutputAsDataset(_inputs, ds_name));
                } else {
                    reject('tree not found');
                }
            });
        }

        /**
         * Get tree result
         * @param  {String} ds_name
         * @param  {Object} _inputs
         * @return {Object} tree
         */

    }, {
        key: 'getTreeResult',
        value: function getTreeResult(ds_name, _inputs) {
            var self = this;
            return new Promise(function (resolve, reject) {
                if (_inputs && _inputs.header && _inputs.tree) {
                    return self.getTreeDataset(ds_name, _inputs).then(function (dataset) {
                        resolve(treeHelper.parseTreeOutput(_inputs, {}, dataset.colors, dataset.variableColors, dataset.raw));
                    }).catch(function (d) {
                        reject(d);
                    });
                } else {
                    reject('tree not found');
                }
            });
        }

        /**
         * Get dataset (base of the process)
         * @param  {String} ds_name - dataset name
         * @param  {Object} _inputs - additional data (touchy)
         * @return {Object}  dataset
         */

    }, {
        key: 'getDataset',
        value: function getDataset(ds_name, _inputs) {
            var self = this;
            var _metadata = {};
            if (_inputs && _inputs.header && _inputs.tree) {
                // tree case (TMP solution)
                return self.getTreeDataset(ds_name, _inputs);
            }
            return new Promise(function (resolve, reject) {
                // preprocessing inputs (table) if neeeded
                if (_inputs && _inputs.table_name) {
                    // if in inputs we have table_name, fetch it
                    var table_name = _inputs.table_name;
                    var table = self.tables[table_name];
                    if (!table) {
                        ikats.api.table.read({
                            table_name: table_name,
                            async: true,
                            success: function success(response) {
                                self.tables[table_name] = response.data;
                                resolve(response.data);
                            },
                            error: function error(e) {
                                reject("Could not get dataset." + ds_name, e);
                            }
                        });
                    } else {
                        resolve(table);
                    }
                } else {
                    resolve({});
                }
            }).then(function (table) {
                if (!table) {
                    return;
                }
                // parse data from table
                _metadata = self._parseInputs(_inputs, _inputs, { inputs: { learningset: table } });
                // apply changes on _inputs
                Object.assign(_inputs, _metadata);
            }).then(function () {
                // in case we have a ts_list in the inputs, dont need to get data by the common dataset request
                if (_metadata && _metadata.ts_list) {
                    return { ts_list: _metadata.ts_list };
                } else {
                    // else, ask the dataset by the api
                    return new Promise(function (resolve, reject) {
                        return ikats.api.ds.read({
                            ds_name: ds_name,
                            async: true,
                            success: function success(response) {
                                resolve(response.data);
                            },
                            error: function error(e) {
                                reject("Could not get dataset." + ds_name, e);
                            }
                        });
                    });
                }
            }).then(function (dataset) {
                // ask the metadata
                return self.getMetaData(dataset.ts_list, _inputs).then(function (metas) {
                    var classes = [],
                        variables = [],
                        mapping = {},
                        paired_ts = [];

                    // prepare ts_list
                    var ts_list = dataset.ts_list.map(function (d) {
                        var ts = Object.assign({}, { patterns: [], key: metas[d.tsuid].obs_id }, metas[d.tsuid], d);
                        if (variables.indexOf(ts.variable) === -1) {
                            variables.push(ts.variable);
                        }
                        if (classes.indexOf(ts.class) === -1) {
                            classes.push(ts.class);
                        }
                        if (mapping[ts.variable]) {
                            mapping[ts.variable].push(ts);
                        } else {
                            mapping[ts.variable] = [ts];
                        }
                        return ts;
                    });

                    var paired_patterns = _metadata.patterns.filter(function (pattern) {
                        return pattern.variable.indexOf(VARIABLE_SEPARATOR) > -1;
                    });

                    paired_patterns.forEach(function (pattern) {
                        d3.keys(pattern.locations).forEach(function (obs_id) {
                            paired_ts.push({
                                key: obs_id,
                                funcId: obs_id + '_' + pattern.variable,
                                tsuid: obs_id + '_' + pattern.variable,
                                class: pattern.class,
                                patterns: [],
                                variable: pattern.variable
                            });
                        });
                    });

                    return {
                        raw: ts_list.concat(paired_ts),
                        md: Object.assign({ datasetid: ds_name }, _metadata),
                        class: classes,
                        variables: _inputs.variables || variables,
                        patterns: _metadata.patterns || []
                    };
                });
            });
        }

        /**
         * Get ts data
         * @param  {String} datasetid - dataset identifier
         * @param  {Object} params - scopes
         * @return {Object} dictionnary - {ts1: data, ts2: data}
         */

    }, {
        key: 'getMetaData',
        value: function getMetaData(ts_list, _inputs) {
            var self = this;

            if (ts_list.length === 0) {
                self.logger.log('no item in ts_list, resolve empty array');
                return Promise.resolve([]);
            }

            var all_tsuids = ts_list.map(function (ts) {
                return ts.tsuid.split(TSUID_VARIABLE_SEPARATOR)[0];
            }),
                missed_tsuids = all_tsuids.filter(function (tsuid) {
                return !self.metadatas[tsuid];
            }),
                gmd0 = performance.now();

            return new Promise(function (resolve, reject) {

                if (missed_tsuids.length === 0) {
                    self.logger.log('no ajax request (md) needed, all md is already in cache');
                    resolve([]);
                }
                // look on metadaa in lookings the tree dataset
                else if (_inputs && _inputs.tree) {
                        return self.getTreeDataset(undefined, _inputs).then(function (d) {
                            d.raw.forEach(function (r) {
                                self.metadatas[r.tsuid] = {
                                    variable: r.variable,
                                    size: r.size,
                                    start: 0,
                                    end: r.size - 1,
                                    start_date: r.raw[0].timestamp,
                                    end_date: r.raw[r.size - 1].timestamp,
                                    min: r.meta_data.min_vals[0],
                                    max: r.meta_data.max_vals[0],
                                    obs_id: r.key,
                                    tsuid: r.tsuid,
                                    funcId: r.key,
                                    class: r.class,
                                    datasetId: _inputs.header.learningSet || _inputs.header.LearningSet // TODO -> check if this property is really needed and used ( seems not )
                                };
                            });
                            resolve(self.metadatas);
                        });
                    } else {
                        var aj0 = performance.now();
                        self.logger.log('ajax request (md) start for ' + missed_tsuids.length + ' sequences');

                        ikats.api.md.read({
                            ts_list: missed_tsuids,
                            async: true,
                            success: function success(metas) {
                                // When api call results in a success, resolve promise
                                var partial_result = {};

                                var classValue = void 0,
                                    identifierValue = void 0,
                                    newTmp = void 0;

                                metas.data.forEach(function (d) {
                                    var tmp = partial_result[d.tsuid] || {};
                                    tmp[d.name] = d.value;
                                    partial_result[d.tsuid] = tmp;
                                });

                                self.logger.log('ajax request (md) success (' + (performance.now() - aj0).toFixed(2) + ' ms)', partial_result);
                                var detected_errors = { start_date: [], end_date: [], min: [], max: [], metric: [], nb: [], diff: [] };
                                for (var key in partial_result) {
                                    if (partial_result.hasOwnProperty(key)) {
                                        var tmp = partial_result[key];
                                        var tmp2 = {};
                                        if (_inputs && _inputs.ts_list) {
                                            tmp2 = _inputs.ts_list[_inputs.ts_list.findIndex(function (d) {
                                                return d.tsuid === key;
                                            })]; // jshint ignore:line
                                        }
                                        classValue = tmp.class || 'c0';
                                        identifierValue = 'obs_id';

                                        if (_inputs && _inputs.population && _inputs.population.data) {
                                            identifierValue = _inputs.population.identifier || identifierValue;
                                            classValue = _inputs.population.data[tmp[identifierValue]] || classValue;
                                        }

                                        // CONTROL IF METADATAS ARE HERE
                                        if (!tmp.metric) {
                                            detected_errors.metric.push(key);
                                        }
                                        if (_.isUndefined(tmp.ikats_start_date)) {
                                            detected_errors.start_date.push(key);
                                        }
                                        if (_.isUndefined(tmp.ikats_end_date)) {
                                            detected_errors.end_date.push(key);
                                        }
                                        if (_.isUndefined(tmp.qual_min_value)) {
                                            detected_errors.min.push(key);
                                        }
                                        if (_.isUndefined(tmp.qual_max_value)) {
                                            detected_errors.max.push(key);
                                        }
                                        if (_.isUndefined(tmp.qual_nb_points)) {
                                            detected_errors.nb.push(key);
                                        }
                                        if (tmp.ikats_end_date === tmp.ikats_start_date) {
                                            detected_errors.diff.push([key, tmp.ikats_start_date, tmp.ikats_end_date]);
                                        }

                                        newTmp = {
                                            variable: tmp.metric,
                                            size: tmp.qual_nb_points, // quality stats
                                            start: 0,
                                            end: tmp.qual_nb_points - 1,
                                            start_date: tmp.ikats_start_date,
                                            end_date: tmp.ikats_end_date,
                                            min: tmp.qual_min_value, // quality stats
                                            max: tmp.qual_max_value, // quality stats
                                            obs_id: tmp[identifierValue] || tmp2[identifierValue],
                                            tsuid: key,
                                            key: tmp2.key,
                                            funcId: tmp.funcId || tmp2.funcId,
                                            class: classValue,
                                            datasetId: tmp.datasetId // TODO -> check if this property is really needed and used ( seems not )
                                        };
                                        self.metadatas[key] = newTmp;
                                    }
                                }

                                // ERRORS
                                var nbTs = d3.keys(self.metadatas).length;
                                if (detected_errors.metric.length > 0) {
                                    self.logger.error('metadata \'metric\' must be defined for ' + detected_errors.metrics.length + ' TS on ' + nbTs);
                                }
                                if (detected_errors.start_date.length > 0) {
                                    self.logger.error('metadata \'ikats_start_date\' must be defined for ' + detected_errors.start_date.length + ' TS on ' + nbTs);
                                }
                                if (detected_errors.end_date.length > 0) {
                                    self.logger.error('metadata \'ikats_end_date\' must be defined for ' + detected_errors.end_date.length + ' TS on ' + nbTs);
                                }
                                if (detected_errors.min.length > 0) {
                                    self.logger.error('metadata \'qual_min_value\' must be defined for ' + detected_errors.min.length + ' TS on ' + nbTs);
                                }
                                if (detected_errors.max.length > 0) {
                                    self.logger.error('metadata \'qual_max_value\' must be defined for ' + detected_errors.max.length + ' TS on ' + nbTs);
                                }
                                if (detected_errors.nb.length > 0) {
                                    self.logger.error('metadata \'qual_nb_points\' must be defined for ' + detected_errors.diff.length + ' TS on ' + nbTs);
                                }
                                if (detected_errors.diff.length > 0) {
                                    self.logger.error('metadata \'ikats_start_date\' and \'ikats_end_date\' shouldnt be the same for ' + detected_errors.diff.length + ' TS on ' + nbTs);
                                }

                                resolve(partial_result);
                            },
                            error: function error(e) {
                                self.logger.warn("Could not get metadata for given TS list.", missed_tsuids);
                                self.logger.error(e);
                                reject("Could not get metadata for given TS list.", missed_tsuids);
                            } });
                    }
            }).then(function () {
                var full_result = {};
                all_tsuids.forEach(function (tsuid) {
                    return full_result[tsuid] = self.metadatas[tsuid];
                });
                self.logger.log('get metadata done (' + (performance.now() - gmd0).toFixed(2) + ' ms)', full_result);
                return full_result;
            });
        }

        /**
         * Get sample aggreagted points for curves to represent them in specific amount of points
         * @param  {String}  datasetid - dataset identifier
         * @param  {Array}   tsuids    - array of ts identifiers
         * @param  {Integer} start     - first index that we start the cut
         * @param  {Integer} end       - last index that we stop the cut
         * @param  {Integer} nbmax     - number of aggregated points
         * @return {Array}             - set of points
         */

    }, {
        key: 'getPoints',
        value: function getPoints(ts_list, start, end, nbmax, diff, mode, inputs) {

            var self = this,
                ajaxRq = [],
                gp0 = performance.now();
            diff = Math.abs(end - start);

            if (_.isUndefined(start)) {
                self.logger.error('start must be defined');
            }
            if (_.isUndefined(end)) {
                self.logger.error('end must be defined');
            }
            if (_.isUndefined(nbmax)) {
                self.logger.error('nbmax must be defined');
            }

            self.logger.log('get points start', start, end, nbmax, ts_list);
            var mainPromise = new Promise(function (resolve, reject) {
                if (inputs && inputs.tree) {
                    return self.getTreeDataset('', inputs).then(function (dataset) {
                        dataset.raw.forEach(function (r) {
                            return self.points[r.tsuid] = r.raw;
                        });
                        resolve(ts_list.map(function (ts) {
                            return self.points[ts.tsuid];
                        }));
                    });
                }
                // Keep track of downsampled timeseries for information display
                var promises = [];
                var total_period = Math.abs(end - start);
                // Calculate dp considering data + out of range and the current number of TS
                // TODO ->if Improve it / Understand it
                var dp = total_period / nbmax;
                //total_period / (10000 / (ts_list.length * 2));
                ts_list.forEach(function (datum) {

                    var promise = new Promise(function (resolve2) {
                        var tsuid = datum.tsuid.split(TSUID_VARIABLE_SEPARATOR)[0];
                        self.logger.log('ajax request (points) start', tsuid, start, end);
                        var aj0 = performance.now();
                        ajaxRq.push(ikats.api.ts.read({
                            tsuid: tsuid,
                            async: true,
                            da: dp && 'avg' || null,
                            dp: dp,
                            sd: start,
                            ed: end,
                            md: [{
                                tsuid: tsuid,
                                name: 'ikats_start_date',
                                value: parseInt(start - diff / 2) //datum.md.start_date,
                            }, {
                                tsuid: tsuid,
                                name: 'ikats_end_date',
                                value: parseInt(end + diff / 2) //datum.md.end_date,
                            }],
                            success: function success(ts_points) {
                                self.logger.log('ajax request (points) success (' + (performance.now() - aj0).toFixed(2) + ' ms)', datum.tsuid);
                                var tmp = d3.entries(ts_points.data).map(function (d) {
                                    return {
                                        value: d.value,
                                        timestamp: +d.key
                                    };
                                });
                                resolve2(tmp);
                            },
                            error: function error(e) {
                                self.logger.error('ajax request (points) with error', e);
                                reject(e);
                            }
                        }));
                    });
                    promises.push(promise);
                });

                Promise.all(promises).then(function (values) {
                    self.logger.log('get ' + values.length + ' points done (' + (performance.now() - gp0).toFixed(2) + ' ms)');
                    resolve(values);
                });
            });
            mainPromise.cancel = function () {
                ajaxRq.forEach(function (d) {
                    return d.abort();
                });
            };
            return mainPromise;
        }
    }]);

    return IkatsDataProvider;
}(DataProvider);

module.exports = IkatsDataProvider;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"29":29,"31":31,"33":33}],31:[function(require,module,exports){
(function (global){
'use strict';

/*jslint node: true*/

var initTimeStamp = +new Date().getTime(),
    d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null),
    variableSeparator = '$',
    _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null),
    LigLogger = require(33),
    logger = new LigLogger('lig-tree'),
    colorManager = require(36),
    patternColorsDomain = d3.schemeCategory10;

/**
 * Generate a fake tree using a set of examples
 * @param  {Array} examples
 * @return {Object} Tree - Root node
 */
var _generateFakeTree = function _generateFakeTree(examples) {
    var idFn = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function () {};
    var sil = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'toto';
    var colors = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : [];

    // randomize order
    examples = d3.shuffle(examples);

    var clss = Array.from(new Set(examples.map(function (d) {
        return d.class;
    })));
    clss.sort();

    var repartition = clss.map(function (name) {
        var nb = examples.filter(function (d) {
            return d.class === name;
        }).length;
        return {
            nb: nb,
            pourcent: nb / examples.length * 100,
            class: name,
            color: colors(name)
        };
    });

    if (examples.length === 1 || clss.length === 1) {
        return { type: 'leaf', examples_list: examples, case: sil, repartition: repartition, class: examples[0].class, id: idFn(), color: colors(examples[0].class) };
    }

    var nb = Math.floor(d3.randomUniform(0, 3)()),
        pattern;

    if (nb === 0) {
        var example = examples[examples.findIndex(function (e) {
            return e.ts_list.findIndex(function (ts) {
                return ts.patterns.length > 0;
            }) > -1;
        })];
        if (example) {
            var ts = example.ts_list[example.ts_list.findIndex(function (ts) {
                return ts.patterns.length > 0;
            })];
            if (ts) {
                pattern = ts.patterns[0];
            } else {
                nb = 1;
            }
        } else {
            nb = 1;
        }
    }

    var idx = Math.floor(examples.length / 2);
    var sets = [examples.slice(0, idx), examples.slice(idx, examples.length + 1)];

    if (pattern) {
        var hasPattern = examples.filter(function (e) {
            return e.ts_list.findIndex(function (t) {
                return t.patterns.findIndex(function (p) {
                    return p.regex === pattern.regex;
                }) > -1;
            }) > -1;
        });
        var hasNotPattern = examples.filter(function (e) {
            return e.ts_list.findIndex(function (t) {
                return t.patterns.findIndex(function (p) {
                    return p.regex === pattern.regex;
                }) > -1;
            }) === -1;
        });

        if (hasNotPattern.length > 0) {
            sets = [hasNotPattern, hasPattern];
        }
    }

    var idxs = [],
        charts = [],
        keys = [];

    while (nb !== charts.length) {
        idx = Math.floor(d3.randomUniform(0, examples.length)());
        if (idxs.indexOf(idx) === -1) {
            idxs.push(idx);
            keys.push(examples[idx].key);
            // for showed charts copy only ts without pair variables
            charts.push(examples[idx].ts_list.filter(function (d) {
                return d.variable.split('-').length === 1;
            }).map(_.clone));
        }
    }

    var nodeType = keys.length > 1 ? 'comparison' : 'similarity',
        subject = keys.length > 1 ? 'comparison (' + keys[0] + '/' + keys[1] + ')' : 'similarity with ' + keys[0];

    if (pattern) {
        nodeType = 'pattern';
        subject = 'pattern ' + pattern.regex;
    }

    return {
        type: 'node',
        id: idFn(),
        nodeType: nodeType,
        pattern: pattern,
        subject: subject,
        examples: examples,
        repartition: repartition,
        case: sil,
        children: sets.map(function (d, i) {
            var txt = nodeType === 'comparison' && examples.length > 1 ? examples[i].key + ' (' + examples[i].class + ')' : Math.random();
            return _generateFakeTree(d, idFn, txt, colors);
        }),
        viz: charts
    };
};

/**
 * Convert regex to an array of discretized intervals
 * @param  {String} regex
 * examples  G.G   -> ['G', '.', 'G']
 *          [A-D]G -> ['AD', 'G']
 * @return {Array}       [description]
 */
var parseRegex = function parseRegex(regex) {
    var t = [];
    for (var i = 0; i < regex.length; i++) {
        var letter = regex[i];
        if (letter === '[') {
            var idx = regex.indexOf(']', i);
            letter = regex.substring(i + 1, idx).replace('-', '');
            i = idx;
        }
        t.push(letter);
    }
    return t;
};

/**
 * Parse recursively a node and its children, and convert it as a common tree node model
 *
 * @param {*} node
 * @param {*} node_case_mapping
 * @param {*} colors
 * @param {*} patternColors
 * @param {*} [examples=[]]
 * @param {*} [definitions=[]]
 * @param {string} [sil='not found']
 * @returns {object} computed _ode
 */
var _parseNode = function _parseNode(node, node_case_mapping, colors, patternColors, examples, definitions, sil) {
    if (!examples) {
        examples = [];
    }
    if (!definitions) {
        definitions = [];
    }
    if (!sil) {
        sil = 'not found';
    }
    // init node
    var computedNode = {
        id: node.node,
        type: node.type !== 'leaf' ? 'node' : 'leaf',
        nodeType: node.type,
        subject: node.name,
        case: sil,
        evaluation: node.evaluation,
        distance: node.distance,
        viz: [],
        raw: node
    };
    if (node_case_mapping[computedNode.id]) {
        computedNode.case = node_case_mapping[computedNode.id];
    }
    // in case of 'similarity' and 'comparison' node
    if (node.type !== 'leaf') {
        // assign children in looking definitions
        computedNode.children = node.description.criteria.map(function (c) {
            var def = definitions[definitions.findIndex(function (d) {
                return d.node === c.child;
            })];
            return _parseNode(def, node_case_mapping, colors, patternColors, examples, definitions);
        });
    } else {
        // assign class
        computedNode.class = node.label;
        computedNode.color = colors(computedNode.class);
    }

    var chartIds = [];

    var _getSequenceId = function _getSequenceId(v) {
        if (!_.isArray(v)) {
            logger.warn('sequence should folloow new format (node.description.sequence)');
            return v;
        } else {
            return v[0];
        }
    };

    if (node.type === 'similarity') {
        chartIds.push(_getSequenceId(node.description.sequence));
    } else if (node.type === 'comparison') {
        node.description.criteria.forEach(function (c) {
            return chartIds.push(_getSequenceId(c.sequence));
        });
    } else if (node.type === 'pattern') {
        var pattern = node.description.pattern;

        var vocabulary = void 0;
        if (pattern.domain) {
            // find direct vocabulary
            vocabulary = pattern.domain;
            //logger.log('find domain', domain);
        } else {
            // compute vocabulary
            vocabulary = pattern.variables.map(function (variable) {
                return variable.breakpoints.map(function (d, i) {
                    return 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'[i];
                });
            });
            //logger.log('compute domain in looking breakpoints', pattern.variables, domain);
        }

        var regexPlain = pattern.definition.split(variableSeparator).map(function (d, i) {
            return parseRegex(d.replace(/\./g, '[' + vocabulary[i][0] + '-' + vocabulary[i][vocabulary[i].length - 1] + ']'));
        });

        computedNode.pattern = {
            regex: pattern.definition.split(variableSeparator),
            bps: vocabulary.map(function (a) {
                return a.map(function (d) {
                    return {
                        key: d,
                        value: d
                    };
                });
            }),
            color: pattern.variables.map(function (v) {
                return patternColors(v.name);
            }),
            regexPlain: regexPlain,
            length: pattern.length || regexPlain[0].length,
            variables: pattern.variables.map(function (v) {
                return v.name;
            })
        };
        //logger.log('pattern description', pattern);
        //logger.log('computedNode pattern', computedNode.pattern, node.description.pattern);
    }

    var total = d3.sum(node.statistics.map(function (d) {
        return d[1];
    }));
    node.statistics.forEach(function (s) {
        return s[2] = s[2] || s[1] / total;
    });
    // assign repartition
    computedNode.repartition = node.statistics.map(function (s) {
        return {
            class: s[0],
            nb: s[1],
            pourcent: s[2] * 100,
            color: colors(s[0])
        };
    });
    if (node.statistics.length === 1) {
        computedNode.class = node.statistics[0][0];
    }
    //logger.log('chartIds', chartIds, examples, node);
    // find all concerned examples data
    computedNode.viz = chartIds.map(function (c) {
        return [examples[examples.findIndex(function (e) {
            return e.key === c;
        })]];
    });
    //logger.log('computedNode viz', computedNode.viz);
    return computedNode;
};

/**
 * Parse tree input and convert it as a tree managed by the hmi
 *
 * @param {*} output
 * @param {*} nodeCaseMapping
 * @param {*} colors
 * @param {*} patternColors
 * @param {*} sequences
 * @returns
 */
var _parseTreeOutput = function _parseTreeOutput(output, nodeCaseMapping, colors, patternColors, sequences) {
    output.tree.forEach(function (d) {
        if (d.type === 'similarity' || d.type === 'variable' || d.type === 'pattern') {
            d.description.criteria.forEach(function (c) {
                // ex : '> 100'
                var value = c.value;
                if (_.isUndefined(value)) {
                    value = c.threshold;
                }
                //logger.log('description criteria', c, v);
                nodeCaseMapping[c.child] = c.test + ' ' + value;
            });
        } else if (d.type === 'comparison') {
            d.description.criteria.forEach(function (c) {
                // sequence for now, ex: 'TS_01'
                nodeCaseMapping[c.child] = c.sequence;
            });
        }
    });
    return _parseNode(output.tree[0], nodeCaseMapping, colors, patternColors, sequences, output.tree);
};

/**
 * Parse Tree Output to the dataset that use to be managed by the hmi
 *
 * @param {*} output
 * @param {*} fileName
 * @returns
 */
var _parseTreeOutputAsDataset = function _parseTreeOutputAsDataset(output, fileName) {
    var classMapping = {},
        variableMapping = {},
        classes = output.tree[0].statistics.map(function (d) {
        return d[0];
    }); // get classes in looking statistics from the root node

    output.data = output.data || [];
    output.validation = output.validation || {};

    classes.sort(); // sort classes by its value

    output.tree.forEach(function (node) {
        node.observations = node.observations || [];
        if (node.type === 'leaf') {
            // compute label if there is no label property, (in looking statistics)
            if (!node.label && node.statistics.length === 1) {
                node.label = node.statistics[0][0];
            }
            // populate classMapping if there is a label
            if (node.label && node.observations) {
                node.observations.forEach(function (obsKey) {
                    return classMapping[obsKey] = node.label;
                });
            }
        } else if (node.type === 'similarity') {
            // in case of similarity populate variableMapping with variable and sequence from description
            variableMapping[node.description.sequence] = node.description.variable;
        } else if (node.type === 'comparison') {
            // in case of comparison populate variableMapping with variable and criteria (n sequences) from description
            node.description.criteria.forEach(function (c) {
                return variableMapping[c.sequence] = node.description.variable;
            });
        }
    });

    // use variableMapping values to define variables and classes from the dataset
    var variables = Array.from(new Set(d3.values(variableMapping)));

    var colors = d3.scaleOrdinal().range(classes.map(function (d) {
        return colorManager.getColor(d, output.header.learningSet || output.header.LearningSet || 'common');
    })).domain(classes);

    var variableColors = d3.scaleOrdinal().range(patternColorsDomain).domain(variables);

    output.data.forEach(function (d) {
        d.class = d.class || d.label || classMapping[d.sequence];
        d.meta_data = {
            max_vals: [d3.max(d.value)],
            min_vals: [d3.min(d.value)]
        };
        d.variable = variableMapping[d.sequence];
        d.key = d.sequence;
        d.tsuid = d.sequence + '-' + d.variable;
        d.raw = (d.value || []).map(function (v, i) {
            return {
                pos: i,
                idx: i,
                timestamp: i * 10000 + initTimeStamp,
                value: v,
                weight: d.weight ? (d.weight[1] || d.weight[0] || [])[i] : null // FIXME: wrong concept
            };
        });
        d.funcId = 'tdt_' + fileName + '-' + d.key, d.color = colors(d.class); // jshint ignore:line
        d.size = d.raw.length;
        d.patterns = [];
    });

    output.validation = output.validation || {};
    // loop on traces
    output.validation.trace = (output.validation.trace || []).map(function (raw_entry) {
        // get the last node
        var trace_entry = _.clone(raw_entry);
        var trace_nodes = trace_entry[2];
        var last_node = output.tree[output.tree.findIndex(function (d) {
            return d.node === trace_nodes[trace_nodes.length - 1];
        })];
        if (last_node) {
            // add at the end of the trace entry an array of labels
            trace_entry.push(_.sortBy(last_node.statistics, function (s) {
                return s[1];
            }).map(function (s) {
                return s[0];
            }).reverse());
        }
        return trace_entry;
    });

    return {
        raw: output.data,
        validation: output.validation,
        md: {
            variables: variables,
            hasWeights: (output.data[0] || {}).weight && (output.data[0] || {}).weight.length > 0,
            date: output.header.date,
            learningSet: output.header.learningSet,
            version: output.header.TDTFormatVersion
        },
        class: classes,
        colors: colors,
        variableColors: variableColors,
        variables: variables,
        patterns: []
    };
};

module.exports = {
    generateFakeTree: _generateFakeTree,
    parseTreeOutput: _parseTreeOutput,
    parseTreeOutputAsDataset: _parseTreeOutputAsDataset
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"33":33,"36":36}],32:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// less integration
require(41);

var FullScreenApiClass = function () {
    function FullScreenApiClass() {
        _classCallCheck(this, FullScreenApiClass);
    }

    /**
     * Get into full screen
     *
     * @param {HTMLElement} element
     * @memberof FullScreenApiClass
     */


    _createClass(FullScreenApiClass, [{
        key: 'in',
        value: function _in(element) {
            var _this = this;

            element.classList.add('fullscreen');

            if (element.requestFullscreen) {
                element.requestFullscreen();
            } else if (element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
            } else if (element.webkitRequestFullscreen) {
                element.webkitRequestFullscreen();
            } else if (element.msRequestFullscreen) {
                element.msRequestFullscreen();
            }

            var callback = function callback() {
                if (!_this.isInFullScreen(element)) {
                    document.removeEventListener("fullscreenchange", callback);
                    document.removeEventListener("mozfullscreenchange", callback);
                    document.removeEventListener("webkitfullscreenchange", callback);
                    element.classList.remove('fullscreen');
                }
            };

            document.addEventListener("fullscreenchange", callback);
            document.addEventListener("mozfullscreenchange", callback);
            document.addEventListener("webkitfullscreenchange", callback);
        }

        /**
         * Get out of full screen
         *
         * @memberof FullScreenApiClass
         */

    }, {
        key: 'out',
        value: function out() {
            document.querySelectorAll('.fullscreen').forEach(function (htmlElement) {
                htmlElement.classList.remove('fullscreen');
            });
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            }
        }

        /**
         * Get the node of the current fullscreen element
         *
         * @returns {HTMLElement}
         * @memberof FullScreenApiClass
         */

    }, {
        key: 'getFullScreenElement',
        value: function getFullScreenElement() {
            // Returns the DOM Node of the fullscreen element
            return document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement;
        }

        /**
         * Check if there is already a fullscreen element
         *
         * @param {HTMLElement} element - if there is no element, check the whole document
         * @returns {Boolean}
         * @memberof FullScreenApiClass
         */

    }, {
        key: 'isInFullScreen',
        value: function isInFullScreen(element) {
            if (element) {
                // check if the current full screen element is same as 'element'
                return this.getFullScreenElement() === element;
            }
            // check the whole document
            return document.fullscreen || document.webkitIsFullScreen || document.mozFullScreen;
        }

        /**
         * Toggle fullscreen state
         *
         * @param {any} element
         * @memberof FullScreenApiClass
         */

    }, {
        key: 'toggle',
        value: function toggle(element) {
            var self = this;
            if (self.isInFullScreen(element)) {
                self.out();
            } else {
                self.in(element);
            }
        }
    }]);

    return FullScreenApiClass;
}();

// entry point


var fullScreenApi = new FullScreenApiClass();
module.exports = fullScreenApi;

},{"41":41}],33:[function(require,module,exports){
(function (global){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null);
// constant used to manage shared debug mode
window.LIG_DEBUG = false;

var _namespaces = new Set();

/**
 * LigLogger.
 * Class to manager logging with a specific context
 * @constructor
 */

var LigLogger = function () {
    function LigLogger(name) {
        var activate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

        _classCallCheck(this, LigLogger);

        this.name = name;

        // keep name in namespaces
        _namespaces.add(name);

        // use name to know wich color to use
        this.namespace_color = d3.schemeCategory20b[Array.from(_namespaces).indexOf(name)];
        this.title_style = 'color: ' + this.namespace_color + '; font-weight: bold';
        // default behavior
        this.unactivate();

        // if activate && LIG_DEBUG is True activate
        if (activate && window.LIG_DEBUG) {
            this.activate();
        }

        // generic error managmeent
        this.error = Function.prototype.bind.call(console.error, console);
    }

    /**
     * Activate logging in attaching log / error / warn / debug function
     * @memberOf LigLogger
     */


    _createClass(LigLogger, [{
        key: 'activate',
        value: function activate() {
            // custom log title
            this.log = Function.prototype.bind.call(console.log, console, '%c' + this.name, this.title_style);
            this.error = Function.prototype.bind.call(console.error, console);
            this.warn = Function.prototype.bind.call(console.warn, console);
            this.debug = Function.prototype.bind.call(console.debug, console);
        }

        /**
         * Unactivate logging for log / warn / debug function
         * @memberOf LigLogger
         */

    }, {
        key: 'unactivate',
        value: function unactivate() {
            this.log = this.warn = this.debug = function () {};
        }
    }]);

    return LigLogger;
}();

module.exports = LigLogger;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],34:[function(require,module,exports){
(function (global){
'use strict';

/*jslint node: true*/

var d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null);

/**
 * Helper to get the scale transform
 * @param  {HTMLNode} elem - html node element
 * @return {Arrays}        - [scaleX, scaleY]
 */
function getScaleTransform(elem) {
    var boundingClientRect = elem.getBoundingClientRect();
    return [boundingClientRect.width / elem.offsetWidth, boundingClientRect.height / elem.offsetHeight];
}

/**
 * Helper to get the scale X
 * @param  {HTMLNode} elem - html node element
 * @return {Float}         - scaleX
 */
function getScaleTransformX(elem) {
    return getScaleTransform(elem)[0];
}

/**
 * Helper to get the scale Y
 * @param  {HTMLNode} elem - html node element
 * @return {Float}         - scaleY
 */
function getScaleTransformY(elem) {
    return getScaleTransform(elem)[1];
}

/**
 * Helper to get the scale Y
 * @param  {HTMLNode} elem - html node element that we will use as context
 * @return {Arrays}        - coords
 */
function getUnscaledMousePosition(elem) {
    var coords = d3.mouse(elem),
        scale = getScaleTransform(elem);
    return [coords[0] * (1 / scale[0]), coords[1] * (1 / scale[1])].map(Math.round);
}

module.exports = {
    getScaleTransform: getScaleTransform,
    getScaleTransformX: getScaleTransformX,
    getScaleTransformY: getScaleTransformY,
    getUnscaledMousePosition: getUnscaledMousePosition
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],35:[function(require,module,exports){
'use strict';

/**
 * Get a generic scroll bar width
 * -- found from stackoverflow
 * @returns {integer} width
 */

var getGenericScrollbarWidth = function getGenericScrollbarWidth() {
  var div,
      width = getGenericScrollbarWidth.width;
  if (width === undefined) {
    div = document.createElement('div');
    div.innerHTML = '<div style="width:50px;height:50px;position:absolute;left:-50px;top:-50px;overflow:auto;"><div style="width:1px;height:100px;"></div></div>';
    div = div.firstChild;
    document.body.appendChild(div);
    width = getGenericScrollbarWidth.width = div.offsetWidth - div.clientWidth;
    document.body.removeChild(div);
  }
  return width;
};

module.exports = {
  getGenericScrollbarWidth: getGenericScrollbarWidth
};

},{}],36:[function(require,module,exports){
(function (global){
'use strict';

var d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null);

var COLORS = ["#007CDB", "#FA8345", "#0AB45A", "#DB1A43", "#00B3FA", "#AF1AD8", "#F0E200", "#14D2DC", "#FA78FA", "#017C93", "#89FA76", "#FACCA5", "#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b", "#e377c2"],
    _cachedColors = {};

/**
 * Helper to centralize color getter
 * @param {String} label
 */

var getColor = function getColor(label) {
    var group = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "common";

    var colors = _cachedColors[group] || {};
    if (!colors[label]) {
        var new_id = d3.keys(colors).length;
        if (new_id > COLORS.length - 1) {
            new_id = new_id - Math.trunc(new_id / COLORS.length) * COLORS.length;
            console.warn('new id ' + d3.keys(colors).length + ' for label "' + label + '" inside group "' + group + '" is out of ' + (COLORS.length - 1) + 'cached colors, new id choosen ' + new_id);
        }
        colors[label] = COLORS[new_id];
        _cachedColors[group] = colors;
    }
    return colors[label];
};

module.exports = {
    getColor: getColor
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],37:[function(require,module,exports){
(function (global){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null),
    resizable = require(39),
    LigLogger = require(33),
    $ = (typeof window !== "undefined" ? window['$'] : typeof global !== "undefined" ? global['$'] : null),
    _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null),
    fullScreenApi = require(32),
    keyMaster = require(2),
    select2 = require(23);

// bind select2 plugin with jquery
select2(window, $);

// less integration
require(43);

/**
 * FixedMenu.
 * Widget showing different items that user can interact with it
 *
 * @constructor
 * @param {Object} selection - d3 element
 */

var SideMenu = function () {
    function SideMenu(selection) {
        _classCallCheck(this, SideMenu);

        this.name = "FixedMenu";
        this.dispatch = d3.dispatch('change', 'resize-x', 'resize-x-move', 'layoutchange');
        this.container = selection;
        this._options = {
            collasped: false,
            width: 200,
            minResizeHeight: 50, // used by resizable
            fitBottom: true // used to know if we fit the last resizable element
        };
        this.logger = new LigLogger('lig-sidemenu');
        this.dom = {
            menuContainer: null,
            container: selection,
            button: null,
            list: null
        };
        this.obj = {
            // items that we will display
            items: [],
            // list will be display only if this property is equal to true
            isCollasped: false,
            // additional informations
            infos: [],
            blocs: [],
            collaspedBlocs: {},
            cache: {}
        };
    }
    /**
     * Update the d3 container node
     *
     * @param {any} containerNode - d3 container node
     * @memberof FixedMenu
     */


    _createClass(SideMenu, [{
        key: 'setContainer',
        value: function setContainer(d3ContainerNode) {
            var self = this;
            self.container = d3ContainerNode;
            self.dom.container = d3ContainerNode;
            return self;
        }

        /**
         * Update/Get blocs
         *
         * @param {Arrays} _ - blocs
         * @memberof FixedMenu
         */

    }, {
        key: 'blocs',
        value: function blocs(_) {
            if (!arguments.length) {
                return this.obj.blocs;
            }
            this.obj.blocs = _;
            return this;
        }

        /**
         * Refresh the menu
         *
         * @memberof FixedMenu
         */

    }, {
        key: 'refresh',
        value: function refresh() {
            var self = this;
            self.logger.log('refresh');

            var refreshSelect2 = function refreshSelect2($elem, d) {
                // TODO: find a way to get a good rendering and reset hacky style (see menu.less)
                $elem.select2({ theme: "bootstrap", dropdownAutoWidth: true }) // select 2 with bootstrap theme
                .off('select2:select') // reset previous event handler
                .on('select2:select', function (e) {
                    // bind event handler
                    this.value = e.params.data.id; // copy value to dom
                    return d.onChange ? d.onChange(d, this) : null; // call callback
                }).off('select2:open') // reset previous event handler
                .on('select2:open', function () {
                    // bind event handler
                    setTimeout(function () {
                        // set val with the one from the cache then trigger input in order to let widget redrawing
                        $('.select2-search--dropdown input').val(self.obj.cache[d.label] || '').trigger('input');
                    });
                }).off('select2:closing') // reset previous event handler
                .on('select2:closing', function () {
                    // bind event handler
                    self.obj.cache[d.label] = $('.select2-search--dropdown input').val();
                });
            };

            var refreshOptions = function refreshOptions(elem, values, value) {
                var options = elem.selectAll('option').data(values);
                var optionsEnter = options.enter().append('option');
                options.merge(optionsEnter).attr('value', function (t) {
                    return t;
                }).text(function (t) {
                    return t;
                });
                options.exit().remove();
                elem.property('value', value);
            };

            var menuContainer = self.dom.container.selectAll('.slide.menu').data([[]]).enter().append('div').attr('nochilddrag', 'nochilddrag').classed('menu', true).classed('slide', true);

            menuContainer.append('span').attr('class', 'btn-default menu__button btn btn-xs').on('click', function () {
                self.obj.isCollasped = !self.obj.isCollasped;
                self.dispatch.call('change', self);
                self.refresh();
            }).append('i').attr('class', 'glyphicon glyphicon-align-justify');

            self.dom.menuContainer = self.dom.container.select('.slide.menu');
            var bloc = self.dom.menuContainer.selectAll('.bloc').data(self.obj.blocs, function (d) {
                return d.title;
            });

            bloc.enter().append('div').attr('class', 'bloc').each(function (d) {
                self.logger.log('create bloc');

                var collapser = d3.select(this).append('span').attr('class', 'collapser').on('click', function (d) {
                    if (d.collapsable) {
                        self.obj.collaspedBlocs[d.title] = !self.obj.collaspedBlocs[d.title];
                        d3.select(this.parentNode).classed('collapsed', self.obj.collaspedBlocs[d.title]);
                        setTimeout(function () {
                            self.dispatch.call('layoutchange', self);
                        }, 500);
                    }
                }).classed('collapsable', function (d) {
                    return d.collapsable === true;
                });

                collapser.append('span').attr('class', 'bloc-title').text(d.title).classed('hidden', function (d) {
                    return d.showTitle === false;
                });

                collapser.append('i').attr('class', 'glyphicon glyphicon-chevron-up');
                collapser.append('i').attr('class', 'glyphicon glyphicon-chevron-down');

                var content = d3.select(this).append('div').attr('class', 'bloc-content');
                if (d.type === 'informations') {
                    content.append('table').attr('class', 'table table-condensed table-striped bloc-informations').append('tbody');
                } else {
                    content.append('ul').attr('class', 'list-group bloc-controls');
                }
            });
            bloc.exit().remove();
            self.dom.menuContainer.selectAll('.bloc').classed('collapsed', function (d) {
                return self.obj.collaspedBlocs[d.title];
            }).classed('hide-title', function (d) {
                return d.showTitle === false;
            }).classed('collapsable', function (d) {
                return d.collapsable;
            });

            // quickly helper to get block data from a blockname
            var _getData = function _getData(blocName) {
                return self.obj.blocs[self.obj.blocs.findIndex(function (d) {
                    return d.title === blocName;
                })].data;
            };

            self.dom.menuContainer.selectAll('.bloc-informations>tbody').each(function (d) {
                var infos = d3.select(this).selectAll('tr.information').data(_getData(d.title) || []);
                var infosEnter = infos.enter().append('tr').attr('class', 'information');

                infosEnter.append('td').attr('class', 'title');
                infosEnter.append('td').attr('class', 'text');

                var infosUpdate = infosEnter.merge(infos);
                infosUpdate.select('.title').classed('fullscreenable', function (d) {
                    return (d[2] || {}).fullscreenable === true;
                }).html(function (d) {
                    return d[0];
                }).on('click', function (d) {
                    if ((d[2] || {}).fullscreenable) {
                        fullScreenApi.toggle($(this).parent().find('td.text')[0]);
                    }
                });

                infosUpdate.select('.text').each(function (d) {
                    if ((d[2] || {}).compute) {
                        // if there is a compute function in options use it
                        d[2].compute(this, d[1]);
                    } else {
                        // if there is not compute function, fill html with the value
                        d3.select(this).html(function (d) {
                            return d[1];
                        });
                    }
                });
                infos.exit().remove();
            });

            self.dom.menuContainer.selectAll('.bloc-controls').attr('bloc-id', function (d) {
                return d.title;
            }).each(function (d) {
                // TODO: need to check why we are not able to get data directly
                var items = d3.select(this).selectAll('li').data(_getData(d.title), function (a) {
                    return a.help + a.label + a.type;
                });

                // ENTER
                var labelEnter = void 0;
                var itemsEnter = items.enter().append('li').attr('type', function (t) {
                    return t.type;
                }).classed('list-group-item', true).classed('inline', function (d) {
                    return d.inline;
                });

                // BUTTON enter
                itemsEnter.filter(function (d) {
                    return d.type === 'button';
                }).append('span').attr('class', 'btn-default btn btn-xs form-control');

                // BUTTONS enter
                itemsEnter.filter(function (d) {
                    return d.type === 'buttons';
                }).append('span').attr('class', 'btn-group form-control');

                // RANGE enter
                labelEnter = itemsEnter.filter(function (d) {
                    return d.type === 'range';
                }).append('label');
                labelEnter.append('span').text(function (d) {
                    return d.label;
                }).attr('class', 'bloc-control-label').classed('hidden', function (d) {
                    return d.hideLabel;
                });
                labelEnter.append('input').attr('type', 'range');

                // NUMBER enter
                labelEnter = itemsEnter.filter(function (d) {
                    return d.type === 'number';
                }).append('label');
                labelEnter.append('span').attr('class', 'bloc-control-label').text(function (d) {
                    return d.label;
                }).classed('hidden', function (d) {
                    return d.hideLabel;
                });

                labelEnter.append('input').attr('type', 'number').attr('class', 'form-control');

                // SELECT enter
                labelEnter = itemsEnter.filter(function (d) {
                    return d.type === 'select';
                }).append('label');
                labelEnter.append('span').text(function (d) {
                    return d.label;
                }).attr('class', 'bloc-control-label').classed('hidden', function (d) {
                    return d.hideLabel;
                });

                labelEnter.append('select').attr('class', 'form-control');

                // BOOLEAN enter
                var checkbox = itemsEnter.filter(function (d) {
                    return d.type === 'boolean';
                }).append('div').attr('class', 'checkbox');

                checkbox.append('label').append('input').attr('type', 'checkbox');

                checkbox.select('label').append('span').text(function (d) {
                    return d.label;
                });

                // SEARCH enter
                labelEnter = itemsEnter.filter(function (d) {
                    return d.type === 'search';
                }).append('label');
                labelEnter.append('span').text(function (d) {
                    return d.label;
                }).attr('class', 'bloc-control-label').classed('hidden', function (d) {
                    return d.hideLabel;
                });
                var searchFieldEnter = labelEnter.append('div').attr('class', 'search-field input-group');

                searchFieldEnter.filter(function (d) {
                    return d.showSelectAll;
                }).append('span').attr('class', 'input-group-addon addon-left').append('i').attr('class', 'glyphicon select-all btn btn-default btn-xs').on('click', function () {
                    d3.select($(this).parents('li')[0]).selectAll('tr.row-table:not(.hidden)').classed('selected', true);
                }).classed('glyphicon-check', true);
                searchFieldEnter.append('input').attr('type', 'search').attr('placeholder', function (c) {
                    return c.placeholder;
                }).property('value', function (d) {
                    return d.filterValue;
                }).attr('class', 'search-input form-control');
                searchFieldEnter.append('span').attr('class', 'input-group-addon').each(function (d) {
                    if (d.showCount) {
                        d3.select(this).append('span').attr('class', 'badge result-count').text(0);
                    } else {
                        d3.select(this).append('i').attr('class', 'glyphicon glyphicon-search');
                    }
                });

                var tableEnter = labelEnter.append('div').attr('class', 'search-table-container').append('table').attr('class', 'search-table table').append('tbody');
                tableEnter.append('tr').attr('class', 'header-arrows');
                tableEnter.append('tr').attr('class', 'drag-element hidden');

                // UPDATE
                var itemsUpdate = itemsEnter.merge(items);

                // BUTTON update
                itemsUpdate.filter(function (d) {
                    return d.type === 'button';
                }).select('span').text(function (d) {
                    return d.label;
                }).on('click', function (d) {
                    return d.onClick(d, this);
                }).classed('active', function (d) {
                    return d.active;
                }).each(function (d) {
                    var elem = d3.select(this);
                    (d.classed || []).forEach(function (classed) {
                        elem.classed(classed[0], function (sequence) {
                            return classed[1](sequence);
                        });
                    });
                });

                // BUTTONS update
                itemsUpdate.filter(function (d) {
                    return d.type === 'buttons';
                }).select('span').each(function (group) {
                    var d3Elem = d3.select(this);
                    var button = d3Elem.selectAll('button').data(group.data);
                    var buttonEnter = button.enter().append('button').attr('class', 'btn btn-default').text(function (b) {
                        return b.text;
                    });
                    button.merge(buttonEnter).classed('active', function (b) {
                        return b.active;
                    }).on('click', function (b) {
                        return group.onClick(b, this);
                    });
                });

                itemsUpdate.filter(function (d) {
                    return d.type === 'boolean';
                }).select('input').property('checked', function (d) {
                    return d.value;
                }).on('change', function (d) {
                    return d.onChange(d, this);
                });

                // SELECT update
                itemsUpdate.filter(function (d) {
                    return d.type === 'select';
                }).select('select').each(function (d) {
                    var $elem = $(this);
                    var elem = d3.select(this);
                    if (d.provider) {
                        d.provider().then(function (datasets) {
                            refreshOptions(elem, datasets, d.value);refreshSelect2($elem, d);
                        });
                    } else {
                        refreshOptions(elem, d.values, d.value);
                        refreshSelect2($elem, d);
                    }
                }).on('change', function (d) {
                    self.logger.log('change select');
                    return d.onChange ? d.onChange(d, this) : null;
                });

                // RANGE update
                itemsUpdate.filter(function (d) {
                    return d.type === 'range';
                }).select('input').attr('min', function (d) {
                    return d.interval[0];
                }).attr('max', function (d) {
                    return d.interval[1];
                }).attr('step', function (d) {
                    return d.step ? d.step : 'any';
                }).property('value', function (d) {
                    return d.value;
                }).attr('title', function (d) {
                    return d.value;
                }).on('change', function (d) {
                    return d.onChange ? d.onChange(d, this) : null;
                });

                // RANGE update
                itemsUpdate.filter(function (d) {
                    return d.type === 'number';
                }).select('input').attr('min', function (d) {
                    return d.interval[0];
                }).attr('max', function (d) {
                    return d.interval[1];
                }).attr('step', function (d) {
                    return d.step;
                }).property('value', function (d) {
                    return d.value;
                }).attr('title', function (d) {
                    return d.value;
                }).on('input', function (d) {
                    self.logger.log('input !!');
                    return d.onChange ? d.onChange(d, this) : null;
                });

                /**
                 * Helper to refresh search table widget
                 *  => search input with keyup event
                 *  => list of value rows with click event
                 *  => arrows row with data sorting
                 */
                var refreshSearchTable = function refreshSearchTable(elem, itemData) {
                    // prepare arrows data
                    var arrows_row_data = itemData.arrows || (itemData.data[0] || []).map(function (d, i) {
                        return {
                            active: false,
                            class: i,
                            order: 'asc',
                            id: i
                        };
                    });

                    // prepare value data
                    var data = void 0;
                    var sortData = arrows_row_data[arrows_row_data.findIndex(function (d) {
                        return d.active === true;
                    })];
                    if (sortData) {
                        data = _.orderBy(itemData.data, [function (ts) {
                            var value = ts[sortData.id],
                                columnDefinition = (itemData.columns || [])[sortData.id] || {};
                            return columnDefinition.onSort ? columnDefinition.onSort(value) : value;
                        }], [sortData.order]);
                    } else {
                        data = itemData.data;
                    }

                    // drag behaviour
                    //self.logger.log('refreshTable', elem, itemData);
                    var initCoords = {};
                    var drag = d3.drag().on('start', function () {
                        initCoords = { x: d3.event.sourceEvent.x, y: d3.event.sourceEvent.y };
                        d3.select(this).classed('list-item-hover', true);
                        elem.select('.drag-element').html(d3.select(this).html()).classed('hidden', true).style('top', initCoords.y - 10 + 'px').style('left', initCoords.x - 10 + 'px');
                        if (itemData.onDragStart) {
                            itemData.onDragStart();
                        }
                    }).on('drag', function () {
                        var coords = { x: d3.event.sourceEvent.x, y: d3.event.sourceEvent.y };
                        if (itemData.draggable && (Math.abs(coords.x - initCoords.x) > 10 || Math.abs(coords.y - initCoords.y) > 10)) {
                            elem.select('.drag-element').classed('hidden', false).style('top', d3.event.sourceEvent.y - 10 + 'px').style('left', d3.event.sourceEvent.x + 'px');
                            if (itemData.onDragMove) {
                                itemData.onDragMove();
                            }
                        }
                    }).on('end', function (d) {
                        var coords = { x: d3.event.sourceEvent.x, y: d3.event.sourceEvent.y };
                        d3.select(this).classed('list-item-hover', false);
                        elem.select('.drag-element').classed('hidden', true);
                        // if current mouse coords are different from the initial mouse coords, real drag end
                        if (itemData.draggable && coords.x !== initCoords.x && coords.y !== initCoords.y) {
                            var _data = elem.selectAll('tr.row-table.selected').data();
                            if (_data.length === 0 || _data.findIndex(function (t) {
                                return t[0] === d[0];
                            }) === -1) {
                                _data = [d];
                            }
                            if (itemData.onDragEnd) {
                                itemData.onDragEnd(_data, this);
                            }
                        }
                        // equivalents to click
                        else {
                                // FIXME: if we select then we search/ then select other one/ then undo search, the old selection is still here
                                // FIXME: how we manage the all select button ? should we lock as now or change behavior
                                // if ctrl or shift combined with click, it modifies pattern activation in current patternList
                                itemData.selected = itemData.selected || [];
                                //itemData.selected = (itemData.selected === d[0]) ? null : d[0];
                                if (keyMaster.command || keyMaster.control) {
                                    var idx = itemData.selected.indexOf(d[0]);
                                    if (idx > -1) {
                                        itemData.selected.splice(idx, 1);
                                    } else {
                                        itemData.selected.push(d[0]);
                                    }
                                } else if (keyMaster.shift) {
                                    var _idx = data.findIndex(function (de) {
                                        return de[0] === d[0];
                                    });
                                    var nearest = -1;
                                    itemData.selected.forEach(function (s) {
                                        var a = data.findIndex(function (de) {
                                            return de[0] === s;
                                        });
                                        if (nearest === -1 || Math.abs(_idx - nearest) > Math.abs(_idx - a)) {
                                            nearest = a;
                                        }
                                    });
                                    if (nearest !== -1) {
                                        var begin = _idx,
                                            end = nearest;
                                        if (end < begin) {
                                            end = _idx;
                                            begin = nearest;
                                        }
                                        for (var index = nearest > _idx ? _idx : nearest; index <= end; index++) {
                                            itemData.selected.push(data[index][0]);
                                        }
                                    }
                                }
                                // otherwise just modify list to activate only the wanted pattern
                                else if (itemData.selected.length > 1 || itemData.selected[0] !== d[0]) {
                                        itemData.selected = [d[0]];
                                    } else {
                                        itemData.selected = [];
                                    }
                                elem.selectAll('tr.row-table').classed('selected', function (d) {
                                    return itemData.selected.indexOf(d[0]) > -1;
                                });
                                if (itemData.onClick) {
                                    itemData.onClick(d, this, { selected: itemData.selected });
                                }
                                if (d3.event.stopPropagation) {
                                    d3.event.stopPropagation();
                                }
                            }
                    });

                    // store
                    itemData.arrows = arrows_row_data;
                    if (itemData.showHeader !== false) {
                        // arrows
                        var arrows_cell = elem.select('.header-arrows').selectAll('td').data(itemData.arrows);
                        var arrows_cell_enter = arrows_cell.enter().append('td');
                        var header_sub_container = arrows_cell_enter.append('span').classed('header-sub-container', true);

                        header_sub_container.append('span').attr('class', 'header-title').classed('hidden', function (d) {
                            return !d.label;
                        }).text(function (d) {
                            return d.label;
                        });
                        var arrows_cell_enter_icons = header_sub_container.append('span').attr('class', 'header-icons');
                        arrows_cell_enter_icons.append('i').attr('class', 'glyphicon glyphicon-chevron-up');
                        arrows_cell_enter_icons.append('i').attr('class', 'glyphicon glyphicon-chevron-down');

                        var arrows_cell_update = arrows_cell_enter.merge(arrows_cell);
                        arrows_cell_update.classed('hidden', function (d, i) {
                            return itemData.columns && itemData.columns[i] && itemData.columns[i].visible === false;
                        }).classed('active', function (d) {
                            return d.active;
                        }).on('click', function (cell) {
                            // change cell value
                            if (!cell.active) {
                                console.log('not active, active it');
                                arrows_row_data.forEach(function (d) {
                                    return d.active = false;
                                });
                                cell.active = true;
                                cell.order = 'asc';
                            } else {
                                console.log('active, desc');
                                cell.order = cell.order === 'asc' ? 'desc' : 'asc';
                            }
                            if (itemData.onSortChange) {
                                itemData.onSortChange(itemData.arrows);
                            }
                            d3.event.stopPropagation(); // stop propagation
                            return refreshSearchTable(elem, itemData); // refresh table
                        }).each(function (d) {
                            var node = d3.select(this);
                            node.select('.glyphicon-chevron-up').classed('invisible', d.order === 'asc' && d.active === true);
                            node.select('.glyphicon-chevron-down').classed('invisible', d.order === 'desc' && d.active === true);
                        });

                        arrows_cell_update.select('span.header-title').classed('hidden', function (d, i) {
                            return !itemData.columns || !itemData.columns[i] || !itemData.columns[i].label;
                        }).text(function (d, i) {
                            return itemData.columns && itemData.columns[i] && itemData.columns[i].label;
                        });
                    }
                    // value rows
                    var row = elem.select('table tbody').selectAll('tr.row-table').data(data, function (t, i) {
                        return t[0] + '-' + i;
                    });

                    row.enter().append('tr').attr('class', 'row-table').attr('draggable', 'true').each(function (rowData) {
                        d3.select(this).selectAll('td').data(rowData).enter().append('td').classed('hidden', function (d, i) {
                            return itemData.columns && itemData.columns[i] && itemData.columns[i].visible === false;
                        }).html(function (d) {
                            return d;
                        });
                    });

                    row.exit().remove();

                    elem.selectAll('tr.row-table').classed('selected', function (d) {
                        return itemData.selected && itemData.selected.indexOf(d[0]) > -1;
                    }).classed('active', function (d) {
                        return itemData.selected && itemData.selected.indexOf(d[0]) > -1;
                    }).classed('text-center', true).call(drag).on('mouseenter', function (d) {
                        if (itemData.onMouseEnter) {
                            itemData.onMouseEnter(d, this);
                        }
                    }).on('mouseleave', function (d) {
                        if (itemData.onMouseLeave) {
                            itemData.onMouseLeave(d, this);
                        }
                    }).each(function () {
                        var elem = d3.select(this);
                        (itemData.classed || []).forEach(function (classed) {
                            elem.classed(classed[0], function (sequence) {
                                return classed[1](sequence);
                            });
                        });
                    });
                    // keyup event handler
                    elem.select('input').property('value', function (d) {
                        return d.filterValue;
                    }).on('keyup', function () {
                        itemData.filterValue = this.value;
                        elem.selectAll('tr.row-table').classed('hidden', function (d) {
                            return d.findIndex(function (a) {
                                return a.indexOf(itemData.filterValue) > -1;
                            }) === -1;
                        });
                        elem.select('.result-count').text(elem.selectAll('.row-table:not(.hidden)').size());
                        if (itemData.onSearch) {
                            return itemData.onSearch(itemData, this);
                        }
                    });
                    elem.select('.result-count').text(elem.selectAll('.row-table:not(.hidden)').size());
                };

                // SEARCH update
                itemsUpdate.filter(function (itemData) {
                    return itemData.type === 'search';
                }).each(function (itemData) {
                    var elem = d3.select(this);
                    if (itemData.showTable !== false) {
                        resizable(elem.select('label').node(), elem.select('.search-table-container').node(), self._options.minResizeHeight, undefined, false, true);
                        return refreshSearchTable(elem, itemData);
                    } else {
                        elem.select('input').on('keyup', function () {
                            itemData.filterValue = this.value;
                            if (itemData.onSearch) {
                                return itemData.onSearch(itemData, this);
                            }
                        });
                    }
                    elem.select('.search-table-container').classed('hidden', itemData.showTable === false);
                    elem.select('.result-count').text(itemData.count);
                });

                // update the help description showed by the title tag
                itemsUpdate.attr('title', function (d) {
                    return d.help;
                });

                // EXIT
                items.exit().remove();
            });
            // resizable is a widget to allow user to resize specified element
            resizable(self.dom.menuContainer.node(), self.dom.container.node(), self._options.minResizeHeight, self.dispatch, true, false);
            var size = self.dom.container.node().getBoundingClientRect();
            self.container.classed('collapsed', self.isCollapsed());
            self.container.select('.menu__button').style('left', function () {
                return size.left + 'px';
            }).style('top', size.top + 'px');
            self.fitBottom();
        }
        /**
         * Function to fit last bloc inside its menu
         *
         * @memberof FixedMenu
         */

    }, {
        key: 'fitBottom',
        value: function fitBottom() {
            var self = this;
            var containerNode = self.container.node(),
                all_blocs = containerNode.querySelectorAll(".menu>div.bloc"),
                lastBloc = all_blocs[all_blocs.length - 1],
                table_dom = lastBloc.querySelector('.resizable');
            // update bloc to assign 'last' class to the last bloc
            d3.selectAll(all_blocs).classed('last', function (d, i) {
                return i === all_blocs.length - 1;
            });
            // if fitBottom is on and there is a resizable element inside, recalculate height
            if (table_dom && self._options.fitBottom) {
                var newHeight = d3.max([$(containerNode).height() - $(lastBloc).position().top - $(lastBloc).outerHeight(false) + $(table_dom).outerHeight(false) - 3, self._options.minResizeHeight]);
                d3.select(table_dom).style('height', newHeight + 'px').style('max-height', newHeight + 'px');
            }
        }

        /**
         * Function to refresh partialy menu (a specific bloc)
         *
         * @param {any} bloc_title - the identifier to get the bloc
         * @param {any} data - data of the bloc
         * @memberof SideMenu
         */

    }, {
        key: 'refreshPartial',
        value: function refreshPartial(bloc_title, data) {
            var self = this;
            var bloc = self.obj.blocs[self.obj.blocs.findIndex(function (b) {
                return b.title === bloc_title;
            })];
            bloc.data = data;
            self.dom.menuContainer.selectAll('.bloc').filter(function (d) {
                return d.title === bloc_title;
            }).each(function () {
                var infos = d3.select(this).select('tbody').selectAll('tr.information').data(bloc.data);
                var infosEnter = infos.enter().append('tr').attr('class', 'information');

                infosEnter.append('td').attr('class', 'title');
                infosEnter.append('td').attr('class', 'text');

                var infosUpdate = infosEnter.merge(infos);
                infosUpdate.select('.title').classed('fullscreenable', function (d) {
                    return (d[2] || {}).fullscreenable === true;
                }).html(function (d) {
                    return d[0];
                }).on('click', function (d) {
                    if ((d[2] || {}).fullscreenable) {
                        fullScreenApi.toggle($(this).parent().find('td.text')[0]);
                    }
                });

                infosUpdate.select('.text').each(function (d) {
                    if ((d[2] || {}).compute) {
                        // if there is a compute function in options use it
                        d[2].compute(this, d[1]);
                    } else {
                        // if there is not compute function, fill html with the value
                        d3.select(this).html(function (d) {
                            return d[1];
                        });
                    }
                });
                infos.exit().remove();
            });
            self.fitBottom();
        }

        /**
         * Getter to know if the menu is collapsed
         * @returns {boolean}
         * @memberof SideMenu
         */

    }, {
        key: 'isCollapsed',
        value: function isCollapsed() {
            return this.obj.isCollasped;
        }

        /**
         * Event generic handler setter
         *
         * @param {any} eventName - name of the event from dispatch property
         * @param {any} handler - function that will be used as handler
         * @memberof SideMenu
         */

    }, {
        key: 'on',
        value: function on(eventName, handler) {
            this.dispatch.on(eventName, handler);
            return this;
        }
    }]);

    return SideMenu;
}();

module.exports = SideMenu;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"2":2,"23":23,"32":32,"33":33,"39":39,"43":43}],38:[function(require,module,exports){
'use strict';

// Widget
// See https://github.com/noraesae/perfect-scrollbar

var ps = require(3);

// Stylesheet
require(44);

/**
 * Manager to be used in order to dont throw error if widget is not available
 */
var manager = function manager() {
    if (ps) {
        return ps;
    } else {
        return {
            update: function update() {},
            initialize: function initialize() {}
        };
    }
};

module.exports = manager();

},{"3":3,"44":44}],39:[function(require,module,exports){
(function (global){
'use strict';

// external lib
var d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null);

// less
require(45);

/**
 * Allow to manage resize behaviors
 *
 * @param {*} location : where the resizers have to be included
 * @param {*} target - element that will be impacted
 * @param {*} minWidth - minimum width allowed
 * @param {*} dispatcher - external dispatcher
 */
var resizable = function () {
    "use strict";

    return function () {
        var location = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
        var target = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
        var minWidth = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 50;
        var dispatcher = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
        var horizontal = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : true;
        var vertical = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : true;

        // Drag behavior (based on d3 drag)
        var drag = d3.drag().on('start', function () {
            // Apply new width on the target
            var d3Element = d3.select(this);
            d3.select(location).classed('x-resizing', d3Element.classed('x-resizing-right')).classed('y-resizing', d3Element.classed('y-resizing-bottom'));
        }).on('drag', function () {
            // Determine resizer position relative to resizable (target or parent)
            var coords = d3.mouse(target || this.parentNode);
            // Avoid negative or really small widths/heights
            var x = Math.max(minWidth, coords[0]),
                y = Math.max(minWidth, coords[1]);
            if (dispatcher) {
                dispatcher.call('resize-x-move', this, x);
            }
            // Apply new width on the target
            if (d3.select(this).classed('x-resizing-right') && horizontal) {
                d3.select(target || this.parentNode).style('width', x + 'px');
            }
            if (d3.select(this).classed('y-resizing-bottom') && vertical) {
                d3.select(target || this.parentNode).style('height', y + 'px').style('max-height', y + 'px');
            }
        }).on('end', function () {
            var x = d3.mouse(target || this.parentNode)[0];
            // Avoid negative or really small widths
            x = Math.max(minWidth, x);
            // TODO: hacky
            // use delegated dispatcher to dispatch resizable events
            if (dispatcher) {
                dispatcher.call('resize-x', this, x);
            }
            d3.select(location).classed('x-resizing', false).classed('y-resizing', false);
        });

        if (horizontal) {
            //  Add horizontal resizer (right side)
            d3.select(location).selectAll('.x-resizing-right').data([0]).enter().append('div').attr('class', 'x-resizing-right').call(drag);
        }

        if (vertical) {
            //  Add horizontal resizer (right side)
            d3.select(location).selectAll('.y-resizing-bottom').data([0]).enter().append('div').attr('class', 'y-resizing-bottom').call(drag);
        }

        d3.select(target).classed('resizable', true);
    };
}();

module.exports = resizable;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"45":45}],40:[function(require,module,exports){
(function() { var head = document.getElementsByTagName('head')[0]; var style = document.createElement('style'); style.type = 'text/css';var css = ".lig-tooltip{position:fixed;display:block;text-align:center;min-width:60px;min-height:28px;padding:5px;margin-left:15px;margin-top:-10px;line-height:28px;font:12px sans-serif;background:lightsteelblue;border:0;border-radius:8px;pointer-events:none;z-index:5000}.curve-subcontainer .curve-canvas,.curve-subcontainer .curve-canvas-mouse{position:absolute}.curve-subcontainer .pointing{cursor:pointer}.curve-subcontainer .xaxis line.tick-v{stroke:darkgrey;stroke-opacity:.5;stroke-width:.8}.curve-subcontainer .zoom{fill:none;pointer-events:all}.curve-subcontainer .minimap{stroke:grey}.curve-subcontainer .minimap .indicator{pointer-events:none}.curve-subcontainer .curve-canvas{z-index:500;display:block}.curve-subcontainer .curve-canvas-mouse{z-index:1000;display:block}.curve-subcontainer .curve{position:relative}.curve-subcontainer svg{z-index:1200;display:block}.curve-subcontainer text.variable{fill:\"#000\";transform:\"rotate(-90)\"}.curve-subcontainer .weights{fill:grey}";if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style);}())
},{}],41:[function(require,module,exports){
(function() { var head = document.getElementsByTagName('head')[0]; var style = document.createElement('style'); style.type = 'text/css';var css = ".fullscreen{width:100% !important;height:100% !important;margin:0;background-color:white;position:fixed;top:0;left:0}.fullscreen:-webkit-full-screen,.fullscreen:-moz-full-screen,.fullscreen:-ms-fullscreenn,.fullscreen:fullscreen{width:100%;height:100%;margin:0;background-color:white;position:fixed;top:0;left:0;z-index:10000}";if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style);}())
},{}],42:[function(require,module,exports){
(function() { var head = document.getElementsByTagName('head')[0]; var style = document.createElement('style'); style.type = 'text/css';var css = ".in-loading{position:relative;pointer-events:none}.in-loading>.loader{position:absolute;left:calc(50% - 60px);top:calc(50% - 60px);border:16px solid #f3f3f3;border-top:16px solid #3498db;border-radius:50%;width:120px;height:120px;opacity:1;z-index:10000;animation:spin 2s linear infinite}.in-loading>.loader.small{left:calc(50% -  25px);top:calc(50% -  25px);border:6px solid #f3f3f3;border-top:6px solid #3498db;width:50px;height:50px}@-webkit-keyframes spin{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}100%{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}@keyframes spin{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}100%{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}";if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style);}())
},{}],43:[function(require,module,exports){
(function() { var head = document.getElementsByTagName('head')[0]; var style = document.createElement('style'); style.type = 'text/css';var css = ".select2-container{box-sizing:border-box;display:inline-block;margin:0;position:relative;vertical-align:middle}.select2-container .select2-selection--single{box-sizing:border-box;cursor:pointer;display:block;height:28px;user-select:none;-webkit-user-select:none}.select2-container .select2-selection--single .select2-selection__rendered{display:block;padding-left:8px;padding-right:20px;overflow:hidden;text-overflow:ellipsis;white-space:nowrap}.select2-container .select2-selection--single .select2-selection__clear{position:relative}.select2-container[dir=\"rtl\"] .select2-selection--single .select2-selection__rendered{padding-right:8px;padding-left:20px}.select2-container .select2-selection--multiple{box-sizing:border-box;cursor:pointer;display:block;min-height:32px;user-select:none;-webkit-user-select:none}.select2-container .select2-selection--multiple .select2-selection__rendered{display:inline-block;overflow:hidden;padding-left:8px;text-overflow:ellipsis;white-space:nowrap}.select2-container .select2-search--inline{float:left}.select2-container .select2-search--inline .select2-search__field{box-sizing:border-box;border:none;font-size:100%;margin-top:5px;padding:0}.select2-container .select2-search--inline .select2-search__field::-webkit-search-cancel-button{-webkit-appearance:none}.select2-dropdown{background-color:white;border:1px solid #aaa;border-radius:4px;box-sizing:border-box;display:block;position:absolute;left:-100000px;width:100%;z-index:1051}.select2-results{display:block}.select2-results__options{list-style:none;margin:0;padding:0}.select2-results__option{padding:6px;user-select:none;-webkit-user-select:none}.select2-results__option[aria-selected]{cursor:pointer}.select2-container--open .select2-dropdown{left:0}.select2-container--open .select2-dropdown--above{border-bottom:none;border-bottom-left-radius:0;border-bottom-right-radius:0}.select2-container--open .select2-dropdown--below{border-top:none;border-top-left-radius:0;border-top-right-radius:0}.select2-search--dropdown{display:block;padding:4px}.select2-search--dropdown .select2-search__field{padding:4px;width:100%;box-sizing:border-box}.select2-search--dropdown .select2-search__field::-webkit-search-cancel-button{-webkit-appearance:none}.select2-search--dropdown.select2-search--hide{display:none}.select2-close-mask{border:0;margin:0;padding:0;display:block;position:fixed;left:0;top:0;min-height:100%;min-width:100%;height:auto;width:auto;opacity:0;z-index:99;background-color:#fff;filter:alpha(opacity=0)}.select2-hidden-accessible{border:0 !important;clip:rect(0 0 0 0) !important;-webkit-clip-path:inset(50%) !important;clip-path:inset(50%) !important;height:1px !important;overflow:hidden !important;padding:0 !important;position:absolute !important;width:1px !important;white-space:nowrap !important}.select2-container--default .select2-selection--single{background-color:#fff;border:1px solid #aaa;border-radius:4px}.select2-container--default .select2-selection--single .select2-selection__rendered{color:#444;line-height:28px}.select2-container--default .select2-selection--single .select2-selection__clear{cursor:pointer;float:right;font-weight:bold}.select2-container--default .select2-selection--single .select2-selection__placeholder{color:#999}.select2-container--default .select2-selection--single .select2-selection__arrow{height:26px;position:absolute;top:1px;right:1px;width:20px}.select2-container--default .select2-selection--single .select2-selection__arrow b{border-color:#888 transparent transparent transparent;border-style:solid;border-width:5px 4px 0 4px;height:0;left:50%;margin-left:-4px;margin-top:-2px;position:absolute;top:50%;width:0}.select2-container--default[dir=\"rtl\"] .select2-selection--single .select2-selection__clear{float:left}.select2-container--default[dir=\"rtl\"] .select2-selection--single .select2-selection__arrow{left:1px;right:auto}.select2-container--default.select2-container--disabled .select2-selection--single{background-color:#eee;cursor:default}.select2-container--default.select2-container--disabled .select2-selection--single .select2-selection__clear{display:none}.select2-container--default.select2-container--open .select2-selection--single .select2-selection__arrow b{border-color:transparent transparent #888 transparent;border-width:0 4px 5px 4px}.select2-container--default .select2-selection--multiple{background-color:white;border:1px solid #aaa;border-radius:4px;cursor:text}.select2-container--default .select2-selection--multiple .select2-selection__rendered{box-sizing:border-box;list-style:none;margin:0;padding:0 5px;width:100%}.select2-container--default .select2-selection--multiple .select2-selection__rendered li{list-style:none}.select2-container--default .select2-selection--multiple .select2-selection__placeholder{color:#999;margin-top:5px;float:left}.select2-container--default .select2-selection--multiple .select2-selection__clear{cursor:pointer;float:right;font-weight:bold;margin-top:5px;margin-right:10px}.select2-container--default .select2-selection--multiple .select2-selection__choice{background-color:#e4e4e4;border:1px solid #aaa;border-radius:4px;cursor:default;float:left;margin-right:5px;margin-top:5px;padding:0 5px}.select2-container--default .select2-selection--multiple .select2-selection__choice__remove{color:#999;cursor:pointer;display:inline-block;font-weight:bold;margin-right:2px}.select2-container--default .select2-selection--multiple .select2-selection__choice__remove:hover{color:#333}.select2-container--default[dir=\"rtl\"] .select2-selection--multiple .select2-selection__choice,.select2-container--default[dir=\"rtl\"] .select2-selection--multiple .select2-selection__placeholder,.select2-container--default[dir=\"rtl\"] .select2-selection--multiple .select2-search--inline{float:right}.select2-container--default[dir=\"rtl\"] .select2-selection--multiple .select2-selection__choice{margin-left:5px;margin-right:auto}.select2-container--default[dir=\"rtl\"] .select2-selection--multiple .select2-selection__choice__remove{margin-left:2px;margin-right:auto}.select2-container--default.select2-container--focus .select2-selection--multiple{border:solid black 1px;outline:0}.select2-container--default.select2-container--disabled .select2-selection--multiple{background-color:#eee;cursor:default}.select2-container--default.select2-container--disabled .select2-selection__choice__remove{display:none}.select2-container--default.select2-container--open.select2-container--above .select2-selection--single,.select2-container--default.select2-container--open.select2-container--above .select2-selection--multiple{border-top-left-radius:0;border-top-right-radius:0}.select2-container--default.select2-container--open.select2-container--below .select2-selection--single,.select2-container--default.select2-container--open.select2-container--below .select2-selection--multiple{border-bottom-left-radius:0;border-bottom-right-radius:0}.select2-container--default .select2-search--dropdown .select2-search__field{border:1px solid #aaa}.select2-container--default .select2-search--inline .select2-search__field{background:transparent;border:none;outline:0;box-shadow:none;-webkit-appearance:textfield}.select2-container--default .select2-results>.select2-results__options{max-height:200px;overflow-y:auto}.select2-container--default .select2-results__option[role=group]{padding:0}.select2-container--default .select2-results__option[aria-disabled=true]{color:#999}.select2-container--default .select2-results__option[aria-selected=true]{background-color:#ddd}.select2-container--default .select2-results__option .select2-results__option{padding-left:1em}.select2-container--default .select2-results__option .select2-results__option .select2-results__group{padding-left:0}.select2-container--default .select2-results__option .select2-results__option .select2-results__option{margin-left:-1em;padding-left:2em}.select2-container--default .select2-results__option .select2-results__option .select2-results__option .select2-results__option{margin-left:-2em;padding-left:3em}.select2-container--default .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option{margin-left:-3em;padding-left:4em}.select2-container--default .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option{margin-left:-4em;padding-left:5em}.select2-container--default .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option{margin-left:-5em;padding-left:6em}.select2-container--default .select2-results__option--highlighted[aria-selected]{background-color:#5897fb;color:white}.select2-container--default .select2-results__group{cursor:default;display:block;padding:6px}.select2-container--classic .select2-selection--single{background-color:#f7f7f7;border:1px solid #aaa;border-radius:4px;outline:0;background-image:-webkit-linear-gradient(top, white 50%, #eeeeee 100%);background-image:-o-linear-gradient(top, white 50%, #eeeeee 100%);background-image:linear-gradient(to bottom, white 50%, #eeeeee 100%);background-repeat:repeat-x;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#FFFFFFFF', endColorstr='#FFEEEEEE', GradientType=0)}.select2-container--classic .select2-selection--single:focus{border:1px solid #5897fb}.select2-container--classic .select2-selection--single .select2-selection__rendered{color:#444;line-height:28px}.select2-container--classic .select2-selection--single .select2-selection__clear{cursor:pointer;float:right;font-weight:bold;margin-right:10px}.select2-container--classic .select2-selection--single .select2-selection__placeholder{color:#999}.select2-container--classic .select2-selection--single .select2-selection__arrow{background-color:#ddd;border:none;border-left:1px solid #aaa;border-top-right-radius:4px;border-bottom-right-radius:4px;height:26px;position:absolute;top:1px;right:1px;width:20px;background-image:-webkit-linear-gradient(top, #eeeeee 50%, #cccccc 100%);background-image:-o-linear-gradient(top, #eeeeee 50%, #cccccc 100%);background-image:linear-gradient(to bottom, #eeeeee 50%, #cccccc 100%);background-repeat:repeat-x;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#FFEEEEEE', endColorstr='#FFCCCCCC', GradientType=0)}.select2-container--classic .select2-selection--single .select2-selection__arrow b{border-color:#888 transparent transparent transparent;border-style:solid;border-width:5px 4px 0 4px;height:0;left:50%;margin-left:-4px;margin-top:-2px;position:absolute;top:50%;width:0}.select2-container--classic[dir=\"rtl\"] .select2-selection--single .select2-selection__clear{float:left}.select2-container--classic[dir=\"rtl\"] .select2-selection--single .select2-selection__arrow{border:none;border-right:1px solid #aaa;border-radius:0;border-top-left-radius:4px;border-bottom-left-radius:4px;left:1px;right:auto}.select2-container--classic.select2-container--open .select2-selection--single{border:1px solid #5897fb}.select2-container--classic.select2-container--open .select2-selection--single .select2-selection__arrow{background:transparent;border:none}.select2-container--classic.select2-container--open .select2-selection--single .select2-selection__arrow b{border-color:transparent transparent #888 transparent;border-width:0 4px 5px 4px}.select2-container--classic.select2-container--open.select2-container--above .select2-selection--single{border-top:none;border-top-left-radius:0;border-top-right-radius:0;background-image:-webkit-linear-gradient(top, white 0, #eeeeee 50%);background-image:-o-linear-gradient(top, white 0, #eeeeee 50%);background-image:linear-gradient(to bottom, white 0, #eeeeee 50%);background-repeat:repeat-x;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#FFFFFFFF', endColorstr='#FFEEEEEE', GradientType=0)}.select2-container--classic.select2-container--open.select2-container--below .select2-selection--single{border-bottom:none;border-bottom-left-radius:0;border-bottom-right-radius:0;background-image:-webkit-linear-gradient(top, #eeeeee 50%, white 100%);background-image:-o-linear-gradient(top, #eeeeee 50%, white 100%);background-image:linear-gradient(to bottom, #eeeeee 50%, white 100%);background-repeat:repeat-x;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#FFEEEEEE', endColorstr='#FFFFFFFF', GradientType=0)}.select2-container--classic .select2-selection--multiple{background-color:white;border:1px solid #aaa;border-radius:4px;cursor:text;outline:0}.select2-container--classic .select2-selection--multiple:focus{border:1px solid #5897fb}.select2-container--classic .select2-selection--multiple .select2-selection__rendered{list-style:none;margin:0;padding:0 5px}.select2-container--classic .select2-selection--multiple .select2-selection__clear{display:none}.select2-container--classic .select2-selection--multiple .select2-selection__choice{background-color:#e4e4e4;border:1px solid #aaa;border-radius:4px;cursor:default;float:left;margin-right:5px;margin-top:5px;padding:0 5px}.select2-container--classic .select2-selection--multiple .select2-selection__choice__remove{color:#888;cursor:pointer;display:inline-block;font-weight:bold;margin-right:2px}.select2-container--classic .select2-selection--multiple .select2-selection__choice__remove:hover{color:#555}.select2-container--classic[dir=\"rtl\"] .select2-selection--multiple .select2-selection__choice{float:right;margin-left:5px;margin-right:auto}.select2-container--classic[dir=\"rtl\"] .select2-selection--multiple .select2-selection__choice__remove{margin-left:2px;margin-right:auto}.select2-container--classic.select2-container--open .select2-selection--multiple{border:1px solid #5897fb}.select2-container--classic.select2-container--open.select2-container--above .select2-selection--multiple{border-top:none;border-top-left-radius:0;border-top-right-radius:0}.select2-container--classic.select2-container--open.select2-container--below .select2-selection--multiple{border-bottom:none;border-bottom-left-radius:0;border-bottom-right-radius:0}.select2-container--classic .select2-search--dropdown .select2-search__field{border:1px solid #aaa;outline:0}.select2-container--classic .select2-search--inline .select2-search__field{outline:0;box-shadow:none}.select2-container--classic .select2-dropdown{background-color:white;border:1px solid transparent}.select2-container--classic .select2-dropdown--above{border-bottom:none}.select2-container--classic .select2-dropdown--below{border-top:none}.select2-container--classic .select2-results>.select2-results__options{max-height:200px;overflow-y:auto}.select2-container--classic .select2-results__option[role=group]{padding:0}.select2-container--classic .select2-results__option[aria-disabled=true]{color:grey}.select2-container--classic .select2-results__option--highlighted[aria-selected]{background-color:#3875d7;color:white}.select2-container--classic .select2-results__group{cursor:default;display:block;padding:6px}.select2-container--classic.select2-container--open .select2-dropdown{border-color:#5897fb}/*!\n * Select2 Bootstrap Theme v0.1.0-beta.10 (https://select2.github.io/select2-bootstrap-theme)\n * Copyright 2015-2017 Florian Kissling and contributors (https://github.com/select2/select2-bootstrap-theme/graphs/contributors)\n * Licensed under MIT (https://github.com/select2/select2-bootstrap-theme/blob/master/LICENSE)\n */.select2-container--bootstrap{display:block}.select2-container--bootstrap .select2-selection{-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075);box-shadow:inset 0 1px 1px rgba(0,0,0,0.075);background-color:#fff;border:1px solid #ccc;border-radius:4px;color:#555;font-size:14px;outline:0}.select2-container--bootstrap .select2-selection.form-control{border-radius:4px}.select2-container--bootstrap .select2-search--dropdown .select2-search__field{-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075);box-shadow:inset 0 1px 1px rgba(0,0,0,0.075);background-color:#fff;border:1px solid #ccc;border-radius:4px;color:#555;font-size:14px}.select2-container--bootstrap .select2-search__field{outline:0}.select2-container--bootstrap .select2-search__field::-webkit-input-placeholder{color:#999}.select2-container--bootstrap .select2-search__field:-moz-placeholder{color:#999}.select2-container--bootstrap .select2-search__field::-moz-placeholder{color:#999;opacity:1}.select2-container--bootstrap .select2-search__field:-ms-input-placeholder{color:#999}.select2-container--bootstrap .select2-results__option{padding:6px 12px}.select2-container--bootstrap .select2-results__option[role=group]{padding:0}.select2-container--bootstrap .select2-results__option[aria-disabled=true]{color:#777;cursor:not-allowed}.select2-container--bootstrap .select2-results__option[aria-selected=true]{background-color:#f5f5f5;color:#262626}.select2-container--bootstrap .select2-results__option--highlighted[aria-selected]{background-color:#337ab7;color:#fff}.select2-container--bootstrap .select2-results__option .select2-results__option{padding:6px 12px}.select2-container--bootstrap .select2-results__option .select2-results__option .select2-results__group{padding-left:0}.select2-container--bootstrap .select2-results__option .select2-results__option .select2-results__option{margin-left:-12px;padding-left:24px}.select2-container--bootstrap .select2-results__option .select2-results__option .select2-results__option .select2-results__option{margin-left:-24px;padding-left:36px}.select2-container--bootstrap .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option{margin-left:-36px;padding-left:48px}.select2-container--bootstrap .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option{margin-left:-48px;padding-left:60px}.select2-container--bootstrap .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option{margin-left:-60px;padding-left:72px}.select2-container--bootstrap .select2-results__group{color:#777;display:block;padding:6px 12px;font-size:12px;line-height:1.42857143;white-space:nowrap}.select2-container--bootstrap.select2-container--focus .select2-selection,.select2-container--bootstrap.select2-container--open .select2-selection{-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(102,175,233,0.6);box-shadow:inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(102,175,233,0.6);-o-transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;-webkit-transition:border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;transition:border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;border-color:#66afe9}.select2-container--bootstrap.select2-container--open .select2-selection .select2-selection__arrow b{border-color:transparent transparent #999;border-width:0 4px 4px}.select2-container--bootstrap.select2-container--open.select2-container--below .select2-selection{border-bottom-right-radius:0;border-bottom-left-radius:0;border-bottom-color:transparent}.select2-container--bootstrap.select2-container--open.select2-container--above .select2-selection{border-top-right-radius:0;border-top-left-radius:0;border-top-color:transparent}.select2-container--bootstrap .select2-selection__clear{color:#999;cursor:pointer;float:right;font-weight:700;margin-right:10px}.select2-container--bootstrap .select2-selection__clear:hover{color:#333}.select2-container--bootstrap.select2-container--disabled .select2-selection{border-color:#ccc;-webkit-box-shadow:none;box-shadow:none}.select2-container--bootstrap.select2-container--disabled .select2-search__field,.select2-container--bootstrap.select2-container--disabled .select2-selection{cursor:not-allowed}.select2-container--bootstrap.select2-container--disabled .select2-selection,.select2-container--bootstrap.select2-container--disabled .select2-selection--multiple .select2-selection__choice{background-color:#eee}.select2-container--bootstrap.select2-container--disabled .select2-selection--multiple .select2-selection__choice__remove,.select2-container--bootstrap.select2-container--disabled .select2-selection__clear{display:none}.select2-container--bootstrap .select2-dropdown{-webkit-box-shadow:0 6px 12px rgba(0,0,0,0.175);box-shadow:0 6px 12px rgba(0,0,0,0.175);border-color:#66afe9;overflow-x:hidden;margin-top:-1px}.select2-container--bootstrap .select2-dropdown--above{-webkit-box-shadow:0 -6px 12px rgba(0,0,0,0.175);box-shadow:0 -6px 12px rgba(0,0,0,0.175);margin-top:1px}.select2-container--bootstrap .select2-results>.select2-results__options{max-height:200px;overflow-y:auto}.select2-container--bootstrap .select2-selection--single{height:34px;line-height:1.42857143;padding:6px 24px 6px 12px}.select2-container--bootstrap .select2-selection--single .select2-selection__arrow{position:absolute;bottom:0;right:12px;top:0;width:4px}.select2-container--bootstrap .select2-selection--single .select2-selection__arrow b{border-color:#999 transparent transparent;border-style:solid;border-width:4px 4px 0;height:0;left:0;margin-left:-4px;margin-top:-2px;position:absolute;top:50%;width:0}.select2-container--bootstrap .select2-selection--single .select2-selection__rendered{color:#555;padding:0}.select2-container--bootstrap .select2-selection--single .select2-selection__placeholder{color:#999}.select2-container--bootstrap .select2-selection--multiple{min-height:34px;padding:0;height:auto}.select2-container--bootstrap .select2-selection--multiple .select2-selection__rendered{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;line-height:1.42857143;list-style:none;margin:0;overflow:hidden;padding:0;width:100%;text-overflow:ellipsis;white-space:nowrap}.select2-container--bootstrap .select2-selection--multiple .select2-selection__placeholder{color:#999;float:left;margin-top:5px}.select2-container--bootstrap .select2-selection--multiple .select2-selection__choice{color:#555;background:#fff;border:1px solid #ccc;border-radius:4px;cursor:default;float:left;margin:5px 0 0 6px;padding:0 6px}.select2-container--bootstrap .select2-selection--multiple .select2-search--inline .select2-search__field{background:0 0;padding:0 12px;height:32px;line-height:1.42857143;margin-top:0;min-width:5em}.select2-container--bootstrap .select2-selection--multiple .select2-selection__choice__remove{color:#999;cursor:pointer;display:inline-block;font-weight:700;margin-right:3px}.select2-container--bootstrap .select2-selection--multiple .select2-selection__choice__remove:hover{color:#333}.select2-container--bootstrap .select2-selection--multiple .select2-selection__clear{margin-top:6px}.form-group-sm .select2-container--bootstrap .select2-selection--single,.input-group-sm .select2-container--bootstrap .select2-selection--single,.select2-container--bootstrap .select2-selection--single.input-sm{border-radius:3px;font-size:12px;height:30px;line-height:1.5;padding:5px 22px 5px 10px}.form-group-sm .select2-container--bootstrap .select2-selection--single .select2-selection__arrow b,.input-group-sm .select2-container--bootstrap .select2-selection--single .select2-selection__arrow b,.select2-container--bootstrap .select2-selection--single.input-sm .select2-selection__arrow b{margin-left:-5px}.form-group-sm .select2-container--bootstrap .select2-selection--multiple,.input-group-sm .select2-container--bootstrap .select2-selection--multiple,.select2-container--bootstrap .select2-selection--multiple.input-sm{min-height:30px;border-radius:3px}.form-group-sm .select2-container--bootstrap .select2-selection--multiple .select2-selection__choice,.input-group-sm .select2-container--bootstrap .select2-selection--multiple .select2-selection__choice,.select2-container--bootstrap .select2-selection--multiple.input-sm .select2-selection__choice{font-size:12px;line-height:1.5;margin:4px 0 0 5px;padding:0 5px}.form-group-sm .select2-container--bootstrap .select2-selection--multiple .select2-search--inline .select2-search__field,.input-group-sm .select2-container--bootstrap .select2-selection--multiple .select2-search--inline .select2-search__field,.select2-container--bootstrap .select2-selection--multiple.input-sm .select2-search--inline .select2-search__field{padding:0 10px;font-size:12px;height:28px;line-height:1.5}.form-group-sm .select2-container--bootstrap .select2-selection--multiple .select2-selection__clear,.input-group-sm .select2-container--bootstrap .select2-selection--multiple .select2-selection__clear,.select2-container--bootstrap .select2-selection--multiple.input-sm .select2-selection__clear{margin-top:5px}.form-group-lg .select2-container--bootstrap .select2-selection--single,.input-group-lg .select2-container--bootstrap .select2-selection--single,.select2-container--bootstrap .select2-selection--single.input-lg{border-radius:6px;font-size:18px;height:46px;line-height:1.3333333;padding:10px 31px 10px 16px}.form-group-lg .select2-container--bootstrap .select2-selection--single .select2-selection__arrow,.input-group-lg .select2-container--bootstrap .select2-selection--single .select2-selection__arrow,.select2-container--bootstrap .select2-selection--single.input-lg .select2-selection__arrow{width:5px}.form-group-lg .select2-container--bootstrap .select2-selection--single .select2-selection__arrow b,.input-group-lg .select2-container--bootstrap .select2-selection--single .select2-selection__arrow b,.select2-container--bootstrap .select2-selection--single.input-lg .select2-selection__arrow b{border-width:5px 5px 0;margin-left:-10px;margin-top:-2.5px}.form-group-lg .select2-container--bootstrap .select2-selection--multiple,.input-group-lg .select2-container--bootstrap .select2-selection--multiple,.select2-container--bootstrap .select2-selection--multiple.input-lg{min-height:46px;border-radius:6px}.form-group-lg .select2-container--bootstrap .select2-selection--multiple .select2-selection__choice,.input-group-lg .select2-container--bootstrap .select2-selection--multiple .select2-selection__choice,.select2-container--bootstrap .select2-selection--multiple.input-lg .select2-selection__choice{font-size:18px;line-height:1.3333333;border-radius:4px;margin:9px 0 0 8px;padding:0 10px}.form-group-lg .select2-container--bootstrap .select2-selection--multiple .select2-search--inline .select2-search__field,.input-group-lg .select2-container--bootstrap .select2-selection--multiple .select2-search--inline .select2-search__field,.select2-container--bootstrap .select2-selection--multiple.input-lg .select2-search--inline .select2-search__field{padding:0 16px;font-size:18px;height:44px;line-height:1.3333333}.form-group-lg .select2-container--bootstrap .select2-selection--multiple .select2-selection__clear,.input-group-lg .select2-container--bootstrap .select2-selection--multiple .select2-selection__clear,.select2-container--bootstrap .select2-selection--multiple.input-lg .select2-selection__clear{margin-top:10px}.input-group-lg .select2-container--bootstrap .select2-selection.select2-container--open .select2-selection--single .select2-selection__arrow b,.select2-container--bootstrap .select2-selection.input-lg.select2-container--open .select2-selection--single .select2-selection__arrow b{border-color:transparent transparent #999;border-width:0 5px 5px}.select2-container--bootstrap[dir=rtl] .select2-selection--single{padding-left:24px;padding-right:12px}.select2-container--bootstrap[dir=rtl] .select2-selection--single .select2-selection__rendered{padding-right:0;padding-left:0;text-align:right}.select2-container--bootstrap[dir=rtl] .select2-selection--single .select2-selection__clear{float:left}.select2-container--bootstrap[dir=rtl] .select2-selection--single .select2-selection__arrow{left:12px;right:auto}.select2-container--bootstrap[dir=rtl] .select2-selection--single .select2-selection__arrow b{margin-left:0}.select2-container--bootstrap[dir=rtl] .select2-selection--multiple .select2-search--inline,.select2-container--bootstrap[dir=rtl] .select2-selection--multiple .select2-selection__choice,.select2-container--bootstrap[dir=rtl] .select2-selection--multiple .select2-selection__placeholder{float:right}.select2-container--bootstrap[dir=rtl] .select2-selection--multiple .select2-selection__choice{margin-left:0;margin-right:6px}.select2-container--bootstrap[dir=rtl] .select2-selection--multiple .select2-selection__choice__remove{margin-left:2px;margin-right:auto}.has-warning .select2-dropdown,.has-warning .select2-selection{border-color:#8a6d3b}.has-warning .select2-container--focus .select2-selection,.has-warning .select2-container--open .select2-selection{-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075),0 0 6px #c0a16b;box-shadow:inset 0 1px 1px rgba(0,0,0,0.075),0 0 6px #c0a16b;border-color:#66512c}.has-warning.select2-drop-active{border-color:#66512c}.has-warning.select2-drop-active.select2-drop.select2-drop-above{border-top-color:#66512c}.has-error .select2-dropdown,.has-error .select2-selection{border-color:#a94442}.has-error .select2-container--focus .select2-selection,.has-error .select2-container--open .select2-selection{-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075),0 0 6px #ce8483;box-shadow:inset 0 1px 1px rgba(0,0,0,0.075),0 0 6px #ce8483;border-color:#843534}.has-error.select2-drop-active{border-color:#843534}.has-error.select2-drop-active.select2-drop.select2-drop-above{border-top-color:#843534}.has-success .select2-dropdown,.has-success .select2-selection{border-color:#3c763d}.has-success .select2-container--focus .select2-selection,.has-success .select2-container--open .select2-selection{-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075),0 0 6px #67b168;box-shadow:inset 0 1px 1px rgba(0,0,0,0.075),0 0 6px #67b168;border-color:#2b542c}.has-success.select2-drop-active{border-color:#2b542c}.has-success.select2-drop-active.select2-drop.select2-drop-above{border-top-color:#2b542c}.input-group>.select2-hidden-accessible:first-child+.select2-container--bootstrap>.selection>.select2-selection,.input-group>.select2-hidden-accessible:first-child+.select2-container--bootstrap>.selection>.select2-selection.form-control{border-bottom-right-radius:0;border-top-right-radius:0}.input-group>.select2-hidden-accessible:not(:first-child)+.select2-container--bootstrap:not(:last-child)>.selection>.select2-selection,.input-group>.select2-hidden-accessible:not(:first-child)+.select2-container--bootstrap:not(:last-child)>.selection>.select2-selection.form-control{border-radius:0}.input-group>.select2-hidden-accessible:not(:first-child):not(:last-child)+.select2-container--bootstrap:last-child>.selection>.select2-selection,.input-group>.select2-hidden-accessible:not(:first-child):not(:last-child)+.select2-container--bootstrap:last-child>.selection>.select2-selection.form-control{border-bottom-left-radius:0;border-top-left-radius:0}.input-group>.select2-container--bootstrap{display:table;table-layout:fixed;position:relative;z-index:2;width:100%;margin-bottom:0}.input-group>.select2-container--bootstrap>.selection>.select2-selection.form-control{float:none}.input-group>.select2-container--bootstrap.select2-container--focus,.input-group>.select2-container--bootstrap.select2-container--open{z-index:3}.input-group>.select2-container--bootstrap,.input-group>.select2-container--bootstrap .input-group-btn,.input-group>.select2-container--bootstrap .input-group-btn .btn{vertical-align:top}.form-control.select2-hidden-accessible{position:absolute !important;width:1px !important}@media (min-width:768px){.form-inline .select2-container--bootstrap{display:inline-block}}.fixed{position:absolute;left:100px;top:0}.collapsed{padding-right:0;border-width:0;margin-right:1px}.collapsed .bloc{display:none}.menu__button{position:fixed;z-index:1000;border:0}.select2-container{z-index:5000}.menu{min-height:100%;overflow:hidden}.menu,.infos{color:#000;position:relative;min-width:100px;width:99%;z-index:1000}.menu .table,.infos .table{color:black;font-size:12px}.menu .table .title,.infos .table .title{font-size:12px;font-weight:bold}.menu .list-group,.infos .list-group{padding:0;margin:0}.menu .list-group .list-group-item,.infos .list-group .list-group-item{border:1px solid #c7c5c5;padding:1px}.menu .list-group .list-group-item>*,.infos .list-group .list-group-item>*{border-width:0;height:auto;width:100%}.menu .list-group .list-group-item select,.infos .list-group .list-group-item select{font-weight:normal;max-width:100%}.menu .list-group .list-group-item .checkbox,.infos .list-group .list-group-item .checkbox{margin:0}.menu .list-group .list-group-item .checkbox label,.infos .list-group .list-group-item .checkbox label{display:flex;width:100%;justify-content:space-around;align-content:center}.menu .list-group .list-group-item .checkbox label input,.infos .list-group .list-group-item .checkbox label input{display:block;margin-left:-55px}.menu .list-group .list-group-item .checkbox label span,.infos .list-group .list-group-item .checkbox label span{margin:0;display:block}.menu .list-group .list-group-item:first-child,.infos .list-group .list-group-item:first-child{border-radius:0}.menu .list-group .list-group-item input[type=\"number\"],.infos .list-group .list-group-item input[type=\"number\"]{font-weight:normal;padding-left:15px}.menu .list-group .list-group-item.inline>label,.infos .list-group .list-group-item.inline>label{display:flex !important;margin-top:2px !important;margin-bottom:2px !important}.menu label,.infos label{font-size:12px}.menu .list-group.actions.collapsed,.infos .list-group.actions.collapsed{opacity:0;transition:opacity .4s ease-in;-ms-transition:opacity .4s ease-in;-moz-transition:opacity .4s ease-in;-webkit-transition:opacity .4s ease-in;display:none}.menu .list-group.actions,.infos .list-group.actions{opacity:1;transition:opacity .4s ease-in;-ms-transition:opacity .4s ease-in;-moz-transition:opacity .4s ease-in;-webkit-transition:opacity .4s ease-in}.menu .drag-element,.infos .drag-element{position:fixed;z-index:100000;opacity:.5}.menu .panel-body{padding:0}.menu .panel-body .infos{margin-top:40px;text-align:left}.menu .panel-body .infos li{border-color:white}.menu .bloc{margin-bottom:20px;padding:0;color:black}.menu .bloc.last{margin-bottom:2px;padding-bottom:0}.menu .bloc .bloc-content{max-height:1500px;transition:max-height .2s;overflow:initial}.menu .bloc table{text-align:left;outline:.5px solid lightgray}.menu .bloc table td{padding-left:10px}.menu .bloc.collapsed{margin:0;padding:0;margin-bottom:30px}.menu .bloc.collapsed .bloc-content{max-height:0 !important;overflow:hidden}.menu .bloc.collapsed .collapser.collapsable{border-bottom:.5px solid lightgray}.menu .bloc.collapsed .collapser.collapsable .glyphicon-chevron-down{display:none}.menu .bloc.collapsed .collapser.collapsable .glyphicon-chevron-up{display:block}.menu .bloc.hide-title:not(.collapsable) .collapser{display:none}.menu .bloc .collapser{background-color:rgba(228,226,226,0.589);border-top:.5px solid lightgray;border-right:.5px solid lightgray;border-left:.5px solid lightgray;width:100%;padding:5px;height:30px;display:block;text-align:right}.menu .bloc .collapser.collapsable{cursor:pointer}.menu .bloc .collapser.collapsable .glyphicon-chevron-up{display:none}.menu .bloc .collapser.collapsable .bloc-title:hover{color:black;text-decoration:underline}.menu .bloc .collapser:not(.collapsable) .glyphicon-chevron-up,.menu .bloc .collapser:not(.collapsable) .glyphicon-chevron-down{display:none}.menu .bloc .collapser .bloc-title{float:left;color:grey;margin-left:5px;font-family:\"Helvetica\";text-transform:uppercase}.menu .bloc-informations{font-size:12px}.menu .bloc-informations td.title{max-width:30%;width:10%;color:black}.menu .bloc-informations td.text{width:90%;word-break:break-word}.menu .bloc-informations td.text td{padding:2px;word-break:break-all}.menu .bloc-informations .table{margin-bottom:0;font-size:12px}.menu .bloc-informations .table-bordered td{word-break:normal !important}.menu .bloc-informations .table-bordered tr:first-child td{font-weight:bold}.menu .fullscreenable{cursor:pointer;color:#337ab7;text-decoration:none;background-color:transparent}.menu .fullscreenable:hover{color:#23527c;text-decoration:underline}.menu .bloc-controls .list-group-item.inline label{display:flex;align-items:center;justify-content:center;margin-top:2px}.menu .bloc-controls .list-group-item.inline label>select,.menu .bloc-controls .list-group-item.inline label>input,.menu .bloc-controls .list-group-item.inline label>.select2{width:75% !important;overflow:hidden;padding-right:3px}.menu .bloc-controls .list-group-item.inline label>.bloc-control-label{display:block;padding-left:15px;text-align:left;width:25%;font-weight:bold;text-overflow:ellipsis;overflow:hidden}.menu .bloc-controls .list-group-item.inline label .search-field{width:75%;padding-right:3px}.menu .bloc-controls .btn-group{display:flex;justify-content:center}.menu .bloc-controls .list-group-item[type=\"search\"] label{display:inline-block;font-weight:normal;padding:0;margin:0}.menu .bloc-controls .list-group-item[type=\"search\"] label>.search-field>*{padding-top:0;padding-bottom:0;height:30px}.menu .bloc-controls .list-group-item[type=\"search\"] label>.search-field .addon-left{border:0;padding:0 5px;font-size:10px}.menu .bloc-controls .list-group-item[type=\"search\"] label>.search-field .addon-left i.select-all{font-size:10px;padding:1px 2px}.menu .bloc-controls .list-group-item[type=\"search\"] label>.search-field>.input-group-addon{background-color:white;border-left:0}.menu .bloc-controls .list-group-item[type=\"search\"] label>.search-field>input{border-right:0}.menu .bloc-controls .list-group-item[type=\"search\"] label>.search-field>input:focus{box-shadow:none;border-color:#ccc}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table-container{max-height:300px;overflow:auto;position:relative}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table{table-layout:fixed;padding:0;margin:0;overflow:hidden !important}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table td{overflow:hidden;text-overflow:ellipsis}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table>tbody>tr>td{padding-top:4px;padding-bottom:1px}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table tr.header-arrows>td{font-size:60%;text-align:center;padding:1px;cursor:pointer;color:rgba(128,128,128,0.54)}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table tr.header-arrows>td .header-icons{padding-left:10px;display:inline-block}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table tr.header-arrows>td .header-title{font-size:11px;font-weight:bold;display:inline-block}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table tr.header-arrows>td .header-sub-container{display:flex;justify-content:center;align-items:center}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table tr.header-arrows>td .header-sub-container:hover{padding:0;outline:1px solid grey}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table tr.header-arrows .disabled{color:rgba(128,128,128,0.54)}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table tr.header-arrows .active{background-color:white;color:black}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table tr.header-arrows i{display:block}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table tr.row-table{cursor:pointer;height:25px}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table tr.row-table:hover{outline:1px solid lightgrey}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table tr.selected,.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table tr.selected:hover{outline:1px solid black;background-color:lightgrey}.menu .bloc-controls .btn-default{text-overflow:ellipsis;overflow:hidden}.select2-container--bootstrap .select2-results>.select2-results__options{max-height:260px;overflow-y:auto}.select2-container--bootstrap .select2-results>.select2-results__options .select2-results__option{padding:4px 6px;font-size:13px}";if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style);}())
},{}],44:[function(require,module,exports){
(function() { var head = document.getElementsByTagName('head')[0]; var style = document.createElement('style'); style.type = 'text/css';var css = ".ps{-ms-touch-action:auto;touch-action:auto;overflow:hidden !important;-ms-overflow-style:none}.ps>.ps__scrollbar-x-rail{display:none;position:absolute;z-index:1000;opacity:0;-webkit-transition:background-color .2s linear,opacity .2s linear;-o-transition:background-color .2s linear,opacity .2s linear;-moz-transition:background-color .2s linear,opacity .2s linear;transition:background-color .2s linear,opacity .2s linear;bottom:0;height:15px}.ps>.ps__scrollbar-x-rail>.ps__scrollbar-x{position:absolute;background-color:#aaa;-webkit-border-radius:6px;-moz-border-radius:6px;border-radius:6px;-webkit-transition:background-color .2s linear,height .2s linear,width .2s ease-in-out,-webkit-border-radius .2s ease-in-out;transition:background-color .2s linear,height .2s linear,width .2s ease-in-out,-webkit-border-radius .2s ease-in-out;-o-transition:background-color .2s linear,height .2s linear,width .2s ease-in-out,border-radius .2s ease-in-out;-moz-transition:background-color .2s linear,height .2s linear,width .2s ease-in-out,border-radius .2s ease-in-out,-moz-border-radius .2s ease-in-out;transition:background-color .2s linear,height .2s linear,width .2s ease-in-out,border-radius .2s ease-in-out;transition:background-color .2s linear,height .2s linear,width .2s ease-in-out,border-radius .2s ease-in-out,-webkit-border-radius .2s ease-in-out,-moz-border-radius .2s ease-in-out;bottom:2px;height:6px}.ps>.ps__scrollbar-x-rail:hover>.ps__scrollbar-x{height:11px}.ps>.ps__scrollbar-x-rail:active>.ps__scrollbar-x{height:11px}.ps>.ps__scrollbar-y-rail{display:none;position:absolute;z-index:1000;opacity:0;-webkit-transition:background-color .2s linear,opacity .2s linear;-o-transition:background-color .2s linear,opacity .2s linear;-moz-transition:background-color .2s linear,opacity .2s linear;transition:background-color .2s linear,opacity .2s linear;right:0;width:15px}.ps>.ps__scrollbar-y-rail>.ps__scrollbar-y{position:absolute;background-color:#aaa;-webkit-border-radius:6px;-moz-border-radius:6px;border-radius:6px;-webkit-transition:background-color .2s linear,height .2s linear,width .2s ease-in-out,-webkit-border-radius .2s ease-in-out;transition:background-color .2s linear,height .2s linear,width .2s ease-in-out,-webkit-border-radius .2s ease-in-out;-o-transition:background-color .2s linear,height .2s linear,width .2s ease-in-out,border-radius .2s ease-in-out;-moz-transition:background-color .2s linear,height .2s linear,width .2s ease-in-out,border-radius .2s ease-in-out,-moz-border-radius .2s ease-in-out;transition:background-color .2s linear,height .2s linear,width .2s ease-in-out,border-radius .2s ease-in-out;transition:background-color .2s linear,height .2s linear,width .2s ease-in-out,border-radius .2s ease-in-out,-webkit-border-radius .2s ease-in-out,-moz-border-radius .2s ease-in-out;right:2px;width:6px}.ps>.ps__scrollbar-y-rail:hover>.ps__scrollbar-y{width:11px}.ps>.ps__scrollbar-y-rail:active>.ps__scrollbar-y{width:11px}.ps:hover.ps--in-scrolling.ps--x>.ps__scrollbar-x-rail{background-color:#eee;opacity:.9}.ps:hover.ps--in-scrolling.ps--x>.ps__scrollbar-x-rail>.ps__scrollbar-x{background-color:#999;height:11px}.ps:hover.ps--in-scrolling.ps--y>.ps__scrollbar-y-rail{background-color:#eee;opacity:.9}.ps:hover.ps--in-scrolling.ps--y>.ps__scrollbar-y-rail>.ps__scrollbar-y{background-color:#999;width:11px}.ps:hover>.ps__scrollbar-x-rail{opacity:.6}.ps:hover>.ps__scrollbar-x-rail:hover{background-color:#eee;opacity:.9}.ps:hover>.ps__scrollbar-x-rail:hover>.ps__scrollbar-x{background-color:#999}.ps:hover>.ps__scrollbar-y-rail{opacity:.6}.ps:hover>.ps__scrollbar-y-rail:hover{background-color:#eee;opacity:.9}.ps:hover>.ps__scrollbar-y-rail:hover>.ps__scrollbar-y{background-color:#999}.ps.ps--active-x>.ps__scrollbar-x-rail{z-index:1000;opacity:.6;display:block;background-color:transparent}.ps.ps--active-y>.ps__scrollbar-y-rail{z-index:1000;opacity:.6;display:block;background-color:transparent}.ps.ps--in-scrolling.ps--x>.ps__scrollbar-x-rail{background-color:#eee;opacity:.9}.ps.ps--in-scrolling.ps--x>.ps__scrollbar-x-rail>.ps__scrollbar-x{background-color:#999;height:11px}.ps.ps--in-scrolling.ps--y>.ps__scrollbar-y-rail{background-color:#eee;opacity:.9}.ps.ps--in-scrolling.ps--y>.ps__scrollbar-y-rail>.ps__scrollbar-y{background-color:#999;width:11px}@media screen and (-ms-high-contrast:active){.ps{overflow:auto !important}}@media (-ms-high-contrast:none){.ps{overflow:auto !important}}";if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style);}())
},{}],45:[function(require,module,exports){
(function() { var head = document.getElementsByTagName('head')[0]; var style = document.createElement('style'); style.type = 'text/css';var css = ".resizable{position:relative}.resizable .x-resizing-right{position:absolute;top:0;right:0;width:5px;z-index:1100;background-color:transparent;bottom:0;cursor:ew-resize}.resizable .x-resizing-right:hover{border-right:2px solid black}.resizable .y-resizing-bottom{position:absolute;bottom:0;right:0;height:10px;background-color:transparent;left:0;cursor:ns-resize}.resizable .y-resizing-bottom:hover{border-bottom:2px solid black}.resizable .x-resizing{cursor:ew-resize}.resizable .x-resizing .x-resizing-right{border-right:2px solid black}.resizable .y-resizing{cursor:ns-resize}.resizable .y-resizing .y-resizing-bottom{border-bottom:2px solid black}";if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style);}())
},{}],46:[function(require,module,exports){
(function() { var head = document.getElementsByTagName('head')[0]; var style = document.createElement('style'); style.type = 'text/css';var css = ".tree{display:flex;height:100%}td.text{position:relative}td.text .confusion-matrix{overflow:auto;max-height:500px}td.text .confusion-matrix>table{table-layout:fixed}td.text .confusion-matrix td{min-width:100px;text-align:center}td.text .confusion-matrix td.diagonal{background-color:rgba(135,210,250,0.24)}td.text .confusion-matrix td.separator{background-image:url('data:image/svg+xml;charset=US-ASCII,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%20standalone%3D%22no%22%3F%3E%0A%3C%21--%20Created%20with%20Inkscape%20%28http%3A//www.inkscape.org/%29%20--%3E%0A%0A%3Csvg%0A%20%20%20xmlns%3Adc%3D%22http%3A//purl.org/dc/elements/1.1/%22%0A%20%20%20xmlns%3Acc%3D%22http%3A//creativecommons.org/ns%23%22%0A%20%20%20xmlns%3Ardf%3D%22http%3A//www.w3.org/1999/02/22-rdf-syntax-ns%23%22%0A%20%20%20xmlns%3Asvg%3D%22http%3A//www.w3.org/2000/svg%22%0A%20%20%20xmlns%3D%22http%3A//www.w3.org/2000/svg%22%0A%20%20%20xmlns%3Asodipodi%3D%22http%3A//sodipodi.sourceforge.net/DTD/sodipodi-0.dtd%22%0A%20%20%20xmlns%3Ainkscape%3D%22http%3A//www.inkscape.org/namespaces/inkscape%22%0A%20%20%20width%3D%2216px%22%0A%20%20%20height%3D%2216px%22%0A%20%20%20viewBox%3D%220%200%2016%2016%22%0A%20%20%20version%3D%221.1%22%0A%20%20%20id%3D%22SVGRoot%22%0A%20%20%20inkscape%3Aversion%3D%220.92.2%20%285c3e80d%2C%202017-08-06%29%22%0A%20%20%20sodipodi%3Adocname%3D%22test.svg%22%3E%0A%20%20%3Csodipodi%3Anamedview%0A%20%20%20%20%20id%3D%22base%22%0A%20%20%20%20%20pagecolor%3D%22%23ffffff%22%0A%20%20%20%20%20bordercolor%3D%22%23666666%22%0A%20%20%20%20%20borderopacity%3D%221.0%22%0A%20%20%20%20%20inkscape%3Apageopacity%3D%220.0%22%0A%20%20%20%20%20inkscape%3Apageshadow%3D%222%22%0A%20%20%20%20%20inkscape%3Azoom%3D%2216%22%0A%20%20%20%20%20inkscape%3Acx%3D%228%22%0A%20%20%20%20%20inkscape%3Acy%3D%2210.578125%22%0A%20%20%20%20%20inkscape%3Adocument-units%3D%22px%22%0A%20%20%20%20%20inkscape%3Acurrent-layer%3D%22layer1%22%0A%20%20%20%20%20showgrid%3D%22false%22%0A%20%20%20%20%20inkscape%3Awindow-width%3D%221920%22%0A%20%20%20%20%20inkscape%3Awindow-height%3D%221007%22%0A%20%20%20%20%20inkscape%3Awindow-x%3D%220%22%0A%20%20%20%20%20inkscape%3Awindow-y%3D%2227%22%0A%20%20%20%20%20inkscape%3Awindow-maximized%3D%221%22%0A%20%20%20%20%20inkscape%3Agrid-bbox%3D%22true%22%20/%3E%0A%20%20%3Cdefs%0A%20%20%20%20%20id%3D%22defs3707%22%20/%3E%0A%20%20%3Cmetadata%0A%20%20%20%20%20id%3D%22metadata3710%22%3E%0A%20%20%20%20%3Crdf%3ARDF%3E%0A%20%20%20%20%20%20%3Ccc%3AWork%0A%20%20%20%20%20%20%20%20%20rdf%3Aabout%3D%22%22%3E%0A%20%20%20%20%20%20%20%20%3Cdc%3Aformat%3Eimage/svg+xml%3C/dc%3Aformat%3E%0A%20%20%20%20%20%20%20%20%3Cdc%3Atype%0A%20%20%20%20%20%20%20%20%20%20%20rdf%3Aresource%3D%22http%3A//purl.org/dc/dcmitype/StillImage%22%20/%3E%0A%20%20%20%20%20%20%20%20%3Cdc%3Atitle%3E%3C/dc%3Atitle%3E%0A%20%20%20%20%20%20%3C/cc%3AWork%3E%0A%20%20%20%20%3C/rdf%3ARDF%3E%0A%20%20%3C/metadata%3E%0A%20%20%3Cg%0A%20%20%20%20%20id%3D%22layer1%22%0A%20%20%20%20%20inkscape%3Agroupmode%3D%22layer%22%0A%20%20%20%20%20inkscape%3Alabel%3D%22Calque%201%22%3E%0A%20%20%20%20%3Ctext%0A%20%20%20%20%20%20%20xml%3Aspace%3D%22preserve%22%0A%20%20%20%20%20%20%20style%3D%22font-style%3Anormal%3Bfont-weight%3Anormal%3Bfont-size%3A10.66666698px%3Bline-height%3A1.25%3Bfont-family%3Asans-serif%3Bletter-spacing%3A0px%3Bword-spacing%3A0px%3Bfill%3A%23000000%3Bfill-opacity%3A1%3Bstroke%3Anone%22%0A%20%20%20%20%20%20%20x%3D%22-0.0625%22%0A%20%20%20%20%20%20%20y%3D%2215.4375%22%0A%20%20%20%20%20%20%20id%3D%22text3725%22%3E%3Ctspan%0A%20%20%20%20%20%20%20%20%20sodipodi%3Arole%3D%22line%22%0A%20%20%20%20%20%20%20%20%20id%3D%22tspan3723%22%0A%20%20%20%20%20%20%20%20%20x%3D%22-0.0625%22%0A%20%20%20%20%20%20%20%20%20y%3D%2215.4375%22%3EP%3C/tspan%3E%3C/text%3E%0A%20%20%20%20%3Ctext%0A%20%20%20%20%20%20%20xml%3Aspace%3D%22preserve%22%0A%20%20%20%20%20%20%20style%3D%22font-style%3Anormal%3Bfont-weight%3Anormal%3Bfont-size%3A10.66666698px%3Bline-height%3A1.25%3Bfont-family%3Asans-serif%3Bletter-spacing%3A0px%3Bword-spacing%3A0px%3Bfill%3A%23000000%3Bfill-opacity%3A1%3Bstroke%3Anone%22%0A%20%20%20%20%20%20%20x%3D%228.479166%22%0A%20%20%20%20%20%20%20y%3D%228.2630215%22%0A%20%20%20%20%20%20%20id%3D%22text3725-3%22%3E%3Ctspan%0A%20%20%20%20%20%20%20%20%20sodipodi%3Arole%3D%22line%22%0A%20%20%20%20%20%20%20%20%20id%3D%22tspan3723-6%22%0A%20%20%20%20%20%20%20%20%20x%3D%228.479166%22%0A%20%20%20%20%20%20%20%20%20y%3D%228.2630215%22%3ER%3C/tspan%3E%3C/text%3E%0A%20%20%20%20%3Crect%0A%20%20%20%20%20%20%20id%3D%22rect3747%22%0A%20%20%20%20%20%20%20width%3D%2232.9375%22%0A%20%20%20%20%20%20%20height%3D%220.75%22%0A%20%20%20%20%20%20%20x%3D%22-7.1678476%22%0A%20%20%20%20%20%20%20y%3D%22-0.56777507%22%0A%20%20%20%20%20%20%20ry%3D%220.34375%22%0A%20%20%20%20%20%20%20transform%3D%22rotate%2846.051276%29%22%20/%3E%0A%20%20%3C/g%3E%0A%3C/svg%3E%0A');background-repeat:no-repeat;background-position:center}td.text .confusion-matrix td.selected{background-color:grey;font-weight:bold}.tree-controls{display:inline-block;border-right:1px solid #c7c5c5;padding-right:10px;padding-top:20px;overflow:auto;position:relative;height:100%}.tree-controls td.text.fullscreen{display:flex;align-items:center}.tree-controls td.text.fullscreen>.confusion-matrix{max-height:none}.tree-controls .menu.slide{padding-right:5px;overflow:hidden}.tree-controls .different,.tree-controls .same,.tree-controls .majority,.tree-controls .minority{display:inline-block;width:15px;height:15px}.tree-controls .cell{cursor:pointer}.tree-controls .different{background-color:red}.tree-controls .minority{background-color:lightcoral}.tree-controls .majority{background-color:lightgreen}.tree-controls .same{background-color:green}.tree-graph{width:100%;height:100%;overflow:auto;display:inline-block;position:relative}.tree-graph .link.highlighted{stroke:rgba(255,0,0,0.61) !important}.tree-graph svg .minimap .handle{stroke:transparent}.tree-graph .node--internal text{text-shadow:0 1px 0 #fff,0 -1px 0 #fff,1px 0 0 #fff,-1px 0 0 #fff}.tree-graph .lig-tooltip{position:fixed}.tree-graph .tree-wrapper{position:relative}.tree-graph .tree-wrapper>*{position:absolute}.tree-graph .tree-wrapper>.html-nodes{z-index:100}.tree-graph>.tree-wrapper>svg{position:absolute;z-index:10;display:block;margin:auto}.tree-graph>.tree-wrapper>svg .link{fill:none;stroke:#ccc;stroke-width:2px;stroke-linejoin:round;stroke-linecap:round}.tree-graph>.tree-wrapper>svg .link-label{text-anchor:middle}.tree-graph>.tree-wrapper>svg .node .pie{cursor:pointer}.tree-graph>.tree-wrapper>svg .node .pie text{font:13px sans-serif}.tree-graph>.tree-wrapper>svg .node circle.ending-node{fill:none;stroke:grey;stroke-width:3px;stroke-dasharray:0}.tree-graph>.tree-wrapper>svg .node.highlighted circle.ending-node{stroke:rgba(255,0,0,0.61)}.tree-graph>.tree-wrapper>svg .node.selected circle{stroke:black !important}.tree-graph>.tree-wrapper>svg .node text.node-label{font-size:18px;pointer-events:none}.tree-graph>.tree-wrapper>svg .node text.link-label{font-size:14px}.tree-graph .pie circle{fill:none;stroke:#d24444;stroke-dasharray:5px;stroke-width:3px}.tree-graph .pie .circle--outer{stroke-dasharray:0;stroke-width:2px}.tree-graph .pie .circle--inner{stroke-dasharray:0;stroke-width:2px}.tree-graph .in-transition .chart{display:none !important}.tree-graph .in-transition img{box-shadow:-1px 0 0 black;display:inline-block !important;z-index:10000}.tree-graph .big-line{stroke:grey}.tree-graph .graph-node{background-color:white;border:1px solid lightgrey;position:absolute;border:1px solid black;z-index:20;font-size:1.1vh}.tree-graph .graph-node img{display:none}.tree-graph .graph-node.highlighted{outline:3px solid rgba(255,0,0,0.61)}.tree-graph .graph-node.selected{outline:3px solid black;border:0;margin:0;padding:0;overflow:hidden}.tree-graph .graph-node.selected .subject{font-weight:bold}.tree-graph .graph-node.variable--node .representation{display:none}.tree-graph .graph-node.variable--node .subject{height:50%}.tree-graph .graph-node.variable--node .class-repartition{height:50%}.tree-graph .graph-node .pattern{margin:0;padding:0}.tree-graph .graph-node .pattern rect{stroke:black;stroke-width:1px}.tree-graph .graph-node .pattern *{vector-effect:'non-scaling-stroke'}.tree-graph .graph-node .pattern .dot{stroke-opacity:.5;fill-opacity:.5}.tree-graph .graph-node .pattern__arrow{border-left:2px solid black;border-right:2px solid black}.tree-graph .graph-node .representation{display:flex;width:100%;height:70%}.tree-graph .graph-node .representation .chart{height:100%;display:inline-block;box-shadow:-1px 0 0 black}.tree-graph .graph-node .representation .chart .tooltip{opacity:1;z-index:10000;background-color:'red'}.tree-graph .graph-node .representation .pattern__container{height:100%;display:inline-block}.tree-graph .graph-node .representation .pattern__container text{font-size:10px}.tree-graph .graph-node .representation .pattern__container.pattern__arrow{display:flex;align-content:center;justify-content:center}.tree-graph .graph-node .representation .pattern__container.pattern__arrow i{display:block;font-size:25px}.tree-graph .graph-node .class-repartition{height:15%;display:flex;overflow:hidden;border-bottom:1px solid black;font-size:1.1vh;z-index:20}.tree-graph .graph-node .class-repartition>div{display:flex;align-items:center;border-left:1px solid black;justify-content:center;overflow:hidden;height:100%}.tree-graph .graph-node .class-repartition>div>p{margin:0;padding:0;white-space:nowrap}.tree-graph .graph-node .subject{height:15%;border-bottom:1px solid black;z-index:20;display:flex;align-items:center;overflow:hidden}.tree-graph .graph-node .subject>p{margin:0;width:100%;text-align:center;text-overflow:ellipsis;white-space:nowrap;overflow:hidden;max-width:100%;padding:0 3px}.tree-graph .graph-node.light .subject{height:15%}.tree-graph .graph-node.light .class-repartition{height:88%}.tree-graph .graph-node.light .representation{display:none}";if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style);}())
},{}],47:[function(require,module,exports){
(function() { var head = document.getElementsByTagName('head')[0]; var style = document.createElement('style'); style.type = 'text/css';var css = ".zoom-controls{position:absolute;right:5%;top:20px;z-index:2000;padding-top:2px;padding-right:3px;padding-left:3px;background-color:white;border:1px solid grey;border-radius:3px}";if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style);}())
},{}],48:[function(require,module,exports){
'use strict';

window.TDecisionTree = require(28);

},{"28":28}]},{},[48])

//# sourceMappingURL=TDecisionTree.bundle.js.map
