# Changelog

## [1.12] - 2018-10-25

### Changed

- Fix error in traces computation

## [1.11] - 2018-10-24

### Changed

- Fix colorManager in order to have a stable set of colors for each dataset/learningset.

## [1.1] - 2018-10-08

### Changed

- Fix traces management
- Change readme
- Change keymaps in viz definition

### Added

- Add autofit option
- Hide dataset if ikats context
- Hide scale, height, fit, width fields if autofit is activated.
- Add fontBottom option

## [1.01] - 2018-10-01

### Changed

- Rename type : 'tdt_result' to 'tdt'
